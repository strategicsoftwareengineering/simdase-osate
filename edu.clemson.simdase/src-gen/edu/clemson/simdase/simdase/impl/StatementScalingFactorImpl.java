/**
 */
package edu.clemson.simdase.simdase.impl;

import edu.clemson.simdase.simdase.SimdasePackage;
import edu.clemson.simdase.simdase.StatementScalingFactor;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Statement Scaling Factor</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class StatementScalingFactorImpl extends StatementLvl4Impl implements StatementScalingFactor
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected StatementScalingFactorImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return SimdasePackage.Literals.STATEMENT_SCALING_FACTOR;
  }

} //StatementScalingFactorImpl
