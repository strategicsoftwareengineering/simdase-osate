package edu.clemson.simdase.parser.antlr.internal; 

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import edu.clemson.simdase.services.SimdaseGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalSimdaseParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "Using_externally_available_software", "Developing_acquisition_strategy", "Organizational_risk_management", "Understanding_relevant_domains", "Customer_interface_management", "Launching_institutionalizing", "Software_system_integration", "Technical_risk_management", "Configuration_management", "Requirements_engineering", "Structuring_organization", "Architecture_definition", "Architecture_evaluation", "Organizational_planning", "Building_business_case", "Mining_existing_assets", "Technology_forecasting", "Component_development", "Measurement_tracking", "Commission_analysis", "Process_discipline", "Technical_planning", "Active_variants", "Market_analysis", "Scaling_factor", "Tool_support", "Is_adaptive", "Classifier", "Operations", "Reference", "Constant", "Random", "Time_max", "Time_min", "Training", "Variants", "Applies", "Average", "Binding", "Compute", "Funding", "Maximum", "Minimum", "Scoping", "Testing", "Result", "Taking", "Delta", "False", "Modes", "Times", "Cos", "Cost", "Else", "Sin", "Tan", "True", "PlusSignEqualsSignGreaterThanSign", "For", "If", "Run", "S", "AmpersandAmpersand", "AsteriskAsterisk", "FullStopFullStop", "ColonColon", "LessThanSignEqualsSign", "EqualsSignEqualsSign", "EqualsSignGreaterThanSign", "GreaterThanSignEqualsSign", "At", "In", "To", "VerticalLineVerticalLine", "ExclamationMark", "PercentSign", "LeftParenthesis", "RightParenthesis", "Asterisk", "PlusSign", "Comma", "HyphenMinus", "FullStop", "Solidus", "Colon", "Semicolon", "LessThanSign", "GreaterThanSign", "LeftSquareBracket", "RightSquareBracket", "I", "T", "LeftCurlyBracket", "RightCurlyBracket", "RULE_INTEGER_LIT", "RULE_FLOAT", "RULE_SL_COMMENT", "RULE_DIGIT", "RULE_EXPONENT", "RULE_INT_EXPONENT", "RULE_REAL_LIT", "RULE_BASED_INTEGER", "RULE_EXTENDED_DIGIT", "RULE_STRING", "RULE_ID", "RULE_WS"
    };
    public static final int Structuring_organization=14;
    public static final int Funding=44;
    public static final int EqualsSignGreaterThanSign=72;
    public static final int Is_adaptive=30;
    public static final int False=52;
    public static final int Time_min=37;
    public static final int LessThanSign=90;
    public static final int Training=38;
    public static final int PlusSignEqualsSignGreaterThanSign=61;
    public static final int LeftParenthesis=80;
    public static final int Scoping=47;
    public static final int Measurement_tracking=22;
    public static final int Operations=32;
    public static final int Time_max=36;
    public static final int ExclamationMark=78;
    public static final int GreaterThanSign=91;
    public static final int RULE_ID=108;
    public static final int Market_analysis=27;
    public static final int RULE_DIGIT=101;
    public static final int GreaterThanSignEqualsSign=73;
    public static final int ColonColon=69;
    public static final int EqualsSignEqualsSign=71;
    public static final int Building_business_case=18;
    public static final int Mining_existing_assets=19;
    public static final int PlusSign=83;
    public static final int LeftSquareBracket=92;
    public static final int If=63;
    public static final int Commission_analysis=23;
    public static final int Architecture_evaluation=16;
    public static final int Minimum=46;
    public static final int Organizational_planning=17;
    public static final int Taking=50;
    public static final int In=75;
    public static final int VerticalLineVerticalLine=77;
    public static final int RULE_REAL_LIT=104;
    public static final int I=94;
    public static final int Classifier=31;
    public static final int Comma=84;
    public static final int HyphenMinus=85;
    public static final int S=65;
    public static final int At=74;
    public static final int T=95;
    public static final int LessThanSignEqualsSign=70;
    public static final int Solidus=87;
    public static final int RightCurlyBracket=97;
    public static final int Modes=53;
    public static final int FullStop=86;
    public static final int Tool_support=29;
    public static final int Reference=33;
    public static final int Cos=55;
    public static final int Average=41;
    public static final int Run=64;
    public static final int Component_development=21;
    public static final int Semicolon=89;
    public static final int RULE_EXPONENT=102;
    public static final int Delta=51;
    public static final int Software_system_integration=10;
    public static final int Else=57;
    public static final int RULE_EXTENDED_DIGIT=106;
    public static final int RULE_FLOAT=99;
    public static final int Tan=59;
    public static final int Active_variants=26;
    public static final int Developing_acquisition_strategy=5;
    public static final int True=60;
    public static final int Architecture_definition=15;
    public static final int Result=49;
    public static final int RULE_INT_EXPONENT=103;
    public static final int PercentSign=79;
    public static final int Maximum=45;
    public static final int Using_externally_available_software=4;
    public static final int Random=35;
    public static final int Launching_institutionalizing=9;
    public static final int FullStopFullStop=68;
    public static final int To=76;
    public static final int Applies=40;
    public static final int RULE_BASED_INTEGER=105;
    public static final int RightSquareBracket=93;
    public static final int Testing=48;
    public static final int Binding=42;
    public static final int Scaling_factor=28;
    public static final int For=62;
    public static final int RightParenthesis=81;
    public static final int Understanding_relevant_domains=7;
    public static final int Process_discipline=24;
    public static final int AsteriskAsterisk=67;
    public static final int Sin=58;
    public static final int Organizational_risk_management=6;
    public static final int Technical_planning=25;
    public static final int RULE_INTEGER_LIT=98;
    public static final int Configuration_management=12;
    public static final int Constant=34;
    public static final int RULE_STRING=107;
    public static final int Cost=56;
    public static final int RULE_SL_COMMENT=100;
    public static final int AmpersandAmpersand=66;
    public static final int Technical_risk_management=11;
    public static final int Colon=88;
    public static final int EOF=-1;
    public static final int Asterisk=82;
    public static final int Customer_interface_management=8;
    public static final int RULE_WS=109;
    public static final int LeftCurlyBracket=96;
    public static final int Variants=39;
    public static final int Requirements_engineering=13;
    public static final int Technology_forecasting=20;
    public static final int Compute=43;
    public static final int Times=54;

    // delegates
    // delegators


        public InternalSimdaseParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalSimdaseParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalSimdaseParser.tokenNames; }
    public String getGrammarFileName() { return "InternalSimdaseParser.g"; }




    	private SimdaseGrammarAccess grammarAccess;
    	 	
    	public InternalSimdaseParser(TokenStream input, SimdaseGrammarAccess grammarAccess) {
    		this(input);
    		this.grammarAccess = grammarAccess;
    		registerRules(grammarAccess.getGrammar());
    	}
    	
    	@Override
    	protected String getFirstRuleName() {
    		return "AnnexLibrary";	
    	} 
    	   	   	
    	@Override
    	protected SimdaseGrammarAccess getGrammarAccess() {
    		return grammarAccess;
    	}



    // $ANTLR start "entryRuleAnnexLibrary"
    // InternalSimdaseParser.g:61:1: entryRuleAnnexLibrary returns [EObject current=null] : iv_ruleAnnexLibrary= ruleAnnexLibrary EOF ;
    public final EObject entryRuleAnnexLibrary() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAnnexLibrary = null;


        try {
            // InternalSimdaseParser.g:62:2: (iv_ruleAnnexLibrary= ruleAnnexLibrary EOF )
            // InternalSimdaseParser.g:63:2: iv_ruleAnnexLibrary= ruleAnnexLibrary EOF
            {
             newCompositeNode(grammarAccess.getAnnexLibraryRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleAnnexLibrary=ruleAnnexLibrary();

            state._fsp--;

             current =iv_ruleAnnexLibrary; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAnnexLibrary"


    // $ANTLR start "ruleAnnexLibrary"
    // InternalSimdaseParser.g:70:1: ruleAnnexLibrary returns [EObject current=null] : this_SimdaseLibrary_0= ruleSimdaseLibrary ;
    public final EObject ruleAnnexLibrary() throws RecognitionException {
        EObject current = null;

        EObject this_SimdaseLibrary_0 = null;


         enterRule(); 
            
        try {
            // InternalSimdaseParser.g:73:28: (this_SimdaseLibrary_0= ruleSimdaseLibrary )
            // InternalSimdaseParser.g:75:5: this_SimdaseLibrary_0= ruleSimdaseLibrary
            {
             
                    newCompositeNode(grammarAccess.getAnnexLibraryAccess().getSimdaseLibraryParserRuleCall()); 
                
            pushFollow(FollowSets000.FOLLOW_2);
            this_SimdaseLibrary_0=ruleSimdaseLibrary();

            state._fsp--;


                    current = this_SimdaseLibrary_0;
                    afterParserOrEnumRuleCall();
                

            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAnnexLibrary"


    // $ANTLR start "entryRuleSimdaseLibrary"
    // InternalSimdaseParser.g:93:1: entryRuleSimdaseLibrary returns [EObject current=null] : iv_ruleSimdaseLibrary= ruleSimdaseLibrary EOF ;
    public final EObject entryRuleSimdaseLibrary() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSimdaseLibrary = null;


        try {
            // InternalSimdaseParser.g:94:2: (iv_ruleSimdaseLibrary= ruleSimdaseLibrary EOF )
            // InternalSimdaseParser.g:95:2: iv_ruleSimdaseLibrary= ruleSimdaseLibrary EOF
            {
             newCompositeNode(grammarAccess.getSimdaseLibraryRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleSimdaseLibrary=ruleSimdaseLibrary();

            state._fsp--;

             current =iv_ruleSimdaseLibrary; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSimdaseLibrary"


    // $ANTLR start "ruleSimdaseLibrary"
    // InternalSimdaseParser.g:102:1: ruleSimdaseLibrary returns [EObject current=null] : ( () ( (lv_contract_1_0= ruleSimdaseContract ) ) ) ;
    public final EObject ruleSimdaseLibrary() throws RecognitionException {
        EObject current = null;

        EObject lv_contract_1_0 = null;


         enterRule(); 
            
        try {
            // InternalSimdaseParser.g:105:28: ( ( () ( (lv_contract_1_0= ruleSimdaseContract ) ) ) )
            // InternalSimdaseParser.g:106:1: ( () ( (lv_contract_1_0= ruleSimdaseContract ) ) )
            {
            // InternalSimdaseParser.g:106:1: ( () ( (lv_contract_1_0= ruleSimdaseContract ) ) )
            // InternalSimdaseParser.g:106:2: () ( (lv_contract_1_0= ruleSimdaseContract ) )
            {
            // InternalSimdaseParser.g:106:2: ()
            // InternalSimdaseParser.g:107:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getSimdaseLibraryAccess().getSimdaseContractLibraryAction_0(),
                        current);
                

            }

            // InternalSimdaseParser.g:112:2: ( (lv_contract_1_0= ruleSimdaseContract ) )
            // InternalSimdaseParser.g:113:1: (lv_contract_1_0= ruleSimdaseContract )
            {
            // InternalSimdaseParser.g:113:1: (lv_contract_1_0= ruleSimdaseContract )
            // InternalSimdaseParser.g:114:3: lv_contract_1_0= ruleSimdaseContract
            {
             
            	        newCompositeNode(grammarAccess.getSimdaseLibraryAccess().getContractSimdaseContractParserRuleCall_1_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_2);
            lv_contract_1_0=ruleSimdaseContract();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getSimdaseLibraryRule());
            	        }
                   		set(
                   			current, 
                   			"contract",
                    		lv_contract_1_0, 
                    		"edu.clemson.simdase.Simdase.SimdaseContract");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSimdaseLibrary"


    // $ANTLR start "entryRuleSimdaseSubclause"
    // InternalSimdaseParser.g:138:1: entryRuleSimdaseSubclause returns [EObject current=null] : iv_ruleSimdaseSubclause= ruleSimdaseSubclause EOF ;
    public final EObject entryRuleSimdaseSubclause() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSimdaseSubclause = null;


        try {
            // InternalSimdaseParser.g:139:2: (iv_ruleSimdaseSubclause= ruleSimdaseSubclause EOF )
            // InternalSimdaseParser.g:140:2: iv_ruleSimdaseSubclause= ruleSimdaseSubclause EOF
            {
             newCompositeNode(grammarAccess.getSimdaseSubclauseRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleSimdaseSubclause=ruleSimdaseSubclause();

            state._fsp--;

             current =iv_ruleSimdaseSubclause; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSimdaseSubclause"


    // $ANTLR start "ruleSimdaseSubclause"
    // InternalSimdaseParser.g:147:1: ruleSimdaseSubclause returns [EObject current=null] : ( () ( (lv_contract_1_0= ruleSimdaseContract ) ) ) ;
    public final EObject ruleSimdaseSubclause() throws RecognitionException {
        EObject current = null;

        EObject lv_contract_1_0 = null;


         enterRule(); 
            
        try {
            // InternalSimdaseParser.g:150:28: ( ( () ( (lv_contract_1_0= ruleSimdaseContract ) ) ) )
            // InternalSimdaseParser.g:151:1: ( () ( (lv_contract_1_0= ruleSimdaseContract ) ) )
            {
            // InternalSimdaseParser.g:151:1: ( () ( (lv_contract_1_0= ruleSimdaseContract ) ) )
            // InternalSimdaseParser.g:151:2: () ( (lv_contract_1_0= ruleSimdaseContract ) )
            {
            // InternalSimdaseParser.g:151:2: ()
            // InternalSimdaseParser.g:152:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getSimdaseSubclauseAccess().getSimdaseContractSubclauseAction_0(),
                        current);
                

            }

            // InternalSimdaseParser.g:157:2: ( (lv_contract_1_0= ruleSimdaseContract ) )
            // InternalSimdaseParser.g:158:1: (lv_contract_1_0= ruleSimdaseContract )
            {
            // InternalSimdaseParser.g:158:1: (lv_contract_1_0= ruleSimdaseContract )
            // InternalSimdaseParser.g:159:3: lv_contract_1_0= ruleSimdaseContract
            {
             
            	        newCompositeNode(grammarAccess.getSimdaseSubclauseAccess().getContractSimdaseContractParserRuleCall_1_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_2);
            lv_contract_1_0=ruleSimdaseContract();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getSimdaseSubclauseRule());
            	        }
                   		set(
                   			current, 
                   			"contract",
                    		lv_contract_1_0, 
                    		"edu.clemson.simdase.Simdase.SimdaseContract");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSimdaseSubclause"


    // $ANTLR start "entryRuleSimdaseContract"
    // InternalSimdaseParser.g:183:1: entryRuleSimdaseContract returns [EObject current=null] : iv_ruleSimdaseContract= ruleSimdaseContract EOF ;
    public final EObject entryRuleSimdaseContract() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSimdaseContract = null;


        try {
            // InternalSimdaseParser.g:184:2: (iv_ruleSimdaseContract= ruleSimdaseContract EOF )
            // InternalSimdaseParser.g:185:2: iv_ruleSimdaseContract= ruleSimdaseContract EOF
            {
             newCompositeNode(grammarAccess.getSimdaseContractRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleSimdaseContract=ruleSimdaseContract();

            state._fsp--;

             current =iv_ruleSimdaseContract; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSimdaseContract"


    // $ANTLR start "ruleSimdaseContract"
    // InternalSimdaseParser.g:192:1: ruleSimdaseContract returns [EObject current=null] : ( () ( (lv_statement_1_0= ruleSIMDASEStatement ) )* ) ;
    public final EObject ruleSimdaseContract() throws RecognitionException {
        EObject current = null;

        EObject lv_statement_1_0 = null;


         enterRule(); 
            
        try {
            // InternalSimdaseParser.g:195:28: ( ( () ( (lv_statement_1_0= ruleSIMDASEStatement ) )* ) )
            // InternalSimdaseParser.g:196:1: ( () ( (lv_statement_1_0= ruleSIMDASEStatement ) )* )
            {
            // InternalSimdaseParser.g:196:1: ( () ( (lv_statement_1_0= ruleSIMDASEStatement ) )* )
            // InternalSimdaseParser.g:196:2: () ( (lv_statement_1_0= ruleSIMDASEStatement ) )*
            {
            // InternalSimdaseParser.g:196:2: ()
            // InternalSimdaseParser.g:197:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getSimdaseContractAccess().getSimdaseContractAction_0(),
                        current);
                

            }

            // InternalSimdaseParser.g:202:2: ( (lv_statement_1_0= ruleSIMDASEStatement ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==Active_variants||LA1_0==Scaling_factor||LA1_0==Is_adaptive||(LA1_0>=Time_max && LA1_0<=Time_min)||LA1_0==Variants||LA1_0==Cost||LA1_0==Run) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalSimdaseParser.g:203:1: (lv_statement_1_0= ruleSIMDASEStatement )
            	    {
            	    // InternalSimdaseParser.g:203:1: (lv_statement_1_0= ruleSIMDASEStatement )
            	    // InternalSimdaseParser.g:204:3: lv_statement_1_0= ruleSIMDASEStatement
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getSimdaseContractAccess().getStatementSIMDASEStatementParserRuleCall_1_0()); 
            	    	    
            	    pushFollow(FollowSets000.FOLLOW_3);
            	    lv_statement_1_0=ruleSIMDASEStatement();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getSimdaseContractRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"statement",
            	            		lv_statement_1_0, 
            	            		"edu.clemson.simdase.Simdase.SIMDASEStatement");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSimdaseContract"


    // $ANTLR start "entryRuleSIMDASEStatement"
    // InternalSimdaseParser.g:228:1: entryRuleSIMDASEStatement returns [EObject current=null] : iv_ruleSIMDASEStatement= ruleSIMDASEStatement EOF ;
    public final EObject entryRuleSIMDASEStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSIMDASEStatement = null;


        try {
            // InternalSimdaseParser.g:229:2: (iv_ruleSIMDASEStatement= ruleSIMDASEStatement EOF )
            // InternalSimdaseParser.g:230:2: iv_ruleSIMDASEStatement= ruleSIMDASEStatement EOF
            {
             newCompositeNode(grammarAccess.getSIMDASEStatementRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleSIMDASEStatement=ruleSIMDASEStatement();

            state._fsp--;

             current =iv_ruleSIMDASEStatement; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSIMDASEStatement"


    // $ANTLR start "ruleSIMDASEStatement"
    // InternalSimdaseParser.g:237:1: ruleSIMDASEStatement returns [EObject current=null] : ( ( () otherlv_1= Is_adaptive otherlv_2= EqualsSignGreaterThanSign ( (lv_value_3_0= ruleBoolean ) ) otherlv_4= Semicolon ) | ( () otherlv_6= Time_max otherlv_7= EqualsSignGreaterThanSign ( (lv_value_8_0= ruleNumber ) ) otherlv_9= Semicolon ) | ( () otherlv_11= Time_min otherlv_12= EqualsSignGreaterThanSign ( (lv_value_13_0= ruleNumber ) ) otherlv_14= Semicolon ) | ( () otherlv_16= Scaling_factor otherlv_17= EqualsSignGreaterThanSign ( (lv_statement_18_0= ruleStatement ) ) otherlv_19= Semicolon ) | ( () otherlv_21= Variants otherlv_22= EqualsSignGreaterThanSign otherlv_23= LeftCurlyBracket ( (lv_variants_24_0= ruleVariants ) )? otherlv_25= RightCurlyBracket otherlv_26= Semicolon ) | ( () otherlv_28= Active_variants otherlv_29= EqualsSignGreaterThanSign otherlv_30= LeftCurlyBracket ( (lv_variants_31_0= ruleActiveVariants ) )? otherlv_32= RightCurlyBracket otherlv_33= Semicolon ) | ( () otherlv_35= Run otherlv_36= For ( (lv_count_37_0= ruleNumber ) ) otherlv_38= Times otherlv_39= Taking ( ( (lv_type_40_1= Average | lv_type_40_2= Minimum | lv_type_40_3= Maximum ) ) ) otherlv_41= Result otherlv_42= Semicolon ) | (this_CostProperties_43= ruleCostProperties otherlv_44= Semicolon ) ) ;
    public final EObject ruleSIMDASEStatement() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        Token otherlv_12=null;
        Token otherlv_14=null;
        Token otherlv_16=null;
        Token otherlv_17=null;
        Token otherlv_19=null;
        Token otherlv_21=null;
        Token otherlv_22=null;
        Token otherlv_23=null;
        Token otherlv_25=null;
        Token otherlv_26=null;
        Token otherlv_28=null;
        Token otherlv_29=null;
        Token otherlv_30=null;
        Token otherlv_32=null;
        Token otherlv_33=null;
        Token otherlv_35=null;
        Token otherlv_36=null;
        Token otherlv_38=null;
        Token otherlv_39=null;
        Token lv_type_40_1=null;
        Token lv_type_40_2=null;
        Token lv_type_40_3=null;
        Token otherlv_41=null;
        Token otherlv_42=null;
        Token otherlv_44=null;
        EObject lv_value_3_0 = null;

        EObject lv_value_8_0 = null;

        EObject lv_value_13_0 = null;

        EObject lv_statement_18_0 = null;

        EObject lv_variants_24_0 = null;

        EObject lv_variants_31_0 = null;

        EObject lv_count_37_0 = null;

        EObject this_CostProperties_43 = null;


         enterRule(); 
            
        try {
            // InternalSimdaseParser.g:240:28: ( ( ( () otherlv_1= Is_adaptive otherlv_2= EqualsSignGreaterThanSign ( (lv_value_3_0= ruleBoolean ) ) otherlv_4= Semicolon ) | ( () otherlv_6= Time_max otherlv_7= EqualsSignGreaterThanSign ( (lv_value_8_0= ruleNumber ) ) otherlv_9= Semicolon ) | ( () otherlv_11= Time_min otherlv_12= EqualsSignGreaterThanSign ( (lv_value_13_0= ruleNumber ) ) otherlv_14= Semicolon ) | ( () otherlv_16= Scaling_factor otherlv_17= EqualsSignGreaterThanSign ( (lv_statement_18_0= ruleStatement ) ) otherlv_19= Semicolon ) | ( () otherlv_21= Variants otherlv_22= EqualsSignGreaterThanSign otherlv_23= LeftCurlyBracket ( (lv_variants_24_0= ruleVariants ) )? otherlv_25= RightCurlyBracket otherlv_26= Semicolon ) | ( () otherlv_28= Active_variants otherlv_29= EqualsSignGreaterThanSign otherlv_30= LeftCurlyBracket ( (lv_variants_31_0= ruleActiveVariants ) )? otherlv_32= RightCurlyBracket otherlv_33= Semicolon ) | ( () otherlv_35= Run otherlv_36= For ( (lv_count_37_0= ruleNumber ) ) otherlv_38= Times otherlv_39= Taking ( ( (lv_type_40_1= Average | lv_type_40_2= Minimum | lv_type_40_3= Maximum ) ) ) otherlv_41= Result otherlv_42= Semicolon ) | (this_CostProperties_43= ruleCostProperties otherlv_44= Semicolon ) ) )
            // InternalSimdaseParser.g:241:1: ( ( () otherlv_1= Is_adaptive otherlv_2= EqualsSignGreaterThanSign ( (lv_value_3_0= ruleBoolean ) ) otherlv_4= Semicolon ) | ( () otherlv_6= Time_max otherlv_7= EqualsSignGreaterThanSign ( (lv_value_8_0= ruleNumber ) ) otherlv_9= Semicolon ) | ( () otherlv_11= Time_min otherlv_12= EqualsSignGreaterThanSign ( (lv_value_13_0= ruleNumber ) ) otherlv_14= Semicolon ) | ( () otherlv_16= Scaling_factor otherlv_17= EqualsSignGreaterThanSign ( (lv_statement_18_0= ruleStatement ) ) otherlv_19= Semicolon ) | ( () otherlv_21= Variants otherlv_22= EqualsSignGreaterThanSign otherlv_23= LeftCurlyBracket ( (lv_variants_24_0= ruleVariants ) )? otherlv_25= RightCurlyBracket otherlv_26= Semicolon ) | ( () otherlv_28= Active_variants otherlv_29= EqualsSignGreaterThanSign otherlv_30= LeftCurlyBracket ( (lv_variants_31_0= ruleActiveVariants ) )? otherlv_32= RightCurlyBracket otherlv_33= Semicolon ) | ( () otherlv_35= Run otherlv_36= For ( (lv_count_37_0= ruleNumber ) ) otherlv_38= Times otherlv_39= Taking ( ( (lv_type_40_1= Average | lv_type_40_2= Minimum | lv_type_40_3= Maximum ) ) ) otherlv_41= Result otherlv_42= Semicolon ) | (this_CostProperties_43= ruleCostProperties otherlv_44= Semicolon ) )
            {
            // InternalSimdaseParser.g:241:1: ( ( () otherlv_1= Is_adaptive otherlv_2= EqualsSignGreaterThanSign ( (lv_value_3_0= ruleBoolean ) ) otherlv_4= Semicolon ) | ( () otherlv_6= Time_max otherlv_7= EqualsSignGreaterThanSign ( (lv_value_8_0= ruleNumber ) ) otherlv_9= Semicolon ) | ( () otherlv_11= Time_min otherlv_12= EqualsSignGreaterThanSign ( (lv_value_13_0= ruleNumber ) ) otherlv_14= Semicolon ) | ( () otherlv_16= Scaling_factor otherlv_17= EqualsSignGreaterThanSign ( (lv_statement_18_0= ruleStatement ) ) otherlv_19= Semicolon ) | ( () otherlv_21= Variants otherlv_22= EqualsSignGreaterThanSign otherlv_23= LeftCurlyBracket ( (lv_variants_24_0= ruleVariants ) )? otherlv_25= RightCurlyBracket otherlv_26= Semicolon ) | ( () otherlv_28= Active_variants otherlv_29= EqualsSignGreaterThanSign otherlv_30= LeftCurlyBracket ( (lv_variants_31_0= ruleActiveVariants ) )? otherlv_32= RightCurlyBracket otherlv_33= Semicolon ) | ( () otherlv_35= Run otherlv_36= For ( (lv_count_37_0= ruleNumber ) ) otherlv_38= Times otherlv_39= Taking ( ( (lv_type_40_1= Average | lv_type_40_2= Minimum | lv_type_40_3= Maximum ) ) ) otherlv_41= Result otherlv_42= Semicolon ) | (this_CostProperties_43= ruleCostProperties otherlv_44= Semicolon ) )
            int alt5=8;
            switch ( input.LA(1) ) {
            case Is_adaptive:
                {
                alt5=1;
                }
                break;
            case Time_max:
                {
                alt5=2;
                }
                break;
            case Time_min:
                {
                alt5=3;
                }
                break;
            case Scaling_factor:
                {
                alt5=4;
                }
                break;
            case Variants:
                {
                alt5=5;
                }
                break;
            case Active_variants:
                {
                alt5=6;
                }
                break;
            case Run:
                {
                alt5=7;
                }
                break;
            case Cost:
                {
                alt5=8;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }

            switch (alt5) {
                case 1 :
                    // InternalSimdaseParser.g:241:2: ( () otherlv_1= Is_adaptive otherlv_2= EqualsSignGreaterThanSign ( (lv_value_3_0= ruleBoolean ) ) otherlv_4= Semicolon )
                    {
                    // InternalSimdaseParser.g:241:2: ( () otherlv_1= Is_adaptive otherlv_2= EqualsSignGreaterThanSign ( (lv_value_3_0= ruleBoolean ) ) otherlv_4= Semicolon )
                    // InternalSimdaseParser.g:241:3: () otherlv_1= Is_adaptive otherlv_2= EqualsSignGreaterThanSign ( (lv_value_3_0= ruleBoolean ) ) otherlv_4= Semicolon
                    {
                    // InternalSimdaseParser.g:241:3: ()
                    // InternalSimdaseParser.g:242:5: 
                    {

                            current = forceCreateModelElement(
                                grammarAccess.getSIMDASEStatementAccess().getIsAdaptiveStatementAction_0_0(),
                                current);
                        

                    }

                    otherlv_1=(Token)match(input,Is_adaptive,FollowSets000.FOLLOW_4); 

                        	newLeafNode(otherlv_1, grammarAccess.getSIMDASEStatementAccess().getIs_adaptiveKeyword_0_1());
                        
                    otherlv_2=(Token)match(input,EqualsSignGreaterThanSign,FollowSets000.FOLLOW_5); 

                        	newLeafNode(otherlv_2, grammarAccess.getSIMDASEStatementAccess().getEqualsSignGreaterThanSignKeyword_0_2());
                        
                    // InternalSimdaseParser.g:257:1: ( (lv_value_3_0= ruleBoolean ) )
                    // InternalSimdaseParser.g:258:1: (lv_value_3_0= ruleBoolean )
                    {
                    // InternalSimdaseParser.g:258:1: (lv_value_3_0= ruleBoolean )
                    // InternalSimdaseParser.g:259:3: lv_value_3_0= ruleBoolean
                    {
                     
                    	        newCompositeNode(grammarAccess.getSIMDASEStatementAccess().getValueBooleanParserRuleCall_0_3_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_6);
                    lv_value_3_0=ruleBoolean();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getSIMDASEStatementRule());
                    	        }
                           		set(
                           			current, 
                           			"value",
                            		lv_value_3_0, 
                            		"edu.clemson.simdase.Simdase.Boolean");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    otherlv_4=(Token)match(input,Semicolon,FollowSets000.FOLLOW_2); 

                        	newLeafNode(otherlv_4, grammarAccess.getSIMDASEStatementAccess().getSemicolonKeyword_0_4());
                        

                    }


                    }
                    break;
                case 2 :
                    // InternalSimdaseParser.g:281:6: ( () otherlv_6= Time_max otherlv_7= EqualsSignGreaterThanSign ( (lv_value_8_0= ruleNumber ) ) otherlv_9= Semicolon )
                    {
                    // InternalSimdaseParser.g:281:6: ( () otherlv_6= Time_max otherlv_7= EqualsSignGreaterThanSign ( (lv_value_8_0= ruleNumber ) ) otherlv_9= Semicolon )
                    // InternalSimdaseParser.g:281:7: () otherlv_6= Time_max otherlv_7= EqualsSignGreaterThanSign ( (lv_value_8_0= ruleNumber ) ) otherlv_9= Semicolon
                    {
                    // InternalSimdaseParser.g:281:7: ()
                    // InternalSimdaseParser.g:282:5: 
                    {

                            current = forceCreateModelElement(
                                grammarAccess.getSIMDASEStatementAccess().getTMaxAction_1_0(),
                                current);
                        

                    }

                    otherlv_6=(Token)match(input,Time_max,FollowSets000.FOLLOW_4); 

                        	newLeafNode(otherlv_6, grammarAccess.getSIMDASEStatementAccess().getTime_maxKeyword_1_1());
                        
                    otherlv_7=(Token)match(input,EqualsSignGreaterThanSign,FollowSets000.FOLLOW_7); 

                        	newLeafNode(otherlv_7, grammarAccess.getSIMDASEStatementAccess().getEqualsSignGreaterThanSignKeyword_1_2());
                        
                    // InternalSimdaseParser.g:297:1: ( (lv_value_8_0= ruleNumber ) )
                    // InternalSimdaseParser.g:298:1: (lv_value_8_0= ruleNumber )
                    {
                    // InternalSimdaseParser.g:298:1: (lv_value_8_0= ruleNumber )
                    // InternalSimdaseParser.g:299:3: lv_value_8_0= ruleNumber
                    {
                     
                    	        newCompositeNode(grammarAccess.getSIMDASEStatementAccess().getValueNumberParserRuleCall_1_3_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_6);
                    lv_value_8_0=ruleNumber();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getSIMDASEStatementRule());
                    	        }
                           		set(
                           			current, 
                           			"value",
                            		lv_value_8_0, 
                            		"edu.clemson.simdase.Simdase.Number");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    otherlv_9=(Token)match(input,Semicolon,FollowSets000.FOLLOW_2); 

                        	newLeafNode(otherlv_9, grammarAccess.getSIMDASEStatementAccess().getSemicolonKeyword_1_4());
                        

                    }


                    }
                    break;
                case 3 :
                    // InternalSimdaseParser.g:321:6: ( () otherlv_11= Time_min otherlv_12= EqualsSignGreaterThanSign ( (lv_value_13_0= ruleNumber ) ) otherlv_14= Semicolon )
                    {
                    // InternalSimdaseParser.g:321:6: ( () otherlv_11= Time_min otherlv_12= EqualsSignGreaterThanSign ( (lv_value_13_0= ruleNumber ) ) otherlv_14= Semicolon )
                    // InternalSimdaseParser.g:321:7: () otherlv_11= Time_min otherlv_12= EqualsSignGreaterThanSign ( (lv_value_13_0= ruleNumber ) ) otherlv_14= Semicolon
                    {
                    // InternalSimdaseParser.g:321:7: ()
                    // InternalSimdaseParser.g:322:5: 
                    {

                            current = forceCreateModelElement(
                                grammarAccess.getSIMDASEStatementAccess().getTMinAction_2_0(),
                                current);
                        

                    }

                    otherlv_11=(Token)match(input,Time_min,FollowSets000.FOLLOW_4); 

                        	newLeafNode(otherlv_11, grammarAccess.getSIMDASEStatementAccess().getTime_minKeyword_2_1());
                        
                    otherlv_12=(Token)match(input,EqualsSignGreaterThanSign,FollowSets000.FOLLOW_7); 

                        	newLeafNode(otherlv_12, grammarAccess.getSIMDASEStatementAccess().getEqualsSignGreaterThanSignKeyword_2_2());
                        
                    // InternalSimdaseParser.g:337:1: ( (lv_value_13_0= ruleNumber ) )
                    // InternalSimdaseParser.g:338:1: (lv_value_13_0= ruleNumber )
                    {
                    // InternalSimdaseParser.g:338:1: (lv_value_13_0= ruleNumber )
                    // InternalSimdaseParser.g:339:3: lv_value_13_0= ruleNumber
                    {
                     
                    	        newCompositeNode(grammarAccess.getSIMDASEStatementAccess().getValueNumberParserRuleCall_2_3_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_6);
                    lv_value_13_0=ruleNumber();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getSIMDASEStatementRule());
                    	        }
                           		set(
                           			current, 
                           			"value",
                            		lv_value_13_0, 
                            		"edu.clemson.simdase.Simdase.Number");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    otherlv_14=(Token)match(input,Semicolon,FollowSets000.FOLLOW_2); 

                        	newLeafNode(otherlv_14, grammarAccess.getSIMDASEStatementAccess().getSemicolonKeyword_2_4());
                        

                    }


                    }
                    break;
                case 4 :
                    // InternalSimdaseParser.g:361:6: ( () otherlv_16= Scaling_factor otherlv_17= EqualsSignGreaterThanSign ( (lv_statement_18_0= ruleStatement ) ) otherlv_19= Semicolon )
                    {
                    // InternalSimdaseParser.g:361:6: ( () otherlv_16= Scaling_factor otherlv_17= EqualsSignGreaterThanSign ( (lv_statement_18_0= ruleStatement ) ) otherlv_19= Semicolon )
                    // InternalSimdaseParser.g:361:7: () otherlv_16= Scaling_factor otherlv_17= EqualsSignGreaterThanSign ( (lv_statement_18_0= ruleStatement ) ) otherlv_19= Semicolon
                    {
                    // InternalSimdaseParser.g:361:7: ()
                    // InternalSimdaseParser.g:362:5: 
                    {

                            current = forceCreateModelElement(
                                grammarAccess.getSIMDASEStatementAccess().getScalingFactorAction_3_0(),
                                current);
                        

                    }

                    otherlv_16=(Token)match(input,Scaling_factor,FollowSets000.FOLLOW_4); 

                        	newLeafNode(otherlv_16, grammarAccess.getSIMDASEStatementAccess().getScaling_factorKeyword_3_1());
                        
                    otherlv_17=(Token)match(input,EqualsSignGreaterThanSign,FollowSets000.FOLLOW_8); 

                        	newLeafNode(otherlv_17, grammarAccess.getSIMDASEStatementAccess().getEqualsSignGreaterThanSignKeyword_3_2());
                        
                    // InternalSimdaseParser.g:377:1: ( (lv_statement_18_0= ruleStatement ) )
                    // InternalSimdaseParser.g:378:1: (lv_statement_18_0= ruleStatement )
                    {
                    // InternalSimdaseParser.g:378:1: (lv_statement_18_0= ruleStatement )
                    // InternalSimdaseParser.g:379:3: lv_statement_18_0= ruleStatement
                    {
                     
                    	        newCompositeNode(grammarAccess.getSIMDASEStatementAccess().getStatementStatementParserRuleCall_3_3_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_6);
                    lv_statement_18_0=ruleStatement();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getSIMDASEStatementRule());
                    	        }
                           		set(
                           			current, 
                           			"statement",
                            		lv_statement_18_0, 
                            		"edu.clemson.simdase.Simdase.Statement");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    otherlv_19=(Token)match(input,Semicolon,FollowSets000.FOLLOW_2); 

                        	newLeafNode(otherlv_19, grammarAccess.getSIMDASEStatementAccess().getSemicolonKeyword_3_4());
                        

                    }


                    }
                    break;
                case 5 :
                    // InternalSimdaseParser.g:401:6: ( () otherlv_21= Variants otherlv_22= EqualsSignGreaterThanSign otherlv_23= LeftCurlyBracket ( (lv_variants_24_0= ruleVariants ) )? otherlv_25= RightCurlyBracket otherlv_26= Semicolon )
                    {
                    // InternalSimdaseParser.g:401:6: ( () otherlv_21= Variants otherlv_22= EqualsSignGreaterThanSign otherlv_23= LeftCurlyBracket ( (lv_variants_24_0= ruleVariants ) )? otherlv_25= RightCurlyBracket otherlv_26= Semicolon )
                    // InternalSimdaseParser.g:401:7: () otherlv_21= Variants otherlv_22= EqualsSignGreaterThanSign otherlv_23= LeftCurlyBracket ( (lv_variants_24_0= ruleVariants ) )? otherlv_25= RightCurlyBracket otherlv_26= Semicolon
                    {
                    // InternalSimdaseParser.g:401:7: ()
                    // InternalSimdaseParser.g:402:5: 
                    {

                            current = forceCreateModelElement(
                                grammarAccess.getSIMDASEStatementAccess().getVariantsAction_4_0(),
                                current);
                        

                    }

                    otherlv_21=(Token)match(input,Variants,FollowSets000.FOLLOW_4); 

                        	newLeafNode(otherlv_21, grammarAccess.getSIMDASEStatementAccess().getVariantsKeyword_4_1());
                        
                    otherlv_22=(Token)match(input,EqualsSignGreaterThanSign,FollowSets000.FOLLOW_9); 

                        	newLeafNode(otherlv_22, grammarAccess.getSIMDASEStatementAccess().getEqualsSignGreaterThanSignKeyword_4_2());
                        
                    otherlv_23=(Token)match(input,LeftCurlyBracket,FollowSets000.FOLLOW_10); 

                        	newLeafNode(otherlv_23, grammarAccess.getSIMDASEStatementAccess().getLeftCurlyBracketKeyword_4_3());
                        
                    // InternalSimdaseParser.g:422:1: ( (lv_variants_24_0= ruleVariants ) )?
                    int alt2=2;
                    int LA2_0 = input.LA(1);

                    if ( (LA2_0==RULE_STRING) ) {
                        alt2=1;
                    }
                    switch (alt2) {
                        case 1 :
                            // InternalSimdaseParser.g:423:1: (lv_variants_24_0= ruleVariants )
                            {
                            // InternalSimdaseParser.g:423:1: (lv_variants_24_0= ruleVariants )
                            // InternalSimdaseParser.g:424:3: lv_variants_24_0= ruleVariants
                            {
                             
                            	        newCompositeNode(grammarAccess.getSIMDASEStatementAccess().getVariantsVariantsParserRuleCall_4_4_0()); 
                            	    
                            pushFollow(FollowSets000.FOLLOW_11);
                            lv_variants_24_0=ruleVariants();

                            state._fsp--;


                            	        if (current==null) {
                            	            current = createModelElementForParent(grammarAccess.getSIMDASEStatementRule());
                            	        }
                                   		set(
                                   			current, 
                                   			"variants",
                                    		lv_variants_24_0, 
                                    		"edu.clemson.simdase.Simdase.Variants");
                            	        afterParserOrEnumRuleCall();
                            	    

                            }


                            }
                            break;

                    }

                    otherlv_25=(Token)match(input,RightCurlyBracket,FollowSets000.FOLLOW_6); 

                        	newLeafNode(otherlv_25, grammarAccess.getSIMDASEStatementAccess().getRightCurlyBracketKeyword_4_5());
                        
                    otherlv_26=(Token)match(input,Semicolon,FollowSets000.FOLLOW_2); 

                        	newLeafNode(otherlv_26, grammarAccess.getSIMDASEStatementAccess().getSemicolonKeyword_4_6());
                        

                    }


                    }
                    break;
                case 6 :
                    // InternalSimdaseParser.g:451:6: ( () otherlv_28= Active_variants otherlv_29= EqualsSignGreaterThanSign otherlv_30= LeftCurlyBracket ( (lv_variants_31_0= ruleActiveVariants ) )? otherlv_32= RightCurlyBracket otherlv_33= Semicolon )
                    {
                    // InternalSimdaseParser.g:451:6: ( () otherlv_28= Active_variants otherlv_29= EqualsSignGreaterThanSign otherlv_30= LeftCurlyBracket ( (lv_variants_31_0= ruleActiveVariants ) )? otherlv_32= RightCurlyBracket otherlv_33= Semicolon )
                    // InternalSimdaseParser.g:451:7: () otherlv_28= Active_variants otherlv_29= EqualsSignGreaterThanSign otherlv_30= LeftCurlyBracket ( (lv_variants_31_0= ruleActiveVariants ) )? otherlv_32= RightCurlyBracket otherlv_33= Semicolon
                    {
                    // InternalSimdaseParser.g:451:7: ()
                    // InternalSimdaseParser.g:452:5: 
                    {

                            current = forceCreateModelElement(
                                grammarAccess.getSIMDASEStatementAccess().getActiveVariantsAction_5_0(),
                                current);
                        

                    }

                    otherlv_28=(Token)match(input,Active_variants,FollowSets000.FOLLOW_4); 

                        	newLeafNode(otherlv_28, grammarAccess.getSIMDASEStatementAccess().getActive_variantsKeyword_5_1());
                        
                    otherlv_29=(Token)match(input,EqualsSignGreaterThanSign,FollowSets000.FOLLOW_9); 

                        	newLeafNode(otherlv_29, grammarAccess.getSIMDASEStatementAccess().getEqualsSignGreaterThanSignKeyword_5_2());
                        
                    otherlv_30=(Token)match(input,LeftCurlyBracket,FollowSets000.FOLLOW_10); 

                        	newLeafNode(otherlv_30, grammarAccess.getSIMDASEStatementAccess().getLeftCurlyBracketKeyword_5_3());
                        
                    // InternalSimdaseParser.g:472:1: ( (lv_variants_31_0= ruleActiveVariants ) )?
                    int alt3=2;
                    int LA3_0 = input.LA(1);

                    if ( (LA3_0==RULE_STRING) ) {
                        alt3=1;
                    }
                    switch (alt3) {
                        case 1 :
                            // InternalSimdaseParser.g:473:1: (lv_variants_31_0= ruleActiveVariants )
                            {
                            // InternalSimdaseParser.g:473:1: (lv_variants_31_0= ruleActiveVariants )
                            // InternalSimdaseParser.g:474:3: lv_variants_31_0= ruleActiveVariants
                            {
                             
                            	        newCompositeNode(grammarAccess.getSIMDASEStatementAccess().getVariantsActiveVariantsParserRuleCall_5_4_0()); 
                            	    
                            pushFollow(FollowSets000.FOLLOW_11);
                            lv_variants_31_0=ruleActiveVariants();

                            state._fsp--;


                            	        if (current==null) {
                            	            current = createModelElementForParent(grammarAccess.getSIMDASEStatementRule());
                            	        }
                                   		set(
                                   			current, 
                                   			"variants",
                                    		lv_variants_31_0, 
                                    		"edu.clemson.simdase.Simdase.ActiveVariants");
                            	        afterParserOrEnumRuleCall();
                            	    

                            }


                            }
                            break;

                    }

                    otherlv_32=(Token)match(input,RightCurlyBracket,FollowSets000.FOLLOW_6); 

                        	newLeafNode(otherlv_32, grammarAccess.getSIMDASEStatementAccess().getRightCurlyBracketKeyword_5_5());
                        
                    otherlv_33=(Token)match(input,Semicolon,FollowSets000.FOLLOW_2); 

                        	newLeafNode(otherlv_33, grammarAccess.getSIMDASEStatementAccess().getSemicolonKeyword_5_6());
                        

                    }


                    }
                    break;
                case 7 :
                    // InternalSimdaseParser.g:501:6: ( () otherlv_35= Run otherlv_36= For ( (lv_count_37_0= ruleNumber ) ) otherlv_38= Times otherlv_39= Taking ( ( (lv_type_40_1= Average | lv_type_40_2= Minimum | lv_type_40_3= Maximum ) ) ) otherlv_41= Result otherlv_42= Semicolon )
                    {
                    // InternalSimdaseParser.g:501:6: ( () otherlv_35= Run otherlv_36= For ( (lv_count_37_0= ruleNumber ) ) otherlv_38= Times otherlv_39= Taking ( ( (lv_type_40_1= Average | lv_type_40_2= Minimum | lv_type_40_3= Maximum ) ) ) otherlv_41= Result otherlv_42= Semicolon )
                    // InternalSimdaseParser.g:501:7: () otherlv_35= Run otherlv_36= For ( (lv_count_37_0= ruleNumber ) ) otherlv_38= Times otherlv_39= Taking ( ( (lv_type_40_1= Average | lv_type_40_2= Minimum | lv_type_40_3= Maximum ) ) ) otherlv_41= Result otherlv_42= Semicolon
                    {
                    // InternalSimdaseParser.g:501:7: ()
                    // InternalSimdaseParser.g:502:5: 
                    {

                            current = forceCreateModelElement(
                                grammarAccess.getSIMDASEStatementAccess().getReEvaluateAveragingAction_6_0(),
                                current);
                        

                    }

                    otherlv_35=(Token)match(input,Run,FollowSets000.FOLLOW_12); 

                        	newLeafNode(otherlv_35, grammarAccess.getSIMDASEStatementAccess().getRunKeyword_6_1());
                        
                    otherlv_36=(Token)match(input,For,FollowSets000.FOLLOW_7); 

                        	newLeafNode(otherlv_36, grammarAccess.getSIMDASEStatementAccess().getForKeyword_6_2());
                        
                    // InternalSimdaseParser.g:517:1: ( (lv_count_37_0= ruleNumber ) )
                    // InternalSimdaseParser.g:518:1: (lv_count_37_0= ruleNumber )
                    {
                    // InternalSimdaseParser.g:518:1: (lv_count_37_0= ruleNumber )
                    // InternalSimdaseParser.g:519:3: lv_count_37_0= ruleNumber
                    {
                     
                    	        newCompositeNode(grammarAccess.getSIMDASEStatementAccess().getCountNumberParserRuleCall_6_3_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_13);
                    lv_count_37_0=ruleNumber();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getSIMDASEStatementRule());
                    	        }
                           		set(
                           			current, 
                           			"count",
                            		lv_count_37_0, 
                            		"edu.clemson.simdase.Simdase.Number");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    otherlv_38=(Token)match(input,Times,FollowSets000.FOLLOW_14); 

                        	newLeafNode(otherlv_38, grammarAccess.getSIMDASEStatementAccess().getTimesKeyword_6_4());
                        
                    otherlv_39=(Token)match(input,Taking,FollowSets000.FOLLOW_15); 

                        	newLeafNode(otherlv_39, grammarAccess.getSIMDASEStatementAccess().getTakingKeyword_6_5());
                        
                    // InternalSimdaseParser.g:545:1: ( ( (lv_type_40_1= Average | lv_type_40_2= Minimum | lv_type_40_3= Maximum ) ) )
                    // InternalSimdaseParser.g:546:1: ( (lv_type_40_1= Average | lv_type_40_2= Minimum | lv_type_40_3= Maximum ) )
                    {
                    // InternalSimdaseParser.g:546:1: ( (lv_type_40_1= Average | lv_type_40_2= Minimum | lv_type_40_3= Maximum ) )
                    // InternalSimdaseParser.g:547:1: (lv_type_40_1= Average | lv_type_40_2= Minimum | lv_type_40_3= Maximum )
                    {
                    // InternalSimdaseParser.g:547:1: (lv_type_40_1= Average | lv_type_40_2= Minimum | lv_type_40_3= Maximum )
                    int alt4=3;
                    switch ( input.LA(1) ) {
                    case Average:
                        {
                        alt4=1;
                        }
                        break;
                    case Minimum:
                        {
                        alt4=2;
                        }
                        break;
                    case Maximum:
                        {
                        alt4=3;
                        }
                        break;
                    default:
                        NoViableAltException nvae =
                            new NoViableAltException("", 4, 0, input);

                        throw nvae;
                    }

                    switch (alt4) {
                        case 1 :
                            // InternalSimdaseParser.g:548:3: lv_type_40_1= Average
                            {
                            lv_type_40_1=(Token)match(input,Average,FollowSets000.FOLLOW_16); 

                                    newLeafNode(lv_type_40_1, grammarAccess.getSIMDASEStatementAccess().getTypeAverageKeyword_6_6_0_0());
                                

                            	        if (current==null) {
                            	            current = createModelElement(grammarAccess.getSIMDASEStatementRule());
                            	        }
                                   		setWithLastConsumed(current, "type", lv_type_40_1, null);
                            	    

                            }
                            break;
                        case 2 :
                            // InternalSimdaseParser.g:561:8: lv_type_40_2= Minimum
                            {
                            lv_type_40_2=(Token)match(input,Minimum,FollowSets000.FOLLOW_16); 

                                    newLeafNode(lv_type_40_2, grammarAccess.getSIMDASEStatementAccess().getTypeMinimumKeyword_6_6_0_1());
                                

                            	        if (current==null) {
                            	            current = createModelElement(grammarAccess.getSIMDASEStatementRule());
                            	        }
                                   		setWithLastConsumed(current, "type", lv_type_40_2, null);
                            	    

                            }
                            break;
                        case 3 :
                            // InternalSimdaseParser.g:574:8: lv_type_40_3= Maximum
                            {
                            lv_type_40_3=(Token)match(input,Maximum,FollowSets000.FOLLOW_16); 

                                    newLeafNode(lv_type_40_3, grammarAccess.getSIMDASEStatementAccess().getTypeMaximumKeyword_6_6_0_2());
                                

                            	        if (current==null) {
                            	            current = createModelElement(grammarAccess.getSIMDASEStatementRule());
                            	        }
                                   		setWithLastConsumed(current, "type", lv_type_40_3, null);
                            	    

                            }
                            break;

                    }


                    }


                    }

                    otherlv_41=(Token)match(input,Result,FollowSets000.FOLLOW_6); 

                        	newLeafNode(otherlv_41, grammarAccess.getSIMDASEStatementAccess().getResultKeyword_6_7());
                        
                    otherlv_42=(Token)match(input,Semicolon,FollowSets000.FOLLOW_2); 

                        	newLeafNode(otherlv_42, grammarAccess.getSIMDASEStatementAccess().getSemicolonKeyword_6_8());
                        

                    }


                    }
                    break;
                case 8 :
                    // InternalSimdaseParser.g:601:6: (this_CostProperties_43= ruleCostProperties otherlv_44= Semicolon )
                    {
                    // InternalSimdaseParser.g:601:6: (this_CostProperties_43= ruleCostProperties otherlv_44= Semicolon )
                    // InternalSimdaseParser.g:602:5: this_CostProperties_43= ruleCostProperties otherlv_44= Semicolon
                    {
                     
                            newCompositeNode(grammarAccess.getSIMDASEStatementAccess().getCostPropertiesParserRuleCall_7_0()); 
                        
                    pushFollow(FollowSets000.FOLLOW_6);
                    this_CostProperties_43=ruleCostProperties();

                    state._fsp--;


                            current = this_CostProperties_43;
                            afterParserOrEnumRuleCall();
                        
                    otherlv_44=(Token)match(input,Semicolon,FollowSets000.FOLLOW_2); 

                        	newLeafNode(otherlv_44, grammarAccess.getSIMDASEStatementAccess().getSemicolonKeyword_7_1());
                        

                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSIMDASEStatement"


    // $ANTLR start "entryRuleVariants"
    // InternalSimdaseParser.g:623:1: entryRuleVariants returns [EObject current=null] : iv_ruleVariants= ruleVariants EOF ;
    public final EObject entryRuleVariants() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVariants = null;


        try {
            // InternalSimdaseParser.g:624:2: (iv_ruleVariants= ruleVariants EOF )
            // InternalSimdaseParser.g:625:2: iv_ruleVariants= ruleVariants EOF
            {
             newCompositeNode(grammarAccess.getVariantsRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleVariants=ruleVariants();

            state._fsp--;

             current =iv_ruleVariants; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVariants"


    // $ANTLR start "ruleVariants"
    // InternalSimdaseParser.g:632:1: ruleVariants returns [EObject current=null] : ( ( (lv_first_0_0= ruleVariant ) ) (otherlv_1= Semicolon ( (lv_rest_2_0= ruleVariant ) ) )* otherlv_3= Semicolon ) ;
    public final EObject ruleVariants() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_first_0_0 = null;

        EObject lv_rest_2_0 = null;


         enterRule(); 
            
        try {
            // InternalSimdaseParser.g:635:28: ( ( ( (lv_first_0_0= ruleVariant ) ) (otherlv_1= Semicolon ( (lv_rest_2_0= ruleVariant ) ) )* otherlv_3= Semicolon ) )
            // InternalSimdaseParser.g:636:1: ( ( (lv_first_0_0= ruleVariant ) ) (otherlv_1= Semicolon ( (lv_rest_2_0= ruleVariant ) ) )* otherlv_3= Semicolon )
            {
            // InternalSimdaseParser.g:636:1: ( ( (lv_first_0_0= ruleVariant ) ) (otherlv_1= Semicolon ( (lv_rest_2_0= ruleVariant ) ) )* otherlv_3= Semicolon )
            // InternalSimdaseParser.g:636:2: ( (lv_first_0_0= ruleVariant ) ) (otherlv_1= Semicolon ( (lv_rest_2_0= ruleVariant ) ) )* otherlv_3= Semicolon
            {
            // InternalSimdaseParser.g:636:2: ( (lv_first_0_0= ruleVariant ) )
            // InternalSimdaseParser.g:637:1: (lv_first_0_0= ruleVariant )
            {
            // InternalSimdaseParser.g:637:1: (lv_first_0_0= ruleVariant )
            // InternalSimdaseParser.g:638:3: lv_first_0_0= ruleVariant
            {
             
            	        newCompositeNode(grammarAccess.getVariantsAccess().getFirstVariantParserRuleCall_0_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_6);
            lv_first_0_0=ruleVariant();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getVariantsRule());
            	        }
                   		set(
                   			current, 
                   			"first",
                    		lv_first_0_0, 
                    		"edu.clemson.simdase.Simdase.Variant");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // InternalSimdaseParser.g:654:2: (otherlv_1= Semicolon ( (lv_rest_2_0= ruleVariant ) ) )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==Semicolon) ) {
                    int LA6_1 = input.LA(2);

                    if ( (LA6_1==RULE_STRING) ) {
                        alt6=1;
                    }


                }


                switch (alt6) {
            	case 1 :
            	    // InternalSimdaseParser.g:655:2: otherlv_1= Semicolon ( (lv_rest_2_0= ruleVariant ) )
            	    {
            	    otherlv_1=(Token)match(input,Semicolon,FollowSets000.FOLLOW_17); 

            	        	newLeafNode(otherlv_1, grammarAccess.getVariantsAccess().getSemicolonKeyword_1_0());
            	        
            	    // InternalSimdaseParser.g:659:1: ( (lv_rest_2_0= ruleVariant ) )
            	    // InternalSimdaseParser.g:660:1: (lv_rest_2_0= ruleVariant )
            	    {
            	    // InternalSimdaseParser.g:660:1: (lv_rest_2_0= ruleVariant )
            	    // InternalSimdaseParser.g:661:3: lv_rest_2_0= ruleVariant
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getVariantsAccess().getRestVariantParserRuleCall_1_1_0()); 
            	    	    
            	    pushFollow(FollowSets000.FOLLOW_6);
            	    lv_rest_2_0=ruleVariant();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getVariantsRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"rest",
            	            		lv_rest_2_0, 
            	            		"edu.clemson.simdase.Simdase.Variant");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);

            otherlv_3=(Token)match(input,Semicolon,FollowSets000.FOLLOW_2); 

                	newLeafNode(otherlv_3, grammarAccess.getVariantsAccess().getSemicolonKeyword_2());
                

            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVariants"


    // $ANTLR start "entryRuleVariant"
    // InternalSimdaseParser.g:690:1: entryRuleVariant returns [EObject current=null] : iv_ruleVariant= ruleVariant EOF ;
    public final EObject entryRuleVariant() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVariant = null;


        try {
            // InternalSimdaseParser.g:691:2: (iv_ruleVariant= ruleVariant EOF )
            // InternalSimdaseParser.g:692:2: iv_ruleVariant= ruleVariant EOF
            {
             newCompositeNode(grammarAccess.getVariantRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleVariant=ruleVariant();

            state._fsp--;

             current =iv_ruleVariant; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVariant"


    // $ANTLR start "ruleVariant"
    // InternalSimdaseParser.g:699:1: ruleVariant returns [EObject current=null] : ( ( (lv_name_0_0= RULE_STRING ) ) otherlv_1= Colon otherlv_2= LeftCurlyBracket ( (lv_variants_3_0= ruleVariantNames ) )? otherlv_4= RightCurlyBracket ) ;
    public final EObject ruleVariant() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_variants_3_0 = null;


         enterRule(); 
            
        try {
            // InternalSimdaseParser.g:702:28: ( ( ( (lv_name_0_0= RULE_STRING ) ) otherlv_1= Colon otherlv_2= LeftCurlyBracket ( (lv_variants_3_0= ruleVariantNames ) )? otherlv_4= RightCurlyBracket ) )
            // InternalSimdaseParser.g:703:1: ( ( (lv_name_0_0= RULE_STRING ) ) otherlv_1= Colon otherlv_2= LeftCurlyBracket ( (lv_variants_3_0= ruleVariantNames ) )? otherlv_4= RightCurlyBracket )
            {
            // InternalSimdaseParser.g:703:1: ( ( (lv_name_0_0= RULE_STRING ) ) otherlv_1= Colon otherlv_2= LeftCurlyBracket ( (lv_variants_3_0= ruleVariantNames ) )? otherlv_4= RightCurlyBracket )
            // InternalSimdaseParser.g:703:2: ( (lv_name_0_0= RULE_STRING ) ) otherlv_1= Colon otherlv_2= LeftCurlyBracket ( (lv_variants_3_0= ruleVariantNames ) )? otherlv_4= RightCurlyBracket
            {
            // InternalSimdaseParser.g:703:2: ( (lv_name_0_0= RULE_STRING ) )
            // InternalSimdaseParser.g:704:1: (lv_name_0_0= RULE_STRING )
            {
            // InternalSimdaseParser.g:704:1: (lv_name_0_0= RULE_STRING )
            // InternalSimdaseParser.g:705:3: lv_name_0_0= RULE_STRING
            {
            lv_name_0_0=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_18); 

            			newLeafNode(lv_name_0_0, grammarAccess.getVariantAccess().getNameSTRINGTerminalRuleCall_0_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getVariantRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_0_0, 
                    		"org.osate.xtext.aadl2.properties.Properties.STRING");
            	    

            }


            }

            otherlv_1=(Token)match(input,Colon,FollowSets000.FOLLOW_9); 

                	newLeafNode(otherlv_1, grammarAccess.getVariantAccess().getColonKeyword_1());
                
            otherlv_2=(Token)match(input,LeftCurlyBracket,FollowSets000.FOLLOW_10); 

                	newLeafNode(otherlv_2, grammarAccess.getVariantAccess().getLeftCurlyBracketKeyword_2());
                
            // InternalSimdaseParser.g:731:1: ( (lv_variants_3_0= ruleVariantNames ) )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==RULE_STRING) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // InternalSimdaseParser.g:732:1: (lv_variants_3_0= ruleVariantNames )
                    {
                    // InternalSimdaseParser.g:732:1: (lv_variants_3_0= ruleVariantNames )
                    // InternalSimdaseParser.g:733:3: lv_variants_3_0= ruleVariantNames
                    {
                     
                    	        newCompositeNode(grammarAccess.getVariantAccess().getVariantsVariantNamesParserRuleCall_3_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_11);
                    lv_variants_3_0=ruleVariantNames();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getVariantRule());
                    	        }
                           		set(
                           			current, 
                           			"variants",
                            		lv_variants_3_0, 
                            		"edu.clemson.simdase.Simdase.VariantNames");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }
                    break;

            }

            otherlv_4=(Token)match(input,RightCurlyBracket,FollowSets000.FOLLOW_2); 

                	newLeafNode(otherlv_4, grammarAccess.getVariantAccess().getRightCurlyBracketKeyword_4());
                

            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVariant"


    // $ANTLR start "entryRuleActiveVariants"
    // InternalSimdaseParser.g:762:1: entryRuleActiveVariants returns [EObject current=null] : iv_ruleActiveVariants= ruleActiveVariants EOF ;
    public final EObject entryRuleActiveVariants() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleActiveVariants = null;


        try {
            // InternalSimdaseParser.g:763:2: (iv_ruleActiveVariants= ruleActiveVariants EOF )
            // InternalSimdaseParser.g:764:2: iv_ruleActiveVariants= ruleActiveVariants EOF
            {
             newCompositeNode(grammarAccess.getActiveVariantsRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleActiveVariants=ruleActiveVariants();

            state._fsp--;

             current =iv_ruleActiveVariants; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleActiveVariants"


    // $ANTLR start "ruleActiveVariants"
    // InternalSimdaseParser.g:771:1: ruleActiveVariants returns [EObject current=null] : ( ( (lv_first_0_0= ruleActiveVariant ) ) (otherlv_1= Semicolon ( (lv_rest_2_0= ruleActiveVariant ) ) )* otherlv_3= Semicolon ) ;
    public final EObject ruleActiveVariants() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_first_0_0 = null;

        EObject lv_rest_2_0 = null;


         enterRule(); 
            
        try {
            // InternalSimdaseParser.g:774:28: ( ( ( (lv_first_0_0= ruleActiveVariant ) ) (otherlv_1= Semicolon ( (lv_rest_2_0= ruleActiveVariant ) ) )* otherlv_3= Semicolon ) )
            // InternalSimdaseParser.g:775:1: ( ( (lv_first_0_0= ruleActiveVariant ) ) (otherlv_1= Semicolon ( (lv_rest_2_0= ruleActiveVariant ) ) )* otherlv_3= Semicolon )
            {
            // InternalSimdaseParser.g:775:1: ( ( (lv_first_0_0= ruleActiveVariant ) ) (otherlv_1= Semicolon ( (lv_rest_2_0= ruleActiveVariant ) ) )* otherlv_3= Semicolon )
            // InternalSimdaseParser.g:775:2: ( (lv_first_0_0= ruleActiveVariant ) ) (otherlv_1= Semicolon ( (lv_rest_2_0= ruleActiveVariant ) ) )* otherlv_3= Semicolon
            {
            // InternalSimdaseParser.g:775:2: ( (lv_first_0_0= ruleActiveVariant ) )
            // InternalSimdaseParser.g:776:1: (lv_first_0_0= ruleActiveVariant )
            {
            // InternalSimdaseParser.g:776:1: (lv_first_0_0= ruleActiveVariant )
            // InternalSimdaseParser.g:777:3: lv_first_0_0= ruleActiveVariant
            {
             
            	        newCompositeNode(grammarAccess.getActiveVariantsAccess().getFirstActiveVariantParserRuleCall_0_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_6);
            lv_first_0_0=ruleActiveVariant();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getActiveVariantsRule());
            	        }
                   		set(
                   			current, 
                   			"first",
                    		lv_first_0_0, 
                    		"edu.clemson.simdase.Simdase.ActiveVariant");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // InternalSimdaseParser.g:793:2: (otherlv_1= Semicolon ( (lv_rest_2_0= ruleActiveVariant ) ) )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( (LA8_0==Semicolon) ) {
                    int LA8_1 = input.LA(2);

                    if ( (LA8_1==RULE_STRING) ) {
                        alt8=1;
                    }


                }


                switch (alt8) {
            	case 1 :
            	    // InternalSimdaseParser.g:794:2: otherlv_1= Semicolon ( (lv_rest_2_0= ruleActiveVariant ) )
            	    {
            	    otherlv_1=(Token)match(input,Semicolon,FollowSets000.FOLLOW_17); 

            	        	newLeafNode(otherlv_1, grammarAccess.getActiveVariantsAccess().getSemicolonKeyword_1_0());
            	        
            	    // InternalSimdaseParser.g:798:1: ( (lv_rest_2_0= ruleActiveVariant ) )
            	    // InternalSimdaseParser.g:799:1: (lv_rest_2_0= ruleActiveVariant )
            	    {
            	    // InternalSimdaseParser.g:799:1: (lv_rest_2_0= ruleActiveVariant )
            	    // InternalSimdaseParser.g:800:3: lv_rest_2_0= ruleActiveVariant
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getActiveVariantsAccess().getRestActiveVariantParserRuleCall_1_1_0()); 
            	    	    
            	    pushFollow(FollowSets000.FOLLOW_6);
            	    lv_rest_2_0=ruleActiveVariant();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getActiveVariantsRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"rest",
            	            		lv_rest_2_0, 
            	            		"edu.clemson.simdase.Simdase.ActiveVariant");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);

            otherlv_3=(Token)match(input,Semicolon,FollowSets000.FOLLOW_2); 

                	newLeafNode(otherlv_3, grammarAccess.getActiveVariantsAccess().getSemicolonKeyword_2());
                

            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleActiveVariants"


    // $ANTLR start "entryRuleActiveVariant"
    // InternalSimdaseParser.g:829:1: entryRuleActiveVariant returns [EObject current=null] : iv_ruleActiveVariant= ruleActiveVariant EOF ;
    public final EObject entryRuleActiveVariant() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleActiveVariant = null;


        try {
            // InternalSimdaseParser.g:830:2: (iv_ruleActiveVariant= ruleActiveVariant EOF )
            // InternalSimdaseParser.g:831:2: iv_ruleActiveVariant= ruleActiveVariant EOF
            {
             newCompositeNode(grammarAccess.getActiveVariantRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleActiveVariant=ruleActiveVariant();

            state._fsp--;

             current =iv_ruleActiveVariant; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleActiveVariant"


    // $ANTLR start "ruleActiveVariant"
    // InternalSimdaseParser.g:838:1: ruleActiveVariant returns [EObject current=null] : ( ( (lv_name_0_0= RULE_STRING ) ) otherlv_1= Colon otherlv_2= LeftSquareBracket ( (lv_variants_3_0= ruleVariantNames ) )? otherlv_4= RightSquareBracket ) ;
    public final EObject ruleActiveVariant() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_variants_3_0 = null;


         enterRule(); 
            
        try {
            // InternalSimdaseParser.g:841:28: ( ( ( (lv_name_0_0= RULE_STRING ) ) otherlv_1= Colon otherlv_2= LeftSquareBracket ( (lv_variants_3_0= ruleVariantNames ) )? otherlv_4= RightSquareBracket ) )
            // InternalSimdaseParser.g:842:1: ( ( (lv_name_0_0= RULE_STRING ) ) otherlv_1= Colon otherlv_2= LeftSquareBracket ( (lv_variants_3_0= ruleVariantNames ) )? otherlv_4= RightSquareBracket )
            {
            // InternalSimdaseParser.g:842:1: ( ( (lv_name_0_0= RULE_STRING ) ) otherlv_1= Colon otherlv_2= LeftSquareBracket ( (lv_variants_3_0= ruleVariantNames ) )? otherlv_4= RightSquareBracket )
            // InternalSimdaseParser.g:842:2: ( (lv_name_0_0= RULE_STRING ) ) otherlv_1= Colon otherlv_2= LeftSquareBracket ( (lv_variants_3_0= ruleVariantNames ) )? otherlv_4= RightSquareBracket
            {
            // InternalSimdaseParser.g:842:2: ( (lv_name_0_0= RULE_STRING ) )
            // InternalSimdaseParser.g:843:1: (lv_name_0_0= RULE_STRING )
            {
            // InternalSimdaseParser.g:843:1: (lv_name_0_0= RULE_STRING )
            // InternalSimdaseParser.g:844:3: lv_name_0_0= RULE_STRING
            {
            lv_name_0_0=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_18); 

            			newLeafNode(lv_name_0_0, grammarAccess.getActiveVariantAccess().getNameSTRINGTerminalRuleCall_0_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getActiveVariantRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_0_0, 
                    		"org.osate.xtext.aadl2.properties.Properties.STRING");
            	    

            }


            }

            otherlv_1=(Token)match(input,Colon,FollowSets000.FOLLOW_19); 

                	newLeafNode(otherlv_1, grammarAccess.getActiveVariantAccess().getColonKeyword_1());
                
            otherlv_2=(Token)match(input,LeftSquareBracket,FollowSets000.FOLLOW_20); 

                	newLeafNode(otherlv_2, grammarAccess.getActiveVariantAccess().getLeftSquareBracketKeyword_2());
                
            // InternalSimdaseParser.g:870:1: ( (lv_variants_3_0= ruleVariantNames ) )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==RULE_STRING) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // InternalSimdaseParser.g:871:1: (lv_variants_3_0= ruleVariantNames )
                    {
                    // InternalSimdaseParser.g:871:1: (lv_variants_3_0= ruleVariantNames )
                    // InternalSimdaseParser.g:872:3: lv_variants_3_0= ruleVariantNames
                    {
                     
                    	        newCompositeNode(grammarAccess.getActiveVariantAccess().getVariantsVariantNamesParserRuleCall_3_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_21);
                    lv_variants_3_0=ruleVariantNames();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getActiveVariantRule());
                    	        }
                           		set(
                           			current, 
                           			"variants",
                            		lv_variants_3_0, 
                            		"edu.clemson.simdase.Simdase.VariantNames");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }
                    break;

            }

            otherlv_4=(Token)match(input,RightSquareBracket,FollowSets000.FOLLOW_2); 

                	newLeafNode(otherlv_4, grammarAccess.getActiveVariantAccess().getRightSquareBracketKeyword_4());
                

            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleActiveVariant"


    // $ANTLR start "entryRuleVariantNames"
    // InternalSimdaseParser.g:901:1: entryRuleVariantNames returns [EObject current=null] : iv_ruleVariantNames= ruleVariantNames EOF ;
    public final EObject entryRuleVariantNames() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVariantNames = null;


        try {
            // InternalSimdaseParser.g:902:2: (iv_ruleVariantNames= ruleVariantNames EOF )
            // InternalSimdaseParser.g:903:2: iv_ruleVariantNames= ruleVariantNames EOF
            {
             newCompositeNode(grammarAccess.getVariantNamesRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleVariantNames=ruleVariantNames();

            state._fsp--;

             current =iv_ruleVariantNames; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVariantNames"


    // $ANTLR start "ruleVariantNames"
    // InternalSimdaseParser.g:910:1: ruleVariantNames returns [EObject current=null] : ( ( (lv_first_0_0= RULE_STRING ) ) (otherlv_1= Comma ( (lv_rest_2_0= RULE_STRING ) ) )* ) ;
    public final EObject ruleVariantNames() throws RecognitionException {
        EObject current = null;

        Token lv_first_0_0=null;
        Token otherlv_1=null;
        Token lv_rest_2_0=null;

         enterRule(); 
            
        try {
            // InternalSimdaseParser.g:913:28: ( ( ( (lv_first_0_0= RULE_STRING ) ) (otherlv_1= Comma ( (lv_rest_2_0= RULE_STRING ) ) )* ) )
            // InternalSimdaseParser.g:914:1: ( ( (lv_first_0_0= RULE_STRING ) ) (otherlv_1= Comma ( (lv_rest_2_0= RULE_STRING ) ) )* )
            {
            // InternalSimdaseParser.g:914:1: ( ( (lv_first_0_0= RULE_STRING ) ) (otherlv_1= Comma ( (lv_rest_2_0= RULE_STRING ) ) )* )
            // InternalSimdaseParser.g:914:2: ( (lv_first_0_0= RULE_STRING ) ) (otherlv_1= Comma ( (lv_rest_2_0= RULE_STRING ) ) )*
            {
            // InternalSimdaseParser.g:914:2: ( (lv_first_0_0= RULE_STRING ) )
            // InternalSimdaseParser.g:915:1: (lv_first_0_0= RULE_STRING )
            {
            // InternalSimdaseParser.g:915:1: (lv_first_0_0= RULE_STRING )
            // InternalSimdaseParser.g:916:3: lv_first_0_0= RULE_STRING
            {
            lv_first_0_0=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_22); 

            			newLeafNode(lv_first_0_0, grammarAccess.getVariantNamesAccess().getFirstSTRINGTerminalRuleCall_0_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getVariantNamesRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"first",
                    		lv_first_0_0, 
                    		"org.osate.xtext.aadl2.properties.Properties.STRING");
            	    

            }


            }

            // InternalSimdaseParser.g:932:2: (otherlv_1= Comma ( (lv_rest_2_0= RULE_STRING ) ) )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( (LA10_0==Comma) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // InternalSimdaseParser.g:933:2: otherlv_1= Comma ( (lv_rest_2_0= RULE_STRING ) )
            	    {
            	    otherlv_1=(Token)match(input,Comma,FollowSets000.FOLLOW_17); 

            	        	newLeafNode(otherlv_1, grammarAccess.getVariantNamesAccess().getCommaKeyword_1_0());
            	        
            	    // InternalSimdaseParser.g:937:1: ( (lv_rest_2_0= RULE_STRING ) )
            	    // InternalSimdaseParser.g:938:1: (lv_rest_2_0= RULE_STRING )
            	    {
            	    // InternalSimdaseParser.g:938:1: (lv_rest_2_0= RULE_STRING )
            	    // InternalSimdaseParser.g:939:3: lv_rest_2_0= RULE_STRING
            	    {
            	    lv_rest_2_0=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_22); 

            	    			newLeafNode(lv_rest_2_0, grammarAccess.getVariantNamesAccess().getRestSTRINGTerminalRuleCall_1_1_0()); 
            	    		

            	    	        if (current==null) {
            	    	            current = createModelElement(grammarAccess.getVariantNamesRule());
            	    	        }
            	           		addWithLastConsumed(
            	           			current, 
            	           			"rest",
            	            		lv_rest_2_0, 
            	            		"org.osate.xtext.aadl2.properties.Properties.STRING");
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVariantNames"


    // $ANTLR start "entryRuleCostProperties"
    // InternalSimdaseParser.g:963:1: entryRuleCostProperties returns [EObject current=null] : iv_ruleCostProperties= ruleCostProperties EOF ;
    public final EObject entryRuleCostProperties() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCostProperties = null;


        try {
            // InternalSimdaseParser.g:964:2: (iv_ruleCostProperties= ruleCostProperties EOF )
            // InternalSimdaseParser.g:965:2: iv_ruleCostProperties= ruleCostProperties EOF
            {
             newCompositeNode(grammarAccess.getCostPropertiesRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleCostProperties=ruleCostProperties();

            state._fsp--;

             current =iv_ruleCostProperties; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCostProperties"


    // $ANTLR start "ruleCostProperties"
    // InternalSimdaseParser.g:972:1: ruleCostProperties returns [EObject current=null] : ( () otherlv_1= Cost otherlv_2= EqualsSignGreaterThanSign otherlv_3= LeftCurlyBracket ( (lv_costStatements_4_0= ruleCostStatement ) )* otherlv_5= RightCurlyBracket ) ;
    public final EObject ruleCostProperties() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_costStatements_4_0 = null;


         enterRule(); 
            
        try {
            // InternalSimdaseParser.g:975:28: ( ( () otherlv_1= Cost otherlv_2= EqualsSignGreaterThanSign otherlv_3= LeftCurlyBracket ( (lv_costStatements_4_0= ruleCostStatement ) )* otherlv_5= RightCurlyBracket ) )
            // InternalSimdaseParser.g:976:1: ( () otherlv_1= Cost otherlv_2= EqualsSignGreaterThanSign otherlv_3= LeftCurlyBracket ( (lv_costStatements_4_0= ruleCostStatement ) )* otherlv_5= RightCurlyBracket )
            {
            // InternalSimdaseParser.g:976:1: ( () otherlv_1= Cost otherlv_2= EqualsSignGreaterThanSign otherlv_3= LeftCurlyBracket ( (lv_costStatements_4_0= ruleCostStatement ) )* otherlv_5= RightCurlyBracket )
            // InternalSimdaseParser.g:976:2: () otherlv_1= Cost otherlv_2= EqualsSignGreaterThanSign otherlv_3= LeftCurlyBracket ( (lv_costStatements_4_0= ruleCostStatement ) )* otherlv_5= RightCurlyBracket
            {
            // InternalSimdaseParser.g:976:2: ()
            // InternalSimdaseParser.g:977:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getCostPropertiesAccess().getCostPropertiesAction_0(),
                        current);
                

            }

            otherlv_1=(Token)match(input,Cost,FollowSets000.FOLLOW_4); 

                	newLeafNode(otherlv_1, grammarAccess.getCostPropertiesAccess().getCostKeyword_1());
                
            otherlv_2=(Token)match(input,EqualsSignGreaterThanSign,FollowSets000.FOLLOW_9); 

                	newLeafNode(otherlv_2, grammarAccess.getCostPropertiesAccess().getEqualsSignGreaterThanSignKeyword_2());
                
            otherlv_3=(Token)match(input,LeftCurlyBracket,FollowSets000.FOLLOW_23); 

                	newLeafNode(otherlv_3, grammarAccess.getCostPropertiesAccess().getLeftCurlyBracketKeyword_3());
                
            // InternalSimdaseParser.g:997:1: ( (lv_costStatements_4_0= ruleCostStatement ) )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( ((LA11_0>=Using_externally_available_software && LA11_0<=Technical_planning)||LA11_0==Market_analysis||LA11_0==Tool_support||LA11_0==Operations||LA11_0==Training||LA11_0==Funding||(LA11_0>=Scoping && LA11_0<=Testing)||LA11_0==RULE_ID) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // InternalSimdaseParser.g:998:1: (lv_costStatements_4_0= ruleCostStatement )
            	    {
            	    // InternalSimdaseParser.g:998:1: (lv_costStatements_4_0= ruleCostStatement )
            	    // InternalSimdaseParser.g:999:3: lv_costStatements_4_0= ruleCostStatement
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getCostPropertiesAccess().getCostStatementsCostStatementParserRuleCall_4_0()); 
            	    	    
            	    pushFollow(FollowSets000.FOLLOW_23);
            	    lv_costStatements_4_0=ruleCostStatement();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getCostPropertiesRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"costStatements",
            	            		lv_costStatements_4_0, 
            	            		"edu.clemson.simdase.Simdase.CostStatement");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);

            otherlv_5=(Token)match(input,RightCurlyBracket,FollowSets000.FOLLOW_2); 

                	newLeafNode(otherlv_5, grammarAccess.getCostPropertiesAccess().getRightCurlyBracketKeyword_5());
                

            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCostProperties"


    // $ANTLR start "entryRuleCostStatement"
    // InternalSimdaseParser.g:1028:1: entryRuleCostStatement returns [EObject current=null] : iv_ruleCostStatement= ruleCostStatement EOF ;
    public final EObject entryRuleCostStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCostStatement = null;


        try {
            // InternalSimdaseParser.g:1029:2: (iv_ruleCostStatement= ruleCostStatement EOF )
            // InternalSimdaseParser.g:1030:2: iv_ruleCostStatement= ruleCostStatement EOF
            {
             newCompositeNode(grammarAccess.getCostStatementRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleCostStatement=ruleCostStatement();

            state._fsp--;

             current =iv_ruleCostStatement; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCostStatement"


    // $ANTLR start "ruleCostStatement"
    // InternalSimdaseParser.g:1037:1: ruleCostStatement returns [EObject current=null] : ( ( ( (lv_type_0_0= Architecture_definition ) ) otherlv_1= EqualsSignGreaterThanSign ( (lv_value_2_0= ruleCostValue ) ) ) | ( ( (lv_type_3_0= Architecture_evaluation ) ) otherlv_4= EqualsSignGreaterThanSign ( (lv_value_5_0= ruleCostValue ) ) ) | ( ( (lv_type_6_0= Component_development ) ) otherlv_7= EqualsSignGreaterThanSign ( (lv_value_8_0= ruleCostValue ) ) ) | ( ( (lv_type_9_0= Mining_existing_assets ) ) otherlv_10= EqualsSignGreaterThanSign ( (lv_value_11_0= ruleCostValue ) ) ) | ( ( (lv_type_12_0= Requirements_engineering ) ) otherlv_13= EqualsSignGreaterThanSign ( (lv_value_14_0= ruleCostValue ) ) ) | ( ( (lv_type_15_0= Software_system_integration ) ) otherlv_16= EqualsSignGreaterThanSign ( (lv_value_17_0= ruleCostValue ) ) ) | ( ( (lv_type_18_0= Testing ) ) otherlv_19= EqualsSignGreaterThanSign ( (lv_value_20_0= ruleCostValue ) ) ) | ( ( (lv_type_21_0= Understanding_relevant_domains ) ) otherlv_22= EqualsSignGreaterThanSign ( (lv_value_23_0= ruleCostValue ) ) ) | ( ( (lv_type_24_0= Using_externally_available_software ) ) otherlv_25= EqualsSignGreaterThanSign ( (lv_value_26_0= ruleCostValue ) ) ) | ( ( (lv_type_27_0= Configuration_management ) ) otherlv_28= EqualsSignGreaterThanSign ( (lv_value_29_0= ruleCostValue ) ) ) | ( ( (lv_type_30_0= Commission_analysis ) ) otherlv_31= EqualsSignGreaterThanSign ( (lv_value_32_0= ruleCostValue ) ) ) | ( ( (lv_type_33_0= Measurement_tracking ) ) otherlv_34= EqualsSignGreaterThanSign ( (lv_value_35_0= ruleCostValue ) ) ) | ( ( (lv_type_36_0= Process_discipline ) ) otherlv_37= EqualsSignGreaterThanSign ( (lv_value_38_0= ruleCostValue ) ) ) | ( ( (lv_type_39_0= Scoping ) ) otherlv_40= EqualsSignGreaterThanSign ( (lv_value_41_0= ruleCostValue ) ) ) | ( ( (lv_type_42_0= Technical_planning ) ) otherlv_43= EqualsSignGreaterThanSign ( (lv_value_44_0= ruleCostValue ) ) ) | ( ( (lv_type_45_0= Technical_risk_management ) ) otherlv_46= EqualsSignGreaterThanSign ( (lv_value_47_0= ruleCostValue ) ) ) | ( ( (lv_type_48_0= Tool_support ) ) otherlv_49= EqualsSignGreaterThanSign ( (lv_value_50_0= ruleCostValue ) ) ) | ( ( (lv_type_51_0= Building_business_case ) ) otherlv_52= EqualsSignGreaterThanSign ( (lv_value_53_0= ruleCostValue ) ) ) | ( ( (lv_type_54_0= Customer_interface_management ) ) otherlv_55= EqualsSignGreaterThanSign ( (lv_value_56_0= ruleCostValue ) ) ) | ( ( (lv_type_57_0= Developing_acquisition_strategy ) ) otherlv_58= EqualsSignGreaterThanSign ( (lv_value_59_0= ruleCostValue ) ) ) | ( ( (lv_type_60_0= Funding ) ) otherlv_61= EqualsSignGreaterThanSign ( (lv_value_62_0= ruleCostValue ) ) ) | ( ( (lv_type_63_0= Launching_institutionalizing ) ) otherlv_64= EqualsSignGreaterThanSign ( (lv_value_65_0= ruleCostValue ) ) ) | ( ( (lv_type_66_0= Market_analysis ) ) otherlv_67= EqualsSignGreaterThanSign ( (lv_value_68_0= ruleCostValue ) ) ) | ( ( (lv_type_69_0= Operations ) ) otherlv_70= EqualsSignGreaterThanSign ( (lv_value_71_0= ruleCostValue ) ) ) | ( ( (lv_type_72_0= Organizational_planning ) ) otherlv_73= EqualsSignGreaterThanSign ( (lv_value_74_0= ruleCostValue ) ) ) | ( ( (lv_type_75_0= Organizational_risk_management ) ) otherlv_76= EqualsSignGreaterThanSign ( (lv_value_77_0= ruleCostValue ) ) ) | ( ( (lv_type_78_0= Structuring_organization ) ) otherlv_79= EqualsSignGreaterThanSign ( (lv_value_80_0= ruleCostValue ) ) ) | ( ( (lv_type_81_0= Technology_forecasting ) ) otherlv_82= EqualsSignGreaterThanSign ( (lv_value_83_0= ruleCostValue ) ) ) | ( ( (lv_type_84_0= Training ) ) otherlv_85= EqualsSignGreaterThanSign ( (lv_value_86_0= ruleCostValue ) ) ) | ( ( (lv_type_87_0= RULE_ID ) ) otherlv_88= EqualsSignGreaterThanSign ( (lv_value_89_0= ruleCostValue ) ) ) ) ;
    public final EObject ruleCostStatement() throws RecognitionException {
        EObject current = null;

        Token lv_type_0_0=null;
        Token otherlv_1=null;
        Token lv_type_3_0=null;
        Token otherlv_4=null;
        Token lv_type_6_0=null;
        Token otherlv_7=null;
        Token lv_type_9_0=null;
        Token otherlv_10=null;
        Token lv_type_12_0=null;
        Token otherlv_13=null;
        Token lv_type_15_0=null;
        Token otherlv_16=null;
        Token lv_type_18_0=null;
        Token otherlv_19=null;
        Token lv_type_21_0=null;
        Token otherlv_22=null;
        Token lv_type_24_0=null;
        Token otherlv_25=null;
        Token lv_type_27_0=null;
        Token otherlv_28=null;
        Token lv_type_30_0=null;
        Token otherlv_31=null;
        Token lv_type_33_0=null;
        Token otherlv_34=null;
        Token lv_type_36_0=null;
        Token otherlv_37=null;
        Token lv_type_39_0=null;
        Token otherlv_40=null;
        Token lv_type_42_0=null;
        Token otherlv_43=null;
        Token lv_type_45_0=null;
        Token otherlv_46=null;
        Token lv_type_48_0=null;
        Token otherlv_49=null;
        Token lv_type_51_0=null;
        Token otherlv_52=null;
        Token lv_type_54_0=null;
        Token otherlv_55=null;
        Token lv_type_57_0=null;
        Token otherlv_58=null;
        Token lv_type_60_0=null;
        Token otherlv_61=null;
        Token lv_type_63_0=null;
        Token otherlv_64=null;
        Token lv_type_66_0=null;
        Token otherlv_67=null;
        Token lv_type_69_0=null;
        Token otherlv_70=null;
        Token lv_type_72_0=null;
        Token otherlv_73=null;
        Token lv_type_75_0=null;
        Token otherlv_76=null;
        Token lv_type_78_0=null;
        Token otherlv_79=null;
        Token lv_type_81_0=null;
        Token otherlv_82=null;
        Token lv_type_84_0=null;
        Token otherlv_85=null;
        Token lv_type_87_0=null;
        Token otherlv_88=null;
        EObject lv_value_2_0 = null;

        EObject lv_value_5_0 = null;

        EObject lv_value_8_0 = null;

        EObject lv_value_11_0 = null;

        EObject lv_value_14_0 = null;

        EObject lv_value_17_0 = null;

        EObject lv_value_20_0 = null;

        EObject lv_value_23_0 = null;

        EObject lv_value_26_0 = null;

        EObject lv_value_29_0 = null;

        EObject lv_value_32_0 = null;

        EObject lv_value_35_0 = null;

        EObject lv_value_38_0 = null;

        EObject lv_value_41_0 = null;

        EObject lv_value_44_0 = null;

        EObject lv_value_47_0 = null;

        EObject lv_value_50_0 = null;

        EObject lv_value_53_0 = null;

        EObject lv_value_56_0 = null;

        EObject lv_value_59_0 = null;

        EObject lv_value_62_0 = null;

        EObject lv_value_65_0 = null;

        EObject lv_value_68_0 = null;

        EObject lv_value_71_0 = null;

        EObject lv_value_74_0 = null;

        EObject lv_value_77_0 = null;

        EObject lv_value_80_0 = null;

        EObject lv_value_83_0 = null;

        EObject lv_value_86_0 = null;

        EObject lv_value_89_0 = null;


         enterRule(); 
            
        try {
            // InternalSimdaseParser.g:1040:28: ( ( ( ( (lv_type_0_0= Architecture_definition ) ) otherlv_1= EqualsSignGreaterThanSign ( (lv_value_2_0= ruleCostValue ) ) ) | ( ( (lv_type_3_0= Architecture_evaluation ) ) otherlv_4= EqualsSignGreaterThanSign ( (lv_value_5_0= ruleCostValue ) ) ) | ( ( (lv_type_6_0= Component_development ) ) otherlv_7= EqualsSignGreaterThanSign ( (lv_value_8_0= ruleCostValue ) ) ) | ( ( (lv_type_9_0= Mining_existing_assets ) ) otherlv_10= EqualsSignGreaterThanSign ( (lv_value_11_0= ruleCostValue ) ) ) | ( ( (lv_type_12_0= Requirements_engineering ) ) otherlv_13= EqualsSignGreaterThanSign ( (lv_value_14_0= ruleCostValue ) ) ) | ( ( (lv_type_15_0= Software_system_integration ) ) otherlv_16= EqualsSignGreaterThanSign ( (lv_value_17_0= ruleCostValue ) ) ) | ( ( (lv_type_18_0= Testing ) ) otherlv_19= EqualsSignGreaterThanSign ( (lv_value_20_0= ruleCostValue ) ) ) | ( ( (lv_type_21_0= Understanding_relevant_domains ) ) otherlv_22= EqualsSignGreaterThanSign ( (lv_value_23_0= ruleCostValue ) ) ) | ( ( (lv_type_24_0= Using_externally_available_software ) ) otherlv_25= EqualsSignGreaterThanSign ( (lv_value_26_0= ruleCostValue ) ) ) | ( ( (lv_type_27_0= Configuration_management ) ) otherlv_28= EqualsSignGreaterThanSign ( (lv_value_29_0= ruleCostValue ) ) ) | ( ( (lv_type_30_0= Commission_analysis ) ) otherlv_31= EqualsSignGreaterThanSign ( (lv_value_32_0= ruleCostValue ) ) ) | ( ( (lv_type_33_0= Measurement_tracking ) ) otherlv_34= EqualsSignGreaterThanSign ( (lv_value_35_0= ruleCostValue ) ) ) | ( ( (lv_type_36_0= Process_discipline ) ) otherlv_37= EqualsSignGreaterThanSign ( (lv_value_38_0= ruleCostValue ) ) ) | ( ( (lv_type_39_0= Scoping ) ) otherlv_40= EqualsSignGreaterThanSign ( (lv_value_41_0= ruleCostValue ) ) ) | ( ( (lv_type_42_0= Technical_planning ) ) otherlv_43= EqualsSignGreaterThanSign ( (lv_value_44_0= ruleCostValue ) ) ) | ( ( (lv_type_45_0= Technical_risk_management ) ) otherlv_46= EqualsSignGreaterThanSign ( (lv_value_47_0= ruleCostValue ) ) ) | ( ( (lv_type_48_0= Tool_support ) ) otherlv_49= EqualsSignGreaterThanSign ( (lv_value_50_0= ruleCostValue ) ) ) | ( ( (lv_type_51_0= Building_business_case ) ) otherlv_52= EqualsSignGreaterThanSign ( (lv_value_53_0= ruleCostValue ) ) ) | ( ( (lv_type_54_0= Customer_interface_management ) ) otherlv_55= EqualsSignGreaterThanSign ( (lv_value_56_0= ruleCostValue ) ) ) | ( ( (lv_type_57_0= Developing_acquisition_strategy ) ) otherlv_58= EqualsSignGreaterThanSign ( (lv_value_59_0= ruleCostValue ) ) ) | ( ( (lv_type_60_0= Funding ) ) otherlv_61= EqualsSignGreaterThanSign ( (lv_value_62_0= ruleCostValue ) ) ) | ( ( (lv_type_63_0= Launching_institutionalizing ) ) otherlv_64= EqualsSignGreaterThanSign ( (lv_value_65_0= ruleCostValue ) ) ) | ( ( (lv_type_66_0= Market_analysis ) ) otherlv_67= EqualsSignGreaterThanSign ( (lv_value_68_0= ruleCostValue ) ) ) | ( ( (lv_type_69_0= Operations ) ) otherlv_70= EqualsSignGreaterThanSign ( (lv_value_71_0= ruleCostValue ) ) ) | ( ( (lv_type_72_0= Organizational_planning ) ) otherlv_73= EqualsSignGreaterThanSign ( (lv_value_74_0= ruleCostValue ) ) ) | ( ( (lv_type_75_0= Organizational_risk_management ) ) otherlv_76= EqualsSignGreaterThanSign ( (lv_value_77_0= ruleCostValue ) ) ) | ( ( (lv_type_78_0= Structuring_organization ) ) otherlv_79= EqualsSignGreaterThanSign ( (lv_value_80_0= ruleCostValue ) ) ) | ( ( (lv_type_81_0= Technology_forecasting ) ) otherlv_82= EqualsSignGreaterThanSign ( (lv_value_83_0= ruleCostValue ) ) ) | ( ( (lv_type_84_0= Training ) ) otherlv_85= EqualsSignGreaterThanSign ( (lv_value_86_0= ruleCostValue ) ) ) | ( ( (lv_type_87_0= RULE_ID ) ) otherlv_88= EqualsSignGreaterThanSign ( (lv_value_89_0= ruleCostValue ) ) ) ) )
            // InternalSimdaseParser.g:1041:1: ( ( ( (lv_type_0_0= Architecture_definition ) ) otherlv_1= EqualsSignGreaterThanSign ( (lv_value_2_0= ruleCostValue ) ) ) | ( ( (lv_type_3_0= Architecture_evaluation ) ) otherlv_4= EqualsSignGreaterThanSign ( (lv_value_5_0= ruleCostValue ) ) ) | ( ( (lv_type_6_0= Component_development ) ) otherlv_7= EqualsSignGreaterThanSign ( (lv_value_8_0= ruleCostValue ) ) ) | ( ( (lv_type_9_0= Mining_existing_assets ) ) otherlv_10= EqualsSignGreaterThanSign ( (lv_value_11_0= ruleCostValue ) ) ) | ( ( (lv_type_12_0= Requirements_engineering ) ) otherlv_13= EqualsSignGreaterThanSign ( (lv_value_14_0= ruleCostValue ) ) ) | ( ( (lv_type_15_0= Software_system_integration ) ) otherlv_16= EqualsSignGreaterThanSign ( (lv_value_17_0= ruleCostValue ) ) ) | ( ( (lv_type_18_0= Testing ) ) otherlv_19= EqualsSignGreaterThanSign ( (lv_value_20_0= ruleCostValue ) ) ) | ( ( (lv_type_21_0= Understanding_relevant_domains ) ) otherlv_22= EqualsSignGreaterThanSign ( (lv_value_23_0= ruleCostValue ) ) ) | ( ( (lv_type_24_0= Using_externally_available_software ) ) otherlv_25= EqualsSignGreaterThanSign ( (lv_value_26_0= ruleCostValue ) ) ) | ( ( (lv_type_27_0= Configuration_management ) ) otherlv_28= EqualsSignGreaterThanSign ( (lv_value_29_0= ruleCostValue ) ) ) | ( ( (lv_type_30_0= Commission_analysis ) ) otherlv_31= EqualsSignGreaterThanSign ( (lv_value_32_0= ruleCostValue ) ) ) | ( ( (lv_type_33_0= Measurement_tracking ) ) otherlv_34= EqualsSignGreaterThanSign ( (lv_value_35_0= ruleCostValue ) ) ) | ( ( (lv_type_36_0= Process_discipline ) ) otherlv_37= EqualsSignGreaterThanSign ( (lv_value_38_0= ruleCostValue ) ) ) | ( ( (lv_type_39_0= Scoping ) ) otherlv_40= EqualsSignGreaterThanSign ( (lv_value_41_0= ruleCostValue ) ) ) | ( ( (lv_type_42_0= Technical_planning ) ) otherlv_43= EqualsSignGreaterThanSign ( (lv_value_44_0= ruleCostValue ) ) ) | ( ( (lv_type_45_0= Technical_risk_management ) ) otherlv_46= EqualsSignGreaterThanSign ( (lv_value_47_0= ruleCostValue ) ) ) | ( ( (lv_type_48_0= Tool_support ) ) otherlv_49= EqualsSignGreaterThanSign ( (lv_value_50_0= ruleCostValue ) ) ) | ( ( (lv_type_51_0= Building_business_case ) ) otherlv_52= EqualsSignGreaterThanSign ( (lv_value_53_0= ruleCostValue ) ) ) | ( ( (lv_type_54_0= Customer_interface_management ) ) otherlv_55= EqualsSignGreaterThanSign ( (lv_value_56_0= ruleCostValue ) ) ) | ( ( (lv_type_57_0= Developing_acquisition_strategy ) ) otherlv_58= EqualsSignGreaterThanSign ( (lv_value_59_0= ruleCostValue ) ) ) | ( ( (lv_type_60_0= Funding ) ) otherlv_61= EqualsSignGreaterThanSign ( (lv_value_62_0= ruleCostValue ) ) ) | ( ( (lv_type_63_0= Launching_institutionalizing ) ) otherlv_64= EqualsSignGreaterThanSign ( (lv_value_65_0= ruleCostValue ) ) ) | ( ( (lv_type_66_0= Market_analysis ) ) otherlv_67= EqualsSignGreaterThanSign ( (lv_value_68_0= ruleCostValue ) ) ) | ( ( (lv_type_69_0= Operations ) ) otherlv_70= EqualsSignGreaterThanSign ( (lv_value_71_0= ruleCostValue ) ) ) | ( ( (lv_type_72_0= Organizational_planning ) ) otherlv_73= EqualsSignGreaterThanSign ( (lv_value_74_0= ruleCostValue ) ) ) | ( ( (lv_type_75_0= Organizational_risk_management ) ) otherlv_76= EqualsSignGreaterThanSign ( (lv_value_77_0= ruleCostValue ) ) ) | ( ( (lv_type_78_0= Structuring_organization ) ) otherlv_79= EqualsSignGreaterThanSign ( (lv_value_80_0= ruleCostValue ) ) ) | ( ( (lv_type_81_0= Technology_forecasting ) ) otherlv_82= EqualsSignGreaterThanSign ( (lv_value_83_0= ruleCostValue ) ) ) | ( ( (lv_type_84_0= Training ) ) otherlv_85= EqualsSignGreaterThanSign ( (lv_value_86_0= ruleCostValue ) ) ) | ( ( (lv_type_87_0= RULE_ID ) ) otherlv_88= EqualsSignGreaterThanSign ( (lv_value_89_0= ruleCostValue ) ) ) )
            {
            // InternalSimdaseParser.g:1041:1: ( ( ( (lv_type_0_0= Architecture_definition ) ) otherlv_1= EqualsSignGreaterThanSign ( (lv_value_2_0= ruleCostValue ) ) ) | ( ( (lv_type_3_0= Architecture_evaluation ) ) otherlv_4= EqualsSignGreaterThanSign ( (lv_value_5_0= ruleCostValue ) ) ) | ( ( (lv_type_6_0= Component_development ) ) otherlv_7= EqualsSignGreaterThanSign ( (lv_value_8_0= ruleCostValue ) ) ) | ( ( (lv_type_9_0= Mining_existing_assets ) ) otherlv_10= EqualsSignGreaterThanSign ( (lv_value_11_0= ruleCostValue ) ) ) | ( ( (lv_type_12_0= Requirements_engineering ) ) otherlv_13= EqualsSignGreaterThanSign ( (lv_value_14_0= ruleCostValue ) ) ) | ( ( (lv_type_15_0= Software_system_integration ) ) otherlv_16= EqualsSignGreaterThanSign ( (lv_value_17_0= ruleCostValue ) ) ) | ( ( (lv_type_18_0= Testing ) ) otherlv_19= EqualsSignGreaterThanSign ( (lv_value_20_0= ruleCostValue ) ) ) | ( ( (lv_type_21_0= Understanding_relevant_domains ) ) otherlv_22= EqualsSignGreaterThanSign ( (lv_value_23_0= ruleCostValue ) ) ) | ( ( (lv_type_24_0= Using_externally_available_software ) ) otherlv_25= EqualsSignGreaterThanSign ( (lv_value_26_0= ruleCostValue ) ) ) | ( ( (lv_type_27_0= Configuration_management ) ) otherlv_28= EqualsSignGreaterThanSign ( (lv_value_29_0= ruleCostValue ) ) ) | ( ( (lv_type_30_0= Commission_analysis ) ) otherlv_31= EqualsSignGreaterThanSign ( (lv_value_32_0= ruleCostValue ) ) ) | ( ( (lv_type_33_0= Measurement_tracking ) ) otherlv_34= EqualsSignGreaterThanSign ( (lv_value_35_0= ruleCostValue ) ) ) | ( ( (lv_type_36_0= Process_discipline ) ) otherlv_37= EqualsSignGreaterThanSign ( (lv_value_38_0= ruleCostValue ) ) ) | ( ( (lv_type_39_0= Scoping ) ) otherlv_40= EqualsSignGreaterThanSign ( (lv_value_41_0= ruleCostValue ) ) ) | ( ( (lv_type_42_0= Technical_planning ) ) otherlv_43= EqualsSignGreaterThanSign ( (lv_value_44_0= ruleCostValue ) ) ) | ( ( (lv_type_45_0= Technical_risk_management ) ) otherlv_46= EqualsSignGreaterThanSign ( (lv_value_47_0= ruleCostValue ) ) ) | ( ( (lv_type_48_0= Tool_support ) ) otherlv_49= EqualsSignGreaterThanSign ( (lv_value_50_0= ruleCostValue ) ) ) | ( ( (lv_type_51_0= Building_business_case ) ) otherlv_52= EqualsSignGreaterThanSign ( (lv_value_53_0= ruleCostValue ) ) ) | ( ( (lv_type_54_0= Customer_interface_management ) ) otherlv_55= EqualsSignGreaterThanSign ( (lv_value_56_0= ruleCostValue ) ) ) | ( ( (lv_type_57_0= Developing_acquisition_strategy ) ) otherlv_58= EqualsSignGreaterThanSign ( (lv_value_59_0= ruleCostValue ) ) ) | ( ( (lv_type_60_0= Funding ) ) otherlv_61= EqualsSignGreaterThanSign ( (lv_value_62_0= ruleCostValue ) ) ) | ( ( (lv_type_63_0= Launching_institutionalizing ) ) otherlv_64= EqualsSignGreaterThanSign ( (lv_value_65_0= ruleCostValue ) ) ) | ( ( (lv_type_66_0= Market_analysis ) ) otherlv_67= EqualsSignGreaterThanSign ( (lv_value_68_0= ruleCostValue ) ) ) | ( ( (lv_type_69_0= Operations ) ) otherlv_70= EqualsSignGreaterThanSign ( (lv_value_71_0= ruleCostValue ) ) ) | ( ( (lv_type_72_0= Organizational_planning ) ) otherlv_73= EqualsSignGreaterThanSign ( (lv_value_74_0= ruleCostValue ) ) ) | ( ( (lv_type_75_0= Organizational_risk_management ) ) otherlv_76= EqualsSignGreaterThanSign ( (lv_value_77_0= ruleCostValue ) ) ) | ( ( (lv_type_78_0= Structuring_organization ) ) otherlv_79= EqualsSignGreaterThanSign ( (lv_value_80_0= ruleCostValue ) ) ) | ( ( (lv_type_81_0= Technology_forecasting ) ) otherlv_82= EqualsSignGreaterThanSign ( (lv_value_83_0= ruleCostValue ) ) ) | ( ( (lv_type_84_0= Training ) ) otherlv_85= EqualsSignGreaterThanSign ( (lv_value_86_0= ruleCostValue ) ) ) | ( ( (lv_type_87_0= RULE_ID ) ) otherlv_88= EqualsSignGreaterThanSign ( (lv_value_89_0= ruleCostValue ) ) ) )
            int alt12=30;
            switch ( input.LA(1) ) {
            case Architecture_definition:
                {
                alt12=1;
                }
                break;
            case Architecture_evaluation:
                {
                alt12=2;
                }
                break;
            case Component_development:
                {
                alt12=3;
                }
                break;
            case Mining_existing_assets:
                {
                alt12=4;
                }
                break;
            case Requirements_engineering:
                {
                alt12=5;
                }
                break;
            case Software_system_integration:
                {
                alt12=6;
                }
                break;
            case Testing:
                {
                alt12=7;
                }
                break;
            case Understanding_relevant_domains:
                {
                alt12=8;
                }
                break;
            case Using_externally_available_software:
                {
                alt12=9;
                }
                break;
            case Configuration_management:
                {
                alt12=10;
                }
                break;
            case Commission_analysis:
                {
                alt12=11;
                }
                break;
            case Measurement_tracking:
                {
                alt12=12;
                }
                break;
            case Process_discipline:
                {
                alt12=13;
                }
                break;
            case Scoping:
                {
                alt12=14;
                }
                break;
            case Technical_planning:
                {
                alt12=15;
                }
                break;
            case Technical_risk_management:
                {
                alt12=16;
                }
                break;
            case Tool_support:
                {
                alt12=17;
                }
                break;
            case Building_business_case:
                {
                alt12=18;
                }
                break;
            case Customer_interface_management:
                {
                alt12=19;
                }
                break;
            case Developing_acquisition_strategy:
                {
                alt12=20;
                }
                break;
            case Funding:
                {
                alt12=21;
                }
                break;
            case Launching_institutionalizing:
                {
                alt12=22;
                }
                break;
            case Market_analysis:
                {
                alt12=23;
                }
                break;
            case Operations:
                {
                alt12=24;
                }
                break;
            case Organizational_planning:
                {
                alt12=25;
                }
                break;
            case Organizational_risk_management:
                {
                alt12=26;
                }
                break;
            case Structuring_organization:
                {
                alt12=27;
                }
                break;
            case Technology_forecasting:
                {
                alt12=28;
                }
                break;
            case Training:
                {
                alt12=29;
                }
                break;
            case RULE_ID:
                {
                alt12=30;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 12, 0, input);

                throw nvae;
            }

            switch (alt12) {
                case 1 :
                    // InternalSimdaseParser.g:1041:2: ( ( (lv_type_0_0= Architecture_definition ) ) otherlv_1= EqualsSignGreaterThanSign ( (lv_value_2_0= ruleCostValue ) ) )
                    {
                    // InternalSimdaseParser.g:1041:2: ( ( (lv_type_0_0= Architecture_definition ) ) otherlv_1= EqualsSignGreaterThanSign ( (lv_value_2_0= ruleCostValue ) ) )
                    // InternalSimdaseParser.g:1041:3: ( (lv_type_0_0= Architecture_definition ) ) otherlv_1= EqualsSignGreaterThanSign ( (lv_value_2_0= ruleCostValue ) )
                    {
                    // InternalSimdaseParser.g:1041:3: ( (lv_type_0_0= Architecture_definition ) )
                    // InternalSimdaseParser.g:1042:1: (lv_type_0_0= Architecture_definition )
                    {
                    // InternalSimdaseParser.g:1042:1: (lv_type_0_0= Architecture_definition )
                    // InternalSimdaseParser.g:1043:3: lv_type_0_0= Architecture_definition
                    {
                    lv_type_0_0=(Token)match(input,Architecture_definition,FollowSets000.FOLLOW_4); 

                            newLeafNode(lv_type_0_0, grammarAccess.getCostStatementAccess().getTypeArchitecture_definitionKeyword_0_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getCostStatementRule());
                    	        }
                           		setWithLastConsumed(current, "type", lv_type_0_0, "architecture_definition");
                    	    

                    }


                    }

                    otherlv_1=(Token)match(input,EqualsSignGreaterThanSign,FollowSets000.FOLLOW_9); 

                        	newLeafNode(otherlv_1, grammarAccess.getCostStatementAccess().getEqualsSignGreaterThanSignKeyword_0_1());
                        
                    // InternalSimdaseParser.g:1062:1: ( (lv_value_2_0= ruleCostValue ) )
                    // InternalSimdaseParser.g:1063:1: (lv_value_2_0= ruleCostValue )
                    {
                    // InternalSimdaseParser.g:1063:1: (lv_value_2_0= ruleCostValue )
                    // InternalSimdaseParser.g:1064:3: lv_value_2_0= ruleCostValue
                    {
                     
                    	        newCompositeNode(grammarAccess.getCostStatementAccess().getValueCostValueParserRuleCall_0_2_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_value_2_0=ruleCostValue();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getCostStatementRule());
                    	        }
                           		set(
                           			current, 
                           			"value",
                            		lv_value_2_0, 
                            		"edu.clemson.simdase.Simdase.CostValue");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalSimdaseParser.g:1081:6: ( ( (lv_type_3_0= Architecture_evaluation ) ) otherlv_4= EqualsSignGreaterThanSign ( (lv_value_5_0= ruleCostValue ) ) )
                    {
                    // InternalSimdaseParser.g:1081:6: ( ( (lv_type_3_0= Architecture_evaluation ) ) otherlv_4= EqualsSignGreaterThanSign ( (lv_value_5_0= ruleCostValue ) ) )
                    // InternalSimdaseParser.g:1081:7: ( (lv_type_3_0= Architecture_evaluation ) ) otherlv_4= EqualsSignGreaterThanSign ( (lv_value_5_0= ruleCostValue ) )
                    {
                    // InternalSimdaseParser.g:1081:7: ( (lv_type_3_0= Architecture_evaluation ) )
                    // InternalSimdaseParser.g:1082:1: (lv_type_3_0= Architecture_evaluation )
                    {
                    // InternalSimdaseParser.g:1082:1: (lv_type_3_0= Architecture_evaluation )
                    // InternalSimdaseParser.g:1083:3: lv_type_3_0= Architecture_evaluation
                    {
                    lv_type_3_0=(Token)match(input,Architecture_evaluation,FollowSets000.FOLLOW_4); 

                            newLeafNode(lv_type_3_0, grammarAccess.getCostStatementAccess().getTypeArchitecture_evaluationKeyword_1_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getCostStatementRule());
                    	        }
                           		setWithLastConsumed(current, "type", lv_type_3_0, "architecture_evaluation");
                    	    

                    }


                    }

                    otherlv_4=(Token)match(input,EqualsSignGreaterThanSign,FollowSets000.FOLLOW_9); 

                        	newLeafNode(otherlv_4, grammarAccess.getCostStatementAccess().getEqualsSignGreaterThanSignKeyword_1_1());
                        
                    // InternalSimdaseParser.g:1102:1: ( (lv_value_5_0= ruleCostValue ) )
                    // InternalSimdaseParser.g:1103:1: (lv_value_5_0= ruleCostValue )
                    {
                    // InternalSimdaseParser.g:1103:1: (lv_value_5_0= ruleCostValue )
                    // InternalSimdaseParser.g:1104:3: lv_value_5_0= ruleCostValue
                    {
                     
                    	        newCompositeNode(grammarAccess.getCostStatementAccess().getValueCostValueParserRuleCall_1_2_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_value_5_0=ruleCostValue();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getCostStatementRule());
                    	        }
                           		set(
                           			current, 
                           			"value",
                            		lv_value_5_0, 
                            		"edu.clemson.simdase.Simdase.CostValue");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 3 :
                    // InternalSimdaseParser.g:1121:6: ( ( (lv_type_6_0= Component_development ) ) otherlv_7= EqualsSignGreaterThanSign ( (lv_value_8_0= ruleCostValue ) ) )
                    {
                    // InternalSimdaseParser.g:1121:6: ( ( (lv_type_6_0= Component_development ) ) otherlv_7= EqualsSignGreaterThanSign ( (lv_value_8_0= ruleCostValue ) ) )
                    // InternalSimdaseParser.g:1121:7: ( (lv_type_6_0= Component_development ) ) otherlv_7= EqualsSignGreaterThanSign ( (lv_value_8_0= ruleCostValue ) )
                    {
                    // InternalSimdaseParser.g:1121:7: ( (lv_type_6_0= Component_development ) )
                    // InternalSimdaseParser.g:1122:1: (lv_type_6_0= Component_development )
                    {
                    // InternalSimdaseParser.g:1122:1: (lv_type_6_0= Component_development )
                    // InternalSimdaseParser.g:1123:3: lv_type_6_0= Component_development
                    {
                    lv_type_6_0=(Token)match(input,Component_development,FollowSets000.FOLLOW_4); 

                            newLeafNode(lv_type_6_0, grammarAccess.getCostStatementAccess().getTypeComponent_developmentKeyword_2_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getCostStatementRule());
                    	        }
                           		setWithLastConsumed(current, "type", lv_type_6_0, "component_development");
                    	    

                    }


                    }

                    otherlv_7=(Token)match(input,EqualsSignGreaterThanSign,FollowSets000.FOLLOW_9); 

                        	newLeafNode(otherlv_7, grammarAccess.getCostStatementAccess().getEqualsSignGreaterThanSignKeyword_2_1());
                        
                    // InternalSimdaseParser.g:1142:1: ( (lv_value_8_0= ruleCostValue ) )
                    // InternalSimdaseParser.g:1143:1: (lv_value_8_0= ruleCostValue )
                    {
                    // InternalSimdaseParser.g:1143:1: (lv_value_8_0= ruleCostValue )
                    // InternalSimdaseParser.g:1144:3: lv_value_8_0= ruleCostValue
                    {
                     
                    	        newCompositeNode(grammarAccess.getCostStatementAccess().getValueCostValueParserRuleCall_2_2_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_value_8_0=ruleCostValue();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getCostStatementRule());
                    	        }
                           		set(
                           			current, 
                           			"value",
                            		lv_value_8_0, 
                            		"edu.clemson.simdase.Simdase.CostValue");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 4 :
                    // InternalSimdaseParser.g:1161:6: ( ( (lv_type_9_0= Mining_existing_assets ) ) otherlv_10= EqualsSignGreaterThanSign ( (lv_value_11_0= ruleCostValue ) ) )
                    {
                    // InternalSimdaseParser.g:1161:6: ( ( (lv_type_9_0= Mining_existing_assets ) ) otherlv_10= EqualsSignGreaterThanSign ( (lv_value_11_0= ruleCostValue ) ) )
                    // InternalSimdaseParser.g:1161:7: ( (lv_type_9_0= Mining_existing_assets ) ) otherlv_10= EqualsSignGreaterThanSign ( (lv_value_11_0= ruleCostValue ) )
                    {
                    // InternalSimdaseParser.g:1161:7: ( (lv_type_9_0= Mining_existing_assets ) )
                    // InternalSimdaseParser.g:1162:1: (lv_type_9_0= Mining_existing_assets )
                    {
                    // InternalSimdaseParser.g:1162:1: (lv_type_9_0= Mining_existing_assets )
                    // InternalSimdaseParser.g:1163:3: lv_type_9_0= Mining_existing_assets
                    {
                    lv_type_9_0=(Token)match(input,Mining_existing_assets,FollowSets000.FOLLOW_4); 

                            newLeafNode(lv_type_9_0, grammarAccess.getCostStatementAccess().getTypeMining_existing_assetsKeyword_3_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getCostStatementRule());
                    	        }
                           		setWithLastConsumed(current, "type", lv_type_9_0, "mining_existing_assets");
                    	    

                    }


                    }

                    otherlv_10=(Token)match(input,EqualsSignGreaterThanSign,FollowSets000.FOLLOW_9); 

                        	newLeafNode(otherlv_10, grammarAccess.getCostStatementAccess().getEqualsSignGreaterThanSignKeyword_3_1());
                        
                    // InternalSimdaseParser.g:1182:1: ( (lv_value_11_0= ruleCostValue ) )
                    // InternalSimdaseParser.g:1183:1: (lv_value_11_0= ruleCostValue )
                    {
                    // InternalSimdaseParser.g:1183:1: (lv_value_11_0= ruleCostValue )
                    // InternalSimdaseParser.g:1184:3: lv_value_11_0= ruleCostValue
                    {
                     
                    	        newCompositeNode(grammarAccess.getCostStatementAccess().getValueCostValueParserRuleCall_3_2_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_value_11_0=ruleCostValue();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getCostStatementRule());
                    	        }
                           		set(
                           			current, 
                           			"value",
                            		lv_value_11_0, 
                            		"edu.clemson.simdase.Simdase.CostValue");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 5 :
                    // InternalSimdaseParser.g:1201:6: ( ( (lv_type_12_0= Requirements_engineering ) ) otherlv_13= EqualsSignGreaterThanSign ( (lv_value_14_0= ruleCostValue ) ) )
                    {
                    // InternalSimdaseParser.g:1201:6: ( ( (lv_type_12_0= Requirements_engineering ) ) otherlv_13= EqualsSignGreaterThanSign ( (lv_value_14_0= ruleCostValue ) ) )
                    // InternalSimdaseParser.g:1201:7: ( (lv_type_12_0= Requirements_engineering ) ) otherlv_13= EqualsSignGreaterThanSign ( (lv_value_14_0= ruleCostValue ) )
                    {
                    // InternalSimdaseParser.g:1201:7: ( (lv_type_12_0= Requirements_engineering ) )
                    // InternalSimdaseParser.g:1202:1: (lv_type_12_0= Requirements_engineering )
                    {
                    // InternalSimdaseParser.g:1202:1: (lv_type_12_0= Requirements_engineering )
                    // InternalSimdaseParser.g:1203:3: lv_type_12_0= Requirements_engineering
                    {
                    lv_type_12_0=(Token)match(input,Requirements_engineering,FollowSets000.FOLLOW_4); 

                            newLeafNode(lv_type_12_0, grammarAccess.getCostStatementAccess().getTypeRequirements_engineeringKeyword_4_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getCostStatementRule());
                    	        }
                           		setWithLastConsumed(current, "type", lv_type_12_0, "requirements_engineering");
                    	    

                    }


                    }

                    otherlv_13=(Token)match(input,EqualsSignGreaterThanSign,FollowSets000.FOLLOW_9); 

                        	newLeafNode(otherlv_13, grammarAccess.getCostStatementAccess().getEqualsSignGreaterThanSignKeyword_4_1());
                        
                    // InternalSimdaseParser.g:1222:1: ( (lv_value_14_0= ruleCostValue ) )
                    // InternalSimdaseParser.g:1223:1: (lv_value_14_0= ruleCostValue )
                    {
                    // InternalSimdaseParser.g:1223:1: (lv_value_14_0= ruleCostValue )
                    // InternalSimdaseParser.g:1224:3: lv_value_14_0= ruleCostValue
                    {
                     
                    	        newCompositeNode(grammarAccess.getCostStatementAccess().getValueCostValueParserRuleCall_4_2_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_value_14_0=ruleCostValue();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getCostStatementRule());
                    	        }
                           		set(
                           			current, 
                           			"value",
                            		lv_value_14_0, 
                            		"edu.clemson.simdase.Simdase.CostValue");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 6 :
                    // InternalSimdaseParser.g:1241:6: ( ( (lv_type_15_0= Software_system_integration ) ) otherlv_16= EqualsSignGreaterThanSign ( (lv_value_17_0= ruleCostValue ) ) )
                    {
                    // InternalSimdaseParser.g:1241:6: ( ( (lv_type_15_0= Software_system_integration ) ) otherlv_16= EqualsSignGreaterThanSign ( (lv_value_17_0= ruleCostValue ) ) )
                    // InternalSimdaseParser.g:1241:7: ( (lv_type_15_0= Software_system_integration ) ) otherlv_16= EqualsSignGreaterThanSign ( (lv_value_17_0= ruleCostValue ) )
                    {
                    // InternalSimdaseParser.g:1241:7: ( (lv_type_15_0= Software_system_integration ) )
                    // InternalSimdaseParser.g:1242:1: (lv_type_15_0= Software_system_integration )
                    {
                    // InternalSimdaseParser.g:1242:1: (lv_type_15_0= Software_system_integration )
                    // InternalSimdaseParser.g:1243:3: lv_type_15_0= Software_system_integration
                    {
                    lv_type_15_0=(Token)match(input,Software_system_integration,FollowSets000.FOLLOW_4); 

                            newLeafNode(lv_type_15_0, grammarAccess.getCostStatementAccess().getTypeSoftware_system_integrationKeyword_5_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getCostStatementRule());
                    	        }
                           		setWithLastConsumed(current, "type", lv_type_15_0, "software_system_integration");
                    	    

                    }


                    }

                    otherlv_16=(Token)match(input,EqualsSignGreaterThanSign,FollowSets000.FOLLOW_9); 

                        	newLeafNode(otherlv_16, grammarAccess.getCostStatementAccess().getEqualsSignGreaterThanSignKeyword_5_1());
                        
                    // InternalSimdaseParser.g:1262:1: ( (lv_value_17_0= ruleCostValue ) )
                    // InternalSimdaseParser.g:1263:1: (lv_value_17_0= ruleCostValue )
                    {
                    // InternalSimdaseParser.g:1263:1: (lv_value_17_0= ruleCostValue )
                    // InternalSimdaseParser.g:1264:3: lv_value_17_0= ruleCostValue
                    {
                     
                    	        newCompositeNode(grammarAccess.getCostStatementAccess().getValueCostValueParserRuleCall_5_2_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_value_17_0=ruleCostValue();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getCostStatementRule());
                    	        }
                           		set(
                           			current, 
                           			"value",
                            		lv_value_17_0, 
                            		"edu.clemson.simdase.Simdase.CostValue");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 7 :
                    // InternalSimdaseParser.g:1281:6: ( ( (lv_type_18_0= Testing ) ) otherlv_19= EqualsSignGreaterThanSign ( (lv_value_20_0= ruleCostValue ) ) )
                    {
                    // InternalSimdaseParser.g:1281:6: ( ( (lv_type_18_0= Testing ) ) otherlv_19= EqualsSignGreaterThanSign ( (lv_value_20_0= ruleCostValue ) ) )
                    // InternalSimdaseParser.g:1281:7: ( (lv_type_18_0= Testing ) ) otherlv_19= EqualsSignGreaterThanSign ( (lv_value_20_0= ruleCostValue ) )
                    {
                    // InternalSimdaseParser.g:1281:7: ( (lv_type_18_0= Testing ) )
                    // InternalSimdaseParser.g:1282:1: (lv_type_18_0= Testing )
                    {
                    // InternalSimdaseParser.g:1282:1: (lv_type_18_0= Testing )
                    // InternalSimdaseParser.g:1283:3: lv_type_18_0= Testing
                    {
                    lv_type_18_0=(Token)match(input,Testing,FollowSets000.FOLLOW_4); 

                            newLeafNode(lv_type_18_0, grammarAccess.getCostStatementAccess().getTypeTestingKeyword_6_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getCostStatementRule());
                    	        }
                           		setWithLastConsumed(current, "type", lv_type_18_0, "testing");
                    	    

                    }


                    }

                    otherlv_19=(Token)match(input,EqualsSignGreaterThanSign,FollowSets000.FOLLOW_9); 

                        	newLeafNode(otherlv_19, grammarAccess.getCostStatementAccess().getEqualsSignGreaterThanSignKeyword_6_1());
                        
                    // InternalSimdaseParser.g:1302:1: ( (lv_value_20_0= ruleCostValue ) )
                    // InternalSimdaseParser.g:1303:1: (lv_value_20_0= ruleCostValue )
                    {
                    // InternalSimdaseParser.g:1303:1: (lv_value_20_0= ruleCostValue )
                    // InternalSimdaseParser.g:1304:3: lv_value_20_0= ruleCostValue
                    {
                     
                    	        newCompositeNode(grammarAccess.getCostStatementAccess().getValueCostValueParserRuleCall_6_2_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_value_20_0=ruleCostValue();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getCostStatementRule());
                    	        }
                           		set(
                           			current, 
                           			"value",
                            		lv_value_20_0, 
                            		"edu.clemson.simdase.Simdase.CostValue");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 8 :
                    // InternalSimdaseParser.g:1321:6: ( ( (lv_type_21_0= Understanding_relevant_domains ) ) otherlv_22= EqualsSignGreaterThanSign ( (lv_value_23_0= ruleCostValue ) ) )
                    {
                    // InternalSimdaseParser.g:1321:6: ( ( (lv_type_21_0= Understanding_relevant_domains ) ) otherlv_22= EqualsSignGreaterThanSign ( (lv_value_23_0= ruleCostValue ) ) )
                    // InternalSimdaseParser.g:1321:7: ( (lv_type_21_0= Understanding_relevant_domains ) ) otherlv_22= EqualsSignGreaterThanSign ( (lv_value_23_0= ruleCostValue ) )
                    {
                    // InternalSimdaseParser.g:1321:7: ( (lv_type_21_0= Understanding_relevant_domains ) )
                    // InternalSimdaseParser.g:1322:1: (lv_type_21_0= Understanding_relevant_domains )
                    {
                    // InternalSimdaseParser.g:1322:1: (lv_type_21_0= Understanding_relevant_domains )
                    // InternalSimdaseParser.g:1323:3: lv_type_21_0= Understanding_relevant_domains
                    {
                    lv_type_21_0=(Token)match(input,Understanding_relevant_domains,FollowSets000.FOLLOW_4); 

                            newLeafNode(lv_type_21_0, grammarAccess.getCostStatementAccess().getTypeUnderstanding_relevant_domainsKeyword_7_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getCostStatementRule());
                    	        }
                           		setWithLastConsumed(current, "type", lv_type_21_0, "understanding_relevant_domains");
                    	    

                    }


                    }

                    otherlv_22=(Token)match(input,EqualsSignGreaterThanSign,FollowSets000.FOLLOW_9); 

                        	newLeafNode(otherlv_22, grammarAccess.getCostStatementAccess().getEqualsSignGreaterThanSignKeyword_7_1());
                        
                    // InternalSimdaseParser.g:1342:1: ( (lv_value_23_0= ruleCostValue ) )
                    // InternalSimdaseParser.g:1343:1: (lv_value_23_0= ruleCostValue )
                    {
                    // InternalSimdaseParser.g:1343:1: (lv_value_23_0= ruleCostValue )
                    // InternalSimdaseParser.g:1344:3: lv_value_23_0= ruleCostValue
                    {
                     
                    	        newCompositeNode(grammarAccess.getCostStatementAccess().getValueCostValueParserRuleCall_7_2_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_value_23_0=ruleCostValue();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getCostStatementRule());
                    	        }
                           		set(
                           			current, 
                           			"value",
                            		lv_value_23_0, 
                            		"edu.clemson.simdase.Simdase.CostValue");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 9 :
                    // InternalSimdaseParser.g:1361:6: ( ( (lv_type_24_0= Using_externally_available_software ) ) otherlv_25= EqualsSignGreaterThanSign ( (lv_value_26_0= ruleCostValue ) ) )
                    {
                    // InternalSimdaseParser.g:1361:6: ( ( (lv_type_24_0= Using_externally_available_software ) ) otherlv_25= EqualsSignGreaterThanSign ( (lv_value_26_0= ruleCostValue ) ) )
                    // InternalSimdaseParser.g:1361:7: ( (lv_type_24_0= Using_externally_available_software ) ) otherlv_25= EqualsSignGreaterThanSign ( (lv_value_26_0= ruleCostValue ) )
                    {
                    // InternalSimdaseParser.g:1361:7: ( (lv_type_24_0= Using_externally_available_software ) )
                    // InternalSimdaseParser.g:1362:1: (lv_type_24_0= Using_externally_available_software )
                    {
                    // InternalSimdaseParser.g:1362:1: (lv_type_24_0= Using_externally_available_software )
                    // InternalSimdaseParser.g:1363:3: lv_type_24_0= Using_externally_available_software
                    {
                    lv_type_24_0=(Token)match(input,Using_externally_available_software,FollowSets000.FOLLOW_4); 

                            newLeafNode(lv_type_24_0, grammarAccess.getCostStatementAccess().getTypeUsing_externally_available_softwareKeyword_8_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getCostStatementRule());
                    	        }
                           		setWithLastConsumed(current, "type", lv_type_24_0, "using_externally_available_software");
                    	    

                    }


                    }

                    otherlv_25=(Token)match(input,EqualsSignGreaterThanSign,FollowSets000.FOLLOW_9); 

                        	newLeafNode(otherlv_25, grammarAccess.getCostStatementAccess().getEqualsSignGreaterThanSignKeyword_8_1());
                        
                    // InternalSimdaseParser.g:1382:1: ( (lv_value_26_0= ruleCostValue ) )
                    // InternalSimdaseParser.g:1383:1: (lv_value_26_0= ruleCostValue )
                    {
                    // InternalSimdaseParser.g:1383:1: (lv_value_26_0= ruleCostValue )
                    // InternalSimdaseParser.g:1384:3: lv_value_26_0= ruleCostValue
                    {
                     
                    	        newCompositeNode(grammarAccess.getCostStatementAccess().getValueCostValueParserRuleCall_8_2_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_value_26_0=ruleCostValue();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getCostStatementRule());
                    	        }
                           		set(
                           			current, 
                           			"value",
                            		lv_value_26_0, 
                            		"edu.clemson.simdase.Simdase.CostValue");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 10 :
                    // InternalSimdaseParser.g:1401:6: ( ( (lv_type_27_0= Configuration_management ) ) otherlv_28= EqualsSignGreaterThanSign ( (lv_value_29_0= ruleCostValue ) ) )
                    {
                    // InternalSimdaseParser.g:1401:6: ( ( (lv_type_27_0= Configuration_management ) ) otherlv_28= EqualsSignGreaterThanSign ( (lv_value_29_0= ruleCostValue ) ) )
                    // InternalSimdaseParser.g:1401:7: ( (lv_type_27_0= Configuration_management ) ) otherlv_28= EqualsSignGreaterThanSign ( (lv_value_29_0= ruleCostValue ) )
                    {
                    // InternalSimdaseParser.g:1401:7: ( (lv_type_27_0= Configuration_management ) )
                    // InternalSimdaseParser.g:1402:1: (lv_type_27_0= Configuration_management )
                    {
                    // InternalSimdaseParser.g:1402:1: (lv_type_27_0= Configuration_management )
                    // InternalSimdaseParser.g:1403:3: lv_type_27_0= Configuration_management
                    {
                    lv_type_27_0=(Token)match(input,Configuration_management,FollowSets000.FOLLOW_4); 

                            newLeafNode(lv_type_27_0, grammarAccess.getCostStatementAccess().getTypeConfiguration_managementKeyword_9_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getCostStatementRule());
                    	        }
                           		setWithLastConsumed(current, "type", lv_type_27_0, "configuration_management");
                    	    

                    }


                    }

                    otherlv_28=(Token)match(input,EqualsSignGreaterThanSign,FollowSets000.FOLLOW_9); 

                        	newLeafNode(otherlv_28, grammarAccess.getCostStatementAccess().getEqualsSignGreaterThanSignKeyword_9_1());
                        
                    // InternalSimdaseParser.g:1422:1: ( (lv_value_29_0= ruleCostValue ) )
                    // InternalSimdaseParser.g:1423:1: (lv_value_29_0= ruleCostValue )
                    {
                    // InternalSimdaseParser.g:1423:1: (lv_value_29_0= ruleCostValue )
                    // InternalSimdaseParser.g:1424:3: lv_value_29_0= ruleCostValue
                    {
                     
                    	        newCompositeNode(grammarAccess.getCostStatementAccess().getValueCostValueParserRuleCall_9_2_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_value_29_0=ruleCostValue();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getCostStatementRule());
                    	        }
                           		set(
                           			current, 
                           			"value",
                            		lv_value_29_0, 
                            		"edu.clemson.simdase.Simdase.CostValue");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 11 :
                    // InternalSimdaseParser.g:1441:6: ( ( (lv_type_30_0= Commission_analysis ) ) otherlv_31= EqualsSignGreaterThanSign ( (lv_value_32_0= ruleCostValue ) ) )
                    {
                    // InternalSimdaseParser.g:1441:6: ( ( (lv_type_30_0= Commission_analysis ) ) otherlv_31= EqualsSignGreaterThanSign ( (lv_value_32_0= ruleCostValue ) ) )
                    // InternalSimdaseParser.g:1441:7: ( (lv_type_30_0= Commission_analysis ) ) otherlv_31= EqualsSignGreaterThanSign ( (lv_value_32_0= ruleCostValue ) )
                    {
                    // InternalSimdaseParser.g:1441:7: ( (lv_type_30_0= Commission_analysis ) )
                    // InternalSimdaseParser.g:1442:1: (lv_type_30_0= Commission_analysis )
                    {
                    // InternalSimdaseParser.g:1442:1: (lv_type_30_0= Commission_analysis )
                    // InternalSimdaseParser.g:1443:3: lv_type_30_0= Commission_analysis
                    {
                    lv_type_30_0=(Token)match(input,Commission_analysis,FollowSets000.FOLLOW_4); 

                            newLeafNode(lv_type_30_0, grammarAccess.getCostStatementAccess().getTypeCommission_analysisKeyword_10_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getCostStatementRule());
                    	        }
                           		setWithLastConsumed(current, "type", lv_type_30_0, "commission_analysis");
                    	    

                    }


                    }

                    otherlv_31=(Token)match(input,EqualsSignGreaterThanSign,FollowSets000.FOLLOW_9); 

                        	newLeafNode(otherlv_31, grammarAccess.getCostStatementAccess().getEqualsSignGreaterThanSignKeyword_10_1());
                        
                    // InternalSimdaseParser.g:1462:1: ( (lv_value_32_0= ruleCostValue ) )
                    // InternalSimdaseParser.g:1463:1: (lv_value_32_0= ruleCostValue )
                    {
                    // InternalSimdaseParser.g:1463:1: (lv_value_32_0= ruleCostValue )
                    // InternalSimdaseParser.g:1464:3: lv_value_32_0= ruleCostValue
                    {
                     
                    	        newCompositeNode(grammarAccess.getCostStatementAccess().getValueCostValueParserRuleCall_10_2_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_value_32_0=ruleCostValue();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getCostStatementRule());
                    	        }
                           		set(
                           			current, 
                           			"value",
                            		lv_value_32_0, 
                            		"edu.clemson.simdase.Simdase.CostValue");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 12 :
                    // InternalSimdaseParser.g:1481:6: ( ( (lv_type_33_0= Measurement_tracking ) ) otherlv_34= EqualsSignGreaterThanSign ( (lv_value_35_0= ruleCostValue ) ) )
                    {
                    // InternalSimdaseParser.g:1481:6: ( ( (lv_type_33_0= Measurement_tracking ) ) otherlv_34= EqualsSignGreaterThanSign ( (lv_value_35_0= ruleCostValue ) ) )
                    // InternalSimdaseParser.g:1481:7: ( (lv_type_33_0= Measurement_tracking ) ) otherlv_34= EqualsSignGreaterThanSign ( (lv_value_35_0= ruleCostValue ) )
                    {
                    // InternalSimdaseParser.g:1481:7: ( (lv_type_33_0= Measurement_tracking ) )
                    // InternalSimdaseParser.g:1482:1: (lv_type_33_0= Measurement_tracking )
                    {
                    // InternalSimdaseParser.g:1482:1: (lv_type_33_0= Measurement_tracking )
                    // InternalSimdaseParser.g:1483:3: lv_type_33_0= Measurement_tracking
                    {
                    lv_type_33_0=(Token)match(input,Measurement_tracking,FollowSets000.FOLLOW_4); 

                            newLeafNode(lv_type_33_0, grammarAccess.getCostStatementAccess().getTypeMeasurement_trackingKeyword_11_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getCostStatementRule());
                    	        }
                           		setWithLastConsumed(current, "type", lv_type_33_0, "measurement_tracking");
                    	    

                    }


                    }

                    otherlv_34=(Token)match(input,EqualsSignGreaterThanSign,FollowSets000.FOLLOW_9); 

                        	newLeafNode(otherlv_34, grammarAccess.getCostStatementAccess().getEqualsSignGreaterThanSignKeyword_11_1());
                        
                    // InternalSimdaseParser.g:1502:1: ( (lv_value_35_0= ruleCostValue ) )
                    // InternalSimdaseParser.g:1503:1: (lv_value_35_0= ruleCostValue )
                    {
                    // InternalSimdaseParser.g:1503:1: (lv_value_35_0= ruleCostValue )
                    // InternalSimdaseParser.g:1504:3: lv_value_35_0= ruleCostValue
                    {
                     
                    	        newCompositeNode(grammarAccess.getCostStatementAccess().getValueCostValueParserRuleCall_11_2_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_value_35_0=ruleCostValue();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getCostStatementRule());
                    	        }
                           		set(
                           			current, 
                           			"value",
                            		lv_value_35_0, 
                            		"edu.clemson.simdase.Simdase.CostValue");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 13 :
                    // InternalSimdaseParser.g:1521:6: ( ( (lv_type_36_0= Process_discipline ) ) otherlv_37= EqualsSignGreaterThanSign ( (lv_value_38_0= ruleCostValue ) ) )
                    {
                    // InternalSimdaseParser.g:1521:6: ( ( (lv_type_36_0= Process_discipline ) ) otherlv_37= EqualsSignGreaterThanSign ( (lv_value_38_0= ruleCostValue ) ) )
                    // InternalSimdaseParser.g:1521:7: ( (lv_type_36_0= Process_discipline ) ) otherlv_37= EqualsSignGreaterThanSign ( (lv_value_38_0= ruleCostValue ) )
                    {
                    // InternalSimdaseParser.g:1521:7: ( (lv_type_36_0= Process_discipline ) )
                    // InternalSimdaseParser.g:1522:1: (lv_type_36_0= Process_discipline )
                    {
                    // InternalSimdaseParser.g:1522:1: (lv_type_36_0= Process_discipline )
                    // InternalSimdaseParser.g:1523:3: lv_type_36_0= Process_discipline
                    {
                    lv_type_36_0=(Token)match(input,Process_discipline,FollowSets000.FOLLOW_4); 

                            newLeafNode(lv_type_36_0, grammarAccess.getCostStatementAccess().getTypeProcess_disciplineKeyword_12_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getCostStatementRule());
                    	        }
                           		setWithLastConsumed(current, "type", lv_type_36_0, "process_discipline");
                    	    

                    }


                    }

                    otherlv_37=(Token)match(input,EqualsSignGreaterThanSign,FollowSets000.FOLLOW_9); 

                        	newLeafNode(otherlv_37, grammarAccess.getCostStatementAccess().getEqualsSignGreaterThanSignKeyword_12_1());
                        
                    // InternalSimdaseParser.g:1542:1: ( (lv_value_38_0= ruleCostValue ) )
                    // InternalSimdaseParser.g:1543:1: (lv_value_38_0= ruleCostValue )
                    {
                    // InternalSimdaseParser.g:1543:1: (lv_value_38_0= ruleCostValue )
                    // InternalSimdaseParser.g:1544:3: lv_value_38_0= ruleCostValue
                    {
                     
                    	        newCompositeNode(grammarAccess.getCostStatementAccess().getValueCostValueParserRuleCall_12_2_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_value_38_0=ruleCostValue();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getCostStatementRule());
                    	        }
                           		set(
                           			current, 
                           			"value",
                            		lv_value_38_0, 
                            		"edu.clemson.simdase.Simdase.CostValue");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 14 :
                    // InternalSimdaseParser.g:1561:6: ( ( (lv_type_39_0= Scoping ) ) otherlv_40= EqualsSignGreaterThanSign ( (lv_value_41_0= ruleCostValue ) ) )
                    {
                    // InternalSimdaseParser.g:1561:6: ( ( (lv_type_39_0= Scoping ) ) otherlv_40= EqualsSignGreaterThanSign ( (lv_value_41_0= ruleCostValue ) ) )
                    // InternalSimdaseParser.g:1561:7: ( (lv_type_39_0= Scoping ) ) otherlv_40= EqualsSignGreaterThanSign ( (lv_value_41_0= ruleCostValue ) )
                    {
                    // InternalSimdaseParser.g:1561:7: ( (lv_type_39_0= Scoping ) )
                    // InternalSimdaseParser.g:1562:1: (lv_type_39_0= Scoping )
                    {
                    // InternalSimdaseParser.g:1562:1: (lv_type_39_0= Scoping )
                    // InternalSimdaseParser.g:1563:3: lv_type_39_0= Scoping
                    {
                    lv_type_39_0=(Token)match(input,Scoping,FollowSets000.FOLLOW_4); 

                            newLeafNode(lv_type_39_0, grammarAccess.getCostStatementAccess().getTypeScopingKeyword_13_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getCostStatementRule());
                    	        }
                           		setWithLastConsumed(current, "type", lv_type_39_0, "scoping");
                    	    

                    }


                    }

                    otherlv_40=(Token)match(input,EqualsSignGreaterThanSign,FollowSets000.FOLLOW_9); 

                        	newLeafNode(otherlv_40, grammarAccess.getCostStatementAccess().getEqualsSignGreaterThanSignKeyword_13_1());
                        
                    // InternalSimdaseParser.g:1582:1: ( (lv_value_41_0= ruleCostValue ) )
                    // InternalSimdaseParser.g:1583:1: (lv_value_41_0= ruleCostValue )
                    {
                    // InternalSimdaseParser.g:1583:1: (lv_value_41_0= ruleCostValue )
                    // InternalSimdaseParser.g:1584:3: lv_value_41_0= ruleCostValue
                    {
                     
                    	        newCompositeNode(grammarAccess.getCostStatementAccess().getValueCostValueParserRuleCall_13_2_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_value_41_0=ruleCostValue();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getCostStatementRule());
                    	        }
                           		set(
                           			current, 
                           			"value",
                            		lv_value_41_0, 
                            		"edu.clemson.simdase.Simdase.CostValue");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 15 :
                    // InternalSimdaseParser.g:1601:6: ( ( (lv_type_42_0= Technical_planning ) ) otherlv_43= EqualsSignGreaterThanSign ( (lv_value_44_0= ruleCostValue ) ) )
                    {
                    // InternalSimdaseParser.g:1601:6: ( ( (lv_type_42_0= Technical_planning ) ) otherlv_43= EqualsSignGreaterThanSign ( (lv_value_44_0= ruleCostValue ) ) )
                    // InternalSimdaseParser.g:1601:7: ( (lv_type_42_0= Technical_planning ) ) otherlv_43= EqualsSignGreaterThanSign ( (lv_value_44_0= ruleCostValue ) )
                    {
                    // InternalSimdaseParser.g:1601:7: ( (lv_type_42_0= Technical_planning ) )
                    // InternalSimdaseParser.g:1602:1: (lv_type_42_0= Technical_planning )
                    {
                    // InternalSimdaseParser.g:1602:1: (lv_type_42_0= Technical_planning )
                    // InternalSimdaseParser.g:1603:3: lv_type_42_0= Technical_planning
                    {
                    lv_type_42_0=(Token)match(input,Technical_planning,FollowSets000.FOLLOW_4); 

                            newLeafNode(lv_type_42_0, grammarAccess.getCostStatementAccess().getTypeTechnical_planningKeyword_14_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getCostStatementRule());
                    	        }
                           		setWithLastConsumed(current, "type", lv_type_42_0, "technical_planning");
                    	    

                    }


                    }

                    otherlv_43=(Token)match(input,EqualsSignGreaterThanSign,FollowSets000.FOLLOW_9); 

                        	newLeafNode(otherlv_43, grammarAccess.getCostStatementAccess().getEqualsSignGreaterThanSignKeyword_14_1());
                        
                    // InternalSimdaseParser.g:1622:1: ( (lv_value_44_0= ruleCostValue ) )
                    // InternalSimdaseParser.g:1623:1: (lv_value_44_0= ruleCostValue )
                    {
                    // InternalSimdaseParser.g:1623:1: (lv_value_44_0= ruleCostValue )
                    // InternalSimdaseParser.g:1624:3: lv_value_44_0= ruleCostValue
                    {
                     
                    	        newCompositeNode(grammarAccess.getCostStatementAccess().getValueCostValueParserRuleCall_14_2_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_value_44_0=ruleCostValue();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getCostStatementRule());
                    	        }
                           		set(
                           			current, 
                           			"value",
                            		lv_value_44_0, 
                            		"edu.clemson.simdase.Simdase.CostValue");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 16 :
                    // InternalSimdaseParser.g:1641:6: ( ( (lv_type_45_0= Technical_risk_management ) ) otherlv_46= EqualsSignGreaterThanSign ( (lv_value_47_0= ruleCostValue ) ) )
                    {
                    // InternalSimdaseParser.g:1641:6: ( ( (lv_type_45_0= Technical_risk_management ) ) otherlv_46= EqualsSignGreaterThanSign ( (lv_value_47_0= ruleCostValue ) ) )
                    // InternalSimdaseParser.g:1641:7: ( (lv_type_45_0= Technical_risk_management ) ) otherlv_46= EqualsSignGreaterThanSign ( (lv_value_47_0= ruleCostValue ) )
                    {
                    // InternalSimdaseParser.g:1641:7: ( (lv_type_45_0= Technical_risk_management ) )
                    // InternalSimdaseParser.g:1642:1: (lv_type_45_0= Technical_risk_management )
                    {
                    // InternalSimdaseParser.g:1642:1: (lv_type_45_0= Technical_risk_management )
                    // InternalSimdaseParser.g:1643:3: lv_type_45_0= Technical_risk_management
                    {
                    lv_type_45_0=(Token)match(input,Technical_risk_management,FollowSets000.FOLLOW_4); 

                            newLeafNode(lv_type_45_0, grammarAccess.getCostStatementAccess().getTypeTechnical_risk_managementKeyword_15_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getCostStatementRule());
                    	        }
                           		setWithLastConsumed(current, "type", lv_type_45_0, "technical_risk_management");
                    	    

                    }


                    }

                    otherlv_46=(Token)match(input,EqualsSignGreaterThanSign,FollowSets000.FOLLOW_9); 

                        	newLeafNode(otherlv_46, grammarAccess.getCostStatementAccess().getEqualsSignGreaterThanSignKeyword_15_1());
                        
                    // InternalSimdaseParser.g:1662:1: ( (lv_value_47_0= ruleCostValue ) )
                    // InternalSimdaseParser.g:1663:1: (lv_value_47_0= ruleCostValue )
                    {
                    // InternalSimdaseParser.g:1663:1: (lv_value_47_0= ruleCostValue )
                    // InternalSimdaseParser.g:1664:3: lv_value_47_0= ruleCostValue
                    {
                     
                    	        newCompositeNode(grammarAccess.getCostStatementAccess().getValueCostValueParserRuleCall_15_2_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_value_47_0=ruleCostValue();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getCostStatementRule());
                    	        }
                           		set(
                           			current, 
                           			"value",
                            		lv_value_47_0, 
                            		"edu.clemson.simdase.Simdase.CostValue");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 17 :
                    // InternalSimdaseParser.g:1681:6: ( ( (lv_type_48_0= Tool_support ) ) otherlv_49= EqualsSignGreaterThanSign ( (lv_value_50_0= ruleCostValue ) ) )
                    {
                    // InternalSimdaseParser.g:1681:6: ( ( (lv_type_48_0= Tool_support ) ) otherlv_49= EqualsSignGreaterThanSign ( (lv_value_50_0= ruleCostValue ) ) )
                    // InternalSimdaseParser.g:1681:7: ( (lv_type_48_0= Tool_support ) ) otherlv_49= EqualsSignGreaterThanSign ( (lv_value_50_0= ruleCostValue ) )
                    {
                    // InternalSimdaseParser.g:1681:7: ( (lv_type_48_0= Tool_support ) )
                    // InternalSimdaseParser.g:1682:1: (lv_type_48_0= Tool_support )
                    {
                    // InternalSimdaseParser.g:1682:1: (lv_type_48_0= Tool_support )
                    // InternalSimdaseParser.g:1683:3: lv_type_48_0= Tool_support
                    {
                    lv_type_48_0=(Token)match(input,Tool_support,FollowSets000.FOLLOW_4); 

                            newLeafNode(lv_type_48_0, grammarAccess.getCostStatementAccess().getTypeTool_supportKeyword_16_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getCostStatementRule());
                    	        }
                           		setWithLastConsumed(current, "type", lv_type_48_0, "tool_support");
                    	    

                    }


                    }

                    otherlv_49=(Token)match(input,EqualsSignGreaterThanSign,FollowSets000.FOLLOW_9); 

                        	newLeafNode(otherlv_49, grammarAccess.getCostStatementAccess().getEqualsSignGreaterThanSignKeyword_16_1());
                        
                    // InternalSimdaseParser.g:1702:1: ( (lv_value_50_0= ruleCostValue ) )
                    // InternalSimdaseParser.g:1703:1: (lv_value_50_0= ruleCostValue )
                    {
                    // InternalSimdaseParser.g:1703:1: (lv_value_50_0= ruleCostValue )
                    // InternalSimdaseParser.g:1704:3: lv_value_50_0= ruleCostValue
                    {
                     
                    	        newCompositeNode(grammarAccess.getCostStatementAccess().getValueCostValueParserRuleCall_16_2_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_value_50_0=ruleCostValue();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getCostStatementRule());
                    	        }
                           		set(
                           			current, 
                           			"value",
                            		lv_value_50_0, 
                            		"edu.clemson.simdase.Simdase.CostValue");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 18 :
                    // InternalSimdaseParser.g:1721:6: ( ( (lv_type_51_0= Building_business_case ) ) otherlv_52= EqualsSignGreaterThanSign ( (lv_value_53_0= ruleCostValue ) ) )
                    {
                    // InternalSimdaseParser.g:1721:6: ( ( (lv_type_51_0= Building_business_case ) ) otherlv_52= EqualsSignGreaterThanSign ( (lv_value_53_0= ruleCostValue ) ) )
                    // InternalSimdaseParser.g:1721:7: ( (lv_type_51_0= Building_business_case ) ) otherlv_52= EqualsSignGreaterThanSign ( (lv_value_53_0= ruleCostValue ) )
                    {
                    // InternalSimdaseParser.g:1721:7: ( (lv_type_51_0= Building_business_case ) )
                    // InternalSimdaseParser.g:1722:1: (lv_type_51_0= Building_business_case )
                    {
                    // InternalSimdaseParser.g:1722:1: (lv_type_51_0= Building_business_case )
                    // InternalSimdaseParser.g:1723:3: lv_type_51_0= Building_business_case
                    {
                    lv_type_51_0=(Token)match(input,Building_business_case,FollowSets000.FOLLOW_4); 

                            newLeafNode(lv_type_51_0, grammarAccess.getCostStatementAccess().getTypeBuilding_business_caseKeyword_17_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getCostStatementRule());
                    	        }
                           		setWithLastConsumed(current, "type", lv_type_51_0, "building_business_case");
                    	    

                    }


                    }

                    otherlv_52=(Token)match(input,EqualsSignGreaterThanSign,FollowSets000.FOLLOW_9); 

                        	newLeafNode(otherlv_52, grammarAccess.getCostStatementAccess().getEqualsSignGreaterThanSignKeyword_17_1());
                        
                    // InternalSimdaseParser.g:1742:1: ( (lv_value_53_0= ruleCostValue ) )
                    // InternalSimdaseParser.g:1743:1: (lv_value_53_0= ruleCostValue )
                    {
                    // InternalSimdaseParser.g:1743:1: (lv_value_53_0= ruleCostValue )
                    // InternalSimdaseParser.g:1744:3: lv_value_53_0= ruleCostValue
                    {
                     
                    	        newCompositeNode(grammarAccess.getCostStatementAccess().getValueCostValueParserRuleCall_17_2_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_value_53_0=ruleCostValue();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getCostStatementRule());
                    	        }
                           		set(
                           			current, 
                           			"value",
                            		lv_value_53_0, 
                            		"edu.clemson.simdase.Simdase.CostValue");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 19 :
                    // InternalSimdaseParser.g:1761:6: ( ( (lv_type_54_0= Customer_interface_management ) ) otherlv_55= EqualsSignGreaterThanSign ( (lv_value_56_0= ruleCostValue ) ) )
                    {
                    // InternalSimdaseParser.g:1761:6: ( ( (lv_type_54_0= Customer_interface_management ) ) otherlv_55= EqualsSignGreaterThanSign ( (lv_value_56_0= ruleCostValue ) ) )
                    // InternalSimdaseParser.g:1761:7: ( (lv_type_54_0= Customer_interface_management ) ) otherlv_55= EqualsSignGreaterThanSign ( (lv_value_56_0= ruleCostValue ) )
                    {
                    // InternalSimdaseParser.g:1761:7: ( (lv_type_54_0= Customer_interface_management ) )
                    // InternalSimdaseParser.g:1762:1: (lv_type_54_0= Customer_interface_management )
                    {
                    // InternalSimdaseParser.g:1762:1: (lv_type_54_0= Customer_interface_management )
                    // InternalSimdaseParser.g:1763:3: lv_type_54_0= Customer_interface_management
                    {
                    lv_type_54_0=(Token)match(input,Customer_interface_management,FollowSets000.FOLLOW_4); 

                            newLeafNode(lv_type_54_0, grammarAccess.getCostStatementAccess().getTypeCustomer_interface_managementKeyword_18_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getCostStatementRule());
                    	        }
                           		setWithLastConsumed(current, "type", lv_type_54_0, "customer_interface_management");
                    	    

                    }


                    }

                    otherlv_55=(Token)match(input,EqualsSignGreaterThanSign,FollowSets000.FOLLOW_9); 

                        	newLeafNode(otherlv_55, grammarAccess.getCostStatementAccess().getEqualsSignGreaterThanSignKeyword_18_1());
                        
                    // InternalSimdaseParser.g:1782:1: ( (lv_value_56_0= ruleCostValue ) )
                    // InternalSimdaseParser.g:1783:1: (lv_value_56_0= ruleCostValue )
                    {
                    // InternalSimdaseParser.g:1783:1: (lv_value_56_0= ruleCostValue )
                    // InternalSimdaseParser.g:1784:3: lv_value_56_0= ruleCostValue
                    {
                     
                    	        newCompositeNode(grammarAccess.getCostStatementAccess().getValueCostValueParserRuleCall_18_2_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_value_56_0=ruleCostValue();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getCostStatementRule());
                    	        }
                           		set(
                           			current, 
                           			"value",
                            		lv_value_56_0, 
                            		"edu.clemson.simdase.Simdase.CostValue");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 20 :
                    // InternalSimdaseParser.g:1801:6: ( ( (lv_type_57_0= Developing_acquisition_strategy ) ) otherlv_58= EqualsSignGreaterThanSign ( (lv_value_59_0= ruleCostValue ) ) )
                    {
                    // InternalSimdaseParser.g:1801:6: ( ( (lv_type_57_0= Developing_acquisition_strategy ) ) otherlv_58= EqualsSignGreaterThanSign ( (lv_value_59_0= ruleCostValue ) ) )
                    // InternalSimdaseParser.g:1801:7: ( (lv_type_57_0= Developing_acquisition_strategy ) ) otherlv_58= EqualsSignGreaterThanSign ( (lv_value_59_0= ruleCostValue ) )
                    {
                    // InternalSimdaseParser.g:1801:7: ( (lv_type_57_0= Developing_acquisition_strategy ) )
                    // InternalSimdaseParser.g:1802:1: (lv_type_57_0= Developing_acquisition_strategy )
                    {
                    // InternalSimdaseParser.g:1802:1: (lv_type_57_0= Developing_acquisition_strategy )
                    // InternalSimdaseParser.g:1803:3: lv_type_57_0= Developing_acquisition_strategy
                    {
                    lv_type_57_0=(Token)match(input,Developing_acquisition_strategy,FollowSets000.FOLLOW_4); 

                            newLeafNode(lv_type_57_0, grammarAccess.getCostStatementAccess().getTypeDeveloping_acquisition_strategyKeyword_19_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getCostStatementRule());
                    	        }
                           		setWithLastConsumed(current, "type", lv_type_57_0, "developing_acquisition_strategy");
                    	    

                    }


                    }

                    otherlv_58=(Token)match(input,EqualsSignGreaterThanSign,FollowSets000.FOLLOW_9); 

                        	newLeafNode(otherlv_58, grammarAccess.getCostStatementAccess().getEqualsSignGreaterThanSignKeyword_19_1());
                        
                    // InternalSimdaseParser.g:1822:1: ( (lv_value_59_0= ruleCostValue ) )
                    // InternalSimdaseParser.g:1823:1: (lv_value_59_0= ruleCostValue )
                    {
                    // InternalSimdaseParser.g:1823:1: (lv_value_59_0= ruleCostValue )
                    // InternalSimdaseParser.g:1824:3: lv_value_59_0= ruleCostValue
                    {
                     
                    	        newCompositeNode(grammarAccess.getCostStatementAccess().getValueCostValueParserRuleCall_19_2_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_value_59_0=ruleCostValue();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getCostStatementRule());
                    	        }
                           		set(
                           			current, 
                           			"value",
                            		lv_value_59_0, 
                            		"edu.clemson.simdase.Simdase.CostValue");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 21 :
                    // InternalSimdaseParser.g:1841:6: ( ( (lv_type_60_0= Funding ) ) otherlv_61= EqualsSignGreaterThanSign ( (lv_value_62_0= ruleCostValue ) ) )
                    {
                    // InternalSimdaseParser.g:1841:6: ( ( (lv_type_60_0= Funding ) ) otherlv_61= EqualsSignGreaterThanSign ( (lv_value_62_0= ruleCostValue ) ) )
                    // InternalSimdaseParser.g:1841:7: ( (lv_type_60_0= Funding ) ) otherlv_61= EqualsSignGreaterThanSign ( (lv_value_62_0= ruleCostValue ) )
                    {
                    // InternalSimdaseParser.g:1841:7: ( (lv_type_60_0= Funding ) )
                    // InternalSimdaseParser.g:1842:1: (lv_type_60_0= Funding )
                    {
                    // InternalSimdaseParser.g:1842:1: (lv_type_60_0= Funding )
                    // InternalSimdaseParser.g:1843:3: lv_type_60_0= Funding
                    {
                    lv_type_60_0=(Token)match(input,Funding,FollowSets000.FOLLOW_4); 

                            newLeafNode(lv_type_60_0, grammarAccess.getCostStatementAccess().getTypeFundingKeyword_20_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getCostStatementRule());
                    	        }
                           		setWithLastConsumed(current, "type", lv_type_60_0, "funding");
                    	    

                    }


                    }

                    otherlv_61=(Token)match(input,EqualsSignGreaterThanSign,FollowSets000.FOLLOW_9); 

                        	newLeafNode(otherlv_61, grammarAccess.getCostStatementAccess().getEqualsSignGreaterThanSignKeyword_20_1());
                        
                    // InternalSimdaseParser.g:1862:1: ( (lv_value_62_0= ruleCostValue ) )
                    // InternalSimdaseParser.g:1863:1: (lv_value_62_0= ruleCostValue )
                    {
                    // InternalSimdaseParser.g:1863:1: (lv_value_62_0= ruleCostValue )
                    // InternalSimdaseParser.g:1864:3: lv_value_62_0= ruleCostValue
                    {
                     
                    	        newCompositeNode(grammarAccess.getCostStatementAccess().getValueCostValueParserRuleCall_20_2_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_value_62_0=ruleCostValue();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getCostStatementRule());
                    	        }
                           		set(
                           			current, 
                           			"value",
                            		lv_value_62_0, 
                            		"edu.clemson.simdase.Simdase.CostValue");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 22 :
                    // InternalSimdaseParser.g:1881:6: ( ( (lv_type_63_0= Launching_institutionalizing ) ) otherlv_64= EqualsSignGreaterThanSign ( (lv_value_65_0= ruleCostValue ) ) )
                    {
                    // InternalSimdaseParser.g:1881:6: ( ( (lv_type_63_0= Launching_institutionalizing ) ) otherlv_64= EqualsSignGreaterThanSign ( (lv_value_65_0= ruleCostValue ) ) )
                    // InternalSimdaseParser.g:1881:7: ( (lv_type_63_0= Launching_institutionalizing ) ) otherlv_64= EqualsSignGreaterThanSign ( (lv_value_65_0= ruleCostValue ) )
                    {
                    // InternalSimdaseParser.g:1881:7: ( (lv_type_63_0= Launching_institutionalizing ) )
                    // InternalSimdaseParser.g:1882:1: (lv_type_63_0= Launching_institutionalizing )
                    {
                    // InternalSimdaseParser.g:1882:1: (lv_type_63_0= Launching_institutionalizing )
                    // InternalSimdaseParser.g:1883:3: lv_type_63_0= Launching_institutionalizing
                    {
                    lv_type_63_0=(Token)match(input,Launching_institutionalizing,FollowSets000.FOLLOW_4); 

                            newLeafNode(lv_type_63_0, grammarAccess.getCostStatementAccess().getTypeLaunching_institutionalizingKeyword_21_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getCostStatementRule());
                    	        }
                           		setWithLastConsumed(current, "type", lv_type_63_0, "launching_institutionalizing");
                    	    

                    }


                    }

                    otherlv_64=(Token)match(input,EqualsSignGreaterThanSign,FollowSets000.FOLLOW_9); 

                        	newLeafNode(otherlv_64, grammarAccess.getCostStatementAccess().getEqualsSignGreaterThanSignKeyword_21_1());
                        
                    // InternalSimdaseParser.g:1902:1: ( (lv_value_65_0= ruleCostValue ) )
                    // InternalSimdaseParser.g:1903:1: (lv_value_65_0= ruleCostValue )
                    {
                    // InternalSimdaseParser.g:1903:1: (lv_value_65_0= ruleCostValue )
                    // InternalSimdaseParser.g:1904:3: lv_value_65_0= ruleCostValue
                    {
                     
                    	        newCompositeNode(grammarAccess.getCostStatementAccess().getValueCostValueParserRuleCall_21_2_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_value_65_0=ruleCostValue();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getCostStatementRule());
                    	        }
                           		set(
                           			current, 
                           			"value",
                            		lv_value_65_0, 
                            		"edu.clemson.simdase.Simdase.CostValue");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 23 :
                    // InternalSimdaseParser.g:1921:6: ( ( (lv_type_66_0= Market_analysis ) ) otherlv_67= EqualsSignGreaterThanSign ( (lv_value_68_0= ruleCostValue ) ) )
                    {
                    // InternalSimdaseParser.g:1921:6: ( ( (lv_type_66_0= Market_analysis ) ) otherlv_67= EqualsSignGreaterThanSign ( (lv_value_68_0= ruleCostValue ) ) )
                    // InternalSimdaseParser.g:1921:7: ( (lv_type_66_0= Market_analysis ) ) otherlv_67= EqualsSignGreaterThanSign ( (lv_value_68_0= ruleCostValue ) )
                    {
                    // InternalSimdaseParser.g:1921:7: ( (lv_type_66_0= Market_analysis ) )
                    // InternalSimdaseParser.g:1922:1: (lv_type_66_0= Market_analysis )
                    {
                    // InternalSimdaseParser.g:1922:1: (lv_type_66_0= Market_analysis )
                    // InternalSimdaseParser.g:1923:3: lv_type_66_0= Market_analysis
                    {
                    lv_type_66_0=(Token)match(input,Market_analysis,FollowSets000.FOLLOW_4); 

                            newLeafNode(lv_type_66_0, grammarAccess.getCostStatementAccess().getTypeMarket_analysisKeyword_22_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getCostStatementRule());
                    	        }
                           		setWithLastConsumed(current, "type", lv_type_66_0, "market_analysis");
                    	    

                    }


                    }

                    otherlv_67=(Token)match(input,EqualsSignGreaterThanSign,FollowSets000.FOLLOW_9); 

                        	newLeafNode(otherlv_67, grammarAccess.getCostStatementAccess().getEqualsSignGreaterThanSignKeyword_22_1());
                        
                    // InternalSimdaseParser.g:1942:1: ( (lv_value_68_0= ruleCostValue ) )
                    // InternalSimdaseParser.g:1943:1: (lv_value_68_0= ruleCostValue )
                    {
                    // InternalSimdaseParser.g:1943:1: (lv_value_68_0= ruleCostValue )
                    // InternalSimdaseParser.g:1944:3: lv_value_68_0= ruleCostValue
                    {
                     
                    	        newCompositeNode(grammarAccess.getCostStatementAccess().getValueCostValueParserRuleCall_22_2_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_value_68_0=ruleCostValue();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getCostStatementRule());
                    	        }
                           		set(
                           			current, 
                           			"value",
                            		lv_value_68_0, 
                            		"edu.clemson.simdase.Simdase.CostValue");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 24 :
                    // InternalSimdaseParser.g:1961:6: ( ( (lv_type_69_0= Operations ) ) otherlv_70= EqualsSignGreaterThanSign ( (lv_value_71_0= ruleCostValue ) ) )
                    {
                    // InternalSimdaseParser.g:1961:6: ( ( (lv_type_69_0= Operations ) ) otherlv_70= EqualsSignGreaterThanSign ( (lv_value_71_0= ruleCostValue ) ) )
                    // InternalSimdaseParser.g:1961:7: ( (lv_type_69_0= Operations ) ) otherlv_70= EqualsSignGreaterThanSign ( (lv_value_71_0= ruleCostValue ) )
                    {
                    // InternalSimdaseParser.g:1961:7: ( (lv_type_69_0= Operations ) )
                    // InternalSimdaseParser.g:1962:1: (lv_type_69_0= Operations )
                    {
                    // InternalSimdaseParser.g:1962:1: (lv_type_69_0= Operations )
                    // InternalSimdaseParser.g:1963:3: lv_type_69_0= Operations
                    {
                    lv_type_69_0=(Token)match(input,Operations,FollowSets000.FOLLOW_4); 

                            newLeafNode(lv_type_69_0, grammarAccess.getCostStatementAccess().getTypeOperationsKeyword_23_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getCostStatementRule());
                    	        }
                           		setWithLastConsumed(current, "type", lv_type_69_0, "operations");
                    	    

                    }


                    }

                    otherlv_70=(Token)match(input,EqualsSignGreaterThanSign,FollowSets000.FOLLOW_9); 

                        	newLeafNode(otherlv_70, grammarAccess.getCostStatementAccess().getEqualsSignGreaterThanSignKeyword_23_1());
                        
                    // InternalSimdaseParser.g:1982:1: ( (lv_value_71_0= ruleCostValue ) )
                    // InternalSimdaseParser.g:1983:1: (lv_value_71_0= ruleCostValue )
                    {
                    // InternalSimdaseParser.g:1983:1: (lv_value_71_0= ruleCostValue )
                    // InternalSimdaseParser.g:1984:3: lv_value_71_0= ruleCostValue
                    {
                     
                    	        newCompositeNode(grammarAccess.getCostStatementAccess().getValueCostValueParserRuleCall_23_2_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_value_71_0=ruleCostValue();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getCostStatementRule());
                    	        }
                           		set(
                           			current, 
                           			"value",
                            		lv_value_71_0, 
                            		"edu.clemson.simdase.Simdase.CostValue");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 25 :
                    // InternalSimdaseParser.g:2001:6: ( ( (lv_type_72_0= Organizational_planning ) ) otherlv_73= EqualsSignGreaterThanSign ( (lv_value_74_0= ruleCostValue ) ) )
                    {
                    // InternalSimdaseParser.g:2001:6: ( ( (lv_type_72_0= Organizational_planning ) ) otherlv_73= EqualsSignGreaterThanSign ( (lv_value_74_0= ruleCostValue ) ) )
                    // InternalSimdaseParser.g:2001:7: ( (lv_type_72_0= Organizational_planning ) ) otherlv_73= EqualsSignGreaterThanSign ( (lv_value_74_0= ruleCostValue ) )
                    {
                    // InternalSimdaseParser.g:2001:7: ( (lv_type_72_0= Organizational_planning ) )
                    // InternalSimdaseParser.g:2002:1: (lv_type_72_0= Organizational_planning )
                    {
                    // InternalSimdaseParser.g:2002:1: (lv_type_72_0= Organizational_planning )
                    // InternalSimdaseParser.g:2003:3: lv_type_72_0= Organizational_planning
                    {
                    lv_type_72_0=(Token)match(input,Organizational_planning,FollowSets000.FOLLOW_4); 

                            newLeafNode(lv_type_72_0, grammarAccess.getCostStatementAccess().getTypeOrganizational_planningKeyword_24_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getCostStatementRule());
                    	        }
                           		setWithLastConsumed(current, "type", lv_type_72_0, "organizational_planning");
                    	    

                    }


                    }

                    otherlv_73=(Token)match(input,EqualsSignGreaterThanSign,FollowSets000.FOLLOW_9); 

                        	newLeafNode(otherlv_73, grammarAccess.getCostStatementAccess().getEqualsSignGreaterThanSignKeyword_24_1());
                        
                    // InternalSimdaseParser.g:2022:1: ( (lv_value_74_0= ruleCostValue ) )
                    // InternalSimdaseParser.g:2023:1: (lv_value_74_0= ruleCostValue )
                    {
                    // InternalSimdaseParser.g:2023:1: (lv_value_74_0= ruleCostValue )
                    // InternalSimdaseParser.g:2024:3: lv_value_74_0= ruleCostValue
                    {
                     
                    	        newCompositeNode(grammarAccess.getCostStatementAccess().getValueCostValueParserRuleCall_24_2_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_value_74_0=ruleCostValue();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getCostStatementRule());
                    	        }
                           		set(
                           			current, 
                           			"value",
                            		lv_value_74_0, 
                            		"edu.clemson.simdase.Simdase.CostValue");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 26 :
                    // InternalSimdaseParser.g:2041:6: ( ( (lv_type_75_0= Organizational_risk_management ) ) otherlv_76= EqualsSignGreaterThanSign ( (lv_value_77_0= ruleCostValue ) ) )
                    {
                    // InternalSimdaseParser.g:2041:6: ( ( (lv_type_75_0= Organizational_risk_management ) ) otherlv_76= EqualsSignGreaterThanSign ( (lv_value_77_0= ruleCostValue ) ) )
                    // InternalSimdaseParser.g:2041:7: ( (lv_type_75_0= Organizational_risk_management ) ) otherlv_76= EqualsSignGreaterThanSign ( (lv_value_77_0= ruleCostValue ) )
                    {
                    // InternalSimdaseParser.g:2041:7: ( (lv_type_75_0= Organizational_risk_management ) )
                    // InternalSimdaseParser.g:2042:1: (lv_type_75_0= Organizational_risk_management )
                    {
                    // InternalSimdaseParser.g:2042:1: (lv_type_75_0= Organizational_risk_management )
                    // InternalSimdaseParser.g:2043:3: lv_type_75_0= Organizational_risk_management
                    {
                    lv_type_75_0=(Token)match(input,Organizational_risk_management,FollowSets000.FOLLOW_4); 

                            newLeafNode(lv_type_75_0, grammarAccess.getCostStatementAccess().getTypeOrganizational_risk_managementKeyword_25_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getCostStatementRule());
                    	        }
                           		setWithLastConsumed(current, "type", lv_type_75_0, "organizational_risk_management");
                    	    

                    }


                    }

                    otherlv_76=(Token)match(input,EqualsSignGreaterThanSign,FollowSets000.FOLLOW_9); 

                        	newLeafNode(otherlv_76, grammarAccess.getCostStatementAccess().getEqualsSignGreaterThanSignKeyword_25_1());
                        
                    // InternalSimdaseParser.g:2062:1: ( (lv_value_77_0= ruleCostValue ) )
                    // InternalSimdaseParser.g:2063:1: (lv_value_77_0= ruleCostValue )
                    {
                    // InternalSimdaseParser.g:2063:1: (lv_value_77_0= ruleCostValue )
                    // InternalSimdaseParser.g:2064:3: lv_value_77_0= ruleCostValue
                    {
                     
                    	        newCompositeNode(grammarAccess.getCostStatementAccess().getValueCostValueParserRuleCall_25_2_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_value_77_0=ruleCostValue();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getCostStatementRule());
                    	        }
                           		set(
                           			current, 
                           			"value",
                            		lv_value_77_0, 
                            		"edu.clemson.simdase.Simdase.CostValue");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 27 :
                    // InternalSimdaseParser.g:2081:6: ( ( (lv_type_78_0= Structuring_organization ) ) otherlv_79= EqualsSignGreaterThanSign ( (lv_value_80_0= ruleCostValue ) ) )
                    {
                    // InternalSimdaseParser.g:2081:6: ( ( (lv_type_78_0= Structuring_organization ) ) otherlv_79= EqualsSignGreaterThanSign ( (lv_value_80_0= ruleCostValue ) ) )
                    // InternalSimdaseParser.g:2081:7: ( (lv_type_78_0= Structuring_organization ) ) otherlv_79= EqualsSignGreaterThanSign ( (lv_value_80_0= ruleCostValue ) )
                    {
                    // InternalSimdaseParser.g:2081:7: ( (lv_type_78_0= Structuring_organization ) )
                    // InternalSimdaseParser.g:2082:1: (lv_type_78_0= Structuring_organization )
                    {
                    // InternalSimdaseParser.g:2082:1: (lv_type_78_0= Structuring_organization )
                    // InternalSimdaseParser.g:2083:3: lv_type_78_0= Structuring_organization
                    {
                    lv_type_78_0=(Token)match(input,Structuring_organization,FollowSets000.FOLLOW_4); 

                            newLeafNode(lv_type_78_0, grammarAccess.getCostStatementAccess().getTypeStructuring_organizationKeyword_26_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getCostStatementRule());
                    	        }
                           		setWithLastConsumed(current, "type", lv_type_78_0, "structuring_organization");
                    	    

                    }


                    }

                    otherlv_79=(Token)match(input,EqualsSignGreaterThanSign,FollowSets000.FOLLOW_9); 

                        	newLeafNode(otherlv_79, grammarAccess.getCostStatementAccess().getEqualsSignGreaterThanSignKeyword_26_1());
                        
                    // InternalSimdaseParser.g:2102:1: ( (lv_value_80_0= ruleCostValue ) )
                    // InternalSimdaseParser.g:2103:1: (lv_value_80_0= ruleCostValue )
                    {
                    // InternalSimdaseParser.g:2103:1: (lv_value_80_0= ruleCostValue )
                    // InternalSimdaseParser.g:2104:3: lv_value_80_0= ruleCostValue
                    {
                     
                    	        newCompositeNode(grammarAccess.getCostStatementAccess().getValueCostValueParserRuleCall_26_2_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_value_80_0=ruleCostValue();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getCostStatementRule());
                    	        }
                           		set(
                           			current, 
                           			"value",
                            		lv_value_80_0, 
                            		"edu.clemson.simdase.Simdase.CostValue");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 28 :
                    // InternalSimdaseParser.g:2121:6: ( ( (lv_type_81_0= Technology_forecasting ) ) otherlv_82= EqualsSignGreaterThanSign ( (lv_value_83_0= ruleCostValue ) ) )
                    {
                    // InternalSimdaseParser.g:2121:6: ( ( (lv_type_81_0= Technology_forecasting ) ) otherlv_82= EqualsSignGreaterThanSign ( (lv_value_83_0= ruleCostValue ) ) )
                    // InternalSimdaseParser.g:2121:7: ( (lv_type_81_0= Technology_forecasting ) ) otherlv_82= EqualsSignGreaterThanSign ( (lv_value_83_0= ruleCostValue ) )
                    {
                    // InternalSimdaseParser.g:2121:7: ( (lv_type_81_0= Technology_forecasting ) )
                    // InternalSimdaseParser.g:2122:1: (lv_type_81_0= Technology_forecasting )
                    {
                    // InternalSimdaseParser.g:2122:1: (lv_type_81_0= Technology_forecasting )
                    // InternalSimdaseParser.g:2123:3: lv_type_81_0= Technology_forecasting
                    {
                    lv_type_81_0=(Token)match(input,Technology_forecasting,FollowSets000.FOLLOW_4); 

                            newLeafNode(lv_type_81_0, grammarAccess.getCostStatementAccess().getTypeTechnology_forecastingKeyword_27_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getCostStatementRule());
                    	        }
                           		setWithLastConsumed(current, "type", lv_type_81_0, "technology_forecasting");
                    	    

                    }


                    }

                    otherlv_82=(Token)match(input,EqualsSignGreaterThanSign,FollowSets000.FOLLOW_9); 

                        	newLeafNode(otherlv_82, grammarAccess.getCostStatementAccess().getEqualsSignGreaterThanSignKeyword_27_1());
                        
                    // InternalSimdaseParser.g:2142:1: ( (lv_value_83_0= ruleCostValue ) )
                    // InternalSimdaseParser.g:2143:1: (lv_value_83_0= ruleCostValue )
                    {
                    // InternalSimdaseParser.g:2143:1: (lv_value_83_0= ruleCostValue )
                    // InternalSimdaseParser.g:2144:3: lv_value_83_0= ruleCostValue
                    {
                     
                    	        newCompositeNode(grammarAccess.getCostStatementAccess().getValueCostValueParserRuleCall_27_2_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_value_83_0=ruleCostValue();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getCostStatementRule());
                    	        }
                           		set(
                           			current, 
                           			"value",
                            		lv_value_83_0, 
                            		"edu.clemson.simdase.Simdase.CostValue");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 29 :
                    // InternalSimdaseParser.g:2161:6: ( ( (lv_type_84_0= Training ) ) otherlv_85= EqualsSignGreaterThanSign ( (lv_value_86_0= ruleCostValue ) ) )
                    {
                    // InternalSimdaseParser.g:2161:6: ( ( (lv_type_84_0= Training ) ) otherlv_85= EqualsSignGreaterThanSign ( (lv_value_86_0= ruleCostValue ) ) )
                    // InternalSimdaseParser.g:2161:7: ( (lv_type_84_0= Training ) ) otherlv_85= EqualsSignGreaterThanSign ( (lv_value_86_0= ruleCostValue ) )
                    {
                    // InternalSimdaseParser.g:2161:7: ( (lv_type_84_0= Training ) )
                    // InternalSimdaseParser.g:2162:1: (lv_type_84_0= Training )
                    {
                    // InternalSimdaseParser.g:2162:1: (lv_type_84_0= Training )
                    // InternalSimdaseParser.g:2163:3: lv_type_84_0= Training
                    {
                    lv_type_84_0=(Token)match(input,Training,FollowSets000.FOLLOW_4); 

                            newLeafNode(lv_type_84_0, grammarAccess.getCostStatementAccess().getTypeTrainingKeyword_28_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getCostStatementRule());
                    	        }
                           		setWithLastConsumed(current, "type", lv_type_84_0, "training");
                    	    

                    }


                    }

                    otherlv_85=(Token)match(input,EqualsSignGreaterThanSign,FollowSets000.FOLLOW_9); 

                        	newLeafNode(otherlv_85, grammarAccess.getCostStatementAccess().getEqualsSignGreaterThanSignKeyword_28_1());
                        
                    // InternalSimdaseParser.g:2182:1: ( (lv_value_86_0= ruleCostValue ) )
                    // InternalSimdaseParser.g:2183:1: (lv_value_86_0= ruleCostValue )
                    {
                    // InternalSimdaseParser.g:2183:1: (lv_value_86_0= ruleCostValue )
                    // InternalSimdaseParser.g:2184:3: lv_value_86_0= ruleCostValue
                    {
                     
                    	        newCompositeNode(grammarAccess.getCostStatementAccess().getValueCostValueParserRuleCall_28_2_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_value_86_0=ruleCostValue();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getCostStatementRule());
                    	        }
                           		set(
                           			current, 
                           			"value",
                            		lv_value_86_0, 
                            		"edu.clemson.simdase.Simdase.CostValue");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 30 :
                    // InternalSimdaseParser.g:2201:6: ( ( (lv_type_87_0= RULE_ID ) ) otherlv_88= EqualsSignGreaterThanSign ( (lv_value_89_0= ruleCostValue ) ) )
                    {
                    // InternalSimdaseParser.g:2201:6: ( ( (lv_type_87_0= RULE_ID ) ) otherlv_88= EqualsSignGreaterThanSign ( (lv_value_89_0= ruleCostValue ) ) )
                    // InternalSimdaseParser.g:2201:7: ( (lv_type_87_0= RULE_ID ) ) otherlv_88= EqualsSignGreaterThanSign ( (lv_value_89_0= ruleCostValue ) )
                    {
                    // InternalSimdaseParser.g:2201:7: ( (lv_type_87_0= RULE_ID ) )
                    // InternalSimdaseParser.g:2202:1: (lv_type_87_0= RULE_ID )
                    {
                    // InternalSimdaseParser.g:2202:1: (lv_type_87_0= RULE_ID )
                    // InternalSimdaseParser.g:2203:3: lv_type_87_0= RULE_ID
                    {
                    lv_type_87_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_4); 

                    			newLeafNode(lv_type_87_0, grammarAccess.getCostStatementAccess().getTypeIDTerminalRuleCall_29_0_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getCostStatementRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"type",
                            		lv_type_87_0, 
                            		"org.osate.xtext.aadl2.properties.Properties.ID");
                    	    

                    }


                    }

                    otherlv_88=(Token)match(input,EqualsSignGreaterThanSign,FollowSets000.FOLLOW_9); 

                        	newLeafNode(otherlv_88, grammarAccess.getCostStatementAccess().getEqualsSignGreaterThanSignKeyword_29_1());
                        
                    // InternalSimdaseParser.g:2224:1: ( (lv_value_89_0= ruleCostValue ) )
                    // InternalSimdaseParser.g:2225:1: (lv_value_89_0= ruleCostValue )
                    {
                    // InternalSimdaseParser.g:2225:1: (lv_value_89_0= ruleCostValue )
                    // InternalSimdaseParser.g:2226:3: lv_value_89_0= ruleCostValue
                    {
                     
                    	        newCompositeNode(grammarAccess.getCostStatementAccess().getValueCostValueParserRuleCall_29_2_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_value_89_0=ruleCostValue();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getCostStatementRule());
                    	        }
                           		set(
                           			current, 
                           			"value",
                            		lv_value_89_0, 
                            		"edu.clemson.simdase.Simdase.CostValue");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCostStatement"


    // $ANTLR start "entryRuleCostValue"
    // InternalSimdaseParser.g:2250:1: entryRuleCostValue returns [EObject current=null] : iv_ruleCostValue= ruleCostValue EOF ;
    public final EObject entryRuleCostValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCostValue = null;


        try {
            // InternalSimdaseParser.g:2251:2: (iv_ruleCostValue= ruleCostValue EOF )
            // InternalSimdaseParser.g:2252:2: iv_ruleCostValue= ruleCostValue EOF
            {
             newCompositeNode(grammarAccess.getCostValueRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleCostValue=ruleCostValue();

            state._fsp--;

             current =iv_ruleCostValue; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCostValue"


    // $ANTLR start "ruleCostValue"
    // InternalSimdaseParser.g:2259:1: ruleCostValue returns [EObject current=null] : (otherlv_0= LeftCurlyBracket ( (lv_first_1_0= ruleEmployeeStatement ) ) (otherlv_2= Comma ( (lv_rest_3_0= ruleEmployeeStatement ) ) )* otherlv_4= RightCurlyBracket otherlv_5= Semicolon ) ;
    public final EObject ruleCostValue() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        EObject lv_first_1_0 = null;

        EObject lv_rest_3_0 = null;


         enterRule(); 
            
        try {
            // InternalSimdaseParser.g:2262:28: ( (otherlv_0= LeftCurlyBracket ( (lv_first_1_0= ruleEmployeeStatement ) ) (otherlv_2= Comma ( (lv_rest_3_0= ruleEmployeeStatement ) ) )* otherlv_4= RightCurlyBracket otherlv_5= Semicolon ) )
            // InternalSimdaseParser.g:2263:1: (otherlv_0= LeftCurlyBracket ( (lv_first_1_0= ruleEmployeeStatement ) ) (otherlv_2= Comma ( (lv_rest_3_0= ruleEmployeeStatement ) ) )* otherlv_4= RightCurlyBracket otherlv_5= Semicolon )
            {
            // InternalSimdaseParser.g:2263:1: (otherlv_0= LeftCurlyBracket ( (lv_first_1_0= ruleEmployeeStatement ) ) (otherlv_2= Comma ( (lv_rest_3_0= ruleEmployeeStatement ) ) )* otherlv_4= RightCurlyBracket otherlv_5= Semicolon )
            // InternalSimdaseParser.g:2264:2: otherlv_0= LeftCurlyBracket ( (lv_first_1_0= ruleEmployeeStatement ) ) (otherlv_2= Comma ( (lv_rest_3_0= ruleEmployeeStatement ) ) )* otherlv_4= RightCurlyBracket otherlv_5= Semicolon
            {
            otherlv_0=(Token)match(input,LeftCurlyBracket,FollowSets000.FOLLOW_7); 

                	newLeafNode(otherlv_0, grammarAccess.getCostValueAccess().getLeftCurlyBracketKeyword_0());
                
            // InternalSimdaseParser.g:2268:1: ( (lv_first_1_0= ruleEmployeeStatement ) )
            // InternalSimdaseParser.g:2269:1: (lv_first_1_0= ruleEmployeeStatement )
            {
            // InternalSimdaseParser.g:2269:1: (lv_first_1_0= ruleEmployeeStatement )
            // InternalSimdaseParser.g:2270:3: lv_first_1_0= ruleEmployeeStatement
            {
             
            	        newCompositeNode(grammarAccess.getCostValueAccess().getFirstEmployeeStatementParserRuleCall_1_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_24);
            lv_first_1_0=ruleEmployeeStatement();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getCostValueRule());
            	        }
                   		set(
                   			current, 
                   			"first",
                    		lv_first_1_0, 
                    		"edu.clemson.simdase.Simdase.EmployeeStatement");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // InternalSimdaseParser.g:2286:2: (otherlv_2= Comma ( (lv_rest_3_0= ruleEmployeeStatement ) ) )*
            loop13:
            do {
                int alt13=2;
                int LA13_0 = input.LA(1);

                if ( (LA13_0==Comma) ) {
                    alt13=1;
                }


                switch (alt13) {
            	case 1 :
            	    // InternalSimdaseParser.g:2287:2: otherlv_2= Comma ( (lv_rest_3_0= ruleEmployeeStatement ) )
            	    {
            	    otherlv_2=(Token)match(input,Comma,FollowSets000.FOLLOW_7); 

            	        	newLeafNode(otherlv_2, grammarAccess.getCostValueAccess().getCommaKeyword_2_0());
            	        
            	    // InternalSimdaseParser.g:2291:1: ( (lv_rest_3_0= ruleEmployeeStatement ) )
            	    // InternalSimdaseParser.g:2292:1: (lv_rest_3_0= ruleEmployeeStatement )
            	    {
            	    // InternalSimdaseParser.g:2292:1: (lv_rest_3_0= ruleEmployeeStatement )
            	    // InternalSimdaseParser.g:2293:3: lv_rest_3_0= ruleEmployeeStatement
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getCostValueAccess().getRestEmployeeStatementParserRuleCall_2_1_0()); 
            	    	    
            	    pushFollow(FollowSets000.FOLLOW_24);
            	    lv_rest_3_0=ruleEmployeeStatement();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getCostValueRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"rest",
            	            		lv_rest_3_0, 
            	            		"edu.clemson.simdase.Simdase.EmployeeStatement");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop13;
                }
            } while (true);

            otherlv_4=(Token)match(input,RightCurlyBracket,FollowSets000.FOLLOW_6); 

                	newLeafNode(otherlv_4, grammarAccess.getCostValueAccess().getRightCurlyBracketKeyword_3());
                
            otherlv_5=(Token)match(input,Semicolon,FollowSets000.FOLLOW_2); 

                	newLeafNode(otherlv_5, grammarAccess.getCostValueAccess().getSemicolonKeyword_4());
                

            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCostValue"


    // $ANTLR start "entryRuleEmployeeStatement"
    // InternalSimdaseParser.g:2327:1: entryRuleEmployeeStatement returns [EObject current=null] : iv_ruleEmployeeStatement= ruleEmployeeStatement EOF ;
    public final EObject entryRuleEmployeeStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEmployeeStatement = null;


        try {
            // InternalSimdaseParser.g:2328:2: (iv_ruleEmployeeStatement= ruleEmployeeStatement EOF )
            // InternalSimdaseParser.g:2329:2: iv_ruleEmployeeStatement= ruleEmployeeStatement EOF
            {
             newCompositeNode(grammarAccess.getEmployeeStatementRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleEmployeeStatement=ruleEmployeeStatement();

            state._fsp--;

             current =iv_ruleEmployeeStatement; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEmployeeStatement"


    // $ANTLR start "ruleEmployeeStatement"
    // InternalSimdaseParser.g:2336:1: ruleEmployeeStatement returns [EObject current=null] : ( ( () ( (lv_count_1_0= ruleNumber ) ) ( (lv_employeeType_2_0= RULE_STRING ) ) otherlv_3= For ( (lv_cost_4_0= ruleStatement ) ) ) | ( () ( (lv_cost_6_0= ruleNumber ) ) otherlv_7= For ( (lv_costReason_8_0= RULE_STRING ) ) otherlv_9= At ( (lv_period_10_0= ruleNumber ) ) ) ) ;
    public final EObject ruleEmployeeStatement() throws RecognitionException {
        EObject current = null;

        Token lv_employeeType_2_0=null;
        Token otherlv_3=null;
        Token otherlv_7=null;
        Token lv_costReason_8_0=null;
        Token otherlv_9=null;
        EObject lv_count_1_0 = null;

        EObject lv_cost_4_0 = null;

        EObject lv_cost_6_0 = null;

        EObject lv_period_10_0 = null;


         enterRule(); 
            
        try {
            // InternalSimdaseParser.g:2339:28: ( ( ( () ( (lv_count_1_0= ruleNumber ) ) ( (lv_employeeType_2_0= RULE_STRING ) ) otherlv_3= For ( (lv_cost_4_0= ruleStatement ) ) ) | ( () ( (lv_cost_6_0= ruleNumber ) ) otherlv_7= For ( (lv_costReason_8_0= RULE_STRING ) ) otherlv_9= At ( (lv_period_10_0= ruleNumber ) ) ) ) )
            // InternalSimdaseParser.g:2340:1: ( ( () ( (lv_count_1_0= ruleNumber ) ) ( (lv_employeeType_2_0= RULE_STRING ) ) otherlv_3= For ( (lv_cost_4_0= ruleStatement ) ) ) | ( () ( (lv_cost_6_0= ruleNumber ) ) otherlv_7= For ( (lv_costReason_8_0= RULE_STRING ) ) otherlv_9= At ( (lv_period_10_0= ruleNumber ) ) ) )
            {
            // InternalSimdaseParser.g:2340:1: ( ( () ( (lv_count_1_0= ruleNumber ) ) ( (lv_employeeType_2_0= RULE_STRING ) ) otherlv_3= For ( (lv_cost_4_0= ruleStatement ) ) ) | ( () ( (lv_cost_6_0= ruleNumber ) ) otherlv_7= For ( (lv_costReason_8_0= RULE_STRING ) ) otherlv_9= At ( (lv_period_10_0= ruleNumber ) ) ) )
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==HyphenMinus) ) {
                int LA14_1 = input.LA(2);

                if ( (LA14_1==RULE_FLOAT) ) {
                    int LA14_2 = input.LA(3);

                    if ( (LA14_2==RULE_STRING) ) {
                        alt14=1;
                    }
                    else if ( (LA14_2==For) ) {
                        alt14=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 14, 2, input);

                        throw nvae;
                    }
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 14, 1, input);

                    throw nvae;
                }
            }
            else if ( (LA14_0==RULE_FLOAT) ) {
                int LA14_2 = input.LA(2);

                if ( (LA14_2==RULE_STRING) ) {
                    alt14=1;
                }
                else if ( (LA14_2==For) ) {
                    alt14=2;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 14, 2, input);

                    throw nvae;
                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 14, 0, input);

                throw nvae;
            }
            switch (alt14) {
                case 1 :
                    // InternalSimdaseParser.g:2340:2: ( () ( (lv_count_1_0= ruleNumber ) ) ( (lv_employeeType_2_0= RULE_STRING ) ) otherlv_3= For ( (lv_cost_4_0= ruleStatement ) ) )
                    {
                    // InternalSimdaseParser.g:2340:2: ( () ( (lv_count_1_0= ruleNumber ) ) ( (lv_employeeType_2_0= RULE_STRING ) ) otherlv_3= For ( (lv_cost_4_0= ruleStatement ) ) )
                    // InternalSimdaseParser.g:2340:3: () ( (lv_count_1_0= ruleNumber ) ) ( (lv_employeeType_2_0= RULE_STRING ) ) otherlv_3= For ( (lv_cost_4_0= ruleStatement ) )
                    {
                    // InternalSimdaseParser.g:2340:3: ()
                    // InternalSimdaseParser.g:2341:5: 
                    {

                            current = forceCreateModelElement(
                                grammarAccess.getEmployeeStatementAccess().getHourlyCostAction_0_0(),
                                current);
                        

                    }

                    // InternalSimdaseParser.g:2346:2: ( (lv_count_1_0= ruleNumber ) )
                    // InternalSimdaseParser.g:2347:1: (lv_count_1_0= ruleNumber )
                    {
                    // InternalSimdaseParser.g:2347:1: (lv_count_1_0= ruleNumber )
                    // InternalSimdaseParser.g:2348:3: lv_count_1_0= ruleNumber
                    {
                     
                    	        newCompositeNode(grammarAccess.getEmployeeStatementAccess().getCountNumberParserRuleCall_0_1_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_17);
                    lv_count_1_0=ruleNumber();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getEmployeeStatementRule());
                    	        }
                           		set(
                           			current, 
                           			"count",
                            		lv_count_1_0, 
                            		"edu.clemson.simdase.Simdase.Number");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // InternalSimdaseParser.g:2364:2: ( (lv_employeeType_2_0= RULE_STRING ) )
                    // InternalSimdaseParser.g:2365:1: (lv_employeeType_2_0= RULE_STRING )
                    {
                    // InternalSimdaseParser.g:2365:1: (lv_employeeType_2_0= RULE_STRING )
                    // InternalSimdaseParser.g:2366:3: lv_employeeType_2_0= RULE_STRING
                    {
                    lv_employeeType_2_0=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_12); 

                    			newLeafNode(lv_employeeType_2_0, grammarAccess.getEmployeeStatementAccess().getEmployeeTypeSTRINGTerminalRuleCall_0_2_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getEmployeeStatementRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"employeeType",
                            		lv_employeeType_2_0, 
                            		"org.osate.xtext.aadl2.properties.Properties.STRING");
                    	    

                    }


                    }

                    otherlv_3=(Token)match(input,For,FollowSets000.FOLLOW_8); 

                        	newLeafNode(otherlv_3, grammarAccess.getEmployeeStatementAccess().getForKeyword_0_3());
                        
                    // InternalSimdaseParser.g:2387:1: ( (lv_cost_4_0= ruleStatement ) )
                    // InternalSimdaseParser.g:2388:1: (lv_cost_4_0= ruleStatement )
                    {
                    // InternalSimdaseParser.g:2388:1: (lv_cost_4_0= ruleStatement )
                    // InternalSimdaseParser.g:2389:3: lv_cost_4_0= ruleStatement
                    {
                     
                    	        newCompositeNode(grammarAccess.getEmployeeStatementAccess().getCostStatementParserRuleCall_0_4_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_cost_4_0=ruleStatement();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getEmployeeStatementRule());
                    	        }
                           		set(
                           			current, 
                           			"cost",
                            		lv_cost_4_0, 
                            		"edu.clemson.simdase.Simdase.Statement");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalSimdaseParser.g:2406:6: ( () ( (lv_cost_6_0= ruleNumber ) ) otherlv_7= For ( (lv_costReason_8_0= RULE_STRING ) ) otherlv_9= At ( (lv_period_10_0= ruleNumber ) ) )
                    {
                    // InternalSimdaseParser.g:2406:6: ( () ( (lv_cost_6_0= ruleNumber ) ) otherlv_7= For ( (lv_costReason_8_0= RULE_STRING ) ) otherlv_9= At ( (lv_period_10_0= ruleNumber ) ) )
                    // InternalSimdaseParser.g:2406:7: () ( (lv_cost_6_0= ruleNumber ) ) otherlv_7= For ( (lv_costReason_8_0= RULE_STRING ) ) otherlv_9= At ( (lv_period_10_0= ruleNumber ) )
                    {
                    // InternalSimdaseParser.g:2406:7: ()
                    // InternalSimdaseParser.g:2407:5: 
                    {

                            current = forceCreateModelElement(
                                grammarAccess.getEmployeeStatementAccess().getFixedCostAction_1_0(),
                                current);
                        

                    }

                    // InternalSimdaseParser.g:2412:2: ( (lv_cost_6_0= ruleNumber ) )
                    // InternalSimdaseParser.g:2413:1: (lv_cost_6_0= ruleNumber )
                    {
                    // InternalSimdaseParser.g:2413:1: (lv_cost_6_0= ruleNumber )
                    // InternalSimdaseParser.g:2414:3: lv_cost_6_0= ruleNumber
                    {
                     
                    	        newCompositeNode(grammarAccess.getEmployeeStatementAccess().getCostNumberParserRuleCall_1_1_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_12);
                    lv_cost_6_0=ruleNumber();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getEmployeeStatementRule());
                    	        }
                           		set(
                           			current, 
                           			"cost",
                            		lv_cost_6_0, 
                            		"edu.clemson.simdase.Simdase.Number");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    otherlv_7=(Token)match(input,For,FollowSets000.FOLLOW_17); 

                        	newLeafNode(otherlv_7, grammarAccess.getEmployeeStatementAccess().getForKeyword_1_2());
                        
                    // InternalSimdaseParser.g:2435:1: ( (lv_costReason_8_0= RULE_STRING ) )
                    // InternalSimdaseParser.g:2436:1: (lv_costReason_8_0= RULE_STRING )
                    {
                    // InternalSimdaseParser.g:2436:1: (lv_costReason_8_0= RULE_STRING )
                    // InternalSimdaseParser.g:2437:3: lv_costReason_8_0= RULE_STRING
                    {
                    lv_costReason_8_0=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_25); 

                    			newLeafNode(lv_costReason_8_0, grammarAccess.getEmployeeStatementAccess().getCostReasonSTRINGTerminalRuleCall_1_3_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getEmployeeStatementRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"costReason",
                            		lv_costReason_8_0, 
                            		"org.osate.xtext.aadl2.properties.Properties.STRING");
                    	    

                    }


                    }

                    otherlv_9=(Token)match(input,At,FollowSets000.FOLLOW_7); 

                        	newLeafNode(otherlv_9, grammarAccess.getEmployeeStatementAccess().getAtKeyword_1_4());
                        
                    // InternalSimdaseParser.g:2458:1: ( (lv_period_10_0= ruleNumber ) )
                    // InternalSimdaseParser.g:2459:1: (lv_period_10_0= ruleNumber )
                    {
                    // InternalSimdaseParser.g:2459:1: (lv_period_10_0= ruleNumber )
                    // InternalSimdaseParser.g:2460:3: lv_period_10_0= ruleNumber
                    {
                     
                    	        newCompositeNode(grammarAccess.getEmployeeStatementAccess().getPeriodNumberParserRuleCall_1_5_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_period_10_0=ruleNumber();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getEmployeeStatementRule());
                    	        }
                           		set(
                           			current, 
                           			"period",
                            		lv_period_10_0, 
                            		"edu.clemson.simdase.Simdase.Number");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEmployeeStatement"


    // $ANTLR start "entryRuleBoolean"
    // InternalSimdaseParser.g:2484:1: entryRuleBoolean returns [EObject current=null] : iv_ruleBoolean= ruleBoolean EOF ;
    public final EObject entryRuleBoolean() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBoolean = null;


        try {
            // InternalSimdaseParser.g:2485:2: (iv_ruleBoolean= ruleBoolean EOF )
            // InternalSimdaseParser.g:2486:2: iv_ruleBoolean= ruleBoolean EOF
            {
             newCompositeNode(grammarAccess.getBooleanRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleBoolean=ruleBoolean();

            state._fsp--;

             current =iv_ruleBoolean; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBoolean"


    // $ANTLR start "ruleBoolean"
    // InternalSimdaseParser.g:2493:1: ruleBoolean returns [EObject current=null] : ( ( (lv_value_0_0= True ) ) | ( (lv_value_1_0= False ) ) ) ;
    public final EObject ruleBoolean() throws RecognitionException {
        EObject current = null;

        Token lv_value_0_0=null;
        Token lv_value_1_0=null;

         enterRule(); 
            
        try {
            // InternalSimdaseParser.g:2496:28: ( ( ( (lv_value_0_0= True ) ) | ( (lv_value_1_0= False ) ) ) )
            // InternalSimdaseParser.g:2497:1: ( ( (lv_value_0_0= True ) ) | ( (lv_value_1_0= False ) ) )
            {
            // InternalSimdaseParser.g:2497:1: ( ( (lv_value_0_0= True ) ) | ( (lv_value_1_0= False ) ) )
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==True) ) {
                alt15=1;
            }
            else if ( (LA15_0==False) ) {
                alt15=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 15, 0, input);

                throw nvae;
            }
            switch (alt15) {
                case 1 :
                    // InternalSimdaseParser.g:2497:2: ( (lv_value_0_0= True ) )
                    {
                    // InternalSimdaseParser.g:2497:2: ( (lv_value_0_0= True ) )
                    // InternalSimdaseParser.g:2498:1: (lv_value_0_0= True )
                    {
                    // InternalSimdaseParser.g:2498:1: (lv_value_0_0= True )
                    // InternalSimdaseParser.g:2499:3: lv_value_0_0= True
                    {
                    lv_value_0_0=(Token)match(input,True,FollowSets000.FOLLOW_2); 

                            newLeafNode(lv_value_0_0, grammarAccess.getBooleanAccess().getValueTrueKeyword_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getBooleanRule());
                    	        }
                           		setWithLastConsumed(current, "value", lv_value_0_0, "true");
                    	    

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalSimdaseParser.g:2514:6: ( (lv_value_1_0= False ) )
                    {
                    // InternalSimdaseParser.g:2514:6: ( (lv_value_1_0= False ) )
                    // InternalSimdaseParser.g:2515:1: (lv_value_1_0= False )
                    {
                    // InternalSimdaseParser.g:2515:1: (lv_value_1_0= False )
                    // InternalSimdaseParser.g:2516:3: lv_value_1_0= False
                    {
                    lv_value_1_0=(Token)match(input,False,FollowSets000.FOLLOW_2); 

                            newLeafNode(lv_value_1_0, grammarAccess.getBooleanAccess().getValueFalseKeyword_1_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getBooleanRule());
                    	        }
                           		setWithLastConsumed(current, "value", lv_value_1_0, "false");
                    	    

                    }


                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBoolean"


    // $ANTLR start "entryRuleNumber"
    // InternalSimdaseParser.g:2538:1: entryRuleNumber returns [EObject current=null] : iv_ruleNumber= ruleNumber EOF ;
    public final EObject entryRuleNumber() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNumber = null;


        try {
            // InternalSimdaseParser.g:2539:2: (iv_ruleNumber= ruleNumber EOF )
            // InternalSimdaseParser.g:2540:2: iv_ruleNumber= ruleNumber EOF
            {
             newCompositeNode(grammarAccess.getNumberRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleNumber=ruleNumber();

            state._fsp--;

             current =iv_ruleNumber; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNumber"


    // $ANTLR start "ruleNumber"
    // InternalSimdaseParser.g:2547:1: ruleNumber returns [EObject current=null] : ( ( (lv_negative_0_0= HyphenMinus ) )? ( (lv_floatValue_1_0= RULE_FLOAT ) ) ) ;
    public final EObject ruleNumber() throws RecognitionException {
        EObject current = null;

        Token lv_negative_0_0=null;
        Token lv_floatValue_1_0=null;

         enterRule(); 
            
        try {
            // InternalSimdaseParser.g:2550:28: ( ( ( (lv_negative_0_0= HyphenMinus ) )? ( (lv_floatValue_1_0= RULE_FLOAT ) ) ) )
            // InternalSimdaseParser.g:2551:1: ( ( (lv_negative_0_0= HyphenMinus ) )? ( (lv_floatValue_1_0= RULE_FLOAT ) ) )
            {
            // InternalSimdaseParser.g:2551:1: ( ( (lv_negative_0_0= HyphenMinus ) )? ( (lv_floatValue_1_0= RULE_FLOAT ) ) )
            // InternalSimdaseParser.g:2551:2: ( (lv_negative_0_0= HyphenMinus ) )? ( (lv_floatValue_1_0= RULE_FLOAT ) )
            {
            // InternalSimdaseParser.g:2551:2: ( (lv_negative_0_0= HyphenMinus ) )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==HyphenMinus) ) {
                alt16=1;
            }
            switch (alt16) {
                case 1 :
                    // InternalSimdaseParser.g:2552:1: (lv_negative_0_0= HyphenMinus )
                    {
                    // InternalSimdaseParser.g:2552:1: (lv_negative_0_0= HyphenMinus )
                    // InternalSimdaseParser.g:2553:3: lv_negative_0_0= HyphenMinus
                    {
                    lv_negative_0_0=(Token)match(input,HyphenMinus,FollowSets000.FOLLOW_26); 

                            newLeafNode(lv_negative_0_0, grammarAccess.getNumberAccess().getNegativeHyphenMinusKeyword_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getNumberRule());
                    	        }
                           		setWithLastConsumed(current, "negative", lv_negative_0_0, "-");
                    	    

                    }


                    }
                    break;

            }

            // InternalSimdaseParser.g:2567:3: ( (lv_floatValue_1_0= RULE_FLOAT ) )
            // InternalSimdaseParser.g:2568:1: (lv_floatValue_1_0= RULE_FLOAT )
            {
            // InternalSimdaseParser.g:2568:1: (lv_floatValue_1_0= RULE_FLOAT )
            // InternalSimdaseParser.g:2569:3: lv_floatValue_1_0= RULE_FLOAT
            {
            lv_floatValue_1_0=(Token)match(input,RULE_FLOAT,FollowSets000.FOLLOW_2); 

            			newLeafNode(lv_floatValue_1_0, grammarAccess.getNumberAccess().getFloatValueFLOATTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getNumberRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"floatValue",
                    		lv_floatValue_1_0, 
                    		"edu.clemson.simdase.Simdase.FLOAT");
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNumber"


    // $ANTLR start "entryRuleStatement"
    // InternalSimdaseParser.g:2593:1: entryRuleStatement returns [EObject current=null] : iv_ruleStatement= ruleStatement EOF ;
    public final EObject entryRuleStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStatement = null;


        try {
            // InternalSimdaseParser.g:2594:2: (iv_ruleStatement= ruleStatement EOF )
            // InternalSimdaseParser.g:2595:2: iv_ruleStatement= ruleStatement EOF
            {
             newCompositeNode(grammarAccess.getStatementRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleStatement=ruleStatement();

            state._fsp--;

             current =iv_ruleStatement; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStatement"


    // $ANTLR start "ruleStatement"
    // InternalSimdaseParser.g:2602:1: ruleStatement returns [EObject current=null] : ( ( (lv_statement_0_0= ruleStatementLvl2 ) ) ( ( ( (lv_op_1_1= PlusSign | lv_op_1_2= HyphenMinus ) ) ) ( (lv_rest_2_0= ruleStatementLvl2 ) ) )* ) ;
    public final EObject ruleStatement() throws RecognitionException {
        EObject current = null;

        Token lv_op_1_1=null;
        Token lv_op_1_2=null;
        EObject lv_statement_0_0 = null;

        EObject lv_rest_2_0 = null;


         enterRule(); 
            
        try {
            // InternalSimdaseParser.g:2605:28: ( ( ( (lv_statement_0_0= ruleStatementLvl2 ) ) ( ( ( (lv_op_1_1= PlusSign | lv_op_1_2= HyphenMinus ) ) ) ( (lv_rest_2_0= ruleStatementLvl2 ) ) )* ) )
            // InternalSimdaseParser.g:2606:1: ( ( (lv_statement_0_0= ruleStatementLvl2 ) ) ( ( ( (lv_op_1_1= PlusSign | lv_op_1_2= HyphenMinus ) ) ) ( (lv_rest_2_0= ruleStatementLvl2 ) ) )* )
            {
            // InternalSimdaseParser.g:2606:1: ( ( (lv_statement_0_0= ruleStatementLvl2 ) ) ( ( ( (lv_op_1_1= PlusSign | lv_op_1_2= HyphenMinus ) ) ) ( (lv_rest_2_0= ruleStatementLvl2 ) ) )* )
            // InternalSimdaseParser.g:2606:2: ( (lv_statement_0_0= ruleStatementLvl2 ) ) ( ( ( (lv_op_1_1= PlusSign | lv_op_1_2= HyphenMinus ) ) ) ( (lv_rest_2_0= ruleStatementLvl2 ) ) )*
            {
            // InternalSimdaseParser.g:2606:2: ( (lv_statement_0_0= ruleStatementLvl2 ) )
            // InternalSimdaseParser.g:2607:1: (lv_statement_0_0= ruleStatementLvl2 )
            {
            // InternalSimdaseParser.g:2607:1: (lv_statement_0_0= ruleStatementLvl2 )
            // InternalSimdaseParser.g:2608:3: lv_statement_0_0= ruleStatementLvl2
            {
             
            	        newCompositeNode(grammarAccess.getStatementAccess().getStatementStatementLvl2ParserRuleCall_0_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_27);
            lv_statement_0_0=ruleStatementLvl2();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getStatementRule());
            	        }
                   		set(
                   			current, 
                   			"statement",
                    		lv_statement_0_0, 
                    		"edu.clemson.simdase.Simdase.StatementLvl2");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // InternalSimdaseParser.g:2624:2: ( ( ( (lv_op_1_1= PlusSign | lv_op_1_2= HyphenMinus ) ) ) ( (lv_rest_2_0= ruleStatementLvl2 ) ) )*
            loop18:
            do {
                int alt18=2;
                int LA18_0 = input.LA(1);

                if ( (LA18_0==PlusSign||LA18_0==HyphenMinus) ) {
                    alt18=1;
                }


                switch (alt18) {
            	case 1 :
            	    // InternalSimdaseParser.g:2624:3: ( ( (lv_op_1_1= PlusSign | lv_op_1_2= HyphenMinus ) ) ) ( (lv_rest_2_0= ruleStatementLvl2 ) )
            	    {
            	    // InternalSimdaseParser.g:2624:3: ( ( (lv_op_1_1= PlusSign | lv_op_1_2= HyphenMinus ) ) )
            	    // InternalSimdaseParser.g:2625:1: ( (lv_op_1_1= PlusSign | lv_op_1_2= HyphenMinus ) )
            	    {
            	    // InternalSimdaseParser.g:2625:1: ( (lv_op_1_1= PlusSign | lv_op_1_2= HyphenMinus ) )
            	    // InternalSimdaseParser.g:2626:1: (lv_op_1_1= PlusSign | lv_op_1_2= HyphenMinus )
            	    {
            	    // InternalSimdaseParser.g:2626:1: (lv_op_1_1= PlusSign | lv_op_1_2= HyphenMinus )
            	    int alt17=2;
            	    int LA17_0 = input.LA(1);

            	    if ( (LA17_0==PlusSign) ) {
            	        alt17=1;
            	    }
            	    else if ( (LA17_0==HyphenMinus) ) {
            	        alt17=2;
            	    }
            	    else {
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 17, 0, input);

            	        throw nvae;
            	    }
            	    switch (alt17) {
            	        case 1 :
            	            // InternalSimdaseParser.g:2627:3: lv_op_1_1= PlusSign
            	            {
            	            lv_op_1_1=(Token)match(input,PlusSign,FollowSets000.FOLLOW_8); 

            	                    newLeafNode(lv_op_1_1, grammarAccess.getStatementAccess().getOpPlusSignKeyword_1_0_0_0());
            	                

            	            	        if (current==null) {
            	            	            current = createModelElement(grammarAccess.getStatementRule());
            	            	        }
            	                   		addWithLastConsumed(current, "op", lv_op_1_1, null);
            	            	    

            	            }
            	            break;
            	        case 2 :
            	            // InternalSimdaseParser.g:2640:8: lv_op_1_2= HyphenMinus
            	            {
            	            lv_op_1_2=(Token)match(input,HyphenMinus,FollowSets000.FOLLOW_8); 

            	                    newLeafNode(lv_op_1_2, grammarAccess.getStatementAccess().getOpHyphenMinusKeyword_1_0_0_1());
            	                

            	            	        if (current==null) {
            	            	            current = createModelElement(grammarAccess.getStatementRule());
            	            	        }
            	                   		addWithLastConsumed(current, "op", lv_op_1_2, null);
            	            	    

            	            }
            	            break;

            	    }


            	    }


            	    }

            	    // InternalSimdaseParser.g:2656:2: ( (lv_rest_2_0= ruleStatementLvl2 ) )
            	    // InternalSimdaseParser.g:2657:1: (lv_rest_2_0= ruleStatementLvl2 )
            	    {
            	    // InternalSimdaseParser.g:2657:1: (lv_rest_2_0= ruleStatementLvl2 )
            	    // InternalSimdaseParser.g:2658:3: lv_rest_2_0= ruleStatementLvl2
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getStatementAccess().getRestStatementLvl2ParserRuleCall_1_1_0()); 
            	    	    
            	    pushFollow(FollowSets000.FOLLOW_27);
            	    lv_rest_2_0=ruleStatementLvl2();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getStatementRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"rest",
            	            		lv_rest_2_0, 
            	            		"edu.clemson.simdase.Simdase.StatementLvl2");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop18;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStatement"


    // $ANTLR start "entryRuleStatementLvl2"
    // InternalSimdaseParser.g:2682:1: entryRuleStatementLvl2 returns [EObject current=null] : iv_ruleStatementLvl2= ruleStatementLvl2 EOF ;
    public final EObject entryRuleStatementLvl2() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStatementLvl2 = null;


        try {
            // InternalSimdaseParser.g:2683:2: (iv_ruleStatementLvl2= ruleStatementLvl2 EOF )
            // InternalSimdaseParser.g:2684:2: iv_ruleStatementLvl2= ruleStatementLvl2 EOF
            {
             newCompositeNode(grammarAccess.getStatementLvl2Rule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleStatementLvl2=ruleStatementLvl2();

            state._fsp--;

             current =iv_ruleStatementLvl2; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStatementLvl2"


    // $ANTLR start "ruleStatementLvl2"
    // InternalSimdaseParser.g:2691:1: ruleStatementLvl2 returns [EObject current=null] : ( ( (lv_statement_0_0= ruleStatementLvl3 ) ) ( ( ( (lv_op_1_1= Asterisk | lv_op_1_2= Solidus | lv_op_1_3= PercentSign ) ) ) ( (lv_rest_2_0= ruleStatementLvl3 ) ) )* ) ;
    public final EObject ruleStatementLvl2() throws RecognitionException {
        EObject current = null;

        Token lv_op_1_1=null;
        Token lv_op_1_2=null;
        Token lv_op_1_3=null;
        EObject lv_statement_0_0 = null;

        EObject lv_rest_2_0 = null;


         enterRule(); 
            
        try {
            // InternalSimdaseParser.g:2694:28: ( ( ( (lv_statement_0_0= ruleStatementLvl3 ) ) ( ( ( (lv_op_1_1= Asterisk | lv_op_1_2= Solidus | lv_op_1_3= PercentSign ) ) ) ( (lv_rest_2_0= ruleStatementLvl3 ) ) )* ) )
            // InternalSimdaseParser.g:2695:1: ( ( (lv_statement_0_0= ruleStatementLvl3 ) ) ( ( ( (lv_op_1_1= Asterisk | lv_op_1_2= Solidus | lv_op_1_3= PercentSign ) ) ) ( (lv_rest_2_0= ruleStatementLvl3 ) ) )* )
            {
            // InternalSimdaseParser.g:2695:1: ( ( (lv_statement_0_0= ruleStatementLvl3 ) ) ( ( ( (lv_op_1_1= Asterisk | lv_op_1_2= Solidus | lv_op_1_3= PercentSign ) ) ) ( (lv_rest_2_0= ruleStatementLvl3 ) ) )* )
            // InternalSimdaseParser.g:2695:2: ( (lv_statement_0_0= ruleStatementLvl3 ) ) ( ( ( (lv_op_1_1= Asterisk | lv_op_1_2= Solidus | lv_op_1_3= PercentSign ) ) ) ( (lv_rest_2_0= ruleStatementLvl3 ) ) )*
            {
            // InternalSimdaseParser.g:2695:2: ( (lv_statement_0_0= ruleStatementLvl3 ) )
            // InternalSimdaseParser.g:2696:1: (lv_statement_0_0= ruleStatementLvl3 )
            {
            // InternalSimdaseParser.g:2696:1: (lv_statement_0_0= ruleStatementLvl3 )
            // InternalSimdaseParser.g:2697:3: lv_statement_0_0= ruleStatementLvl3
            {
             
            	        newCompositeNode(grammarAccess.getStatementLvl2Access().getStatementStatementLvl3ParserRuleCall_0_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_28);
            lv_statement_0_0=ruleStatementLvl3();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getStatementLvl2Rule());
            	        }
                   		set(
                   			current, 
                   			"statement",
                    		lv_statement_0_0, 
                    		"edu.clemson.simdase.Simdase.StatementLvl3");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // InternalSimdaseParser.g:2713:2: ( ( ( (lv_op_1_1= Asterisk | lv_op_1_2= Solidus | lv_op_1_3= PercentSign ) ) ) ( (lv_rest_2_0= ruleStatementLvl3 ) ) )*
            loop20:
            do {
                int alt20=2;
                int LA20_0 = input.LA(1);

                if ( (LA20_0==PercentSign||LA20_0==Asterisk||LA20_0==Solidus) ) {
                    alt20=1;
                }


                switch (alt20) {
            	case 1 :
            	    // InternalSimdaseParser.g:2713:3: ( ( (lv_op_1_1= Asterisk | lv_op_1_2= Solidus | lv_op_1_3= PercentSign ) ) ) ( (lv_rest_2_0= ruleStatementLvl3 ) )
            	    {
            	    // InternalSimdaseParser.g:2713:3: ( ( (lv_op_1_1= Asterisk | lv_op_1_2= Solidus | lv_op_1_3= PercentSign ) ) )
            	    // InternalSimdaseParser.g:2714:1: ( (lv_op_1_1= Asterisk | lv_op_1_2= Solidus | lv_op_1_3= PercentSign ) )
            	    {
            	    // InternalSimdaseParser.g:2714:1: ( (lv_op_1_1= Asterisk | lv_op_1_2= Solidus | lv_op_1_3= PercentSign ) )
            	    // InternalSimdaseParser.g:2715:1: (lv_op_1_1= Asterisk | lv_op_1_2= Solidus | lv_op_1_3= PercentSign )
            	    {
            	    // InternalSimdaseParser.g:2715:1: (lv_op_1_1= Asterisk | lv_op_1_2= Solidus | lv_op_1_3= PercentSign )
            	    int alt19=3;
            	    switch ( input.LA(1) ) {
            	    case Asterisk:
            	        {
            	        alt19=1;
            	        }
            	        break;
            	    case Solidus:
            	        {
            	        alt19=2;
            	        }
            	        break;
            	    case PercentSign:
            	        {
            	        alt19=3;
            	        }
            	        break;
            	    default:
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 19, 0, input);

            	        throw nvae;
            	    }

            	    switch (alt19) {
            	        case 1 :
            	            // InternalSimdaseParser.g:2716:3: lv_op_1_1= Asterisk
            	            {
            	            lv_op_1_1=(Token)match(input,Asterisk,FollowSets000.FOLLOW_8); 

            	                    newLeafNode(lv_op_1_1, grammarAccess.getStatementLvl2Access().getOpAsteriskKeyword_1_0_0_0());
            	                

            	            	        if (current==null) {
            	            	            current = createModelElement(grammarAccess.getStatementLvl2Rule());
            	            	        }
            	                   		addWithLastConsumed(current, "op", lv_op_1_1, null);
            	            	    

            	            }
            	            break;
            	        case 2 :
            	            // InternalSimdaseParser.g:2729:8: lv_op_1_2= Solidus
            	            {
            	            lv_op_1_2=(Token)match(input,Solidus,FollowSets000.FOLLOW_8); 

            	                    newLeafNode(lv_op_1_2, grammarAccess.getStatementLvl2Access().getOpSolidusKeyword_1_0_0_1());
            	                

            	            	        if (current==null) {
            	            	            current = createModelElement(grammarAccess.getStatementLvl2Rule());
            	            	        }
            	                   		addWithLastConsumed(current, "op", lv_op_1_2, null);
            	            	    

            	            }
            	            break;
            	        case 3 :
            	            // InternalSimdaseParser.g:2742:8: lv_op_1_3= PercentSign
            	            {
            	            lv_op_1_3=(Token)match(input,PercentSign,FollowSets000.FOLLOW_8); 

            	                    newLeafNode(lv_op_1_3, grammarAccess.getStatementLvl2Access().getOpPercentSignKeyword_1_0_0_2());
            	                

            	            	        if (current==null) {
            	            	            current = createModelElement(grammarAccess.getStatementLvl2Rule());
            	            	        }
            	                   		addWithLastConsumed(current, "op", lv_op_1_3, null);
            	            	    

            	            }
            	            break;

            	    }


            	    }


            	    }

            	    // InternalSimdaseParser.g:2758:2: ( (lv_rest_2_0= ruleStatementLvl3 ) )
            	    // InternalSimdaseParser.g:2759:1: (lv_rest_2_0= ruleStatementLvl3 )
            	    {
            	    // InternalSimdaseParser.g:2759:1: (lv_rest_2_0= ruleStatementLvl3 )
            	    // InternalSimdaseParser.g:2760:3: lv_rest_2_0= ruleStatementLvl3
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getStatementLvl2Access().getRestStatementLvl3ParserRuleCall_1_1_0()); 
            	    	    
            	    pushFollow(FollowSets000.FOLLOW_28);
            	    lv_rest_2_0=ruleStatementLvl3();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getStatementLvl2Rule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"rest",
            	            		lv_rest_2_0, 
            	            		"edu.clemson.simdase.Simdase.StatementLvl3");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop20;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStatementLvl2"


    // $ANTLR start "entryRuleStatementLvl3"
    // InternalSimdaseParser.g:2784:1: entryRuleStatementLvl3 returns [EObject current=null] : iv_ruleStatementLvl3= ruleStatementLvl3 EOF ;
    public final EObject entryRuleStatementLvl3() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStatementLvl3 = null;


        try {
            // InternalSimdaseParser.g:2785:2: (iv_ruleStatementLvl3= ruleStatementLvl3 EOF )
            // InternalSimdaseParser.g:2786:2: iv_ruleStatementLvl3= ruleStatementLvl3 EOF
            {
             newCompositeNode(grammarAccess.getStatementLvl3Rule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleStatementLvl3=ruleStatementLvl3();

            state._fsp--;

             current =iv_ruleStatementLvl3; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStatementLvl3"


    // $ANTLR start "ruleStatementLvl3"
    // InternalSimdaseParser.g:2793:1: ruleStatementLvl3 returns [EObject current=null] : ( ( (lv_statement_0_0= ruleStatementLvl4 ) ) ( ( (lv_op_1_0= AsteriskAsterisk ) ) ( (lv_rest_2_0= ruleStatementLvl4 ) ) )* ) ;
    public final EObject ruleStatementLvl3() throws RecognitionException {
        EObject current = null;

        Token lv_op_1_0=null;
        EObject lv_statement_0_0 = null;

        EObject lv_rest_2_0 = null;


         enterRule(); 
            
        try {
            // InternalSimdaseParser.g:2796:28: ( ( ( (lv_statement_0_0= ruleStatementLvl4 ) ) ( ( (lv_op_1_0= AsteriskAsterisk ) ) ( (lv_rest_2_0= ruleStatementLvl4 ) ) )* ) )
            // InternalSimdaseParser.g:2797:1: ( ( (lv_statement_0_0= ruleStatementLvl4 ) ) ( ( (lv_op_1_0= AsteriskAsterisk ) ) ( (lv_rest_2_0= ruleStatementLvl4 ) ) )* )
            {
            // InternalSimdaseParser.g:2797:1: ( ( (lv_statement_0_0= ruleStatementLvl4 ) ) ( ( (lv_op_1_0= AsteriskAsterisk ) ) ( (lv_rest_2_0= ruleStatementLvl4 ) ) )* )
            // InternalSimdaseParser.g:2797:2: ( (lv_statement_0_0= ruleStatementLvl4 ) ) ( ( (lv_op_1_0= AsteriskAsterisk ) ) ( (lv_rest_2_0= ruleStatementLvl4 ) ) )*
            {
            // InternalSimdaseParser.g:2797:2: ( (lv_statement_0_0= ruleStatementLvl4 ) )
            // InternalSimdaseParser.g:2798:1: (lv_statement_0_0= ruleStatementLvl4 )
            {
            // InternalSimdaseParser.g:2798:1: (lv_statement_0_0= ruleStatementLvl4 )
            // InternalSimdaseParser.g:2799:3: lv_statement_0_0= ruleStatementLvl4
            {
             
            	        newCompositeNode(grammarAccess.getStatementLvl3Access().getStatementStatementLvl4ParserRuleCall_0_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_29);
            lv_statement_0_0=ruleStatementLvl4();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getStatementLvl3Rule());
            	        }
                   		set(
                   			current, 
                   			"statement",
                    		lv_statement_0_0, 
                    		"edu.clemson.simdase.Simdase.StatementLvl4");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // InternalSimdaseParser.g:2815:2: ( ( (lv_op_1_0= AsteriskAsterisk ) ) ( (lv_rest_2_0= ruleStatementLvl4 ) ) )*
            loop21:
            do {
                int alt21=2;
                int LA21_0 = input.LA(1);

                if ( (LA21_0==AsteriskAsterisk) ) {
                    alt21=1;
                }


                switch (alt21) {
            	case 1 :
            	    // InternalSimdaseParser.g:2815:3: ( (lv_op_1_0= AsteriskAsterisk ) ) ( (lv_rest_2_0= ruleStatementLvl4 ) )
            	    {
            	    // InternalSimdaseParser.g:2815:3: ( (lv_op_1_0= AsteriskAsterisk ) )
            	    // InternalSimdaseParser.g:2816:1: (lv_op_1_0= AsteriskAsterisk )
            	    {
            	    // InternalSimdaseParser.g:2816:1: (lv_op_1_0= AsteriskAsterisk )
            	    // InternalSimdaseParser.g:2817:3: lv_op_1_0= AsteriskAsterisk
            	    {
            	    lv_op_1_0=(Token)match(input,AsteriskAsterisk,FollowSets000.FOLLOW_8); 

            	            newLeafNode(lv_op_1_0, grammarAccess.getStatementLvl3Access().getOpAsteriskAsteriskKeyword_1_0_0());
            	        

            	    	        if (current==null) {
            	    	            current = createModelElement(grammarAccess.getStatementLvl3Rule());
            	    	        }
            	           		addWithLastConsumed(current, "op", lv_op_1_0, "**");
            	    	    

            	    }


            	    }

            	    // InternalSimdaseParser.g:2831:2: ( (lv_rest_2_0= ruleStatementLvl4 ) )
            	    // InternalSimdaseParser.g:2832:1: (lv_rest_2_0= ruleStatementLvl4 )
            	    {
            	    // InternalSimdaseParser.g:2832:1: (lv_rest_2_0= ruleStatementLvl4 )
            	    // InternalSimdaseParser.g:2833:3: lv_rest_2_0= ruleStatementLvl4
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getStatementLvl3Access().getRestStatementLvl4ParserRuleCall_1_1_0()); 
            	    	    
            	    pushFollow(FollowSets000.FOLLOW_29);
            	    lv_rest_2_0=ruleStatementLvl4();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getStatementLvl3Rule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"rest",
            	            		lv_rest_2_0, 
            	            		"edu.clemson.simdase.Simdase.StatementLvl4");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop21;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStatementLvl3"


    // $ANTLR start "entryRuleStatementLvl4"
    // InternalSimdaseParser.g:2857:1: entryRuleStatementLvl4 returns [EObject current=null] : iv_ruleStatementLvl4= ruleStatementLvl4 EOF ;
    public final EObject entryRuleStatementLvl4() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStatementLvl4 = null;


        try {
            // InternalSimdaseParser.g:2858:2: (iv_ruleStatementLvl4= ruleStatementLvl4 EOF )
            // InternalSimdaseParser.g:2859:2: iv_ruleStatementLvl4= ruleStatementLvl4 EOF
            {
             newCompositeNode(grammarAccess.getStatementLvl4Rule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleStatementLvl4=ruleStatementLvl4();

            state._fsp--;

             current =iv_ruleStatementLvl4; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStatementLvl4"


    // $ANTLR start "ruleStatementLvl4"
    // InternalSimdaseParser.g:2866:1: ruleStatementLvl4 returns [EObject current=null] : ( ( () ( (lv_value_1_0= ruleNumber ) ) ) | ( () otherlv_3= T ) | ( () otherlv_5= I ) | ( () otherlv_7= Sin ( (lv_statement_8_0= ruleStatement ) ) otherlv_9= RightParenthesis ) | ( () otherlv_11= Cos ( (lv_statement_12_0= ruleStatement ) ) otherlv_13= RightParenthesis ) | ( () otherlv_15= Tan ( (lv_statement_16_0= ruleStatement ) ) otherlv_17= RightParenthesis ) | ( () otherlv_19= LeftParenthesis ( (lv_statement_20_0= ruleStatement ) ) otherlv_21= RightParenthesis ) | ( () otherlv_23= If ( (lv_booleanStatement_24_0= ruleBooleanStatement ) ) otherlv_25= RightParenthesis otherlv_26= LeftCurlyBracket ( (lv_trueStatement_27_0= ruleStatement ) ) otherlv_28= RightCurlyBracket (otherlv_29= Else otherlv_30= LeftCurlyBracket ( (lv_falseStatement_31_0= ruleStatement ) ) otherlv_32= RightCurlyBracket )? ) | ( () otherlv_34= Random ) | ( () otherlv_36= S ) ) ;
    public final EObject ruleStatementLvl4() throws RecognitionException {
        EObject current = null;

        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        Token otherlv_13=null;
        Token otherlv_15=null;
        Token otherlv_17=null;
        Token otherlv_19=null;
        Token otherlv_21=null;
        Token otherlv_23=null;
        Token otherlv_25=null;
        Token otherlv_26=null;
        Token otherlv_28=null;
        Token otherlv_29=null;
        Token otherlv_30=null;
        Token otherlv_32=null;
        Token otherlv_34=null;
        Token otherlv_36=null;
        EObject lv_value_1_0 = null;

        EObject lv_statement_8_0 = null;

        EObject lv_statement_12_0 = null;

        EObject lv_statement_16_0 = null;

        EObject lv_statement_20_0 = null;

        EObject lv_booleanStatement_24_0 = null;

        EObject lv_trueStatement_27_0 = null;

        EObject lv_falseStatement_31_0 = null;


         enterRule(); 
            
        try {
            // InternalSimdaseParser.g:2869:28: ( ( ( () ( (lv_value_1_0= ruleNumber ) ) ) | ( () otherlv_3= T ) | ( () otherlv_5= I ) | ( () otherlv_7= Sin ( (lv_statement_8_0= ruleStatement ) ) otherlv_9= RightParenthesis ) | ( () otherlv_11= Cos ( (lv_statement_12_0= ruleStatement ) ) otherlv_13= RightParenthesis ) | ( () otherlv_15= Tan ( (lv_statement_16_0= ruleStatement ) ) otherlv_17= RightParenthesis ) | ( () otherlv_19= LeftParenthesis ( (lv_statement_20_0= ruleStatement ) ) otherlv_21= RightParenthesis ) | ( () otherlv_23= If ( (lv_booleanStatement_24_0= ruleBooleanStatement ) ) otherlv_25= RightParenthesis otherlv_26= LeftCurlyBracket ( (lv_trueStatement_27_0= ruleStatement ) ) otherlv_28= RightCurlyBracket (otherlv_29= Else otherlv_30= LeftCurlyBracket ( (lv_falseStatement_31_0= ruleStatement ) ) otherlv_32= RightCurlyBracket )? ) | ( () otherlv_34= Random ) | ( () otherlv_36= S ) ) )
            // InternalSimdaseParser.g:2870:1: ( ( () ( (lv_value_1_0= ruleNumber ) ) ) | ( () otherlv_3= T ) | ( () otherlv_5= I ) | ( () otherlv_7= Sin ( (lv_statement_8_0= ruleStatement ) ) otherlv_9= RightParenthesis ) | ( () otherlv_11= Cos ( (lv_statement_12_0= ruleStatement ) ) otherlv_13= RightParenthesis ) | ( () otherlv_15= Tan ( (lv_statement_16_0= ruleStatement ) ) otherlv_17= RightParenthesis ) | ( () otherlv_19= LeftParenthesis ( (lv_statement_20_0= ruleStatement ) ) otherlv_21= RightParenthesis ) | ( () otherlv_23= If ( (lv_booleanStatement_24_0= ruleBooleanStatement ) ) otherlv_25= RightParenthesis otherlv_26= LeftCurlyBracket ( (lv_trueStatement_27_0= ruleStatement ) ) otherlv_28= RightCurlyBracket (otherlv_29= Else otherlv_30= LeftCurlyBracket ( (lv_falseStatement_31_0= ruleStatement ) ) otherlv_32= RightCurlyBracket )? ) | ( () otherlv_34= Random ) | ( () otherlv_36= S ) )
            {
            // InternalSimdaseParser.g:2870:1: ( ( () ( (lv_value_1_0= ruleNumber ) ) ) | ( () otherlv_3= T ) | ( () otherlv_5= I ) | ( () otherlv_7= Sin ( (lv_statement_8_0= ruleStatement ) ) otherlv_9= RightParenthesis ) | ( () otherlv_11= Cos ( (lv_statement_12_0= ruleStatement ) ) otherlv_13= RightParenthesis ) | ( () otherlv_15= Tan ( (lv_statement_16_0= ruleStatement ) ) otherlv_17= RightParenthesis ) | ( () otherlv_19= LeftParenthesis ( (lv_statement_20_0= ruleStatement ) ) otherlv_21= RightParenthesis ) | ( () otherlv_23= If ( (lv_booleanStatement_24_0= ruleBooleanStatement ) ) otherlv_25= RightParenthesis otherlv_26= LeftCurlyBracket ( (lv_trueStatement_27_0= ruleStatement ) ) otherlv_28= RightCurlyBracket (otherlv_29= Else otherlv_30= LeftCurlyBracket ( (lv_falseStatement_31_0= ruleStatement ) ) otherlv_32= RightCurlyBracket )? ) | ( () otherlv_34= Random ) | ( () otherlv_36= S ) )
            int alt23=10;
            switch ( input.LA(1) ) {
            case HyphenMinus:
            case RULE_FLOAT:
                {
                alt23=1;
                }
                break;
            case T:
                {
                alt23=2;
                }
                break;
            case I:
                {
                alt23=3;
                }
                break;
            case Sin:
                {
                alt23=4;
                }
                break;
            case Cos:
                {
                alt23=5;
                }
                break;
            case Tan:
                {
                alt23=6;
                }
                break;
            case LeftParenthesis:
                {
                alt23=7;
                }
                break;
            case If:
                {
                alt23=8;
                }
                break;
            case Random:
                {
                alt23=9;
                }
                break;
            case S:
                {
                alt23=10;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 23, 0, input);

                throw nvae;
            }

            switch (alt23) {
                case 1 :
                    // InternalSimdaseParser.g:2870:2: ( () ( (lv_value_1_0= ruleNumber ) ) )
                    {
                    // InternalSimdaseParser.g:2870:2: ( () ( (lv_value_1_0= ruleNumber ) ) )
                    // InternalSimdaseParser.g:2870:3: () ( (lv_value_1_0= ruleNumber ) )
                    {
                    // InternalSimdaseParser.g:2870:3: ()
                    // InternalSimdaseParser.g:2871:5: 
                    {

                            current = forceCreateModelElement(
                                grammarAccess.getStatementLvl4Access().getIntegerAction_0_0(),
                                current);
                        

                    }

                    // InternalSimdaseParser.g:2876:2: ( (lv_value_1_0= ruleNumber ) )
                    // InternalSimdaseParser.g:2877:1: (lv_value_1_0= ruleNumber )
                    {
                    // InternalSimdaseParser.g:2877:1: (lv_value_1_0= ruleNumber )
                    // InternalSimdaseParser.g:2878:3: lv_value_1_0= ruleNumber
                    {
                     
                    	        newCompositeNode(grammarAccess.getStatementLvl4Access().getValueNumberParserRuleCall_0_1_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_value_1_0=ruleNumber();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getStatementLvl4Rule());
                    	        }
                           		set(
                           			current, 
                           			"value",
                            		lv_value_1_0, 
                            		"edu.clemson.simdase.Simdase.Number");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalSimdaseParser.g:2895:6: ( () otherlv_3= T )
                    {
                    // InternalSimdaseParser.g:2895:6: ( () otherlv_3= T )
                    // InternalSimdaseParser.g:2895:7: () otherlv_3= T
                    {
                    // InternalSimdaseParser.g:2895:7: ()
                    // InternalSimdaseParser.g:2896:5: 
                    {

                            current = forceCreateModelElement(
                                grammarAccess.getStatementLvl4Access().getTAction_1_0(),
                                current);
                        

                    }

                    otherlv_3=(Token)match(input,T,FollowSets000.FOLLOW_2); 

                        	newLeafNode(otherlv_3, grammarAccess.getStatementLvl4Access().getTKeyword_1_1());
                        

                    }


                    }
                    break;
                case 3 :
                    // InternalSimdaseParser.g:2907:6: ( () otherlv_5= I )
                    {
                    // InternalSimdaseParser.g:2907:6: ( () otherlv_5= I )
                    // InternalSimdaseParser.g:2907:7: () otherlv_5= I
                    {
                    // InternalSimdaseParser.g:2907:7: ()
                    // InternalSimdaseParser.g:2908:5: 
                    {

                            current = forceCreateModelElement(
                                grammarAccess.getStatementLvl4Access().getIAction_2_0(),
                                current);
                        

                    }

                    otherlv_5=(Token)match(input,I,FollowSets000.FOLLOW_2); 

                        	newLeafNode(otherlv_5, grammarAccess.getStatementLvl4Access().getIKeyword_2_1());
                        

                    }


                    }
                    break;
                case 4 :
                    // InternalSimdaseParser.g:2919:6: ( () otherlv_7= Sin ( (lv_statement_8_0= ruleStatement ) ) otherlv_9= RightParenthesis )
                    {
                    // InternalSimdaseParser.g:2919:6: ( () otherlv_7= Sin ( (lv_statement_8_0= ruleStatement ) ) otherlv_9= RightParenthesis )
                    // InternalSimdaseParser.g:2919:7: () otherlv_7= Sin ( (lv_statement_8_0= ruleStatement ) ) otherlv_9= RightParenthesis
                    {
                    // InternalSimdaseParser.g:2919:7: ()
                    // InternalSimdaseParser.g:2920:5: 
                    {

                            current = forceCreateModelElement(
                                grammarAccess.getStatementLvl4Access().getSinAction_3_0(),
                                current);
                        

                    }

                    otherlv_7=(Token)match(input,Sin,FollowSets000.FOLLOW_8); 

                        	newLeafNode(otherlv_7, grammarAccess.getStatementLvl4Access().getSinKeyword_3_1());
                        
                    // InternalSimdaseParser.g:2930:1: ( (lv_statement_8_0= ruleStatement ) )
                    // InternalSimdaseParser.g:2931:1: (lv_statement_8_0= ruleStatement )
                    {
                    // InternalSimdaseParser.g:2931:1: (lv_statement_8_0= ruleStatement )
                    // InternalSimdaseParser.g:2932:3: lv_statement_8_0= ruleStatement
                    {
                     
                    	        newCompositeNode(grammarAccess.getStatementLvl4Access().getStatementStatementParserRuleCall_3_2_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_30);
                    lv_statement_8_0=ruleStatement();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getStatementLvl4Rule());
                    	        }
                           		set(
                           			current, 
                           			"statement",
                            		lv_statement_8_0, 
                            		"edu.clemson.simdase.Simdase.Statement");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    otherlv_9=(Token)match(input,RightParenthesis,FollowSets000.FOLLOW_2); 

                        	newLeafNode(otherlv_9, grammarAccess.getStatementLvl4Access().getRightParenthesisKeyword_3_3());
                        

                    }


                    }
                    break;
                case 5 :
                    // InternalSimdaseParser.g:2954:6: ( () otherlv_11= Cos ( (lv_statement_12_0= ruleStatement ) ) otherlv_13= RightParenthesis )
                    {
                    // InternalSimdaseParser.g:2954:6: ( () otherlv_11= Cos ( (lv_statement_12_0= ruleStatement ) ) otherlv_13= RightParenthesis )
                    // InternalSimdaseParser.g:2954:7: () otherlv_11= Cos ( (lv_statement_12_0= ruleStatement ) ) otherlv_13= RightParenthesis
                    {
                    // InternalSimdaseParser.g:2954:7: ()
                    // InternalSimdaseParser.g:2955:5: 
                    {

                            current = forceCreateModelElement(
                                grammarAccess.getStatementLvl4Access().getCosAction_4_0(),
                                current);
                        

                    }

                    otherlv_11=(Token)match(input,Cos,FollowSets000.FOLLOW_8); 

                        	newLeafNode(otherlv_11, grammarAccess.getStatementLvl4Access().getCosKeyword_4_1());
                        
                    // InternalSimdaseParser.g:2965:1: ( (lv_statement_12_0= ruleStatement ) )
                    // InternalSimdaseParser.g:2966:1: (lv_statement_12_0= ruleStatement )
                    {
                    // InternalSimdaseParser.g:2966:1: (lv_statement_12_0= ruleStatement )
                    // InternalSimdaseParser.g:2967:3: lv_statement_12_0= ruleStatement
                    {
                     
                    	        newCompositeNode(grammarAccess.getStatementLvl4Access().getStatementStatementParserRuleCall_4_2_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_30);
                    lv_statement_12_0=ruleStatement();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getStatementLvl4Rule());
                    	        }
                           		set(
                           			current, 
                           			"statement",
                            		lv_statement_12_0, 
                            		"edu.clemson.simdase.Simdase.Statement");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    otherlv_13=(Token)match(input,RightParenthesis,FollowSets000.FOLLOW_2); 

                        	newLeafNode(otherlv_13, grammarAccess.getStatementLvl4Access().getRightParenthesisKeyword_4_3());
                        

                    }


                    }
                    break;
                case 6 :
                    // InternalSimdaseParser.g:2989:6: ( () otherlv_15= Tan ( (lv_statement_16_0= ruleStatement ) ) otherlv_17= RightParenthesis )
                    {
                    // InternalSimdaseParser.g:2989:6: ( () otherlv_15= Tan ( (lv_statement_16_0= ruleStatement ) ) otherlv_17= RightParenthesis )
                    // InternalSimdaseParser.g:2989:7: () otherlv_15= Tan ( (lv_statement_16_0= ruleStatement ) ) otherlv_17= RightParenthesis
                    {
                    // InternalSimdaseParser.g:2989:7: ()
                    // InternalSimdaseParser.g:2990:5: 
                    {

                            current = forceCreateModelElement(
                                grammarAccess.getStatementLvl4Access().getTanAction_5_0(),
                                current);
                        

                    }

                    otherlv_15=(Token)match(input,Tan,FollowSets000.FOLLOW_8); 

                        	newLeafNode(otherlv_15, grammarAccess.getStatementLvl4Access().getTanKeyword_5_1());
                        
                    // InternalSimdaseParser.g:3000:1: ( (lv_statement_16_0= ruleStatement ) )
                    // InternalSimdaseParser.g:3001:1: (lv_statement_16_0= ruleStatement )
                    {
                    // InternalSimdaseParser.g:3001:1: (lv_statement_16_0= ruleStatement )
                    // InternalSimdaseParser.g:3002:3: lv_statement_16_0= ruleStatement
                    {
                     
                    	        newCompositeNode(grammarAccess.getStatementLvl4Access().getStatementStatementParserRuleCall_5_2_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_30);
                    lv_statement_16_0=ruleStatement();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getStatementLvl4Rule());
                    	        }
                           		set(
                           			current, 
                           			"statement",
                            		lv_statement_16_0, 
                            		"edu.clemson.simdase.Simdase.Statement");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    otherlv_17=(Token)match(input,RightParenthesis,FollowSets000.FOLLOW_2); 

                        	newLeafNode(otherlv_17, grammarAccess.getStatementLvl4Access().getRightParenthesisKeyword_5_3());
                        

                    }


                    }
                    break;
                case 7 :
                    // InternalSimdaseParser.g:3024:6: ( () otherlv_19= LeftParenthesis ( (lv_statement_20_0= ruleStatement ) ) otherlv_21= RightParenthesis )
                    {
                    // InternalSimdaseParser.g:3024:6: ( () otherlv_19= LeftParenthesis ( (lv_statement_20_0= ruleStatement ) ) otherlv_21= RightParenthesis )
                    // InternalSimdaseParser.g:3024:7: () otherlv_19= LeftParenthesis ( (lv_statement_20_0= ruleStatement ) ) otherlv_21= RightParenthesis
                    {
                    // InternalSimdaseParser.g:3024:7: ()
                    // InternalSimdaseParser.g:3025:5: 
                    {

                            current = forceCreateModelElement(
                                grammarAccess.getStatementLvl4Access().getParenAction_6_0(),
                                current);
                        

                    }

                    otherlv_19=(Token)match(input,LeftParenthesis,FollowSets000.FOLLOW_8); 

                        	newLeafNode(otherlv_19, grammarAccess.getStatementLvl4Access().getLeftParenthesisKeyword_6_1());
                        
                    // InternalSimdaseParser.g:3035:1: ( (lv_statement_20_0= ruleStatement ) )
                    // InternalSimdaseParser.g:3036:1: (lv_statement_20_0= ruleStatement )
                    {
                    // InternalSimdaseParser.g:3036:1: (lv_statement_20_0= ruleStatement )
                    // InternalSimdaseParser.g:3037:3: lv_statement_20_0= ruleStatement
                    {
                     
                    	        newCompositeNode(grammarAccess.getStatementLvl4Access().getStatementStatementParserRuleCall_6_2_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_30);
                    lv_statement_20_0=ruleStatement();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getStatementLvl4Rule());
                    	        }
                           		set(
                           			current, 
                           			"statement",
                            		lv_statement_20_0, 
                            		"edu.clemson.simdase.Simdase.Statement");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    otherlv_21=(Token)match(input,RightParenthesis,FollowSets000.FOLLOW_2); 

                        	newLeafNode(otherlv_21, grammarAccess.getStatementLvl4Access().getRightParenthesisKeyword_6_3());
                        

                    }


                    }
                    break;
                case 8 :
                    // InternalSimdaseParser.g:3059:6: ( () otherlv_23= If ( (lv_booleanStatement_24_0= ruleBooleanStatement ) ) otherlv_25= RightParenthesis otherlv_26= LeftCurlyBracket ( (lv_trueStatement_27_0= ruleStatement ) ) otherlv_28= RightCurlyBracket (otherlv_29= Else otherlv_30= LeftCurlyBracket ( (lv_falseStatement_31_0= ruleStatement ) ) otherlv_32= RightCurlyBracket )? )
                    {
                    // InternalSimdaseParser.g:3059:6: ( () otherlv_23= If ( (lv_booleanStatement_24_0= ruleBooleanStatement ) ) otherlv_25= RightParenthesis otherlv_26= LeftCurlyBracket ( (lv_trueStatement_27_0= ruleStatement ) ) otherlv_28= RightCurlyBracket (otherlv_29= Else otherlv_30= LeftCurlyBracket ( (lv_falseStatement_31_0= ruleStatement ) ) otherlv_32= RightCurlyBracket )? )
                    // InternalSimdaseParser.g:3059:7: () otherlv_23= If ( (lv_booleanStatement_24_0= ruleBooleanStatement ) ) otherlv_25= RightParenthesis otherlv_26= LeftCurlyBracket ( (lv_trueStatement_27_0= ruleStatement ) ) otherlv_28= RightCurlyBracket (otherlv_29= Else otherlv_30= LeftCurlyBracket ( (lv_falseStatement_31_0= ruleStatement ) ) otherlv_32= RightCurlyBracket )?
                    {
                    // InternalSimdaseParser.g:3059:7: ()
                    // InternalSimdaseParser.g:3060:5: 
                    {

                            current = forceCreateModelElement(
                                grammarAccess.getStatementLvl4Access().getIfAction_7_0(),
                                current);
                        

                    }

                    otherlv_23=(Token)match(input,If,FollowSets000.FOLLOW_31); 

                        	newLeafNode(otherlv_23, grammarAccess.getStatementLvl4Access().getIfKeyword_7_1());
                        
                    // InternalSimdaseParser.g:3070:1: ( (lv_booleanStatement_24_0= ruleBooleanStatement ) )
                    // InternalSimdaseParser.g:3071:1: (lv_booleanStatement_24_0= ruleBooleanStatement )
                    {
                    // InternalSimdaseParser.g:3071:1: (lv_booleanStatement_24_0= ruleBooleanStatement )
                    // InternalSimdaseParser.g:3072:3: lv_booleanStatement_24_0= ruleBooleanStatement
                    {
                     
                    	        newCompositeNode(grammarAccess.getStatementLvl4Access().getBooleanStatementBooleanStatementParserRuleCall_7_2_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_30);
                    lv_booleanStatement_24_0=ruleBooleanStatement();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getStatementLvl4Rule());
                    	        }
                           		set(
                           			current, 
                           			"booleanStatement",
                            		lv_booleanStatement_24_0, 
                            		"edu.clemson.simdase.Simdase.BooleanStatement");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    otherlv_25=(Token)match(input,RightParenthesis,FollowSets000.FOLLOW_9); 

                        	newLeafNode(otherlv_25, grammarAccess.getStatementLvl4Access().getRightParenthesisKeyword_7_3());
                        
                    otherlv_26=(Token)match(input,LeftCurlyBracket,FollowSets000.FOLLOW_8); 

                        	newLeafNode(otherlv_26, grammarAccess.getStatementLvl4Access().getLeftCurlyBracketKeyword_7_4());
                        
                    // InternalSimdaseParser.g:3098:1: ( (lv_trueStatement_27_0= ruleStatement ) )
                    // InternalSimdaseParser.g:3099:1: (lv_trueStatement_27_0= ruleStatement )
                    {
                    // InternalSimdaseParser.g:3099:1: (lv_trueStatement_27_0= ruleStatement )
                    // InternalSimdaseParser.g:3100:3: lv_trueStatement_27_0= ruleStatement
                    {
                     
                    	        newCompositeNode(grammarAccess.getStatementLvl4Access().getTrueStatementStatementParserRuleCall_7_5_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_11);
                    lv_trueStatement_27_0=ruleStatement();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getStatementLvl4Rule());
                    	        }
                           		set(
                           			current, 
                           			"trueStatement",
                            		lv_trueStatement_27_0, 
                            		"edu.clemson.simdase.Simdase.Statement");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    otherlv_28=(Token)match(input,RightCurlyBracket,FollowSets000.FOLLOW_32); 

                        	newLeafNode(otherlv_28, grammarAccess.getStatementLvl4Access().getRightCurlyBracketKeyword_7_6());
                        
                    // InternalSimdaseParser.g:3121:1: (otherlv_29= Else otherlv_30= LeftCurlyBracket ( (lv_falseStatement_31_0= ruleStatement ) ) otherlv_32= RightCurlyBracket )?
                    int alt22=2;
                    int LA22_0 = input.LA(1);

                    if ( (LA22_0==Else) ) {
                        alt22=1;
                    }
                    switch (alt22) {
                        case 1 :
                            // InternalSimdaseParser.g:3122:2: otherlv_29= Else otherlv_30= LeftCurlyBracket ( (lv_falseStatement_31_0= ruleStatement ) ) otherlv_32= RightCurlyBracket
                            {
                            otherlv_29=(Token)match(input,Else,FollowSets000.FOLLOW_9); 

                                	newLeafNode(otherlv_29, grammarAccess.getStatementLvl4Access().getElseKeyword_7_7_0());
                                
                            otherlv_30=(Token)match(input,LeftCurlyBracket,FollowSets000.FOLLOW_8); 

                                	newLeafNode(otherlv_30, grammarAccess.getStatementLvl4Access().getLeftCurlyBracketKeyword_7_7_1());
                                
                            // InternalSimdaseParser.g:3131:1: ( (lv_falseStatement_31_0= ruleStatement ) )
                            // InternalSimdaseParser.g:3132:1: (lv_falseStatement_31_0= ruleStatement )
                            {
                            // InternalSimdaseParser.g:3132:1: (lv_falseStatement_31_0= ruleStatement )
                            // InternalSimdaseParser.g:3133:3: lv_falseStatement_31_0= ruleStatement
                            {
                             
                            	        newCompositeNode(grammarAccess.getStatementLvl4Access().getFalseStatementStatementParserRuleCall_7_7_2_0()); 
                            	    
                            pushFollow(FollowSets000.FOLLOW_11);
                            lv_falseStatement_31_0=ruleStatement();

                            state._fsp--;


                            	        if (current==null) {
                            	            current = createModelElementForParent(grammarAccess.getStatementLvl4Rule());
                            	        }
                                   		set(
                                   			current, 
                                   			"falseStatement",
                                    		lv_falseStatement_31_0, 
                                    		"edu.clemson.simdase.Simdase.Statement");
                            	        afterParserOrEnumRuleCall();
                            	    

                            }


                            }

                            otherlv_32=(Token)match(input,RightCurlyBracket,FollowSets000.FOLLOW_2); 

                                	newLeafNode(otherlv_32, grammarAccess.getStatementLvl4Access().getRightCurlyBracketKeyword_7_7_3());
                                

                            }
                            break;

                    }


                    }


                    }
                    break;
                case 9 :
                    // InternalSimdaseParser.g:3155:6: ( () otherlv_34= Random )
                    {
                    // InternalSimdaseParser.g:3155:6: ( () otherlv_34= Random )
                    // InternalSimdaseParser.g:3155:7: () otherlv_34= Random
                    {
                    // InternalSimdaseParser.g:3155:7: ()
                    // InternalSimdaseParser.g:3156:5: 
                    {

                            current = forceCreateModelElement(
                                grammarAccess.getStatementLvl4Access().getRandomAction_8_0(),
                                current);
                        

                    }

                    otherlv_34=(Token)match(input,Random,FollowSets000.FOLLOW_2); 

                        	newLeafNode(otherlv_34, grammarAccess.getStatementLvl4Access().getRandomKeyword_8_1());
                        

                    }


                    }
                    break;
                case 10 :
                    // InternalSimdaseParser.g:3167:6: ( () otherlv_36= S )
                    {
                    // InternalSimdaseParser.g:3167:6: ( () otherlv_36= S )
                    // InternalSimdaseParser.g:3167:7: () otherlv_36= S
                    {
                    // InternalSimdaseParser.g:3167:7: ()
                    // InternalSimdaseParser.g:3168:5: 
                    {

                            current = forceCreateModelElement(
                                grammarAccess.getStatementLvl4Access().getStatementScalingFactorAction_9_0(),
                                current);
                        

                    }

                    otherlv_36=(Token)match(input,S,FollowSets000.FOLLOW_2); 

                        	newLeafNode(otherlv_36, grammarAccess.getStatementLvl4Access().getSKeyword_9_1());
                        

                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStatementLvl4"


    // $ANTLR start "entryRuleBooleanStatement"
    // InternalSimdaseParser.g:3186:1: entryRuleBooleanStatement returns [EObject current=null] : iv_ruleBooleanStatement= ruleBooleanStatement EOF ;
    public final EObject entryRuleBooleanStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBooleanStatement = null;


        try {
            // InternalSimdaseParser.g:3187:2: (iv_ruleBooleanStatement= ruleBooleanStatement EOF )
            // InternalSimdaseParser.g:3188:2: iv_ruleBooleanStatement= ruleBooleanStatement EOF
            {
             newCompositeNode(grammarAccess.getBooleanStatementRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleBooleanStatement=ruleBooleanStatement();

            state._fsp--;

             current =iv_ruleBooleanStatement; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBooleanStatement"


    // $ANTLR start "ruleBooleanStatement"
    // InternalSimdaseParser.g:3195:1: ruleBooleanStatement returns [EObject current=null] : ( ( (lv_statement_0_0= ruleBooleanStatementLvl2 ) ) ( ( (lv_op_1_0= VerticalLineVerticalLine ) ) ( (lv_rest_2_0= ruleBooleanStatementLvl2 ) ) )* ) ;
    public final EObject ruleBooleanStatement() throws RecognitionException {
        EObject current = null;

        Token lv_op_1_0=null;
        EObject lv_statement_0_0 = null;

        EObject lv_rest_2_0 = null;


         enterRule(); 
            
        try {
            // InternalSimdaseParser.g:3198:28: ( ( ( (lv_statement_0_0= ruleBooleanStatementLvl2 ) ) ( ( (lv_op_1_0= VerticalLineVerticalLine ) ) ( (lv_rest_2_0= ruleBooleanStatementLvl2 ) ) )* ) )
            // InternalSimdaseParser.g:3199:1: ( ( (lv_statement_0_0= ruleBooleanStatementLvl2 ) ) ( ( (lv_op_1_0= VerticalLineVerticalLine ) ) ( (lv_rest_2_0= ruleBooleanStatementLvl2 ) ) )* )
            {
            // InternalSimdaseParser.g:3199:1: ( ( (lv_statement_0_0= ruleBooleanStatementLvl2 ) ) ( ( (lv_op_1_0= VerticalLineVerticalLine ) ) ( (lv_rest_2_0= ruleBooleanStatementLvl2 ) ) )* )
            // InternalSimdaseParser.g:3199:2: ( (lv_statement_0_0= ruleBooleanStatementLvl2 ) ) ( ( (lv_op_1_0= VerticalLineVerticalLine ) ) ( (lv_rest_2_0= ruleBooleanStatementLvl2 ) ) )*
            {
            // InternalSimdaseParser.g:3199:2: ( (lv_statement_0_0= ruleBooleanStatementLvl2 ) )
            // InternalSimdaseParser.g:3200:1: (lv_statement_0_0= ruleBooleanStatementLvl2 )
            {
            // InternalSimdaseParser.g:3200:1: (lv_statement_0_0= ruleBooleanStatementLvl2 )
            // InternalSimdaseParser.g:3201:3: lv_statement_0_0= ruleBooleanStatementLvl2
            {
             
            	        newCompositeNode(grammarAccess.getBooleanStatementAccess().getStatementBooleanStatementLvl2ParserRuleCall_0_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_33);
            lv_statement_0_0=ruleBooleanStatementLvl2();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getBooleanStatementRule());
            	        }
                   		set(
                   			current, 
                   			"statement",
                    		lv_statement_0_0, 
                    		"edu.clemson.simdase.Simdase.BooleanStatementLvl2");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // InternalSimdaseParser.g:3217:2: ( ( (lv_op_1_0= VerticalLineVerticalLine ) ) ( (lv_rest_2_0= ruleBooleanStatementLvl2 ) ) )*
            loop24:
            do {
                int alt24=2;
                int LA24_0 = input.LA(1);

                if ( (LA24_0==VerticalLineVerticalLine) ) {
                    alt24=1;
                }


                switch (alt24) {
            	case 1 :
            	    // InternalSimdaseParser.g:3217:3: ( (lv_op_1_0= VerticalLineVerticalLine ) ) ( (lv_rest_2_0= ruleBooleanStatementLvl2 ) )
            	    {
            	    // InternalSimdaseParser.g:3217:3: ( (lv_op_1_0= VerticalLineVerticalLine ) )
            	    // InternalSimdaseParser.g:3218:1: (lv_op_1_0= VerticalLineVerticalLine )
            	    {
            	    // InternalSimdaseParser.g:3218:1: (lv_op_1_0= VerticalLineVerticalLine )
            	    // InternalSimdaseParser.g:3219:3: lv_op_1_0= VerticalLineVerticalLine
            	    {
            	    lv_op_1_0=(Token)match(input,VerticalLineVerticalLine,FollowSets000.FOLLOW_31); 

            	            newLeafNode(lv_op_1_0, grammarAccess.getBooleanStatementAccess().getOpVerticalLineVerticalLineKeyword_1_0_0());
            	        

            	    	        if (current==null) {
            	    	            current = createModelElement(grammarAccess.getBooleanStatementRule());
            	    	        }
            	           		addWithLastConsumed(current, "op", lv_op_1_0, "||");
            	    	    

            	    }


            	    }

            	    // InternalSimdaseParser.g:3233:2: ( (lv_rest_2_0= ruleBooleanStatementLvl2 ) )
            	    // InternalSimdaseParser.g:3234:1: (lv_rest_2_0= ruleBooleanStatementLvl2 )
            	    {
            	    // InternalSimdaseParser.g:3234:1: (lv_rest_2_0= ruleBooleanStatementLvl2 )
            	    // InternalSimdaseParser.g:3235:3: lv_rest_2_0= ruleBooleanStatementLvl2
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getBooleanStatementAccess().getRestBooleanStatementLvl2ParserRuleCall_1_1_0()); 
            	    	    
            	    pushFollow(FollowSets000.FOLLOW_33);
            	    lv_rest_2_0=ruleBooleanStatementLvl2();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getBooleanStatementRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"rest",
            	            		lv_rest_2_0, 
            	            		"edu.clemson.simdase.Simdase.BooleanStatementLvl2");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop24;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBooleanStatement"


    // $ANTLR start "entryRuleBooleanStatementLvl2"
    // InternalSimdaseParser.g:3259:1: entryRuleBooleanStatementLvl2 returns [EObject current=null] : iv_ruleBooleanStatementLvl2= ruleBooleanStatementLvl2 EOF ;
    public final EObject entryRuleBooleanStatementLvl2() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBooleanStatementLvl2 = null;


        try {
            // InternalSimdaseParser.g:3260:2: (iv_ruleBooleanStatementLvl2= ruleBooleanStatementLvl2 EOF )
            // InternalSimdaseParser.g:3261:2: iv_ruleBooleanStatementLvl2= ruleBooleanStatementLvl2 EOF
            {
             newCompositeNode(grammarAccess.getBooleanStatementLvl2Rule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleBooleanStatementLvl2=ruleBooleanStatementLvl2();

            state._fsp--;

             current =iv_ruleBooleanStatementLvl2; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBooleanStatementLvl2"


    // $ANTLR start "ruleBooleanStatementLvl2"
    // InternalSimdaseParser.g:3268:1: ruleBooleanStatementLvl2 returns [EObject current=null] : ( ( (lv_statement_0_0= ruleBooleanStatementLvl3 ) ) ( ( (lv_op_1_0= AmpersandAmpersand ) ) ( (lv_rest_2_0= ruleBooleanStatementLvl3 ) ) )* ) ;
    public final EObject ruleBooleanStatementLvl2() throws RecognitionException {
        EObject current = null;

        Token lv_op_1_0=null;
        EObject lv_statement_0_0 = null;

        EObject lv_rest_2_0 = null;


         enterRule(); 
            
        try {
            // InternalSimdaseParser.g:3271:28: ( ( ( (lv_statement_0_0= ruleBooleanStatementLvl3 ) ) ( ( (lv_op_1_0= AmpersandAmpersand ) ) ( (lv_rest_2_0= ruleBooleanStatementLvl3 ) ) )* ) )
            // InternalSimdaseParser.g:3272:1: ( ( (lv_statement_0_0= ruleBooleanStatementLvl3 ) ) ( ( (lv_op_1_0= AmpersandAmpersand ) ) ( (lv_rest_2_0= ruleBooleanStatementLvl3 ) ) )* )
            {
            // InternalSimdaseParser.g:3272:1: ( ( (lv_statement_0_0= ruleBooleanStatementLvl3 ) ) ( ( (lv_op_1_0= AmpersandAmpersand ) ) ( (lv_rest_2_0= ruleBooleanStatementLvl3 ) ) )* )
            // InternalSimdaseParser.g:3272:2: ( (lv_statement_0_0= ruleBooleanStatementLvl3 ) ) ( ( (lv_op_1_0= AmpersandAmpersand ) ) ( (lv_rest_2_0= ruleBooleanStatementLvl3 ) ) )*
            {
            // InternalSimdaseParser.g:3272:2: ( (lv_statement_0_0= ruleBooleanStatementLvl3 ) )
            // InternalSimdaseParser.g:3273:1: (lv_statement_0_0= ruleBooleanStatementLvl3 )
            {
            // InternalSimdaseParser.g:3273:1: (lv_statement_0_0= ruleBooleanStatementLvl3 )
            // InternalSimdaseParser.g:3274:3: lv_statement_0_0= ruleBooleanStatementLvl3
            {
             
            	        newCompositeNode(grammarAccess.getBooleanStatementLvl2Access().getStatementBooleanStatementLvl3ParserRuleCall_0_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_34);
            lv_statement_0_0=ruleBooleanStatementLvl3();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getBooleanStatementLvl2Rule());
            	        }
                   		set(
                   			current, 
                   			"statement",
                    		lv_statement_0_0, 
                    		"edu.clemson.simdase.Simdase.BooleanStatementLvl3");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // InternalSimdaseParser.g:3290:2: ( ( (lv_op_1_0= AmpersandAmpersand ) ) ( (lv_rest_2_0= ruleBooleanStatementLvl3 ) ) )*
            loop25:
            do {
                int alt25=2;
                int LA25_0 = input.LA(1);

                if ( (LA25_0==AmpersandAmpersand) ) {
                    alt25=1;
                }


                switch (alt25) {
            	case 1 :
            	    // InternalSimdaseParser.g:3290:3: ( (lv_op_1_0= AmpersandAmpersand ) ) ( (lv_rest_2_0= ruleBooleanStatementLvl3 ) )
            	    {
            	    // InternalSimdaseParser.g:3290:3: ( (lv_op_1_0= AmpersandAmpersand ) )
            	    // InternalSimdaseParser.g:3291:1: (lv_op_1_0= AmpersandAmpersand )
            	    {
            	    // InternalSimdaseParser.g:3291:1: (lv_op_1_0= AmpersandAmpersand )
            	    // InternalSimdaseParser.g:3292:3: lv_op_1_0= AmpersandAmpersand
            	    {
            	    lv_op_1_0=(Token)match(input,AmpersandAmpersand,FollowSets000.FOLLOW_31); 

            	            newLeafNode(lv_op_1_0, grammarAccess.getBooleanStatementLvl2Access().getOpAmpersandAmpersandKeyword_1_0_0());
            	        

            	    	        if (current==null) {
            	    	            current = createModelElement(grammarAccess.getBooleanStatementLvl2Rule());
            	    	        }
            	           		addWithLastConsumed(current, "op", lv_op_1_0, "&&");
            	    	    

            	    }


            	    }

            	    // InternalSimdaseParser.g:3306:2: ( (lv_rest_2_0= ruleBooleanStatementLvl3 ) )
            	    // InternalSimdaseParser.g:3307:1: (lv_rest_2_0= ruleBooleanStatementLvl3 )
            	    {
            	    // InternalSimdaseParser.g:3307:1: (lv_rest_2_0= ruleBooleanStatementLvl3 )
            	    // InternalSimdaseParser.g:3308:3: lv_rest_2_0= ruleBooleanStatementLvl3
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getBooleanStatementLvl2Access().getRestBooleanStatementLvl3ParserRuleCall_1_1_0()); 
            	    	    
            	    pushFollow(FollowSets000.FOLLOW_34);
            	    lv_rest_2_0=ruleBooleanStatementLvl3();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getBooleanStatementLvl2Rule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"rest",
            	            		lv_rest_2_0, 
            	            		"edu.clemson.simdase.Simdase.BooleanStatementLvl3");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop25;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBooleanStatementLvl2"


    // $ANTLR start "entryRuleBooleanStatementLvl3"
    // InternalSimdaseParser.g:3332:1: entryRuleBooleanStatementLvl3 returns [EObject current=null] : iv_ruleBooleanStatementLvl3= ruleBooleanStatementLvl3 EOF ;
    public final EObject entryRuleBooleanStatementLvl3() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBooleanStatementLvl3 = null;


        try {
            // InternalSimdaseParser.g:3333:2: (iv_ruleBooleanStatementLvl3= ruleBooleanStatementLvl3 EOF )
            // InternalSimdaseParser.g:3334:2: iv_ruleBooleanStatementLvl3= ruleBooleanStatementLvl3 EOF
            {
             newCompositeNode(grammarAccess.getBooleanStatementLvl3Rule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleBooleanStatementLvl3=ruleBooleanStatementLvl3();

            state._fsp--;

             current =iv_ruleBooleanStatementLvl3; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBooleanStatementLvl3"


    // $ANTLR start "ruleBooleanStatementLvl3"
    // InternalSimdaseParser.g:3341:1: ruleBooleanStatementLvl3 returns [EObject current=null] : ( ( (lv_statement_0_0= ruleBooleanStatementLvl4 ) ) ( ( ( (lv_op_1_1= GreaterThanSign | lv_op_1_2= GreaterThanSignEqualsSign | lv_op_1_3= LessThanSign | lv_op_1_4= LessThanSignEqualsSign | lv_op_1_5= EqualsSignEqualsSign ) ) ) ( (lv_rest_2_0= ruleBooleanStatementLvl4 ) ) )* ) ;
    public final EObject ruleBooleanStatementLvl3() throws RecognitionException {
        EObject current = null;

        Token lv_op_1_1=null;
        Token lv_op_1_2=null;
        Token lv_op_1_3=null;
        Token lv_op_1_4=null;
        Token lv_op_1_5=null;
        EObject lv_statement_0_0 = null;

        EObject lv_rest_2_0 = null;


         enterRule(); 
            
        try {
            // InternalSimdaseParser.g:3344:28: ( ( ( (lv_statement_0_0= ruleBooleanStatementLvl4 ) ) ( ( ( (lv_op_1_1= GreaterThanSign | lv_op_1_2= GreaterThanSignEqualsSign | lv_op_1_3= LessThanSign | lv_op_1_4= LessThanSignEqualsSign | lv_op_1_5= EqualsSignEqualsSign ) ) ) ( (lv_rest_2_0= ruleBooleanStatementLvl4 ) ) )* ) )
            // InternalSimdaseParser.g:3345:1: ( ( (lv_statement_0_0= ruleBooleanStatementLvl4 ) ) ( ( ( (lv_op_1_1= GreaterThanSign | lv_op_1_2= GreaterThanSignEqualsSign | lv_op_1_3= LessThanSign | lv_op_1_4= LessThanSignEqualsSign | lv_op_1_5= EqualsSignEqualsSign ) ) ) ( (lv_rest_2_0= ruleBooleanStatementLvl4 ) ) )* )
            {
            // InternalSimdaseParser.g:3345:1: ( ( (lv_statement_0_0= ruleBooleanStatementLvl4 ) ) ( ( ( (lv_op_1_1= GreaterThanSign | lv_op_1_2= GreaterThanSignEqualsSign | lv_op_1_3= LessThanSign | lv_op_1_4= LessThanSignEqualsSign | lv_op_1_5= EqualsSignEqualsSign ) ) ) ( (lv_rest_2_0= ruleBooleanStatementLvl4 ) ) )* )
            // InternalSimdaseParser.g:3345:2: ( (lv_statement_0_0= ruleBooleanStatementLvl4 ) ) ( ( ( (lv_op_1_1= GreaterThanSign | lv_op_1_2= GreaterThanSignEqualsSign | lv_op_1_3= LessThanSign | lv_op_1_4= LessThanSignEqualsSign | lv_op_1_5= EqualsSignEqualsSign ) ) ) ( (lv_rest_2_0= ruleBooleanStatementLvl4 ) ) )*
            {
            // InternalSimdaseParser.g:3345:2: ( (lv_statement_0_0= ruleBooleanStatementLvl4 ) )
            // InternalSimdaseParser.g:3346:1: (lv_statement_0_0= ruleBooleanStatementLvl4 )
            {
            // InternalSimdaseParser.g:3346:1: (lv_statement_0_0= ruleBooleanStatementLvl4 )
            // InternalSimdaseParser.g:3347:3: lv_statement_0_0= ruleBooleanStatementLvl4
            {
             
            	        newCompositeNode(grammarAccess.getBooleanStatementLvl3Access().getStatementBooleanStatementLvl4ParserRuleCall_0_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_35);
            lv_statement_0_0=ruleBooleanStatementLvl4();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getBooleanStatementLvl3Rule());
            	        }
                   		set(
                   			current, 
                   			"statement",
                    		lv_statement_0_0, 
                    		"edu.clemson.simdase.Simdase.BooleanStatementLvl4");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // InternalSimdaseParser.g:3363:2: ( ( ( (lv_op_1_1= GreaterThanSign | lv_op_1_2= GreaterThanSignEqualsSign | lv_op_1_3= LessThanSign | lv_op_1_4= LessThanSignEqualsSign | lv_op_1_5= EqualsSignEqualsSign ) ) ) ( (lv_rest_2_0= ruleBooleanStatementLvl4 ) ) )*
            loop27:
            do {
                int alt27=2;
                int LA27_0 = input.LA(1);

                if ( ((LA27_0>=LessThanSignEqualsSign && LA27_0<=EqualsSignEqualsSign)||LA27_0==GreaterThanSignEqualsSign||(LA27_0>=LessThanSign && LA27_0<=GreaterThanSign)) ) {
                    alt27=1;
                }


                switch (alt27) {
            	case 1 :
            	    // InternalSimdaseParser.g:3363:3: ( ( (lv_op_1_1= GreaterThanSign | lv_op_1_2= GreaterThanSignEqualsSign | lv_op_1_3= LessThanSign | lv_op_1_4= LessThanSignEqualsSign | lv_op_1_5= EqualsSignEqualsSign ) ) ) ( (lv_rest_2_0= ruleBooleanStatementLvl4 ) )
            	    {
            	    // InternalSimdaseParser.g:3363:3: ( ( (lv_op_1_1= GreaterThanSign | lv_op_1_2= GreaterThanSignEqualsSign | lv_op_1_3= LessThanSign | lv_op_1_4= LessThanSignEqualsSign | lv_op_1_5= EqualsSignEqualsSign ) ) )
            	    // InternalSimdaseParser.g:3364:1: ( (lv_op_1_1= GreaterThanSign | lv_op_1_2= GreaterThanSignEqualsSign | lv_op_1_3= LessThanSign | lv_op_1_4= LessThanSignEqualsSign | lv_op_1_5= EqualsSignEqualsSign ) )
            	    {
            	    // InternalSimdaseParser.g:3364:1: ( (lv_op_1_1= GreaterThanSign | lv_op_1_2= GreaterThanSignEqualsSign | lv_op_1_3= LessThanSign | lv_op_1_4= LessThanSignEqualsSign | lv_op_1_5= EqualsSignEqualsSign ) )
            	    // InternalSimdaseParser.g:3365:1: (lv_op_1_1= GreaterThanSign | lv_op_1_2= GreaterThanSignEqualsSign | lv_op_1_3= LessThanSign | lv_op_1_4= LessThanSignEqualsSign | lv_op_1_5= EqualsSignEqualsSign )
            	    {
            	    // InternalSimdaseParser.g:3365:1: (lv_op_1_1= GreaterThanSign | lv_op_1_2= GreaterThanSignEqualsSign | lv_op_1_3= LessThanSign | lv_op_1_4= LessThanSignEqualsSign | lv_op_1_5= EqualsSignEqualsSign )
            	    int alt26=5;
            	    switch ( input.LA(1) ) {
            	    case GreaterThanSign:
            	        {
            	        alt26=1;
            	        }
            	        break;
            	    case GreaterThanSignEqualsSign:
            	        {
            	        alt26=2;
            	        }
            	        break;
            	    case LessThanSign:
            	        {
            	        alt26=3;
            	        }
            	        break;
            	    case LessThanSignEqualsSign:
            	        {
            	        alt26=4;
            	        }
            	        break;
            	    case EqualsSignEqualsSign:
            	        {
            	        alt26=5;
            	        }
            	        break;
            	    default:
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 26, 0, input);

            	        throw nvae;
            	    }

            	    switch (alt26) {
            	        case 1 :
            	            // InternalSimdaseParser.g:3366:3: lv_op_1_1= GreaterThanSign
            	            {
            	            lv_op_1_1=(Token)match(input,GreaterThanSign,FollowSets000.FOLLOW_31); 

            	                    newLeafNode(lv_op_1_1, grammarAccess.getBooleanStatementLvl3Access().getOpGreaterThanSignKeyword_1_0_0_0());
            	                

            	            	        if (current==null) {
            	            	            current = createModelElement(grammarAccess.getBooleanStatementLvl3Rule());
            	            	        }
            	                   		addWithLastConsumed(current, "op", lv_op_1_1, null);
            	            	    

            	            }
            	            break;
            	        case 2 :
            	            // InternalSimdaseParser.g:3379:8: lv_op_1_2= GreaterThanSignEqualsSign
            	            {
            	            lv_op_1_2=(Token)match(input,GreaterThanSignEqualsSign,FollowSets000.FOLLOW_31); 

            	                    newLeafNode(lv_op_1_2, grammarAccess.getBooleanStatementLvl3Access().getOpGreaterThanSignEqualsSignKeyword_1_0_0_1());
            	                

            	            	        if (current==null) {
            	            	            current = createModelElement(grammarAccess.getBooleanStatementLvl3Rule());
            	            	        }
            	                   		addWithLastConsumed(current, "op", lv_op_1_2, null);
            	            	    

            	            }
            	            break;
            	        case 3 :
            	            // InternalSimdaseParser.g:3392:8: lv_op_1_3= LessThanSign
            	            {
            	            lv_op_1_3=(Token)match(input,LessThanSign,FollowSets000.FOLLOW_31); 

            	                    newLeafNode(lv_op_1_3, grammarAccess.getBooleanStatementLvl3Access().getOpLessThanSignKeyword_1_0_0_2());
            	                

            	            	        if (current==null) {
            	            	            current = createModelElement(grammarAccess.getBooleanStatementLvl3Rule());
            	            	        }
            	                   		addWithLastConsumed(current, "op", lv_op_1_3, null);
            	            	    

            	            }
            	            break;
            	        case 4 :
            	            // InternalSimdaseParser.g:3405:8: lv_op_1_4= LessThanSignEqualsSign
            	            {
            	            lv_op_1_4=(Token)match(input,LessThanSignEqualsSign,FollowSets000.FOLLOW_31); 

            	                    newLeafNode(lv_op_1_4, grammarAccess.getBooleanStatementLvl3Access().getOpLessThanSignEqualsSignKeyword_1_0_0_3());
            	                

            	            	        if (current==null) {
            	            	            current = createModelElement(grammarAccess.getBooleanStatementLvl3Rule());
            	            	        }
            	                   		addWithLastConsumed(current, "op", lv_op_1_4, null);
            	            	    

            	            }
            	            break;
            	        case 5 :
            	            // InternalSimdaseParser.g:3418:8: lv_op_1_5= EqualsSignEqualsSign
            	            {
            	            lv_op_1_5=(Token)match(input,EqualsSignEqualsSign,FollowSets000.FOLLOW_31); 

            	                    newLeafNode(lv_op_1_5, grammarAccess.getBooleanStatementLvl3Access().getOpEqualsSignEqualsSignKeyword_1_0_0_4());
            	                

            	            	        if (current==null) {
            	            	            current = createModelElement(grammarAccess.getBooleanStatementLvl3Rule());
            	            	        }
            	                   		addWithLastConsumed(current, "op", lv_op_1_5, null);
            	            	    

            	            }
            	            break;

            	    }


            	    }


            	    }

            	    // InternalSimdaseParser.g:3434:2: ( (lv_rest_2_0= ruleBooleanStatementLvl4 ) )
            	    // InternalSimdaseParser.g:3435:1: (lv_rest_2_0= ruleBooleanStatementLvl4 )
            	    {
            	    // InternalSimdaseParser.g:3435:1: (lv_rest_2_0= ruleBooleanStatementLvl4 )
            	    // InternalSimdaseParser.g:3436:3: lv_rest_2_0= ruleBooleanStatementLvl4
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getBooleanStatementLvl3Access().getRestBooleanStatementLvl4ParserRuleCall_1_1_0()); 
            	    	    
            	    pushFollow(FollowSets000.FOLLOW_35);
            	    lv_rest_2_0=ruleBooleanStatementLvl4();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getBooleanStatementLvl3Rule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"rest",
            	            		lv_rest_2_0, 
            	            		"edu.clemson.simdase.Simdase.BooleanStatementLvl4");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop27;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBooleanStatementLvl3"


    // $ANTLR start "entryRuleBooleanStatementLvl4"
    // InternalSimdaseParser.g:3460:1: entryRuleBooleanStatementLvl4 returns [EObject current=null] : iv_ruleBooleanStatementLvl4= ruleBooleanStatementLvl4 EOF ;
    public final EObject entryRuleBooleanStatementLvl4() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBooleanStatementLvl4 = null;


        try {
            // InternalSimdaseParser.g:3461:2: (iv_ruleBooleanStatementLvl4= ruleBooleanStatementLvl4 EOF )
            // InternalSimdaseParser.g:3462:2: iv_ruleBooleanStatementLvl4= ruleBooleanStatementLvl4 EOF
            {
             newCompositeNode(grammarAccess.getBooleanStatementLvl4Rule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleBooleanStatementLvl4=ruleBooleanStatementLvl4();

            state._fsp--;

             current =iv_ruleBooleanStatementLvl4; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBooleanStatementLvl4"


    // $ANTLR start "ruleBooleanStatementLvl4"
    // InternalSimdaseParser.g:3469:1: ruleBooleanStatementLvl4 returns [EObject current=null] : ( ( () otherlv_1= ExclamationMark ( (lv_statement_2_0= ruleBooleanStatementLvl4 ) ) ) | ( () ( (lv_statement_4_0= ruleBooleanStatementLvl5 ) ) ) ) ;
    public final EObject ruleBooleanStatementLvl4() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_statement_2_0 = null;

        EObject lv_statement_4_0 = null;


         enterRule(); 
            
        try {
            // InternalSimdaseParser.g:3472:28: ( ( ( () otherlv_1= ExclamationMark ( (lv_statement_2_0= ruleBooleanStatementLvl4 ) ) ) | ( () ( (lv_statement_4_0= ruleBooleanStatementLvl5 ) ) ) ) )
            // InternalSimdaseParser.g:3473:1: ( ( () otherlv_1= ExclamationMark ( (lv_statement_2_0= ruleBooleanStatementLvl4 ) ) ) | ( () ( (lv_statement_4_0= ruleBooleanStatementLvl5 ) ) ) )
            {
            // InternalSimdaseParser.g:3473:1: ( ( () otherlv_1= ExclamationMark ( (lv_statement_2_0= ruleBooleanStatementLvl4 ) ) ) | ( () ( (lv_statement_4_0= ruleBooleanStatementLvl5 ) ) ) )
            int alt28=2;
            int LA28_0 = input.LA(1);

            if ( (LA28_0==ExclamationMark) ) {
                alt28=1;
            }
            else if ( (LA28_0==Random||LA28_0==False||LA28_0==Cos||(LA28_0>=Sin && LA28_0<=True)||LA28_0==S||LA28_0==LeftParenthesis||LA28_0==HyphenMinus||(LA28_0>=I && LA28_0<=T)||LA28_0==RULE_FLOAT) ) {
                alt28=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 28, 0, input);

                throw nvae;
            }
            switch (alt28) {
                case 1 :
                    // InternalSimdaseParser.g:3473:2: ( () otherlv_1= ExclamationMark ( (lv_statement_2_0= ruleBooleanStatementLvl4 ) ) )
                    {
                    // InternalSimdaseParser.g:3473:2: ( () otherlv_1= ExclamationMark ( (lv_statement_2_0= ruleBooleanStatementLvl4 ) ) )
                    // InternalSimdaseParser.g:3473:3: () otherlv_1= ExclamationMark ( (lv_statement_2_0= ruleBooleanStatementLvl4 ) )
                    {
                    // InternalSimdaseParser.g:3473:3: ()
                    // InternalSimdaseParser.g:3474:5: 
                    {

                            current = forceCreateModelElement(
                                grammarAccess.getBooleanStatementLvl4Access().getWithNotAction_0_0(),
                                current);
                        

                    }

                    otherlv_1=(Token)match(input,ExclamationMark,FollowSets000.FOLLOW_31); 

                        	newLeafNode(otherlv_1, grammarAccess.getBooleanStatementLvl4Access().getExclamationMarkKeyword_0_1());
                        
                    // InternalSimdaseParser.g:3484:1: ( (lv_statement_2_0= ruleBooleanStatementLvl4 ) )
                    // InternalSimdaseParser.g:3485:1: (lv_statement_2_0= ruleBooleanStatementLvl4 )
                    {
                    // InternalSimdaseParser.g:3485:1: (lv_statement_2_0= ruleBooleanStatementLvl4 )
                    // InternalSimdaseParser.g:3486:3: lv_statement_2_0= ruleBooleanStatementLvl4
                    {
                     
                    	        newCompositeNode(grammarAccess.getBooleanStatementLvl4Access().getStatementBooleanStatementLvl4ParserRuleCall_0_2_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_statement_2_0=ruleBooleanStatementLvl4();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getBooleanStatementLvl4Rule());
                    	        }
                           		set(
                           			current, 
                           			"statement",
                            		lv_statement_2_0, 
                            		"edu.clemson.simdase.Simdase.BooleanStatementLvl4");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalSimdaseParser.g:3503:6: ( () ( (lv_statement_4_0= ruleBooleanStatementLvl5 ) ) )
                    {
                    // InternalSimdaseParser.g:3503:6: ( () ( (lv_statement_4_0= ruleBooleanStatementLvl5 ) ) )
                    // InternalSimdaseParser.g:3503:7: () ( (lv_statement_4_0= ruleBooleanStatementLvl5 ) )
                    {
                    // InternalSimdaseParser.g:3503:7: ()
                    // InternalSimdaseParser.g:3504:5: 
                    {

                            current = forceCreateModelElement(
                                grammarAccess.getBooleanStatementLvl4Access().getWithoutNotAction_1_0(),
                                current);
                        

                    }

                    // InternalSimdaseParser.g:3509:2: ( (lv_statement_4_0= ruleBooleanStatementLvl5 ) )
                    // InternalSimdaseParser.g:3510:1: (lv_statement_4_0= ruleBooleanStatementLvl5 )
                    {
                    // InternalSimdaseParser.g:3510:1: (lv_statement_4_0= ruleBooleanStatementLvl5 )
                    // InternalSimdaseParser.g:3511:3: lv_statement_4_0= ruleBooleanStatementLvl5
                    {
                     
                    	        newCompositeNode(grammarAccess.getBooleanStatementLvl4Access().getStatementBooleanStatementLvl5ParserRuleCall_1_1_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_statement_4_0=ruleBooleanStatementLvl5();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getBooleanStatementLvl4Rule());
                    	        }
                           		set(
                           			current, 
                           			"statement",
                            		lv_statement_4_0, 
                            		"edu.clemson.simdase.Simdase.BooleanStatementLvl5");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBooleanStatementLvl4"


    // $ANTLR start "entryRuleBooleanStatementLvl5"
    // InternalSimdaseParser.g:3535:1: entryRuleBooleanStatementLvl5 returns [EObject current=null] : iv_ruleBooleanStatementLvl5= ruleBooleanStatementLvl5 EOF ;
    public final EObject entryRuleBooleanStatementLvl5() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBooleanStatementLvl5 = null;


        try {
            // InternalSimdaseParser.g:3536:2: (iv_ruleBooleanStatementLvl5= ruleBooleanStatementLvl5 EOF )
            // InternalSimdaseParser.g:3537:2: iv_ruleBooleanStatementLvl5= ruleBooleanStatementLvl5 EOF
            {
             newCompositeNode(grammarAccess.getBooleanStatementLvl5Rule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleBooleanStatementLvl5=ruleBooleanStatementLvl5();

            state._fsp--;

             current =iv_ruleBooleanStatementLvl5; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBooleanStatementLvl5"


    // $ANTLR start "ruleBooleanStatementLvl5"
    // InternalSimdaseParser.g:3544:1: ruleBooleanStatementLvl5 returns [EObject current=null] : ( ( (lv_statement_0_0= ruleBooleanStatementLvl6 ) ) ( ( ( (lv_op_1_1= PlusSign | lv_op_1_2= HyphenMinus ) ) ) ( (lv_rest_2_0= ruleBooleanStatementLvl6 ) ) )* ) ;
    public final EObject ruleBooleanStatementLvl5() throws RecognitionException {
        EObject current = null;

        Token lv_op_1_1=null;
        Token lv_op_1_2=null;
        EObject lv_statement_0_0 = null;

        EObject lv_rest_2_0 = null;


         enterRule(); 
            
        try {
            // InternalSimdaseParser.g:3547:28: ( ( ( (lv_statement_0_0= ruleBooleanStatementLvl6 ) ) ( ( ( (lv_op_1_1= PlusSign | lv_op_1_2= HyphenMinus ) ) ) ( (lv_rest_2_0= ruleBooleanStatementLvl6 ) ) )* ) )
            // InternalSimdaseParser.g:3548:1: ( ( (lv_statement_0_0= ruleBooleanStatementLvl6 ) ) ( ( ( (lv_op_1_1= PlusSign | lv_op_1_2= HyphenMinus ) ) ) ( (lv_rest_2_0= ruleBooleanStatementLvl6 ) ) )* )
            {
            // InternalSimdaseParser.g:3548:1: ( ( (lv_statement_0_0= ruleBooleanStatementLvl6 ) ) ( ( ( (lv_op_1_1= PlusSign | lv_op_1_2= HyphenMinus ) ) ) ( (lv_rest_2_0= ruleBooleanStatementLvl6 ) ) )* )
            // InternalSimdaseParser.g:3548:2: ( (lv_statement_0_0= ruleBooleanStatementLvl6 ) ) ( ( ( (lv_op_1_1= PlusSign | lv_op_1_2= HyphenMinus ) ) ) ( (lv_rest_2_0= ruleBooleanStatementLvl6 ) ) )*
            {
            // InternalSimdaseParser.g:3548:2: ( (lv_statement_0_0= ruleBooleanStatementLvl6 ) )
            // InternalSimdaseParser.g:3549:1: (lv_statement_0_0= ruleBooleanStatementLvl6 )
            {
            // InternalSimdaseParser.g:3549:1: (lv_statement_0_0= ruleBooleanStatementLvl6 )
            // InternalSimdaseParser.g:3550:3: lv_statement_0_0= ruleBooleanStatementLvl6
            {
             
            	        newCompositeNode(grammarAccess.getBooleanStatementLvl5Access().getStatementBooleanStatementLvl6ParserRuleCall_0_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_27);
            lv_statement_0_0=ruleBooleanStatementLvl6();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getBooleanStatementLvl5Rule());
            	        }
                   		set(
                   			current, 
                   			"statement",
                    		lv_statement_0_0, 
                    		"edu.clemson.simdase.Simdase.BooleanStatementLvl6");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // InternalSimdaseParser.g:3566:2: ( ( ( (lv_op_1_1= PlusSign | lv_op_1_2= HyphenMinus ) ) ) ( (lv_rest_2_0= ruleBooleanStatementLvl6 ) ) )*
            loop30:
            do {
                int alt30=2;
                int LA30_0 = input.LA(1);

                if ( (LA30_0==PlusSign||LA30_0==HyphenMinus) ) {
                    alt30=1;
                }


                switch (alt30) {
            	case 1 :
            	    // InternalSimdaseParser.g:3566:3: ( ( (lv_op_1_1= PlusSign | lv_op_1_2= HyphenMinus ) ) ) ( (lv_rest_2_0= ruleBooleanStatementLvl6 ) )
            	    {
            	    // InternalSimdaseParser.g:3566:3: ( ( (lv_op_1_1= PlusSign | lv_op_1_2= HyphenMinus ) ) )
            	    // InternalSimdaseParser.g:3567:1: ( (lv_op_1_1= PlusSign | lv_op_1_2= HyphenMinus ) )
            	    {
            	    // InternalSimdaseParser.g:3567:1: ( (lv_op_1_1= PlusSign | lv_op_1_2= HyphenMinus ) )
            	    // InternalSimdaseParser.g:3568:1: (lv_op_1_1= PlusSign | lv_op_1_2= HyphenMinus )
            	    {
            	    // InternalSimdaseParser.g:3568:1: (lv_op_1_1= PlusSign | lv_op_1_2= HyphenMinus )
            	    int alt29=2;
            	    int LA29_0 = input.LA(1);

            	    if ( (LA29_0==PlusSign) ) {
            	        alt29=1;
            	    }
            	    else if ( (LA29_0==HyphenMinus) ) {
            	        alt29=2;
            	    }
            	    else {
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 29, 0, input);

            	        throw nvae;
            	    }
            	    switch (alt29) {
            	        case 1 :
            	            // InternalSimdaseParser.g:3569:3: lv_op_1_1= PlusSign
            	            {
            	            lv_op_1_1=(Token)match(input,PlusSign,FollowSets000.FOLLOW_31); 

            	                    newLeafNode(lv_op_1_1, grammarAccess.getBooleanStatementLvl5Access().getOpPlusSignKeyword_1_0_0_0());
            	                

            	            	        if (current==null) {
            	            	            current = createModelElement(grammarAccess.getBooleanStatementLvl5Rule());
            	            	        }
            	                   		addWithLastConsumed(current, "op", lv_op_1_1, null);
            	            	    

            	            }
            	            break;
            	        case 2 :
            	            // InternalSimdaseParser.g:3582:8: lv_op_1_2= HyphenMinus
            	            {
            	            lv_op_1_2=(Token)match(input,HyphenMinus,FollowSets000.FOLLOW_31); 

            	                    newLeafNode(lv_op_1_2, grammarAccess.getBooleanStatementLvl5Access().getOpHyphenMinusKeyword_1_0_0_1());
            	                

            	            	        if (current==null) {
            	            	            current = createModelElement(grammarAccess.getBooleanStatementLvl5Rule());
            	            	        }
            	                   		addWithLastConsumed(current, "op", lv_op_1_2, null);
            	            	    

            	            }
            	            break;

            	    }


            	    }


            	    }

            	    // InternalSimdaseParser.g:3598:2: ( (lv_rest_2_0= ruleBooleanStatementLvl6 ) )
            	    // InternalSimdaseParser.g:3599:1: (lv_rest_2_0= ruleBooleanStatementLvl6 )
            	    {
            	    // InternalSimdaseParser.g:3599:1: (lv_rest_2_0= ruleBooleanStatementLvl6 )
            	    // InternalSimdaseParser.g:3600:3: lv_rest_2_0= ruleBooleanStatementLvl6
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getBooleanStatementLvl5Access().getRestBooleanStatementLvl6ParserRuleCall_1_1_0()); 
            	    	    
            	    pushFollow(FollowSets000.FOLLOW_27);
            	    lv_rest_2_0=ruleBooleanStatementLvl6();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getBooleanStatementLvl5Rule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"rest",
            	            		lv_rest_2_0, 
            	            		"edu.clemson.simdase.Simdase.BooleanStatementLvl6");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop30;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBooleanStatementLvl5"


    // $ANTLR start "entryRuleBooleanStatementLvl6"
    // InternalSimdaseParser.g:3624:1: entryRuleBooleanStatementLvl6 returns [EObject current=null] : iv_ruleBooleanStatementLvl6= ruleBooleanStatementLvl6 EOF ;
    public final EObject entryRuleBooleanStatementLvl6() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBooleanStatementLvl6 = null;


        try {
            // InternalSimdaseParser.g:3625:2: (iv_ruleBooleanStatementLvl6= ruleBooleanStatementLvl6 EOF )
            // InternalSimdaseParser.g:3626:2: iv_ruleBooleanStatementLvl6= ruleBooleanStatementLvl6 EOF
            {
             newCompositeNode(grammarAccess.getBooleanStatementLvl6Rule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleBooleanStatementLvl6=ruleBooleanStatementLvl6();

            state._fsp--;

             current =iv_ruleBooleanStatementLvl6; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBooleanStatementLvl6"


    // $ANTLR start "ruleBooleanStatementLvl6"
    // InternalSimdaseParser.g:3633:1: ruleBooleanStatementLvl6 returns [EObject current=null] : ( ( (lv_statement_0_0= ruleBooleanStatementLvl7 ) ) ( ( ( (lv_op_1_1= Asterisk | lv_op_1_2= Solidus | lv_op_1_3= PercentSign ) ) ) ( (lv_rest_2_0= ruleBooleanStatementLvl7 ) ) )* ) ;
    public final EObject ruleBooleanStatementLvl6() throws RecognitionException {
        EObject current = null;

        Token lv_op_1_1=null;
        Token lv_op_1_2=null;
        Token lv_op_1_3=null;
        EObject lv_statement_0_0 = null;

        EObject lv_rest_2_0 = null;


         enterRule(); 
            
        try {
            // InternalSimdaseParser.g:3636:28: ( ( ( (lv_statement_0_0= ruleBooleanStatementLvl7 ) ) ( ( ( (lv_op_1_1= Asterisk | lv_op_1_2= Solidus | lv_op_1_3= PercentSign ) ) ) ( (lv_rest_2_0= ruleBooleanStatementLvl7 ) ) )* ) )
            // InternalSimdaseParser.g:3637:1: ( ( (lv_statement_0_0= ruleBooleanStatementLvl7 ) ) ( ( ( (lv_op_1_1= Asterisk | lv_op_1_2= Solidus | lv_op_1_3= PercentSign ) ) ) ( (lv_rest_2_0= ruleBooleanStatementLvl7 ) ) )* )
            {
            // InternalSimdaseParser.g:3637:1: ( ( (lv_statement_0_0= ruleBooleanStatementLvl7 ) ) ( ( ( (lv_op_1_1= Asterisk | lv_op_1_2= Solidus | lv_op_1_3= PercentSign ) ) ) ( (lv_rest_2_0= ruleBooleanStatementLvl7 ) ) )* )
            // InternalSimdaseParser.g:3637:2: ( (lv_statement_0_0= ruleBooleanStatementLvl7 ) ) ( ( ( (lv_op_1_1= Asterisk | lv_op_1_2= Solidus | lv_op_1_3= PercentSign ) ) ) ( (lv_rest_2_0= ruleBooleanStatementLvl7 ) ) )*
            {
            // InternalSimdaseParser.g:3637:2: ( (lv_statement_0_0= ruleBooleanStatementLvl7 ) )
            // InternalSimdaseParser.g:3638:1: (lv_statement_0_0= ruleBooleanStatementLvl7 )
            {
            // InternalSimdaseParser.g:3638:1: (lv_statement_0_0= ruleBooleanStatementLvl7 )
            // InternalSimdaseParser.g:3639:3: lv_statement_0_0= ruleBooleanStatementLvl7
            {
             
            	        newCompositeNode(grammarAccess.getBooleanStatementLvl6Access().getStatementBooleanStatementLvl7ParserRuleCall_0_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_28);
            lv_statement_0_0=ruleBooleanStatementLvl7();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getBooleanStatementLvl6Rule());
            	        }
                   		set(
                   			current, 
                   			"statement",
                    		lv_statement_0_0, 
                    		"edu.clemson.simdase.Simdase.BooleanStatementLvl7");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // InternalSimdaseParser.g:3655:2: ( ( ( (lv_op_1_1= Asterisk | lv_op_1_2= Solidus | lv_op_1_3= PercentSign ) ) ) ( (lv_rest_2_0= ruleBooleanStatementLvl7 ) ) )*
            loop32:
            do {
                int alt32=2;
                int LA32_0 = input.LA(1);

                if ( (LA32_0==PercentSign||LA32_0==Asterisk||LA32_0==Solidus) ) {
                    alt32=1;
                }


                switch (alt32) {
            	case 1 :
            	    // InternalSimdaseParser.g:3655:3: ( ( (lv_op_1_1= Asterisk | lv_op_1_2= Solidus | lv_op_1_3= PercentSign ) ) ) ( (lv_rest_2_0= ruleBooleanStatementLvl7 ) )
            	    {
            	    // InternalSimdaseParser.g:3655:3: ( ( (lv_op_1_1= Asterisk | lv_op_1_2= Solidus | lv_op_1_3= PercentSign ) ) )
            	    // InternalSimdaseParser.g:3656:1: ( (lv_op_1_1= Asterisk | lv_op_1_2= Solidus | lv_op_1_3= PercentSign ) )
            	    {
            	    // InternalSimdaseParser.g:3656:1: ( (lv_op_1_1= Asterisk | lv_op_1_2= Solidus | lv_op_1_3= PercentSign ) )
            	    // InternalSimdaseParser.g:3657:1: (lv_op_1_1= Asterisk | lv_op_1_2= Solidus | lv_op_1_3= PercentSign )
            	    {
            	    // InternalSimdaseParser.g:3657:1: (lv_op_1_1= Asterisk | lv_op_1_2= Solidus | lv_op_1_3= PercentSign )
            	    int alt31=3;
            	    switch ( input.LA(1) ) {
            	    case Asterisk:
            	        {
            	        alt31=1;
            	        }
            	        break;
            	    case Solidus:
            	        {
            	        alt31=2;
            	        }
            	        break;
            	    case PercentSign:
            	        {
            	        alt31=3;
            	        }
            	        break;
            	    default:
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 31, 0, input);

            	        throw nvae;
            	    }

            	    switch (alt31) {
            	        case 1 :
            	            // InternalSimdaseParser.g:3658:3: lv_op_1_1= Asterisk
            	            {
            	            lv_op_1_1=(Token)match(input,Asterisk,FollowSets000.FOLLOW_31); 

            	                    newLeafNode(lv_op_1_1, grammarAccess.getBooleanStatementLvl6Access().getOpAsteriskKeyword_1_0_0_0());
            	                

            	            	        if (current==null) {
            	            	            current = createModelElement(grammarAccess.getBooleanStatementLvl6Rule());
            	            	        }
            	                   		addWithLastConsumed(current, "op", lv_op_1_1, null);
            	            	    

            	            }
            	            break;
            	        case 2 :
            	            // InternalSimdaseParser.g:3671:8: lv_op_1_2= Solidus
            	            {
            	            lv_op_1_2=(Token)match(input,Solidus,FollowSets000.FOLLOW_31); 

            	                    newLeafNode(lv_op_1_2, grammarAccess.getBooleanStatementLvl6Access().getOpSolidusKeyword_1_0_0_1());
            	                

            	            	        if (current==null) {
            	            	            current = createModelElement(grammarAccess.getBooleanStatementLvl6Rule());
            	            	        }
            	                   		addWithLastConsumed(current, "op", lv_op_1_2, null);
            	            	    

            	            }
            	            break;
            	        case 3 :
            	            // InternalSimdaseParser.g:3684:8: lv_op_1_3= PercentSign
            	            {
            	            lv_op_1_3=(Token)match(input,PercentSign,FollowSets000.FOLLOW_31); 

            	                    newLeafNode(lv_op_1_3, grammarAccess.getBooleanStatementLvl6Access().getOpPercentSignKeyword_1_0_0_2());
            	                

            	            	        if (current==null) {
            	            	            current = createModelElement(grammarAccess.getBooleanStatementLvl6Rule());
            	            	        }
            	                   		addWithLastConsumed(current, "op", lv_op_1_3, null);
            	            	    

            	            }
            	            break;

            	    }


            	    }


            	    }

            	    // InternalSimdaseParser.g:3700:2: ( (lv_rest_2_0= ruleBooleanStatementLvl7 ) )
            	    // InternalSimdaseParser.g:3701:1: (lv_rest_2_0= ruleBooleanStatementLvl7 )
            	    {
            	    // InternalSimdaseParser.g:3701:1: (lv_rest_2_0= ruleBooleanStatementLvl7 )
            	    // InternalSimdaseParser.g:3702:3: lv_rest_2_0= ruleBooleanStatementLvl7
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getBooleanStatementLvl6Access().getRestBooleanStatementLvl7ParserRuleCall_1_1_0()); 
            	    	    
            	    pushFollow(FollowSets000.FOLLOW_28);
            	    lv_rest_2_0=ruleBooleanStatementLvl7();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getBooleanStatementLvl6Rule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"rest",
            	            		lv_rest_2_0, 
            	            		"edu.clemson.simdase.Simdase.BooleanStatementLvl7");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop32;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBooleanStatementLvl6"


    // $ANTLR start "entryRuleBooleanStatementLvl7"
    // InternalSimdaseParser.g:3726:1: entryRuleBooleanStatementLvl7 returns [EObject current=null] : iv_ruleBooleanStatementLvl7= ruleBooleanStatementLvl7 EOF ;
    public final EObject entryRuleBooleanStatementLvl7() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBooleanStatementLvl7 = null;


        try {
            // InternalSimdaseParser.g:3727:2: (iv_ruleBooleanStatementLvl7= ruleBooleanStatementLvl7 EOF )
            // InternalSimdaseParser.g:3728:2: iv_ruleBooleanStatementLvl7= ruleBooleanStatementLvl7 EOF
            {
             newCompositeNode(grammarAccess.getBooleanStatementLvl7Rule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleBooleanStatementLvl7=ruleBooleanStatementLvl7();

            state._fsp--;

             current =iv_ruleBooleanStatementLvl7; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBooleanStatementLvl7"


    // $ANTLR start "ruleBooleanStatementLvl7"
    // InternalSimdaseParser.g:3735:1: ruleBooleanStatementLvl7 returns [EObject current=null] : ( ( (lv_statement_0_0= ruleBooleanStatementLvl8 ) ) ( ( (lv_op_1_0= AsteriskAsterisk ) ) ( (lv_rest_2_0= ruleBooleanStatementLvl8 ) ) )* ) ;
    public final EObject ruleBooleanStatementLvl7() throws RecognitionException {
        EObject current = null;

        Token lv_op_1_0=null;
        EObject lv_statement_0_0 = null;

        EObject lv_rest_2_0 = null;


         enterRule(); 
            
        try {
            // InternalSimdaseParser.g:3738:28: ( ( ( (lv_statement_0_0= ruleBooleanStatementLvl8 ) ) ( ( (lv_op_1_0= AsteriskAsterisk ) ) ( (lv_rest_2_0= ruleBooleanStatementLvl8 ) ) )* ) )
            // InternalSimdaseParser.g:3739:1: ( ( (lv_statement_0_0= ruleBooleanStatementLvl8 ) ) ( ( (lv_op_1_0= AsteriskAsterisk ) ) ( (lv_rest_2_0= ruleBooleanStatementLvl8 ) ) )* )
            {
            // InternalSimdaseParser.g:3739:1: ( ( (lv_statement_0_0= ruleBooleanStatementLvl8 ) ) ( ( (lv_op_1_0= AsteriskAsterisk ) ) ( (lv_rest_2_0= ruleBooleanStatementLvl8 ) ) )* )
            // InternalSimdaseParser.g:3739:2: ( (lv_statement_0_0= ruleBooleanStatementLvl8 ) ) ( ( (lv_op_1_0= AsteriskAsterisk ) ) ( (lv_rest_2_0= ruleBooleanStatementLvl8 ) ) )*
            {
            // InternalSimdaseParser.g:3739:2: ( (lv_statement_0_0= ruleBooleanStatementLvl8 ) )
            // InternalSimdaseParser.g:3740:1: (lv_statement_0_0= ruleBooleanStatementLvl8 )
            {
            // InternalSimdaseParser.g:3740:1: (lv_statement_0_0= ruleBooleanStatementLvl8 )
            // InternalSimdaseParser.g:3741:3: lv_statement_0_0= ruleBooleanStatementLvl8
            {
             
            	        newCompositeNode(grammarAccess.getBooleanStatementLvl7Access().getStatementBooleanStatementLvl8ParserRuleCall_0_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_29);
            lv_statement_0_0=ruleBooleanStatementLvl8();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getBooleanStatementLvl7Rule());
            	        }
                   		set(
                   			current, 
                   			"statement",
                    		lv_statement_0_0, 
                    		"edu.clemson.simdase.Simdase.BooleanStatementLvl8");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // InternalSimdaseParser.g:3757:2: ( ( (lv_op_1_0= AsteriskAsterisk ) ) ( (lv_rest_2_0= ruleBooleanStatementLvl8 ) ) )*
            loop33:
            do {
                int alt33=2;
                int LA33_0 = input.LA(1);

                if ( (LA33_0==AsteriskAsterisk) ) {
                    alt33=1;
                }


                switch (alt33) {
            	case 1 :
            	    // InternalSimdaseParser.g:3757:3: ( (lv_op_1_0= AsteriskAsterisk ) ) ( (lv_rest_2_0= ruleBooleanStatementLvl8 ) )
            	    {
            	    // InternalSimdaseParser.g:3757:3: ( (lv_op_1_0= AsteriskAsterisk ) )
            	    // InternalSimdaseParser.g:3758:1: (lv_op_1_0= AsteriskAsterisk )
            	    {
            	    // InternalSimdaseParser.g:3758:1: (lv_op_1_0= AsteriskAsterisk )
            	    // InternalSimdaseParser.g:3759:3: lv_op_1_0= AsteriskAsterisk
            	    {
            	    lv_op_1_0=(Token)match(input,AsteriskAsterisk,FollowSets000.FOLLOW_31); 

            	            newLeafNode(lv_op_1_0, grammarAccess.getBooleanStatementLvl7Access().getOpAsteriskAsteriskKeyword_1_0_0());
            	        

            	    	        if (current==null) {
            	    	            current = createModelElement(grammarAccess.getBooleanStatementLvl7Rule());
            	    	        }
            	           		addWithLastConsumed(current, "op", lv_op_1_0, "**");
            	    	    

            	    }


            	    }

            	    // InternalSimdaseParser.g:3773:2: ( (lv_rest_2_0= ruleBooleanStatementLvl8 ) )
            	    // InternalSimdaseParser.g:3774:1: (lv_rest_2_0= ruleBooleanStatementLvl8 )
            	    {
            	    // InternalSimdaseParser.g:3774:1: (lv_rest_2_0= ruleBooleanStatementLvl8 )
            	    // InternalSimdaseParser.g:3775:3: lv_rest_2_0= ruleBooleanStatementLvl8
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getBooleanStatementLvl7Access().getRestBooleanStatementLvl8ParserRuleCall_1_1_0()); 
            	    	    
            	    pushFollow(FollowSets000.FOLLOW_29);
            	    lv_rest_2_0=ruleBooleanStatementLvl8();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getBooleanStatementLvl7Rule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"rest",
            	            		lv_rest_2_0, 
            	            		"edu.clemson.simdase.Simdase.BooleanStatementLvl8");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop33;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBooleanStatementLvl7"


    // $ANTLR start "entryRuleBooleanStatementLvl8"
    // InternalSimdaseParser.g:3799:1: entryRuleBooleanStatementLvl8 returns [EObject current=null] : iv_ruleBooleanStatementLvl8= ruleBooleanStatementLvl8 EOF ;
    public final EObject entryRuleBooleanStatementLvl8() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBooleanStatementLvl8 = null;


        try {
            // InternalSimdaseParser.g:3800:2: (iv_ruleBooleanStatementLvl8= ruleBooleanStatementLvl8 EOF )
            // InternalSimdaseParser.g:3801:2: iv_ruleBooleanStatementLvl8= ruleBooleanStatementLvl8 EOF
            {
             newCompositeNode(grammarAccess.getBooleanStatementLvl8Rule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleBooleanStatementLvl8=ruleBooleanStatementLvl8();

            state._fsp--;

             current =iv_ruleBooleanStatementLvl8; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBooleanStatementLvl8"


    // $ANTLR start "ruleBooleanStatementLvl8"
    // InternalSimdaseParser.g:3808:1: ruleBooleanStatementLvl8 returns [EObject current=null] : ( ( () ( (lv_value_1_0= ruleBoolean ) ) ) | ( () otherlv_3= T ) | ( () otherlv_5= I ) | ( () ( (lv_value_7_0= ruleNumber ) ) ) | ( () otherlv_9= Sin ( (lv_statement_10_0= ruleStatement ) ) otherlv_11= RightParenthesis ) | ( () otherlv_13= Cos ( (lv_statement_14_0= ruleStatement ) ) otherlv_15= RightParenthesis ) | ( () otherlv_17= Tan ( (lv_statement_18_0= ruleStatement ) ) otherlv_19= RightParenthesis ) | ( () otherlv_21= LeftParenthesis ( (lv_statement_22_0= ruleBooleanStatement ) ) otherlv_23= RightParenthesis ) | ( () otherlv_25= Random ) | ( () otherlv_27= S ) ) ;
    public final EObject ruleBooleanStatementLvl8() throws RecognitionException {
        EObject current = null;

        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        Token otherlv_13=null;
        Token otherlv_15=null;
        Token otherlv_17=null;
        Token otherlv_19=null;
        Token otherlv_21=null;
        Token otherlv_23=null;
        Token otherlv_25=null;
        Token otherlv_27=null;
        EObject lv_value_1_0 = null;

        EObject lv_value_7_0 = null;

        EObject lv_statement_10_0 = null;

        EObject lv_statement_14_0 = null;

        EObject lv_statement_18_0 = null;

        EObject lv_statement_22_0 = null;


         enterRule(); 
            
        try {
            // InternalSimdaseParser.g:3811:28: ( ( ( () ( (lv_value_1_0= ruleBoolean ) ) ) | ( () otherlv_3= T ) | ( () otherlv_5= I ) | ( () ( (lv_value_7_0= ruleNumber ) ) ) | ( () otherlv_9= Sin ( (lv_statement_10_0= ruleStatement ) ) otherlv_11= RightParenthesis ) | ( () otherlv_13= Cos ( (lv_statement_14_0= ruleStatement ) ) otherlv_15= RightParenthesis ) | ( () otherlv_17= Tan ( (lv_statement_18_0= ruleStatement ) ) otherlv_19= RightParenthesis ) | ( () otherlv_21= LeftParenthesis ( (lv_statement_22_0= ruleBooleanStatement ) ) otherlv_23= RightParenthesis ) | ( () otherlv_25= Random ) | ( () otherlv_27= S ) ) )
            // InternalSimdaseParser.g:3812:1: ( ( () ( (lv_value_1_0= ruleBoolean ) ) ) | ( () otherlv_3= T ) | ( () otherlv_5= I ) | ( () ( (lv_value_7_0= ruleNumber ) ) ) | ( () otherlv_9= Sin ( (lv_statement_10_0= ruleStatement ) ) otherlv_11= RightParenthesis ) | ( () otherlv_13= Cos ( (lv_statement_14_0= ruleStatement ) ) otherlv_15= RightParenthesis ) | ( () otherlv_17= Tan ( (lv_statement_18_0= ruleStatement ) ) otherlv_19= RightParenthesis ) | ( () otherlv_21= LeftParenthesis ( (lv_statement_22_0= ruleBooleanStatement ) ) otherlv_23= RightParenthesis ) | ( () otherlv_25= Random ) | ( () otherlv_27= S ) )
            {
            // InternalSimdaseParser.g:3812:1: ( ( () ( (lv_value_1_0= ruleBoolean ) ) ) | ( () otherlv_3= T ) | ( () otherlv_5= I ) | ( () ( (lv_value_7_0= ruleNumber ) ) ) | ( () otherlv_9= Sin ( (lv_statement_10_0= ruleStatement ) ) otherlv_11= RightParenthesis ) | ( () otherlv_13= Cos ( (lv_statement_14_0= ruleStatement ) ) otherlv_15= RightParenthesis ) | ( () otherlv_17= Tan ( (lv_statement_18_0= ruleStatement ) ) otherlv_19= RightParenthesis ) | ( () otherlv_21= LeftParenthesis ( (lv_statement_22_0= ruleBooleanStatement ) ) otherlv_23= RightParenthesis ) | ( () otherlv_25= Random ) | ( () otherlv_27= S ) )
            int alt34=10;
            switch ( input.LA(1) ) {
            case False:
            case True:
                {
                alt34=1;
                }
                break;
            case T:
                {
                alt34=2;
                }
                break;
            case I:
                {
                alt34=3;
                }
                break;
            case HyphenMinus:
            case RULE_FLOAT:
                {
                alt34=4;
                }
                break;
            case Sin:
                {
                alt34=5;
                }
                break;
            case Cos:
                {
                alt34=6;
                }
                break;
            case Tan:
                {
                alt34=7;
                }
                break;
            case LeftParenthesis:
                {
                alt34=8;
                }
                break;
            case Random:
                {
                alt34=9;
                }
                break;
            case S:
                {
                alt34=10;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 34, 0, input);

                throw nvae;
            }

            switch (alt34) {
                case 1 :
                    // InternalSimdaseParser.g:3812:2: ( () ( (lv_value_1_0= ruleBoolean ) ) )
                    {
                    // InternalSimdaseParser.g:3812:2: ( () ( (lv_value_1_0= ruleBoolean ) ) )
                    // InternalSimdaseParser.g:3812:3: () ( (lv_value_1_0= ruleBoolean ) )
                    {
                    // InternalSimdaseParser.g:3812:3: ()
                    // InternalSimdaseParser.g:3813:5: 
                    {

                            current = forceCreateModelElement(
                                grammarAccess.getBooleanStatementLvl8Access().getBoolBooleanAction_0_0(),
                                current);
                        

                    }

                    // InternalSimdaseParser.g:3818:2: ( (lv_value_1_0= ruleBoolean ) )
                    // InternalSimdaseParser.g:3819:1: (lv_value_1_0= ruleBoolean )
                    {
                    // InternalSimdaseParser.g:3819:1: (lv_value_1_0= ruleBoolean )
                    // InternalSimdaseParser.g:3820:3: lv_value_1_0= ruleBoolean
                    {
                     
                    	        newCompositeNode(grammarAccess.getBooleanStatementLvl8Access().getValueBooleanParserRuleCall_0_1_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_value_1_0=ruleBoolean();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getBooleanStatementLvl8Rule());
                    	        }
                           		set(
                           			current, 
                           			"value",
                            		lv_value_1_0, 
                            		"edu.clemson.simdase.Simdase.Boolean");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalSimdaseParser.g:3837:6: ( () otherlv_3= T )
                    {
                    // InternalSimdaseParser.g:3837:6: ( () otherlv_3= T )
                    // InternalSimdaseParser.g:3837:7: () otherlv_3= T
                    {
                    // InternalSimdaseParser.g:3837:7: ()
                    // InternalSimdaseParser.g:3838:5: 
                    {

                            current = forceCreateModelElement(
                                grammarAccess.getBooleanStatementLvl8Access().getBoolTAction_1_0(),
                                current);
                        

                    }

                    otherlv_3=(Token)match(input,T,FollowSets000.FOLLOW_2); 

                        	newLeafNode(otherlv_3, grammarAccess.getBooleanStatementLvl8Access().getTKeyword_1_1());
                        

                    }


                    }
                    break;
                case 3 :
                    // InternalSimdaseParser.g:3849:6: ( () otherlv_5= I )
                    {
                    // InternalSimdaseParser.g:3849:6: ( () otherlv_5= I )
                    // InternalSimdaseParser.g:3849:7: () otherlv_5= I
                    {
                    // InternalSimdaseParser.g:3849:7: ()
                    // InternalSimdaseParser.g:3850:5: 
                    {

                            current = forceCreateModelElement(
                                grammarAccess.getBooleanStatementLvl8Access().getBoolIAction_2_0(),
                                current);
                        

                    }

                    otherlv_5=(Token)match(input,I,FollowSets000.FOLLOW_2); 

                        	newLeafNode(otherlv_5, grammarAccess.getBooleanStatementLvl8Access().getIKeyword_2_1());
                        

                    }


                    }
                    break;
                case 4 :
                    // InternalSimdaseParser.g:3861:6: ( () ( (lv_value_7_0= ruleNumber ) ) )
                    {
                    // InternalSimdaseParser.g:3861:6: ( () ( (lv_value_7_0= ruleNumber ) ) )
                    // InternalSimdaseParser.g:3861:7: () ( (lv_value_7_0= ruleNumber ) )
                    {
                    // InternalSimdaseParser.g:3861:7: ()
                    // InternalSimdaseParser.g:3862:5: 
                    {

                            current = forceCreateModelElement(
                                grammarAccess.getBooleanStatementLvl8Access().getBoolIntegerAction_3_0(),
                                current);
                        

                    }

                    // InternalSimdaseParser.g:3867:2: ( (lv_value_7_0= ruleNumber ) )
                    // InternalSimdaseParser.g:3868:1: (lv_value_7_0= ruleNumber )
                    {
                    // InternalSimdaseParser.g:3868:1: (lv_value_7_0= ruleNumber )
                    // InternalSimdaseParser.g:3869:3: lv_value_7_0= ruleNumber
                    {
                     
                    	        newCompositeNode(grammarAccess.getBooleanStatementLvl8Access().getValueNumberParserRuleCall_3_1_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_value_7_0=ruleNumber();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getBooleanStatementLvl8Rule());
                    	        }
                           		set(
                           			current, 
                           			"value",
                            		lv_value_7_0, 
                            		"edu.clemson.simdase.Simdase.Number");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 5 :
                    // InternalSimdaseParser.g:3886:6: ( () otherlv_9= Sin ( (lv_statement_10_0= ruleStatement ) ) otherlv_11= RightParenthesis )
                    {
                    // InternalSimdaseParser.g:3886:6: ( () otherlv_9= Sin ( (lv_statement_10_0= ruleStatement ) ) otherlv_11= RightParenthesis )
                    // InternalSimdaseParser.g:3886:7: () otherlv_9= Sin ( (lv_statement_10_0= ruleStatement ) ) otherlv_11= RightParenthesis
                    {
                    // InternalSimdaseParser.g:3886:7: ()
                    // InternalSimdaseParser.g:3887:5: 
                    {

                            current = forceCreateModelElement(
                                grammarAccess.getBooleanStatementLvl8Access().getBoolSinAction_4_0(),
                                current);
                        

                    }

                    otherlv_9=(Token)match(input,Sin,FollowSets000.FOLLOW_8); 

                        	newLeafNode(otherlv_9, grammarAccess.getBooleanStatementLvl8Access().getSinKeyword_4_1());
                        
                    // InternalSimdaseParser.g:3897:1: ( (lv_statement_10_0= ruleStatement ) )
                    // InternalSimdaseParser.g:3898:1: (lv_statement_10_0= ruleStatement )
                    {
                    // InternalSimdaseParser.g:3898:1: (lv_statement_10_0= ruleStatement )
                    // InternalSimdaseParser.g:3899:3: lv_statement_10_0= ruleStatement
                    {
                     
                    	        newCompositeNode(grammarAccess.getBooleanStatementLvl8Access().getStatementStatementParserRuleCall_4_2_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_30);
                    lv_statement_10_0=ruleStatement();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getBooleanStatementLvl8Rule());
                    	        }
                           		set(
                           			current, 
                           			"statement",
                            		lv_statement_10_0, 
                            		"edu.clemson.simdase.Simdase.Statement");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    otherlv_11=(Token)match(input,RightParenthesis,FollowSets000.FOLLOW_2); 

                        	newLeafNode(otherlv_11, grammarAccess.getBooleanStatementLvl8Access().getRightParenthesisKeyword_4_3());
                        

                    }


                    }
                    break;
                case 6 :
                    // InternalSimdaseParser.g:3921:6: ( () otherlv_13= Cos ( (lv_statement_14_0= ruleStatement ) ) otherlv_15= RightParenthesis )
                    {
                    // InternalSimdaseParser.g:3921:6: ( () otherlv_13= Cos ( (lv_statement_14_0= ruleStatement ) ) otherlv_15= RightParenthesis )
                    // InternalSimdaseParser.g:3921:7: () otherlv_13= Cos ( (lv_statement_14_0= ruleStatement ) ) otherlv_15= RightParenthesis
                    {
                    // InternalSimdaseParser.g:3921:7: ()
                    // InternalSimdaseParser.g:3922:5: 
                    {

                            current = forceCreateModelElement(
                                grammarAccess.getBooleanStatementLvl8Access().getBoolCosAction_5_0(),
                                current);
                        

                    }

                    otherlv_13=(Token)match(input,Cos,FollowSets000.FOLLOW_8); 

                        	newLeafNode(otherlv_13, grammarAccess.getBooleanStatementLvl8Access().getCosKeyword_5_1());
                        
                    // InternalSimdaseParser.g:3932:1: ( (lv_statement_14_0= ruleStatement ) )
                    // InternalSimdaseParser.g:3933:1: (lv_statement_14_0= ruleStatement )
                    {
                    // InternalSimdaseParser.g:3933:1: (lv_statement_14_0= ruleStatement )
                    // InternalSimdaseParser.g:3934:3: lv_statement_14_0= ruleStatement
                    {
                     
                    	        newCompositeNode(grammarAccess.getBooleanStatementLvl8Access().getStatementStatementParserRuleCall_5_2_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_30);
                    lv_statement_14_0=ruleStatement();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getBooleanStatementLvl8Rule());
                    	        }
                           		set(
                           			current, 
                           			"statement",
                            		lv_statement_14_0, 
                            		"edu.clemson.simdase.Simdase.Statement");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    otherlv_15=(Token)match(input,RightParenthesis,FollowSets000.FOLLOW_2); 

                        	newLeafNode(otherlv_15, grammarAccess.getBooleanStatementLvl8Access().getRightParenthesisKeyword_5_3());
                        

                    }


                    }
                    break;
                case 7 :
                    // InternalSimdaseParser.g:3956:6: ( () otherlv_17= Tan ( (lv_statement_18_0= ruleStatement ) ) otherlv_19= RightParenthesis )
                    {
                    // InternalSimdaseParser.g:3956:6: ( () otherlv_17= Tan ( (lv_statement_18_0= ruleStatement ) ) otherlv_19= RightParenthesis )
                    // InternalSimdaseParser.g:3956:7: () otherlv_17= Tan ( (lv_statement_18_0= ruleStatement ) ) otherlv_19= RightParenthesis
                    {
                    // InternalSimdaseParser.g:3956:7: ()
                    // InternalSimdaseParser.g:3957:5: 
                    {

                            current = forceCreateModelElement(
                                grammarAccess.getBooleanStatementLvl8Access().getBoolTanAction_6_0(),
                                current);
                        

                    }

                    otherlv_17=(Token)match(input,Tan,FollowSets000.FOLLOW_8); 

                        	newLeafNode(otherlv_17, grammarAccess.getBooleanStatementLvl8Access().getTanKeyword_6_1());
                        
                    // InternalSimdaseParser.g:3967:1: ( (lv_statement_18_0= ruleStatement ) )
                    // InternalSimdaseParser.g:3968:1: (lv_statement_18_0= ruleStatement )
                    {
                    // InternalSimdaseParser.g:3968:1: (lv_statement_18_0= ruleStatement )
                    // InternalSimdaseParser.g:3969:3: lv_statement_18_0= ruleStatement
                    {
                     
                    	        newCompositeNode(grammarAccess.getBooleanStatementLvl8Access().getStatementStatementParserRuleCall_6_2_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_30);
                    lv_statement_18_0=ruleStatement();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getBooleanStatementLvl8Rule());
                    	        }
                           		set(
                           			current, 
                           			"statement",
                            		lv_statement_18_0, 
                            		"edu.clemson.simdase.Simdase.Statement");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    otherlv_19=(Token)match(input,RightParenthesis,FollowSets000.FOLLOW_2); 

                        	newLeafNode(otherlv_19, grammarAccess.getBooleanStatementLvl8Access().getRightParenthesisKeyword_6_3());
                        

                    }


                    }
                    break;
                case 8 :
                    // InternalSimdaseParser.g:3991:6: ( () otherlv_21= LeftParenthesis ( (lv_statement_22_0= ruleBooleanStatement ) ) otherlv_23= RightParenthesis )
                    {
                    // InternalSimdaseParser.g:3991:6: ( () otherlv_21= LeftParenthesis ( (lv_statement_22_0= ruleBooleanStatement ) ) otherlv_23= RightParenthesis )
                    // InternalSimdaseParser.g:3991:7: () otherlv_21= LeftParenthesis ( (lv_statement_22_0= ruleBooleanStatement ) ) otherlv_23= RightParenthesis
                    {
                    // InternalSimdaseParser.g:3991:7: ()
                    // InternalSimdaseParser.g:3992:5: 
                    {

                            current = forceCreateModelElement(
                                grammarAccess.getBooleanStatementLvl8Access().getBoolParenAction_7_0(),
                                current);
                        

                    }

                    otherlv_21=(Token)match(input,LeftParenthesis,FollowSets000.FOLLOW_31); 

                        	newLeafNode(otherlv_21, grammarAccess.getBooleanStatementLvl8Access().getLeftParenthesisKeyword_7_1());
                        
                    // InternalSimdaseParser.g:4002:1: ( (lv_statement_22_0= ruleBooleanStatement ) )
                    // InternalSimdaseParser.g:4003:1: (lv_statement_22_0= ruleBooleanStatement )
                    {
                    // InternalSimdaseParser.g:4003:1: (lv_statement_22_0= ruleBooleanStatement )
                    // InternalSimdaseParser.g:4004:3: lv_statement_22_0= ruleBooleanStatement
                    {
                     
                    	        newCompositeNode(grammarAccess.getBooleanStatementLvl8Access().getStatementBooleanStatementParserRuleCall_7_2_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_30);
                    lv_statement_22_0=ruleBooleanStatement();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getBooleanStatementLvl8Rule());
                    	        }
                           		set(
                           			current, 
                           			"statement",
                            		lv_statement_22_0, 
                            		"edu.clemson.simdase.Simdase.BooleanStatement");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    otherlv_23=(Token)match(input,RightParenthesis,FollowSets000.FOLLOW_2); 

                        	newLeafNode(otherlv_23, grammarAccess.getBooleanStatementLvl8Access().getRightParenthesisKeyword_7_3());
                        

                    }


                    }
                    break;
                case 9 :
                    // InternalSimdaseParser.g:4026:6: ( () otherlv_25= Random )
                    {
                    // InternalSimdaseParser.g:4026:6: ( () otherlv_25= Random )
                    // InternalSimdaseParser.g:4026:7: () otherlv_25= Random
                    {
                    // InternalSimdaseParser.g:4026:7: ()
                    // InternalSimdaseParser.g:4027:5: 
                    {

                            current = forceCreateModelElement(
                                grammarAccess.getBooleanStatementLvl8Access().getBoolRandomAction_8_0(),
                                current);
                        

                    }

                    otherlv_25=(Token)match(input,Random,FollowSets000.FOLLOW_2); 

                        	newLeafNode(otherlv_25, grammarAccess.getBooleanStatementLvl8Access().getRandomKeyword_8_1());
                        

                    }


                    }
                    break;
                case 10 :
                    // InternalSimdaseParser.g:4038:6: ( () otherlv_27= S )
                    {
                    // InternalSimdaseParser.g:4038:6: ( () otherlv_27= S )
                    // InternalSimdaseParser.g:4038:7: () otherlv_27= S
                    {
                    // InternalSimdaseParser.g:4038:7: ()
                    // InternalSimdaseParser.g:4039:5: 
                    {

                            current = forceCreateModelElement(
                                grammarAccess.getBooleanStatementLvl8Access().getBoolScalingFactorAction_9_0(),
                                current);
                        

                    }

                    otherlv_27=(Token)match(input,S,FollowSets000.FOLLOW_2); 

                        	newLeafNode(otherlv_27, grammarAccess.getBooleanStatementLvl8Access().getSKeyword_9_1());
                        

                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBooleanStatementLvl8"


    // $ANTLR start "entryRuleContainedPropertyAssociation"
    // InternalSimdaseParser.g:4059:1: entryRuleContainedPropertyAssociation returns [EObject current=null] : iv_ruleContainedPropertyAssociation= ruleContainedPropertyAssociation EOF ;
    public final EObject entryRuleContainedPropertyAssociation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleContainedPropertyAssociation = null;


        try {
            // InternalSimdaseParser.g:4060:2: (iv_ruleContainedPropertyAssociation= ruleContainedPropertyAssociation EOF )
            // InternalSimdaseParser.g:4061:2: iv_ruleContainedPropertyAssociation= ruleContainedPropertyAssociation EOF
            {
             newCompositeNode(grammarAccess.getContainedPropertyAssociationRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleContainedPropertyAssociation=ruleContainedPropertyAssociation();

            state._fsp--;

             current =iv_ruleContainedPropertyAssociation; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleContainedPropertyAssociation"


    // $ANTLR start "ruleContainedPropertyAssociation"
    // InternalSimdaseParser.g:4068:1: ruleContainedPropertyAssociation returns [EObject current=null] : ( ( ( ruleQPREF ) ) (otherlv_1= EqualsSignGreaterThanSign | ( (lv_append_2_0= PlusSignEqualsSignGreaterThanSign ) ) ) ( (lv_constant_3_0= Constant ) )? ( ( (lv_ownedValue_4_0= ruleOptionalModalPropertyValue ) ) (otherlv_5= Comma ( (lv_ownedValue_6_0= ruleOptionalModalPropertyValue ) ) )* ) ( ruleAppliesToKeywords ( (lv_appliesTo_8_0= ruleContainmentPath ) ) (otherlv_9= Comma ( (lv_appliesTo_10_0= ruleContainmentPath ) ) )* )? ( ruleInBindingKeywords otherlv_12= LeftParenthesis ( ( ruleQCREF ) ) otherlv_14= RightParenthesis )? otherlv_15= Semicolon ) ;
    public final EObject ruleContainedPropertyAssociation() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_append_2_0=null;
        Token lv_constant_3_0=null;
        Token otherlv_5=null;
        Token otherlv_9=null;
        Token otherlv_12=null;
        Token otherlv_14=null;
        Token otherlv_15=null;
        EObject lv_ownedValue_4_0 = null;

        EObject lv_ownedValue_6_0 = null;

        EObject lv_appliesTo_8_0 = null;

        EObject lv_appliesTo_10_0 = null;


         enterRule(); 
            
        try {
            // InternalSimdaseParser.g:4071:28: ( ( ( ( ruleQPREF ) ) (otherlv_1= EqualsSignGreaterThanSign | ( (lv_append_2_0= PlusSignEqualsSignGreaterThanSign ) ) ) ( (lv_constant_3_0= Constant ) )? ( ( (lv_ownedValue_4_0= ruleOptionalModalPropertyValue ) ) (otherlv_5= Comma ( (lv_ownedValue_6_0= ruleOptionalModalPropertyValue ) ) )* ) ( ruleAppliesToKeywords ( (lv_appliesTo_8_0= ruleContainmentPath ) ) (otherlv_9= Comma ( (lv_appliesTo_10_0= ruleContainmentPath ) ) )* )? ( ruleInBindingKeywords otherlv_12= LeftParenthesis ( ( ruleQCREF ) ) otherlv_14= RightParenthesis )? otherlv_15= Semicolon ) )
            // InternalSimdaseParser.g:4072:1: ( ( ( ruleQPREF ) ) (otherlv_1= EqualsSignGreaterThanSign | ( (lv_append_2_0= PlusSignEqualsSignGreaterThanSign ) ) ) ( (lv_constant_3_0= Constant ) )? ( ( (lv_ownedValue_4_0= ruleOptionalModalPropertyValue ) ) (otherlv_5= Comma ( (lv_ownedValue_6_0= ruleOptionalModalPropertyValue ) ) )* ) ( ruleAppliesToKeywords ( (lv_appliesTo_8_0= ruleContainmentPath ) ) (otherlv_9= Comma ( (lv_appliesTo_10_0= ruleContainmentPath ) ) )* )? ( ruleInBindingKeywords otherlv_12= LeftParenthesis ( ( ruleQCREF ) ) otherlv_14= RightParenthesis )? otherlv_15= Semicolon )
            {
            // InternalSimdaseParser.g:4072:1: ( ( ( ruleQPREF ) ) (otherlv_1= EqualsSignGreaterThanSign | ( (lv_append_2_0= PlusSignEqualsSignGreaterThanSign ) ) ) ( (lv_constant_3_0= Constant ) )? ( ( (lv_ownedValue_4_0= ruleOptionalModalPropertyValue ) ) (otherlv_5= Comma ( (lv_ownedValue_6_0= ruleOptionalModalPropertyValue ) ) )* ) ( ruleAppliesToKeywords ( (lv_appliesTo_8_0= ruleContainmentPath ) ) (otherlv_9= Comma ( (lv_appliesTo_10_0= ruleContainmentPath ) ) )* )? ( ruleInBindingKeywords otherlv_12= LeftParenthesis ( ( ruleQCREF ) ) otherlv_14= RightParenthesis )? otherlv_15= Semicolon )
            // InternalSimdaseParser.g:4072:2: ( ( ruleQPREF ) ) (otherlv_1= EqualsSignGreaterThanSign | ( (lv_append_2_0= PlusSignEqualsSignGreaterThanSign ) ) ) ( (lv_constant_3_0= Constant ) )? ( ( (lv_ownedValue_4_0= ruleOptionalModalPropertyValue ) ) (otherlv_5= Comma ( (lv_ownedValue_6_0= ruleOptionalModalPropertyValue ) ) )* ) ( ruleAppliesToKeywords ( (lv_appliesTo_8_0= ruleContainmentPath ) ) (otherlv_9= Comma ( (lv_appliesTo_10_0= ruleContainmentPath ) ) )* )? ( ruleInBindingKeywords otherlv_12= LeftParenthesis ( ( ruleQCREF ) ) otherlv_14= RightParenthesis )? otherlv_15= Semicolon
            {
            // InternalSimdaseParser.g:4072:2: ( ( ruleQPREF ) )
            // InternalSimdaseParser.g:4073:1: ( ruleQPREF )
            {
            // InternalSimdaseParser.g:4073:1: ( ruleQPREF )
            // InternalSimdaseParser.g:4074:3: ruleQPREF
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getContainedPropertyAssociationRule());
            	        }
                    
             
            	        newCompositeNode(grammarAccess.getContainedPropertyAssociationAccess().getPropertyPropertyCrossReference_0_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_36);
            ruleQPREF();

            state._fsp--;

             
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // InternalSimdaseParser.g:4088:2: (otherlv_1= EqualsSignGreaterThanSign | ( (lv_append_2_0= PlusSignEqualsSignGreaterThanSign ) ) )
            int alt35=2;
            int LA35_0 = input.LA(1);

            if ( (LA35_0==EqualsSignGreaterThanSign) ) {
                alt35=1;
            }
            else if ( (LA35_0==PlusSignEqualsSignGreaterThanSign) ) {
                alt35=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 35, 0, input);

                throw nvae;
            }
            switch (alt35) {
                case 1 :
                    // InternalSimdaseParser.g:4089:2: otherlv_1= EqualsSignGreaterThanSign
                    {
                    otherlv_1=(Token)match(input,EqualsSignGreaterThanSign,FollowSets000.FOLLOW_37); 

                        	newLeafNode(otherlv_1, grammarAccess.getContainedPropertyAssociationAccess().getEqualsSignGreaterThanSignKeyword_1_0());
                        

                    }
                    break;
                case 2 :
                    // InternalSimdaseParser.g:4094:6: ( (lv_append_2_0= PlusSignEqualsSignGreaterThanSign ) )
                    {
                    // InternalSimdaseParser.g:4094:6: ( (lv_append_2_0= PlusSignEqualsSignGreaterThanSign ) )
                    // InternalSimdaseParser.g:4095:1: (lv_append_2_0= PlusSignEqualsSignGreaterThanSign )
                    {
                    // InternalSimdaseParser.g:4095:1: (lv_append_2_0= PlusSignEqualsSignGreaterThanSign )
                    // InternalSimdaseParser.g:4096:3: lv_append_2_0= PlusSignEqualsSignGreaterThanSign
                    {
                    lv_append_2_0=(Token)match(input,PlusSignEqualsSignGreaterThanSign,FollowSets000.FOLLOW_37); 

                            newLeafNode(lv_append_2_0, grammarAccess.getContainedPropertyAssociationAccess().getAppendPlusSignEqualsSignGreaterThanSignKeyword_1_1_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getContainedPropertyAssociationRule());
                    	        }
                           		setWithLastConsumed(current, "append", true, "+=>");
                    	    

                    }


                    }


                    }
                    break;

            }

            // InternalSimdaseParser.g:4110:3: ( (lv_constant_3_0= Constant ) )?
            int alt36=2;
            int LA36_0 = input.LA(1);

            if ( (LA36_0==Constant) ) {
                alt36=1;
            }
            switch (alt36) {
                case 1 :
                    // InternalSimdaseParser.g:4111:1: (lv_constant_3_0= Constant )
                    {
                    // InternalSimdaseParser.g:4111:1: (lv_constant_3_0= Constant )
                    // InternalSimdaseParser.g:4112:3: lv_constant_3_0= Constant
                    {
                    lv_constant_3_0=(Token)match(input,Constant,FollowSets000.FOLLOW_37); 

                            newLeafNode(lv_constant_3_0, grammarAccess.getContainedPropertyAssociationAccess().getConstantConstantKeyword_2_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getContainedPropertyAssociationRule());
                    	        }
                           		setWithLastConsumed(current, "constant", true, "constant");
                    	    

                    }


                    }
                    break;

            }

            // InternalSimdaseParser.g:4126:3: ( ( (lv_ownedValue_4_0= ruleOptionalModalPropertyValue ) ) (otherlv_5= Comma ( (lv_ownedValue_6_0= ruleOptionalModalPropertyValue ) ) )* )
            // InternalSimdaseParser.g:4126:4: ( (lv_ownedValue_4_0= ruleOptionalModalPropertyValue ) ) (otherlv_5= Comma ( (lv_ownedValue_6_0= ruleOptionalModalPropertyValue ) ) )*
            {
            // InternalSimdaseParser.g:4126:4: ( (lv_ownedValue_4_0= ruleOptionalModalPropertyValue ) )
            // InternalSimdaseParser.g:4127:1: (lv_ownedValue_4_0= ruleOptionalModalPropertyValue )
            {
            // InternalSimdaseParser.g:4127:1: (lv_ownedValue_4_0= ruleOptionalModalPropertyValue )
            // InternalSimdaseParser.g:4128:3: lv_ownedValue_4_0= ruleOptionalModalPropertyValue
            {
             
            	        newCompositeNode(grammarAccess.getContainedPropertyAssociationAccess().getOwnedValueOptionalModalPropertyValueParserRuleCall_3_0_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_38);
            lv_ownedValue_4_0=ruleOptionalModalPropertyValue();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getContainedPropertyAssociationRule());
            	        }
                   		add(
                   			current, 
                   			"ownedValue",
                    		lv_ownedValue_4_0, 
                    		"org.osate.xtext.aadl2.properties.Properties.OptionalModalPropertyValue");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // InternalSimdaseParser.g:4144:2: (otherlv_5= Comma ( (lv_ownedValue_6_0= ruleOptionalModalPropertyValue ) ) )*
            loop37:
            do {
                int alt37=2;
                int LA37_0 = input.LA(1);

                if ( (LA37_0==Comma) ) {
                    alt37=1;
                }


                switch (alt37) {
            	case 1 :
            	    // InternalSimdaseParser.g:4145:2: otherlv_5= Comma ( (lv_ownedValue_6_0= ruleOptionalModalPropertyValue ) )
            	    {
            	    otherlv_5=(Token)match(input,Comma,FollowSets000.FOLLOW_37); 

            	        	newLeafNode(otherlv_5, grammarAccess.getContainedPropertyAssociationAccess().getCommaKeyword_3_1_0());
            	        
            	    // InternalSimdaseParser.g:4149:1: ( (lv_ownedValue_6_0= ruleOptionalModalPropertyValue ) )
            	    // InternalSimdaseParser.g:4150:1: (lv_ownedValue_6_0= ruleOptionalModalPropertyValue )
            	    {
            	    // InternalSimdaseParser.g:4150:1: (lv_ownedValue_6_0= ruleOptionalModalPropertyValue )
            	    // InternalSimdaseParser.g:4151:3: lv_ownedValue_6_0= ruleOptionalModalPropertyValue
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getContainedPropertyAssociationAccess().getOwnedValueOptionalModalPropertyValueParserRuleCall_3_1_1_0()); 
            	    	    
            	    pushFollow(FollowSets000.FOLLOW_38);
            	    lv_ownedValue_6_0=ruleOptionalModalPropertyValue();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getContainedPropertyAssociationRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"ownedValue",
            	            		lv_ownedValue_6_0, 
            	            		"org.osate.xtext.aadl2.properties.Properties.OptionalModalPropertyValue");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop37;
                }
            } while (true);


            }

            // InternalSimdaseParser.g:4167:5: ( ruleAppliesToKeywords ( (lv_appliesTo_8_0= ruleContainmentPath ) ) (otherlv_9= Comma ( (lv_appliesTo_10_0= ruleContainmentPath ) ) )* )?
            int alt39=2;
            int LA39_0 = input.LA(1);

            if ( (LA39_0==Applies) ) {
                alt39=1;
            }
            switch (alt39) {
                case 1 :
                    // InternalSimdaseParser.g:4168:5: ruleAppliesToKeywords ( (lv_appliesTo_8_0= ruleContainmentPath ) ) (otherlv_9= Comma ( (lv_appliesTo_10_0= ruleContainmentPath ) ) )*
                    {
                     
                            newCompositeNode(grammarAccess.getContainedPropertyAssociationAccess().getAppliesToKeywordsParserRuleCall_4_0()); 
                        
                    pushFollow(FollowSets000.FOLLOW_39);
                    ruleAppliesToKeywords();

                    state._fsp--;


                            afterParserOrEnumRuleCall();
                        
                    // InternalSimdaseParser.g:4175:1: ( (lv_appliesTo_8_0= ruleContainmentPath ) )
                    // InternalSimdaseParser.g:4176:1: (lv_appliesTo_8_0= ruleContainmentPath )
                    {
                    // InternalSimdaseParser.g:4176:1: (lv_appliesTo_8_0= ruleContainmentPath )
                    // InternalSimdaseParser.g:4177:3: lv_appliesTo_8_0= ruleContainmentPath
                    {
                     
                    	        newCompositeNode(grammarAccess.getContainedPropertyAssociationAccess().getAppliesToContainmentPathParserRuleCall_4_1_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_40);
                    lv_appliesTo_8_0=ruleContainmentPath();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getContainedPropertyAssociationRule());
                    	        }
                           		add(
                           			current, 
                           			"appliesTo",
                            		lv_appliesTo_8_0, 
                            		"org.osate.xtext.aadl2.properties.Properties.ContainmentPath");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // InternalSimdaseParser.g:4193:2: (otherlv_9= Comma ( (lv_appliesTo_10_0= ruleContainmentPath ) ) )*
                    loop38:
                    do {
                        int alt38=2;
                        int LA38_0 = input.LA(1);

                        if ( (LA38_0==Comma) ) {
                            alt38=1;
                        }


                        switch (alt38) {
                    	case 1 :
                    	    // InternalSimdaseParser.g:4194:2: otherlv_9= Comma ( (lv_appliesTo_10_0= ruleContainmentPath ) )
                    	    {
                    	    otherlv_9=(Token)match(input,Comma,FollowSets000.FOLLOW_39); 

                    	        	newLeafNode(otherlv_9, grammarAccess.getContainedPropertyAssociationAccess().getCommaKeyword_4_2_0());
                    	        
                    	    // InternalSimdaseParser.g:4198:1: ( (lv_appliesTo_10_0= ruleContainmentPath ) )
                    	    // InternalSimdaseParser.g:4199:1: (lv_appliesTo_10_0= ruleContainmentPath )
                    	    {
                    	    // InternalSimdaseParser.g:4199:1: (lv_appliesTo_10_0= ruleContainmentPath )
                    	    // InternalSimdaseParser.g:4200:3: lv_appliesTo_10_0= ruleContainmentPath
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getContainedPropertyAssociationAccess().getAppliesToContainmentPathParserRuleCall_4_2_1_0()); 
                    	    	    
                    	    pushFollow(FollowSets000.FOLLOW_40);
                    	    lv_appliesTo_10_0=ruleContainmentPath();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getContainedPropertyAssociationRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"appliesTo",
                    	            		lv_appliesTo_10_0, 
                    	            		"org.osate.xtext.aadl2.properties.Properties.ContainmentPath");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop38;
                        }
                    } while (true);


                    }
                    break;

            }

            // InternalSimdaseParser.g:4216:6: ( ruleInBindingKeywords otherlv_12= LeftParenthesis ( ( ruleQCREF ) ) otherlv_14= RightParenthesis )?
            int alt40=2;
            int LA40_0 = input.LA(1);

            if ( (LA40_0==In) ) {
                alt40=1;
            }
            switch (alt40) {
                case 1 :
                    // InternalSimdaseParser.g:4217:5: ruleInBindingKeywords otherlv_12= LeftParenthesis ( ( ruleQCREF ) ) otherlv_14= RightParenthesis
                    {
                     
                            newCompositeNode(grammarAccess.getContainedPropertyAssociationAccess().getInBindingKeywordsParserRuleCall_5_0()); 
                        
                    pushFollow(FollowSets000.FOLLOW_41);
                    ruleInBindingKeywords();

                    state._fsp--;


                            afterParserOrEnumRuleCall();
                        
                    otherlv_12=(Token)match(input,LeftParenthesis,FollowSets000.FOLLOW_39); 

                        	newLeafNode(otherlv_12, grammarAccess.getContainedPropertyAssociationAccess().getLeftParenthesisKeyword_5_1());
                        
                    // InternalSimdaseParser.g:4229:1: ( ( ruleQCREF ) )
                    // InternalSimdaseParser.g:4230:1: ( ruleQCREF )
                    {
                    // InternalSimdaseParser.g:4230:1: ( ruleQCREF )
                    // InternalSimdaseParser.g:4231:3: ruleQCREF
                    {

                    			if (current==null) {
                    	            current = createModelElement(grammarAccess.getContainedPropertyAssociationRule());
                    	        }
                            
                     
                    	        newCompositeNode(grammarAccess.getContainedPropertyAssociationAccess().getInBindingClassifierCrossReference_5_2_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_30);
                    ruleQCREF();

                    state._fsp--;

                     
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    otherlv_14=(Token)match(input,RightParenthesis,FollowSets000.FOLLOW_6); 

                        	newLeafNode(otherlv_14, grammarAccess.getContainedPropertyAssociationAccess().getRightParenthesisKeyword_5_3());
                        

                    }
                    break;

            }

            otherlv_15=(Token)match(input,Semicolon,FollowSets000.FOLLOW_2); 

                	newLeafNode(otherlv_15, grammarAccess.getContainedPropertyAssociationAccess().getSemicolonKeyword_6());
                

            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleContainedPropertyAssociation"


    // $ANTLR start "entryRuleContainmentPath"
    // InternalSimdaseParser.g:4267:1: entryRuleContainmentPath returns [EObject current=null] : iv_ruleContainmentPath= ruleContainmentPath EOF ;
    public final EObject entryRuleContainmentPath() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleContainmentPath = null;


        try {
            // InternalSimdaseParser.g:4268:2: (iv_ruleContainmentPath= ruleContainmentPath EOF )
            // InternalSimdaseParser.g:4269:2: iv_ruleContainmentPath= ruleContainmentPath EOF
            {
             newCompositeNode(grammarAccess.getContainmentPathRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleContainmentPath=ruleContainmentPath();

            state._fsp--;

             current =iv_ruleContainmentPath; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleContainmentPath"


    // $ANTLR start "ruleContainmentPath"
    // InternalSimdaseParser.g:4276:1: ruleContainmentPath returns [EObject current=null] : ( (lv_path_0_0= ruleContainmentPathElement ) ) ;
    public final EObject ruleContainmentPath() throws RecognitionException {
        EObject current = null;

        EObject lv_path_0_0 = null;


         enterRule(); 
            
        try {
            // InternalSimdaseParser.g:4279:28: ( ( (lv_path_0_0= ruleContainmentPathElement ) ) )
            // InternalSimdaseParser.g:4280:1: ( (lv_path_0_0= ruleContainmentPathElement ) )
            {
            // InternalSimdaseParser.g:4280:1: ( (lv_path_0_0= ruleContainmentPathElement ) )
            // InternalSimdaseParser.g:4281:1: (lv_path_0_0= ruleContainmentPathElement )
            {
            // InternalSimdaseParser.g:4281:1: (lv_path_0_0= ruleContainmentPathElement )
            // InternalSimdaseParser.g:4282:3: lv_path_0_0= ruleContainmentPathElement
            {
             
            	        newCompositeNode(grammarAccess.getContainmentPathAccess().getPathContainmentPathElementParserRuleCall_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_2);
            lv_path_0_0=ruleContainmentPathElement();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getContainmentPathRule());
            	        }
                   		set(
                   			current, 
                   			"path",
                    		lv_path_0_0, 
                    		"org.osate.xtext.aadl2.properties.Properties.ContainmentPathElement");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleContainmentPath"


    // $ANTLR start "entryRuleOptionalModalPropertyValue"
    // InternalSimdaseParser.g:4308:1: entryRuleOptionalModalPropertyValue returns [EObject current=null] : iv_ruleOptionalModalPropertyValue= ruleOptionalModalPropertyValue EOF ;
    public final EObject entryRuleOptionalModalPropertyValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOptionalModalPropertyValue = null;


        try {
            // InternalSimdaseParser.g:4309:2: (iv_ruleOptionalModalPropertyValue= ruleOptionalModalPropertyValue EOF )
            // InternalSimdaseParser.g:4310:2: iv_ruleOptionalModalPropertyValue= ruleOptionalModalPropertyValue EOF
            {
             newCompositeNode(grammarAccess.getOptionalModalPropertyValueRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleOptionalModalPropertyValue=ruleOptionalModalPropertyValue();

            state._fsp--;

             current =iv_ruleOptionalModalPropertyValue; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOptionalModalPropertyValue"


    // $ANTLR start "ruleOptionalModalPropertyValue"
    // InternalSimdaseParser.g:4317:1: ruleOptionalModalPropertyValue returns [EObject current=null] : ( ( (lv_ownedValue_0_0= rulePropertyExpression ) ) ( ruleInModesKeywords otherlv_2= LeftParenthesis ( (otherlv_3= RULE_ID ) ) (otherlv_4= Comma ( (otherlv_5= RULE_ID ) ) )* otherlv_6= RightParenthesis )? ) ;
    public final EObject ruleOptionalModalPropertyValue() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        EObject lv_ownedValue_0_0 = null;


         enterRule(); 
            
        try {
            // InternalSimdaseParser.g:4320:28: ( ( ( (lv_ownedValue_0_0= rulePropertyExpression ) ) ( ruleInModesKeywords otherlv_2= LeftParenthesis ( (otherlv_3= RULE_ID ) ) (otherlv_4= Comma ( (otherlv_5= RULE_ID ) ) )* otherlv_6= RightParenthesis )? ) )
            // InternalSimdaseParser.g:4321:1: ( ( (lv_ownedValue_0_0= rulePropertyExpression ) ) ( ruleInModesKeywords otherlv_2= LeftParenthesis ( (otherlv_3= RULE_ID ) ) (otherlv_4= Comma ( (otherlv_5= RULE_ID ) ) )* otherlv_6= RightParenthesis )? )
            {
            // InternalSimdaseParser.g:4321:1: ( ( (lv_ownedValue_0_0= rulePropertyExpression ) ) ( ruleInModesKeywords otherlv_2= LeftParenthesis ( (otherlv_3= RULE_ID ) ) (otherlv_4= Comma ( (otherlv_5= RULE_ID ) ) )* otherlv_6= RightParenthesis )? )
            // InternalSimdaseParser.g:4321:2: ( (lv_ownedValue_0_0= rulePropertyExpression ) ) ( ruleInModesKeywords otherlv_2= LeftParenthesis ( (otherlv_3= RULE_ID ) ) (otherlv_4= Comma ( (otherlv_5= RULE_ID ) ) )* otherlv_6= RightParenthesis )?
            {
            // InternalSimdaseParser.g:4321:2: ( (lv_ownedValue_0_0= rulePropertyExpression ) )
            // InternalSimdaseParser.g:4322:1: (lv_ownedValue_0_0= rulePropertyExpression )
            {
            // InternalSimdaseParser.g:4322:1: (lv_ownedValue_0_0= rulePropertyExpression )
            // InternalSimdaseParser.g:4323:3: lv_ownedValue_0_0= rulePropertyExpression
            {
             
            	        newCompositeNode(grammarAccess.getOptionalModalPropertyValueAccess().getOwnedValuePropertyExpressionParserRuleCall_0_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_42);
            lv_ownedValue_0_0=rulePropertyExpression();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getOptionalModalPropertyValueRule());
            	        }
                   		set(
                   			current, 
                   			"ownedValue",
                    		lv_ownedValue_0_0, 
                    		"org.osate.xtext.aadl2.properties.Properties.PropertyExpression");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // InternalSimdaseParser.g:4339:2: ( ruleInModesKeywords otherlv_2= LeftParenthesis ( (otherlv_3= RULE_ID ) ) (otherlv_4= Comma ( (otherlv_5= RULE_ID ) ) )* otherlv_6= RightParenthesis )?
            int alt42=2;
            int LA42_0 = input.LA(1);

            if ( (LA42_0==In) ) {
                int LA42_1 = input.LA(2);

                if ( (LA42_1==Modes) ) {
                    alt42=1;
                }
            }
            switch (alt42) {
                case 1 :
                    // InternalSimdaseParser.g:4340:5: ruleInModesKeywords otherlv_2= LeftParenthesis ( (otherlv_3= RULE_ID ) ) (otherlv_4= Comma ( (otherlv_5= RULE_ID ) ) )* otherlv_6= RightParenthesis
                    {
                     
                            newCompositeNode(grammarAccess.getOptionalModalPropertyValueAccess().getInModesKeywordsParserRuleCall_1_0()); 
                        
                    pushFollow(FollowSets000.FOLLOW_41);
                    ruleInModesKeywords();

                    state._fsp--;


                            afterParserOrEnumRuleCall();
                        
                    otherlv_2=(Token)match(input,LeftParenthesis,FollowSets000.FOLLOW_39); 

                        	newLeafNode(otherlv_2, grammarAccess.getOptionalModalPropertyValueAccess().getLeftParenthesisKeyword_1_1());
                        
                    // InternalSimdaseParser.g:4352:1: ( (otherlv_3= RULE_ID ) )
                    // InternalSimdaseParser.g:4353:1: (otherlv_3= RULE_ID )
                    {
                    // InternalSimdaseParser.g:4353:1: (otherlv_3= RULE_ID )
                    // InternalSimdaseParser.g:4354:3: otherlv_3= RULE_ID
                    {

                    			if (current==null) {
                    	            current = createModelElement(grammarAccess.getOptionalModalPropertyValueRule());
                    	        }
                            
                    otherlv_3=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_43); 

                    		newLeafNode(otherlv_3, grammarAccess.getOptionalModalPropertyValueAccess().getInModeModeCrossReference_1_2_0()); 
                    	

                    }


                    }

                    // InternalSimdaseParser.g:4365:2: (otherlv_4= Comma ( (otherlv_5= RULE_ID ) ) )*
                    loop41:
                    do {
                        int alt41=2;
                        int LA41_0 = input.LA(1);

                        if ( (LA41_0==Comma) ) {
                            alt41=1;
                        }


                        switch (alt41) {
                    	case 1 :
                    	    // InternalSimdaseParser.g:4366:2: otherlv_4= Comma ( (otherlv_5= RULE_ID ) )
                    	    {
                    	    otherlv_4=(Token)match(input,Comma,FollowSets000.FOLLOW_39); 

                    	        	newLeafNode(otherlv_4, grammarAccess.getOptionalModalPropertyValueAccess().getCommaKeyword_1_3_0());
                    	        
                    	    // InternalSimdaseParser.g:4370:1: ( (otherlv_5= RULE_ID ) )
                    	    // InternalSimdaseParser.g:4371:1: (otherlv_5= RULE_ID )
                    	    {
                    	    // InternalSimdaseParser.g:4371:1: (otherlv_5= RULE_ID )
                    	    // InternalSimdaseParser.g:4372:3: otherlv_5= RULE_ID
                    	    {

                    	    			if (current==null) {
                    	    	            current = createModelElement(grammarAccess.getOptionalModalPropertyValueRule());
                    	    	        }
                    	            
                    	    otherlv_5=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_43); 

                    	    		newLeafNode(otherlv_5, grammarAccess.getOptionalModalPropertyValueAccess().getInModeModeCrossReference_1_3_1_0()); 
                    	    	

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop41;
                        }
                    } while (true);

                    otherlv_6=(Token)match(input,RightParenthesis,FollowSets000.FOLLOW_2); 

                        	newLeafNode(otherlv_6, grammarAccess.getOptionalModalPropertyValueAccess().getRightParenthesisKeyword_1_4());
                        

                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOptionalModalPropertyValue"


    // $ANTLR start "entryRulePropertyValue"
    // InternalSimdaseParser.g:4396:1: entryRulePropertyValue returns [EObject current=null] : iv_rulePropertyValue= rulePropertyValue EOF ;
    public final EObject entryRulePropertyValue() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePropertyValue = null;


        try {
            // InternalSimdaseParser.g:4397:2: (iv_rulePropertyValue= rulePropertyValue EOF )
            // InternalSimdaseParser.g:4398:2: iv_rulePropertyValue= rulePropertyValue EOF
            {
             newCompositeNode(grammarAccess.getPropertyValueRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_rulePropertyValue=rulePropertyValue();

            state._fsp--;

             current =iv_rulePropertyValue; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePropertyValue"


    // $ANTLR start "rulePropertyValue"
    // InternalSimdaseParser.g:4405:1: rulePropertyValue returns [EObject current=null] : ( (lv_ownedValue_0_0= rulePropertyExpression ) ) ;
    public final EObject rulePropertyValue() throws RecognitionException {
        EObject current = null;

        EObject lv_ownedValue_0_0 = null;


         enterRule(); 
            
        try {
            // InternalSimdaseParser.g:4408:28: ( ( (lv_ownedValue_0_0= rulePropertyExpression ) ) )
            // InternalSimdaseParser.g:4409:1: ( (lv_ownedValue_0_0= rulePropertyExpression ) )
            {
            // InternalSimdaseParser.g:4409:1: ( (lv_ownedValue_0_0= rulePropertyExpression ) )
            // InternalSimdaseParser.g:4410:1: (lv_ownedValue_0_0= rulePropertyExpression )
            {
            // InternalSimdaseParser.g:4410:1: (lv_ownedValue_0_0= rulePropertyExpression )
            // InternalSimdaseParser.g:4411:3: lv_ownedValue_0_0= rulePropertyExpression
            {
             
            	        newCompositeNode(grammarAccess.getPropertyValueAccess().getOwnedValuePropertyExpressionParserRuleCall_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_2);
            lv_ownedValue_0_0=rulePropertyExpression();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getPropertyValueRule());
            	        }
                   		set(
                   			current, 
                   			"ownedValue",
                    		lv_ownedValue_0_0, 
                    		"org.osate.xtext.aadl2.properties.Properties.PropertyExpression");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePropertyValue"


    // $ANTLR start "entryRulePropertyExpression"
    // InternalSimdaseParser.g:4435:1: entryRulePropertyExpression returns [EObject current=null] : iv_rulePropertyExpression= rulePropertyExpression EOF ;
    public final EObject entryRulePropertyExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePropertyExpression = null;


        try {
            // InternalSimdaseParser.g:4436:2: (iv_rulePropertyExpression= rulePropertyExpression EOF )
            // InternalSimdaseParser.g:4437:2: iv_rulePropertyExpression= rulePropertyExpression EOF
            {
             newCompositeNode(grammarAccess.getPropertyExpressionRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_rulePropertyExpression=rulePropertyExpression();

            state._fsp--;

             current =iv_rulePropertyExpression; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePropertyExpression"


    // $ANTLR start "rulePropertyExpression"
    // InternalSimdaseParser.g:4444:1: rulePropertyExpression returns [EObject current=null] : (this_RecordTerm_0= ruleRecordTerm | this_ReferenceTerm_1= ruleReferenceTerm | this_ComponentClassifierTerm_2= ruleComponentClassifierTerm | this_ComputedTerm_3= ruleComputedTerm | this_StringTerm_4= ruleStringTerm | this_NumericRangeTerm_5= ruleNumericRangeTerm | this_RealTerm_6= ruleRealTerm | this_IntegerTerm_7= ruleIntegerTerm | this_ListTerm_8= ruleListTerm | this_BooleanLiteral_9= ruleBooleanLiteral | this_LiteralorReferenceTerm_10= ruleLiteralorReferenceTerm ) ;
    public final EObject rulePropertyExpression() throws RecognitionException {
        EObject current = null;

        EObject this_RecordTerm_0 = null;

        EObject this_ReferenceTerm_1 = null;

        EObject this_ComponentClassifierTerm_2 = null;

        EObject this_ComputedTerm_3 = null;

        EObject this_StringTerm_4 = null;

        EObject this_NumericRangeTerm_5 = null;

        EObject this_RealTerm_6 = null;

        EObject this_IntegerTerm_7 = null;

        EObject this_ListTerm_8 = null;

        EObject this_BooleanLiteral_9 = null;

        EObject this_LiteralorReferenceTerm_10 = null;


         enterRule(); 
            
        try {
            // InternalSimdaseParser.g:4447:28: ( (this_RecordTerm_0= ruleRecordTerm | this_ReferenceTerm_1= ruleReferenceTerm | this_ComponentClassifierTerm_2= ruleComponentClassifierTerm | this_ComputedTerm_3= ruleComputedTerm | this_StringTerm_4= ruleStringTerm | this_NumericRangeTerm_5= ruleNumericRangeTerm | this_RealTerm_6= ruleRealTerm | this_IntegerTerm_7= ruleIntegerTerm | this_ListTerm_8= ruleListTerm | this_BooleanLiteral_9= ruleBooleanLiteral | this_LiteralorReferenceTerm_10= ruleLiteralorReferenceTerm ) )
            // InternalSimdaseParser.g:4448:1: (this_RecordTerm_0= ruleRecordTerm | this_ReferenceTerm_1= ruleReferenceTerm | this_ComponentClassifierTerm_2= ruleComponentClassifierTerm | this_ComputedTerm_3= ruleComputedTerm | this_StringTerm_4= ruleStringTerm | this_NumericRangeTerm_5= ruleNumericRangeTerm | this_RealTerm_6= ruleRealTerm | this_IntegerTerm_7= ruleIntegerTerm | this_ListTerm_8= ruleListTerm | this_BooleanLiteral_9= ruleBooleanLiteral | this_LiteralorReferenceTerm_10= ruleLiteralorReferenceTerm )
            {
            // InternalSimdaseParser.g:4448:1: (this_RecordTerm_0= ruleRecordTerm | this_ReferenceTerm_1= ruleReferenceTerm | this_ComponentClassifierTerm_2= ruleComponentClassifierTerm | this_ComputedTerm_3= ruleComputedTerm | this_StringTerm_4= ruleStringTerm | this_NumericRangeTerm_5= ruleNumericRangeTerm | this_RealTerm_6= ruleRealTerm | this_IntegerTerm_7= ruleIntegerTerm | this_ListTerm_8= ruleListTerm | this_BooleanLiteral_9= ruleBooleanLiteral | this_LiteralorReferenceTerm_10= ruleLiteralorReferenceTerm )
            int alt43=11;
            alt43 = dfa43.predict(input);
            switch (alt43) {
                case 1 :
                    // InternalSimdaseParser.g:4449:5: this_RecordTerm_0= ruleRecordTerm
                    {
                     
                            newCompositeNode(grammarAccess.getPropertyExpressionAccess().getRecordTermParserRuleCall_0()); 
                        
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_RecordTerm_0=ruleRecordTerm();

                    state._fsp--;


                            current = this_RecordTerm_0;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // InternalSimdaseParser.g:4459:5: this_ReferenceTerm_1= ruleReferenceTerm
                    {
                     
                            newCompositeNode(grammarAccess.getPropertyExpressionAccess().getReferenceTermParserRuleCall_1()); 
                        
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_ReferenceTerm_1=ruleReferenceTerm();

                    state._fsp--;


                            current = this_ReferenceTerm_1;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 3 :
                    // InternalSimdaseParser.g:4469:5: this_ComponentClassifierTerm_2= ruleComponentClassifierTerm
                    {
                     
                            newCompositeNode(grammarAccess.getPropertyExpressionAccess().getComponentClassifierTermParserRuleCall_2()); 
                        
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_ComponentClassifierTerm_2=ruleComponentClassifierTerm();

                    state._fsp--;


                            current = this_ComponentClassifierTerm_2;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 4 :
                    // InternalSimdaseParser.g:4479:5: this_ComputedTerm_3= ruleComputedTerm
                    {
                     
                            newCompositeNode(grammarAccess.getPropertyExpressionAccess().getComputedTermParserRuleCall_3()); 
                        
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_ComputedTerm_3=ruleComputedTerm();

                    state._fsp--;


                            current = this_ComputedTerm_3;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 5 :
                    // InternalSimdaseParser.g:4489:5: this_StringTerm_4= ruleStringTerm
                    {
                     
                            newCompositeNode(grammarAccess.getPropertyExpressionAccess().getStringTermParserRuleCall_4()); 
                        
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_StringTerm_4=ruleStringTerm();

                    state._fsp--;


                            current = this_StringTerm_4;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 6 :
                    // InternalSimdaseParser.g:4499:5: this_NumericRangeTerm_5= ruleNumericRangeTerm
                    {
                     
                            newCompositeNode(grammarAccess.getPropertyExpressionAccess().getNumericRangeTermParserRuleCall_5()); 
                        
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_NumericRangeTerm_5=ruleNumericRangeTerm();

                    state._fsp--;


                            current = this_NumericRangeTerm_5;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 7 :
                    // InternalSimdaseParser.g:4509:5: this_RealTerm_6= ruleRealTerm
                    {
                     
                            newCompositeNode(grammarAccess.getPropertyExpressionAccess().getRealTermParserRuleCall_6()); 
                        
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_RealTerm_6=ruleRealTerm();

                    state._fsp--;


                            current = this_RealTerm_6;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 8 :
                    // InternalSimdaseParser.g:4519:5: this_IntegerTerm_7= ruleIntegerTerm
                    {
                     
                            newCompositeNode(grammarAccess.getPropertyExpressionAccess().getIntegerTermParserRuleCall_7()); 
                        
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_IntegerTerm_7=ruleIntegerTerm();

                    state._fsp--;


                            current = this_IntegerTerm_7;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 9 :
                    // InternalSimdaseParser.g:4529:5: this_ListTerm_8= ruleListTerm
                    {
                     
                            newCompositeNode(grammarAccess.getPropertyExpressionAccess().getListTermParserRuleCall_8()); 
                        
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_ListTerm_8=ruleListTerm();

                    state._fsp--;


                            current = this_ListTerm_8;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 10 :
                    // InternalSimdaseParser.g:4539:5: this_BooleanLiteral_9= ruleBooleanLiteral
                    {
                     
                            newCompositeNode(grammarAccess.getPropertyExpressionAccess().getBooleanLiteralParserRuleCall_9()); 
                        
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_BooleanLiteral_9=ruleBooleanLiteral();

                    state._fsp--;


                            current = this_BooleanLiteral_9;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 11 :
                    // InternalSimdaseParser.g:4549:5: this_LiteralorReferenceTerm_10= ruleLiteralorReferenceTerm
                    {
                     
                            newCompositeNode(grammarAccess.getPropertyExpressionAccess().getLiteralorReferenceTermParserRuleCall_10()); 
                        
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_LiteralorReferenceTerm_10=ruleLiteralorReferenceTerm();

                    state._fsp--;


                            current = this_LiteralorReferenceTerm_10;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePropertyExpression"


    // $ANTLR start "entryRuleLiteralorReferenceTerm"
    // InternalSimdaseParser.g:4565:1: entryRuleLiteralorReferenceTerm returns [EObject current=null] : iv_ruleLiteralorReferenceTerm= ruleLiteralorReferenceTerm EOF ;
    public final EObject entryRuleLiteralorReferenceTerm() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLiteralorReferenceTerm = null;


        try {
            // InternalSimdaseParser.g:4566:2: (iv_ruleLiteralorReferenceTerm= ruleLiteralorReferenceTerm EOF )
            // InternalSimdaseParser.g:4567:2: iv_ruleLiteralorReferenceTerm= ruleLiteralorReferenceTerm EOF
            {
             newCompositeNode(grammarAccess.getLiteralorReferenceTermRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleLiteralorReferenceTerm=ruleLiteralorReferenceTerm();

            state._fsp--;

             current =iv_ruleLiteralorReferenceTerm; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLiteralorReferenceTerm"


    // $ANTLR start "ruleLiteralorReferenceTerm"
    // InternalSimdaseParser.g:4574:1: ruleLiteralorReferenceTerm returns [EObject current=null] : ( ( ruleQPREF ) ) ;
    public final EObject ruleLiteralorReferenceTerm() throws RecognitionException {
        EObject current = null;

         enterRule(); 
            
        try {
            // InternalSimdaseParser.g:4577:28: ( ( ( ruleQPREF ) ) )
            // InternalSimdaseParser.g:4578:1: ( ( ruleQPREF ) )
            {
            // InternalSimdaseParser.g:4578:1: ( ( ruleQPREF ) )
            // InternalSimdaseParser.g:4579:1: ( ruleQPREF )
            {
            // InternalSimdaseParser.g:4579:1: ( ruleQPREF )
            // InternalSimdaseParser.g:4580:3: ruleQPREF
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getLiteralorReferenceTermRule());
            	        }
                    
             
            	        newCompositeNode(grammarAccess.getLiteralorReferenceTermAccess().getNamedValueAbstractNamedValueCrossReference_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_2);
            ruleQPREF();

            state._fsp--;

             
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLiteralorReferenceTerm"


    // $ANTLR start "entryRuleBooleanLiteral"
    // InternalSimdaseParser.g:4602:1: entryRuleBooleanLiteral returns [EObject current=null] : iv_ruleBooleanLiteral= ruleBooleanLiteral EOF ;
    public final EObject entryRuleBooleanLiteral() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBooleanLiteral = null;


        try {
            // InternalSimdaseParser.g:4603:2: (iv_ruleBooleanLiteral= ruleBooleanLiteral EOF )
            // InternalSimdaseParser.g:4604:2: iv_ruleBooleanLiteral= ruleBooleanLiteral EOF
            {
             newCompositeNode(grammarAccess.getBooleanLiteralRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleBooleanLiteral=ruleBooleanLiteral();

            state._fsp--;

             current =iv_ruleBooleanLiteral; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBooleanLiteral"


    // $ANTLR start "ruleBooleanLiteral"
    // InternalSimdaseParser.g:4611:1: ruleBooleanLiteral returns [EObject current=null] : ( () ( ( (lv_value_1_0= True ) ) | otherlv_2= False ) ) ;
    public final EObject ruleBooleanLiteral() throws RecognitionException {
        EObject current = null;

        Token lv_value_1_0=null;
        Token otherlv_2=null;

         enterRule(); 
            
        try {
            // InternalSimdaseParser.g:4614:28: ( ( () ( ( (lv_value_1_0= True ) ) | otherlv_2= False ) ) )
            // InternalSimdaseParser.g:4615:1: ( () ( ( (lv_value_1_0= True ) ) | otherlv_2= False ) )
            {
            // InternalSimdaseParser.g:4615:1: ( () ( ( (lv_value_1_0= True ) ) | otherlv_2= False ) )
            // InternalSimdaseParser.g:4615:2: () ( ( (lv_value_1_0= True ) ) | otherlv_2= False )
            {
            // InternalSimdaseParser.g:4615:2: ()
            // InternalSimdaseParser.g:4616:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getBooleanLiteralAccess().getBooleanLiteralAction_0(),
                        current);
                

            }

            // InternalSimdaseParser.g:4621:2: ( ( (lv_value_1_0= True ) ) | otherlv_2= False )
            int alt44=2;
            int LA44_0 = input.LA(1);

            if ( (LA44_0==True) ) {
                alt44=1;
            }
            else if ( (LA44_0==False) ) {
                alt44=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 44, 0, input);

                throw nvae;
            }
            switch (alt44) {
                case 1 :
                    // InternalSimdaseParser.g:4621:3: ( (lv_value_1_0= True ) )
                    {
                    // InternalSimdaseParser.g:4621:3: ( (lv_value_1_0= True ) )
                    // InternalSimdaseParser.g:4622:1: (lv_value_1_0= True )
                    {
                    // InternalSimdaseParser.g:4622:1: (lv_value_1_0= True )
                    // InternalSimdaseParser.g:4623:3: lv_value_1_0= True
                    {
                    lv_value_1_0=(Token)match(input,True,FollowSets000.FOLLOW_2); 

                            newLeafNode(lv_value_1_0, grammarAccess.getBooleanLiteralAccess().getValueTrueKeyword_1_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getBooleanLiteralRule());
                    	        }
                           		setWithLastConsumed(current, "value", true, "true");
                    	    

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalSimdaseParser.g:4639:2: otherlv_2= False
                    {
                    otherlv_2=(Token)match(input,False,FollowSets000.FOLLOW_2); 

                        	newLeafNode(otherlv_2, grammarAccess.getBooleanLiteralAccess().getFalseKeyword_1_1());
                        

                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBooleanLiteral"


    // $ANTLR start "entryRuleConstantValue"
    // InternalSimdaseParser.g:4651:1: entryRuleConstantValue returns [EObject current=null] : iv_ruleConstantValue= ruleConstantValue EOF ;
    public final EObject entryRuleConstantValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConstantValue = null;


        try {
            // InternalSimdaseParser.g:4652:2: (iv_ruleConstantValue= ruleConstantValue EOF )
            // InternalSimdaseParser.g:4653:2: iv_ruleConstantValue= ruleConstantValue EOF
            {
             newCompositeNode(grammarAccess.getConstantValueRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleConstantValue=ruleConstantValue();

            state._fsp--;

             current =iv_ruleConstantValue; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConstantValue"


    // $ANTLR start "ruleConstantValue"
    // InternalSimdaseParser.g:4660:1: ruleConstantValue returns [EObject current=null] : ( ( ruleQPREF ) ) ;
    public final EObject ruleConstantValue() throws RecognitionException {
        EObject current = null;

         enterRule(); 
            
        try {
            // InternalSimdaseParser.g:4663:28: ( ( ( ruleQPREF ) ) )
            // InternalSimdaseParser.g:4664:1: ( ( ruleQPREF ) )
            {
            // InternalSimdaseParser.g:4664:1: ( ( ruleQPREF ) )
            // InternalSimdaseParser.g:4665:1: ( ruleQPREF )
            {
            // InternalSimdaseParser.g:4665:1: ( ruleQPREF )
            // InternalSimdaseParser.g:4666:3: ruleQPREF
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getConstantValueRule());
            	        }
                    
             
            	        newCompositeNode(grammarAccess.getConstantValueAccess().getNamedValuePropertyConstantCrossReference_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_2);
            ruleQPREF();

            state._fsp--;

             
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConstantValue"


    // $ANTLR start "entryRuleReferenceTerm"
    // InternalSimdaseParser.g:4688:1: entryRuleReferenceTerm returns [EObject current=null] : iv_ruleReferenceTerm= ruleReferenceTerm EOF ;
    public final EObject entryRuleReferenceTerm() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleReferenceTerm = null;


        try {
            // InternalSimdaseParser.g:4689:2: (iv_ruleReferenceTerm= ruleReferenceTerm EOF )
            // InternalSimdaseParser.g:4690:2: iv_ruleReferenceTerm= ruleReferenceTerm EOF
            {
             newCompositeNode(grammarAccess.getReferenceTermRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleReferenceTerm=ruleReferenceTerm();

            state._fsp--;

             current =iv_ruleReferenceTerm; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleReferenceTerm"


    // $ANTLR start "ruleReferenceTerm"
    // InternalSimdaseParser.g:4697:1: ruleReferenceTerm returns [EObject current=null] : (otherlv_0= Reference otherlv_1= LeftParenthesis ( (lv_path_2_0= ruleContainmentPathElement ) ) otherlv_3= RightParenthesis ) ;
    public final EObject ruleReferenceTerm() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_path_2_0 = null;


         enterRule(); 
            
        try {
            // InternalSimdaseParser.g:4700:28: ( (otherlv_0= Reference otherlv_1= LeftParenthesis ( (lv_path_2_0= ruleContainmentPathElement ) ) otherlv_3= RightParenthesis ) )
            // InternalSimdaseParser.g:4701:1: (otherlv_0= Reference otherlv_1= LeftParenthesis ( (lv_path_2_0= ruleContainmentPathElement ) ) otherlv_3= RightParenthesis )
            {
            // InternalSimdaseParser.g:4701:1: (otherlv_0= Reference otherlv_1= LeftParenthesis ( (lv_path_2_0= ruleContainmentPathElement ) ) otherlv_3= RightParenthesis )
            // InternalSimdaseParser.g:4702:2: otherlv_0= Reference otherlv_1= LeftParenthesis ( (lv_path_2_0= ruleContainmentPathElement ) ) otherlv_3= RightParenthesis
            {
            otherlv_0=(Token)match(input,Reference,FollowSets000.FOLLOW_41); 

                	newLeafNode(otherlv_0, grammarAccess.getReferenceTermAccess().getReferenceKeyword_0());
                
            otherlv_1=(Token)match(input,LeftParenthesis,FollowSets000.FOLLOW_39); 

                	newLeafNode(otherlv_1, grammarAccess.getReferenceTermAccess().getLeftParenthesisKeyword_1());
                
            // InternalSimdaseParser.g:4711:1: ( (lv_path_2_0= ruleContainmentPathElement ) )
            // InternalSimdaseParser.g:4712:1: (lv_path_2_0= ruleContainmentPathElement )
            {
            // InternalSimdaseParser.g:4712:1: (lv_path_2_0= ruleContainmentPathElement )
            // InternalSimdaseParser.g:4713:3: lv_path_2_0= ruleContainmentPathElement
            {
             
            	        newCompositeNode(grammarAccess.getReferenceTermAccess().getPathContainmentPathElementParserRuleCall_2_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_30);
            lv_path_2_0=ruleContainmentPathElement();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getReferenceTermRule());
            	        }
                   		set(
                   			current, 
                   			"path",
                    		lv_path_2_0, 
                    		"org.osate.xtext.aadl2.properties.Properties.ContainmentPathElement");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_3=(Token)match(input,RightParenthesis,FollowSets000.FOLLOW_2); 

                	newLeafNode(otherlv_3, grammarAccess.getReferenceTermAccess().getRightParenthesisKeyword_3());
                

            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleReferenceTerm"


    // $ANTLR start "entryRuleRecordTerm"
    // InternalSimdaseParser.g:4742:1: entryRuleRecordTerm returns [EObject current=null] : iv_ruleRecordTerm= ruleRecordTerm EOF ;
    public final EObject entryRuleRecordTerm() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRecordTerm = null;


        try {
            // InternalSimdaseParser.g:4743:2: (iv_ruleRecordTerm= ruleRecordTerm EOF )
            // InternalSimdaseParser.g:4744:2: iv_ruleRecordTerm= ruleRecordTerm EOF
            {
             newCompositeNode(grammarAccess.getRecordTermRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleRecordTerm=ruleRecordTerm();

            state._fsp--;

             current =iv_ruleRecordTerm; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRecordTerm"


    // $ANTLR start "ruleRecordTerm"
    // InternalSimdaseParser.g:4751:1: ruleRecordTerm returns [EObject current=null] : (otherlv_0= LeftSquareBracket ( (lv_ownedFieldValue_1_0= ruleFieldPropertyAssociation ) )+ otherlv_2= RightSquareBracket ) ;
    public final EObject ruleRecordTerm() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        EObject lv_ownedFieldValue_1_0 = null;


         enterRule(); 
            
        try {
            // InternalSimdaseParser.g:4754:28: ( (otherlv_0= LeftSquareBracket ( (lv_ownedFieldValue_1_0= ruleFieldPropertyAssociation ) )+ otherlv_2= RightSquareBracket ) )
            // InternalSimdaseParser.g:4755:1: (otherlv_0= LeftSquareBracket ( (lv_ownedFieldValue_1_0= ruleFieldPropertyAssociation ) )+ otherlv_2= RightSquareBracket )
            {
            // InternalSimdaseParser.g:4755:1: (otherlv_0= LeftSquareBracket ( (lv_ownedFieldValue_1_0= ruleFieldPropertyAssociation ) )+ otherlv_2= RightSquareBracket )
            // InternalSimdaseParser.g:4756:2: otherlv_0= LeftSquareBracket ( (lv_ownedFieldValue_1_0= ruleFieldPropertyAssociation ) )+ otherlv_2= RightSquareBracket
            {
            otherlv_0=(Token)match(input,LeftSquareBracket,FollowSets000.FOLLOW_39); 

                	newLeafNode(otherlv_0, grammarAccess.getRecordTermAccess().getLeftSquareBracketKeyword_0());
                
            // InternalSimdaseParser.g:4760:1: ( (lv_ownedFieldValue_1_0= ruleFieldPropertyAssociation ) )+
            int cnt45=0;
            loop45:
            do {
                int alt45=2;
                int LA45_0 = input.LA(1);

                if ( (LA45_0==RULE_ID) ) {
                    alt45=1;
                }


                switch (alt45) {
            	case 1 :
            	    // InternalSimdaseParser.g:4761:1: (lv_ownedFieldValue_1_0= ruleFieldPropertyAssociation )
            	    {
            	    // InternalSimdaseParser.g:4761:1: (lv_ownedFieldValue_1_0= ruleFieldPropertyAssociation )
            	    // InternalSimdaseParser.g:4762:3: lv_ownedFieldValue_1_0= ruleFieldPropertyAssociation
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getRecordTermAccess().getOwnedFieldValueFieldPropertyAssociationParserRuleCall_1_0()); 
            	    	    
            	    pushFollow(FollowSets000.FOLLOW_44);
            	    lv_ownedFieldValue_1_0=ruleFieldPropertyAssociation();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getRecordTermRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"ownedFieldValue",
            	            		lv_ownedFieldValue_1_0, 
            	            		"org.osate.xtext.aadl2.properties.Properties.FieldPropertyAssociation");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt45 >= 1 ) break loop45;
                        EarlyExitException eee =
                            new EarlyExitException(45, input);
                        throw eee;
                }
                cnt45++;
            } while (true);

            otherlv_2=(Token)match(input,RightSquareBracket,FollowSets000.FOLLOW_2); 

                	newLeafNode(otherlv_2, grammarAccess.getRecordTermAccess().getRightSquareBracketKeyword_2());
                

            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRecordTerm"


    // $ANTLR start "entryRuleComputedTerm"
    // InternalSimdaseParser.g:4793:1: entryRuleComputedTerm returns [EObject current=null] : iv_ruleComputedTerm= ruleComputedTerm EOF ;
    public final EObject entryRuleComputedTerm() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleComputedTerm = null;


        try {
            // InternalSimdaseParser.g:4794:2: (iv_ruleComputedTerm= ruleComputedTerm EOF )
            // InternalSimdaseParser.g:4795:2: iv_ruleComputedTerm= ruleComputedTerm EOF
            {
             newCompositeNode(grammarAccess.getComputedTermRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleComputedTerm=ruleComputedTerm();

            state._fsp--;

             current =iv_ruleComputedTerm; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleComputedTerm"


    // $ANTLR start "ruleComputedTerm"
    // InternalSimdaseParser.g:4802:1: ruleComputedTerm returns [EObject current=null] : (otherlv_0= Compute otherlv_1= LeftParenthesis ( (lv_function_2_0= RULE_ID ) ) otherlv_3= RightParenthesis ) ;
    public final EObject ruleComputedTerm() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token lv_function_2_0=null;
        Token otherlv_3=null;

         enterRule(); 
            
        try {
            // InternalSimdaseParser.g:4805:28: ( (otherlv_0= Compute otherlv_1= LeftParenthesis ( (lv_function_2_0= RULE_ID ) ) otherlv_3= RightParenthesis ) )
            // InternalSimdaseParser.g:4806:1: (otherlv_0= Compute otherlv_1= LeftParenthesis ( (lv_function_2_0= RULE_ID ) ) otherlv_3= RightParenthesis )
            {
            // InternalSimdaseParser.g:4806:1: (otherlv_0= Compute otherlv_1= LeftParenthesis ( (lv_function_2_0= RULE_ID ) ) otherlv_3= RightParenthesis )
            // InternalSimdaseParser.g:4807:2: otherlv_0= Compute otherlv_1= LeftParenthesis ( (lv_function_2_0= RULE_ID ) ) otherlv_3= RightParenthesis
            {
            otherlv_0=(Token)match(input,Compute,FollowSets000.FOLLOW_41); 

                	newLeafNode(otherlv_0, grammarAccess.getComputedTermAccess().getComputeKeyword_0());
                
            otherlv_1=(Token)match(input,LeftParenthesis,FollowSets000.FOLLOW_39); 

                	newLeafNode(otherlv_1, grammarAccess.getComputedTermAccess().getLeftParenthesisKeyword_1());
                
            // InternalSimdaseParser.g:4816:1: ( (lv_function_2_0= RULE_ID ) )
            // InternalSimdaseParser.g:4817:1: (lv_function_2_0= RULE_ID )
            {
            // InternalSimdaseParser.g:4817:1: (lv_function_2_0= RULE_ID )
            // InternalSimdaseParser.g:4818:3: lv_function_2_0= RULE_ID
            {
            lv_function_2_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_30); 

            			newLeafNode(lv_function_2_0, grammarAccess.getComputedTermAccess().getFunctionIDTerminalRuleCall_2_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getComputedTermRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"function",
                    		lv_function_2_0, 
                    		"org.osate.xtext.aadl2.properties.Properties.ID");
            	    

            }


            }

            otherlv_3=(Token)match(input,RightParenthesis,FollowSets000.FOLLOW_2); 

                	newLeafNode(otherlv_3, grammarAccess.getComputedTermAccess().getRightParenthesisKeyword_3());
                

            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleComputedTerm"


    // $ANTLR start "entryRuleComponentClassifierTerm"
    // InternalSimdaseParser.g:4847:1: entryRuleComponentClassifierTerm returns [EObject current=null] : iv_ruleComponentClassifierTerm= ruleComponentClassifierTerm EOF ;
    public final EObject entryRuleComponentClassifierTerm() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleComponentClassifierTerm = null;


        try {
            // InternalSimdaseParser.g:4848:2: (iv_ruleComponentClassifierTerm= ruleComponentClassifierTerm EOF )
            // InternalSimdaseParser.g:4849:2: iv_ruleComponentClassifierTerm= ruleComponentClassifierTerm EOF
            {
             newCompositeNode(grammarAccess.getComponentClassifierTermRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleComponentClassifierTerm=ruleComponentClassifierTerm();

            state._fsp--;

             current =iv_ruleComponentClassifierTerm; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleComponentClassifierTerm"


    // $ANTLR start "ruleComponentClassifierTerm"
    // InternalSimdaseParser.g:4856:1: ruleComponentClassifierTerm returns [EObject current=null] : (otherlv_0= Classifier otherlv_1= LeftParenthesis ( ( ruleQCREF ) ) otherlv_3= RightParenthesis ) ;
    public final EObject ruleComponentClassifierTerm() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;

         enterRule(); 
            
        try {
            // InternalSimdaseParser.g:4859:28: ( (otherlv_0= Classifier otherlv_1= LeftParenthesis ( ( ruleQCREF ) ) otherlv_3= RightParenthesis ) )
            // InternalSimdaseParser.g:4860:1: (otherlv_0= Classifier otherlv_1= LeftParenthesis ( ( ruleQCREF ) ) otherlv_3= RightParenthesis )
            {
            // InternalSimdaseParser.g:4860:1: (otherlv_0= Classifier otherlv_1= LeftParenthesis ( ( ruleQCREF ) ) otherlv_3= RightParenthesis )
            // InternalSimdaseParser.g:4861:2: otherlv_0= Classifier otherlv_1= LeftParenthesis ( ( ruleQCREF ) ) otherlv_3= RightParenthesis
            {
            otherlv_0=(Token)match(input,Classifier,FollowSets000.FOLLOW_41); 

                	newLeafNode(otherlv_0, grammarAccess.getComponentClassifierTermAccess().getClassifierKeyword_0());
                
            otherlv_1=(Token)match(input,LeftParenthesis,FollowSets000.FOLLOW_39); 

                	newLeafNode(otherlv_1, grammarAccess.getComponentClassifierTermAccess().getLeftParenthesisKeyword_1());
                
            // InternalSimdaseParser.g:4870:1: ( ( ruleQCREF ) )
            // InternalSimdaseParser.g:4871:1: ( ruleQCREF )
            {
            // InternalSimdaseParser.g:4871:1: ( ruleQCREF )
            // InternalSimdaseParser.g:4872:3: ruleQCREF
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getComponentClassifierTermRule());
            	        }
                    
             
            	        newCompositeNode(grammarAccess.getComponentClassifierTermAccess().getClassifierComponentClassifierCrossReference_2_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_30);
            ruleQCREF();

            state._fsp--;

             
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_3=(Token)match(input,RightParenthesis,FollowSets000.FOLLOW_2); 

                	newLeafNode(otherlv_3, grammarAccess.getComponentClassifierTermAccess().getRightParenthesisKeyword_3());
                

            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleComponentClassifierTerm"


    // $ANTLR start "entryRuleListTerm"
    // InternalSimdaseParser.g:4899:1: entryRuleListTerm returns [EObject current=null] : iv_ruleListTerm= ruleListTerm EOF ;
    public final EObject entryRuleListTerm() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleListTerm = null;


        try {
            // InternalSimdaseParser.g:4900:2: (iv_ruleListTerm= ruleListTerm EOF )
            // InternalSimdaseParser.g:4901:2: iv_ruleListTerm= ruleListTerm EOF
            {
             newCompositeNode(grammarAccess.getListTermRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleListTerm=ruleListTerm();

            state._fsp--;

             current =iv_ruleListTerm; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleListTerm"


    // $ANTLR start "ruleListTerm"
    // InternalSimdaseParser.g:4908:1: ruleListTerm returns [EObject current=null] : ( () otherlv_1= LeftParenthesis ( ( (lv_ownedListElement_2_0= rulePropertyExpression ) ) (otherlv_3= Comma ( (lv_ownedListElement_4_0= rulePropertyExpression ) ) )* )? otherlv_5= RightParenthesis ) ;
    public final EObject ruleListTerm() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_ownedListElement_2_0 = null;

        EObject lv_ownedListElement_4_0 = null;


         enterRule(); 
            
        try {
            // InternalSimdaseParser.g:4911:28: ( ( () otherlv_1= LeftParenthesis ( ( (lv_ownedListElement_2_0= rulePropertyExpression ) ) (otherlv_3= Comma ( (lv_ownedListElement_4_0= rulePropertyExpression ) ) )* )? otherlv_5= RightParenthesis ) )
            // InternalSimdaseParser.g:4912:1: ( () otherlv_1= LeftParenthesis ( ( (lv_ownedListElement_2_0= rulePropertyExpression ) ) (otherlv_3= Comma ( (lv_ownedListElement_4_0= rulePropertyExpression ) ) )* )? otherlv_5= RightParenthesis )
            {
            // InternalSimdaseParser.g:4912:1: ( () otherlv_1= LeftParenthesis ( ( (lv_ownedListElement_2_0= rulePropertyExpression ) ) (otherlv_3= Comma ( (lv_ownedListElement_4_0= rulePropertyExpression ) ) )* )? otherlv_5= RightParenthesis )
            // InternalSimdaseParser.g:4912:2: () otherlv_1= LeftParenthesis ( ( (lv_ownedListElement_2_0= rulePropertyExpression ) ) (otherlv_3= Comma ( (lv_ownedListElement_4_0= rulePropertyExpression ) ) )* )? otherlv_5= RightParenthesis
            {
            // InternalSimdaseParser.g:4912:2: ()
            // InternalSimdaseParser.g:4913:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getListTermAccess().getListValueAction_0(),
                        current);
                

            }

            otherlv_1=(Token)match(input,LeftParenthesis,FollowSets000.FOLLOW_45); 

                	newLeafNode(otherlv_1, grammarAccess.getListTermAccess().getLeftParenthesisKeyword_1());
                
            // InternalSimdaseParser.g:4923:1: ( ( (lv_ownedListElement_2_0= rulePropertyExpression ) ) (otherlv_3= Comma ( (lv_ownedListElement_4_0= rulePropertyExpression ) ) )* )?
            int alt47=2;
            int LA47_0 = input.LA(1);

            if ( (LA47_0==Classifier||LA47_0==Reference||LA47_0==Compute||LA47_0==False||LA47_0==True||LA47_0==LeftParenthesis||LA47_0==PlusSign||LA47_0==HyphenMinus||LA47_0==LeftSquareBracket||LA47_0==RULE_INTEGER_LIT||LA47_0==RULE_REAL_LIT||(LA47_0>=RULE_STRING && LA47_0<=RULE_ID)) ) {
                alt47=1;
            }
            switch (alt47) {
                case 1 :
                    // InternalSimdaseParser.g:4923:2: ( (lv_ownedListElement_2_0= rulePropertyExpression ) ) (otherlv_3= Comma ( (lv_ownedListElement_4_0= rulePropertyExpression ) ) )*
                    {
                    // InternalSimdaseParser.g:4923:2: ( (lv_ownedListElement_2_0= rulePropertyExpression ) )
                    // InternalSimdaseParser.g:4924:1: (lv_ownedListElement_2_0= rulePropertyExpression )
                    {
                    // InternalSimdaseParser.g:4924:1: (lv_ownedListElement_2_0= rulePropertyExpression )
                    // InternalSimdaseParser.g:4925:3: lv_ownedListElement_2_0= rulePropertyExpression
                    {
                     
                    	        newCompositeNode(grammarAccess.getListTermAccess().getOwnedListElementPropertyExpressionParserRuleCall_2_0_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_43);
                    lv_ownedListElement_2_0=rulePropertyExpression();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getListTermRule());
                    	        }
                           		add(
                           			current, 
                           			"ownedListElement",
                            		lv_ownedListElement_2_0, 
                            		"org.osate.xtext.aadl2.properties.Properties.PropertyExpression");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // InternalSimdaseParser.g:4941:2: (otherlv_3= Comma ( (lv_ownedListElement_4_0= rulePropertyExpression ) ) )*
                    loop46:
                    do {
                        int alt46=2;
                        int LA46_0 = input.LA(1);

                        if ( (LA46_0==Comma) ) {
                            alt46=1;
                        }


                        switch (alt46) {
                    	case 1 :
                    	    // InternalSimdaseParser.g:4942:2: otherlv_3= Comma ( (lv_ownedListElement_4_0= rulePropertyExpression ) )
                    	    {
                    	    otherlv_3=(Token)match(input,Comma,FollowSets000.FOLLOW_37); 

                    	        	newLeafNode(otherlv_3, grammarAccess.getListTermAccess().getCommaKeyword_2_1_0());
                    	        
                    	    // InternalSimdaseParser.g:4946:1: ( (lv_ownedListElement_4_0= rulePropertyExpression ) )
                    	    // InternalSimdaseParser.g:4947:1: (lv_ownedListElement_4_0= rulePropertyExpression )
                    	    {
                    	    // InternalSimdaseParser.g:4947:1: (lv_ownedListElement_4_0= rulePropertyExpression )
                    	    // InternalSimdaseParser.g:4948:3: lv_ownedListElement_4_0= rulePropertyExpression
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getListTermAccess().getOwnedListElementPropertyExpressionParserRuleCall_2_1_1_0()); 
                    	    	    
                    	    pushFollow(FollowSets000.FOLLOW_43);
                    	    lv_ownedListElement_4_0=rulePropertyExpression();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getListTermRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"ownedListElement",
                    	            		lv_ownedListElement_4_0, 
                    	            		"org.osate.xtext.aadl2.properties.Properties.PropertyExpression");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop46;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_5=(Token)match(input,RightParenthesis,FollowSets000.FOLLOW_2); 

                	newLeafNode(otherlv_5, grammarAccess.getListTermAccess().getRightParenthesisKeyword_3());
                

            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleListTerm"


    // $ANTLR start "entryRuleFieldPropertyAssociation"
    // InternalSimdaseParser.g:4977:1: entryRuleFieldPropertyAssociation returns [EObject current=null] : iv_ruleFieldPropertyAssociation= ruleFieldPropertyAssociation EOF ;
    public final EObject entryRuleFieldPropertyAssociation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFieldPropertyAssociation = null;


        try {
            // InternalSimdaseParser.g:4978:2: (iv_ruleFieldPropertyAssociation= ruleFieldPropertyAssociation EOF )
            // InternalSimdaseParser.g:4979:2: iv_ruleFieldPropertyAssociation= ruleFieldPropertyAssociation EOF
            {
             newCompositeNode(grammarAccess.getFieldPropertyAssociationRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleFieldPropertyAssociation=ruleFieldPropertyAssociation();

            state._fsp--;

             current =iv_ruleFieldPropertyAssociation; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFieldPropertyAssociation"


    // $ANTLR start "ruleFieldPropertyAssociation"
    // InternalSimdaseParser.g:4986:1: ruleFieldPropertyAssociation returns [EObject current=null] : ( ( (otherlv_0= RULE_ID ) ) otherlv_1= EqualsSignGreaterThanSign ( (lv_ownedValue_2_0= rulePropertyExpression ) ) otherlv_3= Semicolon ) ;
    public final EObject ruleFieldPropertyAssociation() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_ownedValue_2_0 = null;


         enterRule(); 
            
        try {
            // InternalSimdaseParser.g:4989:28: ( ( ( (otherlv_0= RULE_ID ) ) otherlv_1= EqualsSignGreaterThanSign ( (lv_ownedValue_2_0= rulePropertyExpression ) ) otherlv_3= Semicolon ) )
            // InternalSimdaseParser.g:4990:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= EqualsSignGreaterThanSign ( (lv_ownedValue_2_0= rulePropertyExpression ) ) otherlv_3= Semicolon )
            {
            // InternalSimdaseParser.g:4990:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= EqualsSignGreaterThanSign ( (lv_ownedValue_2_0= rulePropertyExpression ) ) otherlv_3= Semicolon )
            // InternalSimdaseParser.g:4990:2: ( (otherlv_0= RULE_ID ) ) otherlv_1= EqualsSignGreaterThanSign ( (lv_ownedValue_2_0= rulePropertyExpression ) ) otherlv_3= Semicolon
            {
            // InternalSimdaseParser.g:4990:2: ( (otherlv_0= RULE_ID ) )
            // InternalSimdaseParser.g:4991:1: (otherlv_0= RULE_ID )
            {
            // InternalSimdaseParser.g:4991:1: (otherlv_0= RULE_ID )
            // InternalSimdaseParser.g:4992:3: otherlv_0= RULE_ID
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getFieldPropertyAssociationRule());
            	        }
                    
            otherlv_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_4); 

            		newLeafNode(otherlv_0, grammarAccess.getFieldPropertyAssociationAccess().getPropertyBasicPropertyCrossReference_0_0()); 
            	

            }


            }

            otherlv_1=(Token)match(input,EqualsSignGreaterThanSign,FollowSets000.FOLLOW_37); 

                	newLeafNode(otherlv_1, grammarAccess.getFieldPropertyAssociationAccess().getEqualsSignGreaterThanSignKeyword_1());
                
            // InternalSimdaseParser.g:5008:1: ( (lv_ownedValue_2_0= rulePropertyExpression ) )
            // InternalSimdaseParser.g:5009:1: (lv_ownedValue_2_0= rulePropertyExpression )
            {
            // InternalSimdaseParser.g:5009:1: (lv_ownedValue_2_0= rulePropertyExpression )
            // InternalSimdaseParser.g:5010:3: lv_ownedValue_2_0= rulePropertyExpression
            {
             
            	        newCompositeNode(grammarAccess.getFieldPropertyAssociationAccess().getOwnedValuePropertyExpressionParserRuleCall_2_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_6);
            lv_ownedValue_2_0=rulePropertyExpression();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getFieldPropertyAssociationRule());
            	        }
                   		set(
                   			current, 
                   			"ownedValue",
                    		lv_ownedValue_2_0, 
                    		"org.osate.xtext.aadl2.properties.Properties.PropertyExpression");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_3=(Token)match(input,Semicolon,FollowSets000.FOLLOW_2); 

                	newLeafNode(otherlv_3, grammarAccess.getFieldPropertyAssociationAccess().getSemicolonKeyword_3());
                

            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFieldPropertyAssociation"


    // $ANTLR start "entryRuleContainmentPathElement"
    // InternalSimdaseParser.g:5039:1: entryRuleContainmentPathElement returns [EObject current=null] : iv_ruleContainmentPathElement= ruleContainmentPathElement EOF ;
    public final EObject entryRuleContainmentPathElement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleContainmentPathElement = null;


        try {
            // InternalSimdaseParser.g:5040:2: (iv_ruleContainmentPathElement= ruleContainmentPathElement EOF )
            // InternalSimdaseParser.g:5041:2: iv_ruleContainmentPathElement= ruleContainmentPathElement EOF
            {
             newCompositeNode(grammarAccess.getContainmentPathElementRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleContainmentPathElement=ruleContainmentPathElement();

            state._fsp--;

             current =iv_ruleContainmentPathElement; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleContainmentPathElement"


    // $ANTLR start "ruleContainmentPathElement"
    // InternalSimdaseParser.g:5048:1: ruleContainmentPathElement returns [EObject current=null] : ( ( ( (otherlv_0= RULE_ID ) ) ( (lv_arrayRange_1_0= ruleArrayRange ) )* ) (otherlv_2= FullStop ( (lv_path_3_0= ruleContainmentPathElement ) ) )? ) ;
    public final EObject ruleContainmentPathElement() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        EObject lv_arrayRange_1_0 = null;

        EObject lv_path_3_0 = null;


         enterRule(); 
            
        try {
            // InternalSimdaseParser.g:5051:28: ( ( ( ( (otherlv_0= RULE_ID ) ) ( (lv_arrayRange_1_0= ruleArrayRange ) )* ) (otherlv_2= FullStop ( (lv_path_3_0= ruleContainmentPathElement ) ) )? ) )
            // InternalSimdaseParser.g:5052:1: ( ( ( (otherlv_0= RULE_ID ) ) ( (lv_arrayRange_1_0= ruleArrayRange ) )* ) (otherlv_2= FullStop ( (lv_path_3_0= ruleContainmentPathElement ) ) )? )
            {
            // InternalSimdaseParser.g:5052:1: ( ( ( (otherlv_0= RULE_ID ) ) ( (lv_arrayRange_1_0= ruleArrayRange ) )* ) (otherlv_2= FullStop ( (lv_path_3_0= ruleContainmentPathElement ) ) )? )
            // InternalSimdaseParser.g:5052:2: ( ( (otherlv_0= RULE_ID ) ) ( (lv_arrayRange_1_0= ruleArrayRange ) )* ) (otherlv_2= FullStop ( (lv_path_3_0= ruleContainmentPathElement ) ) )?
            {
            // InternalSimdaseParser.g:5052:2: ( ( (otherlv_0= RULE_ID ) ) ( (lv_arrayRange_1_0= ruleArrayRange ) )* )
            // InternalSimdaseParser.g:5052:3: ( (otherlv_0= RULE_ID ) ) ( (lv_arrayRange_1_0= ruleArrayRange ) )*
            {
            // InternalSimdaseParser.g:5052:3: ( (otherlv_0= RULE_ID ) )
            // InternalSimdaseParser.g:5053:1: (otherlv_0= RULE_ID )
            {
            // InternalSimdaseParser.g:5053:1: (otherlv_0= RULE_ID )
            // InternalSimdaseParser.g:5054:3: otherlv_0= RULE_ID
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getContainmentPathElementRule());
            	        }
                    
            otherlv_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_46); 

            		newLeafNode(otherlv_0, grammarAccess.getContainmentPathElementAccess().getNamedElementNamedElementCrossReference_0_0_0()); 
            	

            }


            }

            // InternalSimdaseParser.g:5065:2: ( (lv_arrayRange_1_0= ruleArrayRange ) )*
            loop48:
            do {
                int alt48=2;
                int LA48_0 = input.LA(1);

                if ( (LA48_0==LeftSquareBracket) ) {
                    alt48=1;
                }


                switch (alt48) {
            	case 1 :
            	    // InternalSimdaseParser.g:5066:1: (lv_arrayRange_1_0= ruleArrayRange )
            	    {
            	    // InternalSimdaseParser.g:5066:1: (lv_arrayRange_1_0= ruleArrayRange )
            	    // InternalSimdaseParser.g:5067:3: lv_arrayRange_1_0= ruleArrayRange
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getContainmentPathElementAccess().getArrayRangeArrayRangeParserRuleCall_0_1_0()); 
            	    	    
            	    pushFollow(FollowSets000.FOLLOW_46);
            	    lv_arrayRange_1_0=ruleArrayRange();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getContainmentPathElementRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"arrayRange",
            	            		lv_arrayRange_1_0, 
            	            		"org.osate.xtext.aadl2.properties.Properties.ArrayRange");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop48;
                }
            } while (true);


            }

            // InternalSimdaseParser.g:5083:4: (otherlv_2= FullStop ( (lv_path_3_0= ruleContainmentPathElement ) ) )?
            int alt49=2;
            int LA49_0 = input.LA(1);

            if ( (LA49_0==FullStop) ) {
                alt49=1;
            }
            switch (alt49) {
                case 1 :
                    // InternalSimdaseParser.g:5084:2: otherlv_2= FullStop ( (lv_path_3_0= ruleContainmentPathElement ) )
                    {
                    otherlv_2=(Token)match(input,FullStop,FollowSets000.FOLLOW_39); 

                        	newLeafNode(otherlv_2, grammarAccess.getContainmentPathElementAccess().getFullStopKeyword_1_0());
                        
                    // InternalSimdaseParser.g:5088:1: ( (lv_path_3_0= ruleContainmentPathElement ) )
                    // InternalSimdaseParser.g:5089:1: (lv_path_3_0= ruleContainmentPathElement )
                    {
                    // InternalSimdaseParser.g:5089:1: (lv_path_3_0= ruleContainmentPathElement )
                    // InternalSimdaseParser.g:5090:3: lv_path_3_0= ruleContainmentPathElement
                    {
                     
                    	        newCompositeNode(grammarAccess.getContainmentPathElementAccess().getPathContainmentPathElementParserRuleCall_1_1_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_path_3_0=ruleContainmentPathElement();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getContainmentPathElementRule());
                    	        }
                           		set(
                           			current, 
                           			"path",
                            		lv_path_3_0, 
                            		"org.osate.xtext.aadl2.properties.Properties.ContainmentPathElement");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleContainmentPathElement"


    // $ANTLR start "entryRulePlusMinus"
    // InternalSimdaseParser.g:5116:1: entryRulePlusMinus returns [String current=null] : iv_rulePlusMinus= rulePlusMinus EOF ;
    public final String entryRulePlusMinus() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_rulePlusMinus = null;


        try {
            // InternalSimdaseParser.g:5117:1: (iv_rulePlusMinus= rulePlusMinus EOF )
            // InternalSimdaseParser.g:5118:2: iv_rulePlusMinus= rulePlusMinus EOF
            {
             newCompositeNode(grammarAccess.getPlusMinusRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_rulePlusMinus=rulePlusMinus();

            state._fsp--;

             current =iv_rulePlusMinus.getText(); 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePlusMinus"


    // $ANTLR start "rulePlusMinus"
    // InternalSimdaseParser.g:5125:1: rulePlusMinus returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= PlusSign | kw= HyphenMinus ) ;
    public final AntlrDatatypeRuleToken rulePlusMinus() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;

         enterRule(); 
            
        try {
            // InternalSimdaseParser.g:5129:6: ( (kw= PlusSign | kw= HyphenMinus ) )
            // InternalSimdaseParser.g:5130:1: (kw= PlusSign | kw= HyphenMinus )
            {
            // InternalSimdaseParser.g:5130:1: (kw= PlusSign | kw= HyphenMinus )
            int alt50=2;
            int LA50_0 = input.LA(1);

            if ( (LA50_0==PlusSign) ) {
                alt50=1;
            }
            else if ( (LA50_0==HyphenMinus) ) {
                alt50=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 50, 0, input);

                throw nvae;
            }
            switch (alt50) {
                case 1 :
                    // InternalSimdaseParser.g:5131:2: kw= PlusSign
                    {
                    kw=(Token)match(input,PlusSign,FollowSets000.FOLLOW_2); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getPlusMinusAccess().getPlusSignKeyword_0()); 
                        

                    }
                    break;
                case 2 :
                    // InternalSimdaseParser.g:5138:2: kw= HyphenMinus
                    {
                    kw=(Token)match(input,HyphenMinus,FollowSets000.FOLLOW_2); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getPlusMinusAccess().getHyphenMinusKeyword_1()); 
                        

                    }
                    break;

            }


            }

             leaveRule();
                
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePlusMinus"


    // $ANTLR start "entryRuleStringTerm"
    // InternalSimdaseParser.g:5151:1: entryRuleStringTerm returns [EObject current=null] : iv_ruleStringTerm= ruleStringTerm EOF ;
    public final EObject entryRuleStringTerm() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStringTerm = null;


        try {
            // InternalSimdaseParser.g:5152:2: (iv_ruleStringTerm= ruleStringTerm EOF )
            // InternalSimdaseParser.g:5153:2: iv_ruleStringTerm= ruleStringTerm EOF
            {
             newCompositeNode(grammarAccess.getStringTermRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleStringTerm=ruleStringTerm();

            state._fsp--;

             current =iv_ruleStringTerm; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStringTerm"


    // $ANTLR start "ruleStringTerm"
    // InternalSimdaseParser.g:5160:1: ruleStringTerm returns [EObject current=null] : ( (lv_value_0_0= ruleNoQuoteString ) ) ;
    public final EObject ruleStringTerm() throws RecognitionException {
        EObject current = null;

        AntlrDatatypeRuleToken lv_value_0_0 = null;


         enterRule(); 
            
        try {
            // InternalSimdaseParser.g:5163:28: ( ( (lv_value_0_0= ruleNoQuoteString ) ) )
            // InternalSimdaseParser.g:5164:1: ( (lv_value_0_0= ruleNoQuoteString ) )
            {
            // InternalSimdaseParser.g:5164:1: ( (lv_value_0_0= ruleNoQuoteString ) )
            // InternalSimdaseParser.g:5165:1: (lv_value_0_0= ruleNoQuoteString )
            {
            // InternalSimdaseParser.g:5165:1: (lv_value_0_0= ruleNoQuoteString )
            // InternalSimdaseParser.g:5166:3: lv_value_0_0= ruleNoQuoteString
            {
             
            	        newCompositeNode(grammarAccess.getStringTermAccess().getValueNoQuoteStringParserRuleCall_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_2);
            lv_value_0_0=ruleNoQuoteString();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getStringTermRule());
            	        }
                   		set(
                   			current, 
                   			"value",
                    		lv_value_0_0, 
                    		"org.osate.xtext.aadl2.properties.Properties.NoQuoteString");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStringTerm"


    // $ANTLR start "entryRuleNoQuoteString"
    // InternalSimdaseParser.g:5190:1: entryRuleNoQuoteString returns [String current=null] : iv_ruleNoQuoteString= ruleNoQuoteString EOF ;
    public final String entryRuleNoQuoteString() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleNoQuoteString = null;


        try {
            // InternalSimdaseParser.g:5191:1: (iv_ruleNoQuoteString= ruleNoQuoteString EOF )
            // InternalSimdaseParser.g:5192:2: iv_ruleNoQuoteString= ruleNoQuoteString EOF
            {
             newCompositeNode(grammarAccess.getNoQuoteStringRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleNoQuoteString=ruleNoQuoteString();

            state._fsp--;

             current =iv_ruleNoQuoteString.getText(); 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNoQuoteString"


    // $ANTLR start "ruleNoQuoteString"
    // InternalSimdaseParser.g:5199:1: ruleNoQuoteString returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : this_STRING_0= RULE_STRING ;
    public final AntlrDatatypeRuleToken ruleNoQuoteString() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_STRING_0=null;

         enterRule(); 
            
        try {
            // InternalSimdaseParser.g:5203:6: (this_STRING_0= RULE_STRING )
            // InternalSimdaseParser.g:5204:5: this_STRING_0= RULE_STRING
            {
            this_STRING_0=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_2); 

            		current.merge(this_STRING_0);
                
             
                newLeafNode(this_STRING_0, grammarAccess.getNoQuoteStringAccess().getSTRINGTerminalRuleCall()); 
                

            }

             leaveRule();
                
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNoQuoteString"


    // $ANTLR start "entryRuleArrayRange"
    // InternalSimdaseParser.g:5219:1: entryRuleArrayRange returns [EObject current=null] : iv_ruleArrayRange= ruleArrayRange EOF ;
    public final EObject entryRuleArrayRange() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleArrayRange = null;


        try {
            // InternalSimdaseParser.g:5220:2: (iv_ruleArrayRange= ruleArrayRange EOF )
            // InternalSimdaseParser.g:5221:2: iv_ruleArrayRange= ruleArrayRange EOF
            {
             newCompositeNode(grammarAccess.getArrayRangeRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleArrayRange=ruleArrayRange();

            state._fsp--;

             current =iv_ruleArrayRange; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleArrayRange"


    // $ANTLR start "ruleArrayRange"
    // InternalSimdaseParser.g:5228:1: ruleArrayRange returns [EObject current=null] : ( () otherlv_1= LeftSquareBracket ( (lv_lowerBound_2_0= ruleINTVALUE ) ) (otherlv_3= FullStopFullStop ( (lv_upperBound_4_0= ruleINTVALUE ) ) )? otherlv_5= RightSquareBracket ) ;
    public final EObject ruleArrayRange() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        AntlrDatatypeRuleToken lv_lowerBound_2_0 = null;

        AntlrDatatypeRuleToken lv_upperBound_4_0 = null;


         enterRule(); 
            
        try {
            // InternalSimdaseParser.g:5231:28: ( ( () otherlv_1= LeftSquareBracket ( (lv_lowerBound_2_0= ruleINTVALUE ) ) (otherlv_3= FullStopFullStop ( (lv_upperBound_4_0= ruleINTVALUE ) ) )? otherlv_5= RightSquareBracket ) )
            // InternalSimdaseParser.g:5232:1: ( () otherlv_1= LeftSquareBracket ( (lv_lowerBound_2_0= ruleINTVALUE ) ) (otherlv_3= FullStopFullStop ( (lv_upperBound_4_0= ruleINTVALUE ) ) )? otherlv_5= RightSquareBracket )
            {
            // InternalSimdaseParser.g:5232:1: ( () otherlv_1= LeftSquareBracket ( (lv_lowerBound_2_0= ruleINTVALUE ) ) (otherlv_3= FullStopFullStop ( (lv_upperBound_4_0= ruleINTVALUE ) ) )? otherlv_5= RightSquareBracket )
            // InternalSimdaseParser.g:5232:2: () otherlv_1= LeftSquareBracket ( (lv_lowerBound_2_0= ruleINTVALUE ) ) (otherlv_3= FullStopFullStop ( (lv_upperBound_4_0= ruleINTVALUE ) ) )? otherlv_5= RightSquareBracket
            {
            // InternalSimdaseParser.g:5232:2: ()
            // InternalSimdaseParser.g:5233:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getArrayRangeAccess().getArrayRangeAction_0(),
                        current);
                

            }

            otherlv_1=(Token)match(input,LeftSquareBracket,FollowSets000.FOLLOW_47); 

                	newLeafNode(otherlv_1, grammarAccess.getArrayRangeAccess().getLeftSquareBracketKeyword_1());
                
            // InternalSimdaseParser.g:5243:1: ( (lv_lowerBound_2_0= ruleINTVALUE ) )
            // InternalSimdaseParser.g:5244:1: (lv_lowerBound_2_0= ruleINTVALUE )
            {
            // InternalSimdaseParser.g:5244:1: (lv_lowerBound_2_0= ruleINTVALUE )
            // InternalSimdaseParser.g:5245:3: lv_lowerBound_2_0= ruleINTVALUE
            {
             
            	        newCompositeNode(grammarAccess.getArrayRangeAccess().getLowerBoundINTVALUEParserRuleCall_2_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_48);
            lv_lowerBound_2_0=ruleINTVALUE();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getArrayRangeRule());
            	        }
                   		set(
                   			current, 
                   			"lowerBound",
                    		lv_lowerBound_2_0, 
                    		"org.osate.xtext.aadl2.properties.Properties.INTVALUE");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // InternalSimdaseParser.g:5261:2: (otherlv_3= FullStopFullStop ( (lv_upperBound_4_0= ruleINTVALUE ) ) )?
            int alt51=2;
            int LA51_0 = input.LA(1);

            if ( (LA51_0==FullStopFullStop) ) {
                alt51=1;
            }
            switch (alt51) {
                case 1 :
                    // InternalSimdaseParser.g:5262:2: otherlv_3= FullStopFullStop ( (lv_upperBound_4_0= ruleINTVALUE ) )
                    {
                    otherlv_3=(Token)match(input,FullStopFullStop,FollowSets000.FOLLOW_47); 

                        	newLeafNode(otherlv_3, grammarAccess.getArrayRangeAccess().getFullStopFullStopKeyword_3_0());
                        
                    // InternalSimdaseParser.g:5266:1: ( (lv_upperBound_4_0= ruleINTVALUE ) )
                    // InternalSimdaseParser.g:5267:1: (lv_upperBound_4_0= ruleINTVALUE )
                    {
                    // InternalSimdaseParser.g:5267:1: (lv_upperBound_4_0= ruleINTVALUE )
                    // InternalSimdaseParser.g:5268:3: lv_upperBound_4_0= ruleINTVALUE
                    {
                     
                    	        newCompositeNode(grammarAccess.getArrayRangeAccess().getUpperBoundINTVALUEParserRuleCall_3_1_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_21);
                    lv_upperBound_4_0=ruleINTVALUE();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getArrayRangeRule());
                    	        }
                           		set(
                           			current, 
                           			"upperBound",
                            		lv_upperBound_4_0, 
                            		"org.osate.xtext.aadl2.properties.Properties.INTVALUE");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }

            otherlv_5=(Token)match(input,RightSquareBracket,FollowSets000.FOLLOW_2); 

                	newLeafNode(otherlv_5, grammarAccess.getArrayRangeAccess().getRightSquareBracketKeyword_4());
                

            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleArrayRange"


    // $ANTLR start "entryRuleSignedConstant"
    // InternalSimdaseParser.g:5297:1: entryRuleSignedConstant returns [EObject current=null] : iv_ruleSignedConstant= ruleSignedConstant EOF ;
    public final EObject entryRuleSignedConstant() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSignedConstant = null;


        try {
            // InternalSimdaseParser.g:5298:2: (iv_ruleSignedConstant= ruleSignedConstant EOF )
            // InternalSimdaseParser.g:5299:2: iv_ruleSignedConstant= ruleSignedConstant EOF
            {
             newCompositeNode(grammarAccess.getSignedConstantRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleSignedConstant=ruleSignedConstant();

            state._fsp--;

             current =iv_ruleSignedConstant; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSignedConstant"


    // $ANTLR start "ruleSignedConstant"
    // InternalSimdaseParser.g:5306:1: ruleSignedConstant returns [EObject current=null] : ( ( (lv_op_0_0= rulePlusMinus ) ) ( (lv_ownedPropertyExpression_1_0= ruleConstantValue ) ) ) ;
    public final EObject ruleSignedConstant() throws RecognitionException {
        EObject current = null;

        AntlrDatatypeRuleToken lv_op_0_0 = null;

        EObject lv_ownedPropertyExpression_1_0 = null;


         enterRule(); 
            
        try {
            // InternalSimdaseParser.g:5309:28: ( ( ( (lv_op_0_0= rulePlusMinus ) ) ( (lv_ownedPropertyExpression_1_0= ruleConstantValue ) ) ) )
            // InternalSimdaseParser.g:5310:1: ( ( (lv_op_0_0= rulePlusMinus ) ) ( (lv_ownedPropertyExpression_1_0= ruleConstantValue ) ) )
            {
            // InternalSimdaseParser.g:5310:1: ( ( (lv_op_0_0= rulePlusMinus ) ) ( (lv_ownedPropertyExpression_1_0= ruleConstantValue ) ) )
            // InternalSimdaseParser.g:5310:2: ( (lv_op_0_0= rulePlusMinus ) ) ( (lv_ownedPropertyExpression_1_0= ruleConstantValue ) )
            {
            // InternalSimdaseParser.g:5310:2: ( (lv_op_0_0= rulePlusMinus ) )
            // InternalSimdaseParser.g:5311:1: (lv_op_0_0= rulePlusMinus )
            {
            // InternalSimdaseParser.g:5311:1: (lv_op_0_0= rulePlusMinus )
            // InternalSimdaseParser.g:5312:3: lv_op_0_0= rulePlusMinus
            {
             
            	        newCompositeNode(grammarAccess.getSignedConstantAccess().getOpPlusMinusParserRuleCall_0_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_49);
            lv_op_0_0=rulePlusMinus();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getSignedConstantRule());
            	        }
                   		set(
                   			current, 
                   			"op",
                    		lv_op_0_0, 
                    		"org.osate.xtext.aadl2.properties.Properties.PlusMinus");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // InternalSimdaseParser.g:5328:2: ( (lv_ownedPropertyExpression_1_0= ruleConstantValue ) )
            // InternalSimdaseParser.g:5329:1: (lv_ownedPropertyExpression_1_0= ruleConstantValue )
            {
            // InternalSimdaseParser.g:5329:1: (lv_ownedPropertyExpression_1_0= ruleConstantValue )
            // InternalSimdaseParser.g:5330:3: lv_ownedPropertyExpression_1_0= ruleConstantValue
            {
             
            	        newCompositeNode(grammarAccess.getSignedConstantAccess().getOwnedPropertyExpressionConstantValueParserRuleCall_1_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_2);
            lv_ownedPropertyExpression_1_0=ruleConstantValue();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getSignedConstantRule());
            	        }
                   		add(
                   			current, 
                   			"ownedPropertyExpression",
                    		lv_ownedPropertyExpression_1_0, 
                    		"org.osate.xtext.aadl2.properties.Properties.ConstantValue");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSignedConstant"


    // $ANTLR start "entryRuleIntegerTerm"
    // InternalSimdaseParser.g:5354:1: entryRuleIntegerTerm returns [EObject current=null] : iv_ruleIntegerTerm= ruleIntegerTerm EOF ;
    public final EObject entryRuleIntegerTerm() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIntegerTerm = null;


        try {
            // InternalSimdaseParser.g:5355:2: (iv_ruleIntegerTerm= ruleIntegerTerm EOF )
            // InternalSimdaseParser.g:5356:2: iv_ruleIntegerTerm= ruleIntegerTerm EOF
            {
             newCompositeNode(grammarAccess.getIntegerTermRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleIntegerTerm=ruleIntegerTerm();

            state._fsp--;

             current =iv_ruleIntegerTerm; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIntegerTerm"


    // $ANTLR start "ruleIntegerTerm"
    // InternalSimdaseParser.g:5363:1: ruleIntegerTerm returns [EObject current=null] : ( ( (lv_value_0_0= ruleSignedInt ) ) ( (otherlv_1= RULE_ID ) )? ) ;
    public final EObject ruleIntegerTerm() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        AntlrDatatypeRuleToken lv_value_0_0 = null;


         enterRule(); 
            
        try {
            // InternalSimdaseParser.g:5366:28: ( ( ( (lv_value_0_0= ruleSignedInt ) ) ( (otherlv_1= RULE_ID ) )? ) )
            // InternalSimdaseParser.g:5367:1: ( ( (lv_value_0_0= ruleSignedInt ) ) ( (otherlv_1= RULE_ID ) )? )
            {
            // InternalSimdaseParser.g:5367:1: ( ( (lv_value_0_0= ruleSignedInt ) ) ( (otherlv_1= RULE_ID ) )? )
            // InternalSimdaseParser.g:5367:2: ( (lv_value_0_0= ruleSignedInt ) ) ( (otherlv_1= RULE_ID ) )?
            {
            // InternalSimdaseParser.g:5367:2: ( (lv_value_0_0= ruleSignedInt ) )
            // InternalSimdaseParser.g:5368:1: (lv_value_0_0= ruleSignedInt )
            {
            // InternalSimdaseParser.g:5368:1: (lv_value_0_0= ruleSignedInt )
            // InternalSimdaseParser.g:5369:3: lv_value_0_0= ruleSignedInt
            {
             
            	        newCompositeNode(grammarAccess.getIntegerTermAccess().getValueSignedIntParserRuleCall_0_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_50);
            lv_value_0_0=ruleSignedInt();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getIntegerTermRule());
            	        }
                   		set(
                   			current, 
                   			"value",
                    		lv_value_0_0, 
                    		"org.osate.xtext.aadl2.properties.Properties.SignedInt");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // InternalSimdaseParser.g:5385:2: ( (otherlv_1= RULE_ID ) )?
            int alt52=2;
            int LA52_0 = input.LA(1);

            if ( (LA52_0==RULE_ID) ) {
                alt52=1;
            }
            switch (alt52) {
                case 1 :
                    // InternalSimdaseParser.g:5386:1: (otherlv_1= RULE_ID )
                    {
                    // InternalSimdaseParser.g:5386:1: (otherlv_1= RULE_ID )
                    // InternalSimdaseParser.g:5387:3: otherlv_1= RULE_ID
                    {

                    			if (current==null) {
                    	            current = createModelElement(grammarAccess.getIntegerTermRule());
                    	        }
                            
                    otherlv_1=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_2); 

                    		newLeafNode(otherlv_1, grammarAccess.getIntegerTermAccess().getUnitUnitLiteralCrossReference_1_0()); 
                    	

                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIntegerTerm"


    // $ANTLR start "entryRuleSignedInt"
    // InternalSimdaseParser.g:5406:1: entryRuleSignedInt returns [String current=null] : iv_ruleSignedInt= ruleSignedInt EOF ;
    public final String entryRuleSignedInt() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleSignedInt = null;


        try {
            // InternalSimdaseParser.g:5407:1: (iv_ruleSignedInt= ruleSignedInt EOF )
            // InternalSimdaseParser.g:5408:2: iv_ruleSignedInt= ruleSignedInt EOF
            {
             newCompositeNode(grammarAccess.getSignedIntRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleSignedInt=ruleSignedInt();

            state._fsp--;

             current =iv_ruleSignedInt.getText(); 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSignedInt"


    // $ANTLR start "ruleSignedInt"
    // InternalSimdaseParser.g:5415:1: ruleSignedInt returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : ( (kw= PlusSign | kw= HyphenMinus )? this_INTEGER_LIT_2= RULE_INTEGER_LIT ) ;
    public final AntlrDatatypeRuleToken ruleSignedInt() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        Token this_INTEGER_LIT_2=null;

         enterRule(); 
            
        try {
            // InternalSimdaseParser.g:5419:6: ( ( (kw= PlusSign | kw= HyphenMinus )? this_INTEGER_LIT_2= RULE_INTEGER_LIT ) )
            // InternalSimdaseParser.g:5420:1: ( (kw= PlusSign | kw= HyphenMinus )? this_INTEGER_LIT_2= RULE_INTEGER_LIT )
            {
            // InternalSimdaseParser.g:5420:1: ( (kw= PlusSign | kw= HyphenMinus )? this_INTEGER_LIT_2= RULE_INTEGER_LIT )
            // InternalSimdaseParser.g:5420:2: (kw= PlusSign | kw= HyphenMinus )? this_INTEGER_LIT_2= RULE_INTEGER_LIT
            {
            // InternalSimdaseParser.g:5420:2: (kw= PlusSign | kw= HyphenMinus )?
            int alt53=3;
            int LA53_0 = input.LA(1);

            if ( (LA53_0==PlusSign) ) {
                alt53=1;
            }
            else if ( (LA53_0==HyphenMinus) ) {
                alt53=2;
            }
            switch (alt53) {
                case 1 :
                    // InternalSimdaseParser.g:5421:2: kw= PlusSign
                    {
                    kw=(Token)match(input,PlusSign,FollowSets000.FOLLOW_47); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getSignedIntAccess().getPlusSignKeyword_0_0()); 
                        

                    }
                    break;
                case 2 :
                    // InternalSimdaseParser.g:5428:2: kw= HyphenMinus
                    {
                    kw=(Token)match(input,HyphenMinus,FollowSets000.FOLLOW_47); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getSignedIntAccess().getHyphenMinusKeyword_0_1()); 
                        

                    }
                    break;

            }

            this_INTEGER_LIT_2=(Token)match(input,RULE_INTEGER_LIT,FollowSets000.FOLLOW_2); 

            		current.merge(this_INTEGER_LIT_2);
                
             
                newLeafNode(this_INTEGER_LIT_2, grammarAccess.getSignedIntAccess().getINTEGER_LITTerminalRuleCall_1()); 
                

            }


            }

             leaveRule();
                
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSignedInt"


    // $ANTLR start "entryRuleRealTerm"
    // InternalSimdaseParser.g:5448:1: entryRuleRealTerm returns [EObject current=null] : iv_ruleRealTerm= ruleRealTerm EOF ;
    public final EObject entryRuleRealTerm() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRealTerm = null;


        try {
            // InternalSimdaseParser.g:5449:2: (iv_ruleRealTerm= ruleRealTerm EOF )
            // InternalSimdaseParser.g:5450:2: iv_ruleRealTerm= ruleRealTerm EOF
            {
             newCompositeNode(grammarAccess.getRealTermRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleRealTerm=ruleRealTerm();

            state._fsp--;

             current =iv_ruleRealTerm; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRealTerm"


    // $ANTLR start "ruleRealTerm"
    // InternalSimdaseParser.g:5457:1: ruleRealTerm returns [EObject current=null] : ( ( (lv_value_0_0= ruleSignedReal ) ) ( (otherlv_1= RULE_ID ) )? ) ;
    public final EObject ruleRealTerm() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        AntlrDatatypeRuleToken lv_value_0_0 = null;


         enterRule(); 
            
        try {
            // InternalSimdaseParser.g:5460:28: ( ( ( (lv_value_0_0= ruleSignedReal ) ) ( (otherlv_1= RULE_ID ) )? ) )
            // InternalSimdaseParser.g:5461:1: ( ( (lv_value_0_0= ruleSignedReal ) ) ( (otherlv_1= RULE_ID ) )? )
            {
            // InternalSimdaseParser.g:5461:1: ( ( (lv_value_0_0= ruleSignedReal ) ) ( (otherlv_1= RULE_ID ) )? )
            // InternalSimdaseParser.g:5461:2: ( (lv_value_0_0= ruleSignedReal ) ) ( (otherlv_1= RULE_ID ) )?
            {
            // InternalSimdaseParser.g:5461:2: ( (lv_value_0_0= ruleSignedReal ) )
            // InternalSimdaseParser.g:5462:1: (lv_value_0_0= ruleSignedReal )
            {
            // InternalSimdaseParser.g:5462:1: (lv_value_0_0= ruleSignedReal )
            // InternalSimdaseParser.g:5463:3: lv_value_0_0= ruleSignedReal
            {
             
            	        newCompositeNode(grammarAccess.getRealTermAccess().getValueSignedRealParserRuleCall_0_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_50);
            lv_value_0_0=ruleSignedReal();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getRealTermRule());
            	        }
                   		set(
                   			current, 
                   			"value",
                    		lv_value_0_0, 
                    		"org.osate.xtext.aadl2.properties.Properties.SignedReal");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // InternalSimdaseParser.g:5479:2: ( (otherlv_1= RULE_ID ) )?
            int alt54=2;
            int LA54_0 = input.LA(1);

            if ( (LA54_0==RULE_ID) ) {
                alt54=1;
            }
            switch (alt54) {
                case 1 :
                    // InternalSimdaseParser.g:5480:1: (otherlv_1= RULE_ID )
                    {
                    // InternalSimdaseParser.g:5480:1: (otherlv_1= RULE_ID )
                    // InternalSimdaseParser.g:5481:3: otherlv_1= RULE_ID
                    {

                    			if (current==null) {
                    	            current = createModelElement(grammarAccess.getRealTermRule());
                    	        }
                            
                    otherlv_1=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_2); 

                    		newLeafNode(otherlv_1, grammarAccess.getRealTermAccess().getUnitUnitLiteralCrossReference_1_0()); 
                    	

                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRealTerm"


    // $ANTLR start "entryRuleSignedReal"
    // InternalSimdaseParser.g:5500:1: entryRuleSignedReal returns [String current=null] : iv_ruleSignedReal= ruleSignedReal EOF ;
    public final String entryRuleSignedReal() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleSignedReal = null;


        try {
            // InternalSimdaseParser.g:5501:1: (iv_ruleSignedReal= ruleSignedReal EOF )
            // InternalSimdaseParser.g:5502:2: iv_ruleSignedReal= ruleSignedReal EOF
            {
             newCompositeNode(grammarAccess.getSignedRealRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleSignedReal=ruleSignedReal();

            state._fsp--;

             current =iv_ruleSignedReal.getText(); 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSignedReal"


    // $ANTLR start "ruleSignedReal"
    // InternalSimdaseParser.g:5509:1: ruleSignedReal returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : ( (kw= PlusSign | kw= HyphenMinus )? this_REAL_LIT_2= RULE_REAL_LIT ) ;
    public final AntlrDatatypeRuleToken ruleSignedReal() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        Token this_REAL_LIT_2=null;

         enterRule(); 
            
        try {
            // InternalSimdaseParser.g:5513:6: ( ( (kw= PlusSign | kw= HyphenMinus )? this_REAL_LIT_2= RULE_REAL_LIT ) )
            // InternalSimdaseParser.g:5514:1: ( (kw= PlusSign | kw= HyphenMinus )? this_REAL_LIT_2= RULE_REAL_LIT )
            {
            // InternalSimdaseParser.g:5514:1: ( (kw= PlusSign | kw= HyphenMinus )? this_REAL_LIT_2= RULE_REAL_LIT )
            // InternalSimdaseParser.g:5514:2: (kw= PlusSign | kw= HyphenMinus )? this_REAL_LIT_2= RULE_REAL_LIT
            {
            // InternalSimdaseParser.g:5514:2: (kw= PlusSign | kw= HyphenMinus )?
            int alt55=3;
            int LA55_0 = input.LA(1);

            if ( (LA55_0==PlusSign) ) {
                alt55=1;
            }
            else if ( (LA55_0==HyphenMinus) ) {
                alt55=2;
            }
            switch (alt55) {
                case 1 :
                    // InternalSimdaseParser.g:5515:2: kw= PlusSign
                    {
                    kw=(Token)match(input,PlusSign,FollowSets000.FOLLOW_51); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getSignedRealAccess().getPlusSignKeyword_0_0()); 
                        

                    }
                    break;
                case 2 :
                    // InternalSimdaseParser.g:5522:2: kw= HyphenMinus
                    {
                    kw=(Token)match(input,HyphenMinus,FollowSets000.FOLLOW_51); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getSignedRealAccess().getHyphenMinusKeyword_0_1()); 
                        

                    }
                    break;

            }

            this_REAL_LIT_2=(Token)match(input,RULE_REAL_LIT,FollowSets000.FOLLOW_2); 

            		current.merge(this_REAL_LIT_2);
                
             
                newLeafNode(this_REAL_LIT_2, grammarAccess.getSignedRealAccess().getREAL_LITTerminalRuleCall_1()); 
                

            }


            }

             leaveRule();
                
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSignedReal"


    // $ANTLR start "entryRuleNumericRangeTerm"
    // InternalSimdaseParser.g:5542:1: entryRuleNumericRangeTerm returns [EObject current=null] : iv_ruleNumericRangeTerm= ruleNumericRangeTerm EOF ;
    public final EObject entryRuleNumericRangeTerm() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNumericRangeTerm = null;


        try {
            // InternalSimdaseParser.g:5543:2: (iv_ruleNumericRangeTerm= ruleNumericRangeTerm EOF )
            // InternalSimdaseParser.g:5544:2: iv_ruleNumericRangeTerm= ruleNumericRangeTerm EOF
            {
             newCompositeNode(grammarAccess.getNumericRangeTermRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleNumericRangeTerm=ruleNumericRangeTerm();

            state._fsp--;

             current =iv_ruleNumericRangeTerm; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNumericRangeTerm"


    // $ANTLR start "ruleNumericRangeTerm"
    // InternalSimdaseParser.g:5551:1: ruleNumericRangeTerm returns [EObject current=null] : ( ( (lv_minimum_0_0= ruleNumAlt ) ) otherlv_1= FullStopFullStop ( (lv_maximum_2_0= ruleNumAlt ) ) (otherlv_3= Delta ( (lv_delta_4_0= ruleNumAlt ) ) )? ) ;
    public final EObject ruleNumericRangeTerm() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_minimum_0_0 = null;

        EObject lv_maximum_2_0 = null;

        EObject lv_delta_4_0 = null;


         enterRule(); 
            
        try {
            // InternalSimdaseParser.g:5554:28: ( ( ( (lv_minimum_0_0= ruleNumAlt ) ) otherlv_1= FullStopFullStop ( (lv_maximum_2_0= ruleNumAlt ) ) (otherlv_3= Delta ( (lv_delta_4_0= ruleNumAlt ) ) )? ) )
            // InternalSimdaseParser.g:5555:1: ( ( (lv_minimum_0_0= ruleNumAlt ) ) otherlv_1= FullStopFullStop ( (lv_maximum_2_0= ruleNumAlt ) ) (otherlv_3= Delta ( (lv_delta_4_0= ruleNumAlt ) ) )? )
            {
            // InternalSimdaseParser.g:5555:1: ( ( (lv_minimum_0_0= ruleNumAlt ) ) otherlv_1= FullStopFullStop ( (lv_maximum_2_0= ruleNumAlt ) ) (otherlv_3= Delta ( (lv_delta_4_0= ruleNumAlt ) ) )? )
            // InternalSimdaseParser.g:5555:2: ( (lv_minimum_0_0= ruleNumAlt ) ) otherlv_1= FullStopFullStop ( (lv_maximum_2_0= ruleNumAlt ) ) (otherlv_3= Delta ( (lv_delta_4_0= ruleNumAlt ) ) )?
            {
            // InternalSimdaseParser.g:5555:2: ( (lv_minimum_0_0= ruleNumAlt ) )
            // InternalSimdaseParser.g:5556:1: (lv_minimum_0_0= ruleNumAlt )
            {
            // InternalSimdaseParser.g:5556:1: (lv_minimum_0_0= ruleNumAlt )
            // InternalSimdaseParser.g:5557:3: lv_minimum_0_0= ruleNumAlt
            {
             
            	        newCompositeNode(grammarAccess.getNumericRangeTermAccess().getMinimumNumAltParserRuleCall_0_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_52);
            lv_minimum_0_0=ruleNumAlt();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getNumericRangeTermRule());
            	        }
                   		set(
                   			current, 
                   			"minimum",
                    		lv_minimum_0_0, 
                    		"org.osate.xtext.aadl2.properties.Properties.NumAlt");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_1=(Token)match(input,FullStopFullStop,FollowSets000.FOLLOW_49); 

                	newLeafNode(otherlv_1, grammarAccess.getNumericRangeTermAccess().getFullStopFullStopKeyword_1());
                
            // InternalSimdaseParser.g:5578:1: ( (lv_maximum_2_0= ruleNumAlt ) )
            // InternalSimdaseParser.g:5579:1: (lv_maximum_2_0= ruleNumAlt )
            {
            // InternalSimdaseParser.g:5579:1: (lv_maximum_2_0= ruleNumAlt )
            // InternalSimdaseParser.g:5580:3: lv_maximum_2_0= ruleNumAlt
            {
             
            	        newCompositeNode(grammarAccess.getNumericRangeTermAccess().getMaximumNumAltParserRuleCall_2_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_53);
            lv_maximum_2_0=ruleNumAlt();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getNumericRangeTermRule());
            	        }
                   		set(
                   			current, 
                   			"maximum",
                    		lv_maximum_2_0, 
                    		"org.osate.xtext.aadl2.properties.Properties.NumAlt");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // InternalSimdaseParser.g:5596:2: (otherlv_3= Delta ( (lv_delta_4_0= ruleNumAlt ) ) )?
            int alt56=2;
            int LA56_0 = input.LA(1);

            if ( (LA56_0==Delta) ) {
                alt56=1;
            }
            switch (alt56) {
                case 1 :
                    // InternalSimdaseParser.g:5597:2: otherlv_3= Delta ( (lv_delta_4_0= ruleNumAlt ) )
                    {
                    otherlv_3=(Token)match(input,Delta,FollowSets000.FOLLOW_49); 

                        	newLeafNode(otherlv_3, grammarAccess.getNumericRangeTermAccess().getDeltaKeyword_3_0());
                        
                    // InternalSimdaseParser.g:5601:1: ( (lv_delta_4_0= ruleNumAlt ) )
                    // InternalSimdaseParser.g:5602:1: (lv_delta_4_0= ruleNumAlt )
                    {
                    // InternalSimdaseParser.g:5602:1: (lv_delta_4_0= ruleNumAlt )
                    // InternalSimdaseParser.g:5603:3: lv_delta_4_0= ruleNumAlt
                    {
                     
                    	        newCompositeNode(grammarAccess.getNumericRangeTermAccess().getDeltaNumAltParserRuleCall_3_1_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_delta_4_0=ruleNumAlt();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getNumericRangeTermRule());
                    	        }
                           		set(
                           			current, 
                           			"delta",
                            		lv_delta_4_0, 
                            		"org.osate.xtext.aadl2.properties.Properties.NumAlt");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNumericRangeTerm"


    // $ANTLR start "entryRuleNumAlt"
    // InternalSimdaseParser.g:5627:1: entryRuleNumAlt returns [EObject current=null] : iv_ruleNumAlt= ruleNumAlt EOF ;
    public final EObject entryRuleNumAlt() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNumAlt = null;


        try {
            // InternalSimdaseParser.g:5628:2: (iv_ruleNumAlt= ruleNumAlt EOF )
            // InternalSimdaseParser.g:5629:2: iv_ruleNumAlt= ruleNumAlt EOF
            {
             newCompositeNode(grammarAccess.getNumAltRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleNumAlt=ruleNumAlt();

            state._fsp--;

             current =iv_ruleNumAlt; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNumAlt"


    // $ANTLR start "ruleNumAlt"
    // InternalSimdaseParser.g:5636:1: ruleNumAlt returns [EObject current=null] : (this_RealTerm_0= ruleRealTerm | this_IntegerTerm_1= ruleIntegerTerm | this_SignedConstant_2= ruleSignedConstant | this_ConstantValue_3= ruleConstantValue ) ;
    public final EObject ruleNumAlt() throws RecognitionException {
        EObject current = null;

        EObject this_RealTerm_0 = null;

        EObject this_IntegerTerm_1 = null;

        EObject this_SignedConstant_2 = null;

        EObject this_ConstantValue_3 = null;


         enterRule(); 
            
        try {
            // InternalSimdaseParser.g:5639:28: ( (this_RealTerm_0= ruleRealTerm | this_IntegerTerm_1= ruleIntegerTerm | this_SignedConstant_2= ruleSignedConstant | this_ConstantValue_3= ruleConstantValue ) )
            // InternalSimdaseParser.g:5640:1: (this_RealTerm_0= ruleRealTerm | this_IntegerTerm_1= ruleIntegerTerm | this_SignedConstant_2= ruleSignedConstant | this_ConstantValue_3= ruleConstantValue )
            {
            // InternalSimdaseParser.g:5640:1: (this_RealTerm_0= ruleRealTerm | this_IntegerTerm_1= ruleIntegerTerm | this_SignedConstant_2= ruleSignedConstant | this_ConstantValue_3= ruleConstantValue )
            int alt57=4;
            switch ( input.LA(1) ) {
            case PlusSign:
                {
                switch ( input.LA(2) ) {
                case RULE_ID:
                    {
                    alt57=3;
                    }
                    break;
                case RULE_REAL_LIT:
                    {
                    alt57=1;
                    }
                    break;
                case RULE_INTEGER_LIT:
                    {
                    alt57=2;
                    }
                    break;
                default:
                    NoViableAltException nvae =
                        new NoViableAltException("", 57, 1, input);

                    throw nvae;
                }

                }
                break;
            case HyphenMinus:
                {
                switch ( input.LA(2) ) {
                case RULE_REAL_LIT:
                    {
                    alt57=1;
                    }
                    break;
                case RULE_INTEGER_LIT:
                    {
                    alt57=2;
                    }
                    break;
                case RULE_ID:
                    {
                    alt57=3;
                    }
                    break;
                default:
                    NoViableAltException nvae =
                        new NoViableAltException("", 57, 2, input);

                    throw nvae;
                }

                }
                break;
            case RULE_REAL_LIT:
                {
                alt57=1;
                }
                break;
            case RULE_INTEGER_LIT:
                {
                alt57=2;
                }
                break;
            case RULE_ID:
                {
                alt57=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 57, 0, input);

                throw nvae;
            }

            switch (alt57) {
                case 1 :
                    // InternalSimdaseParser.g:5641:5: this_RealTerm_0= ruleRealTerm
                    {
                     
                            newCompositeNode(grammarAccess.getNumAltAccess().getRealTermParserRuleCall_0()); 
                        
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_RealTerm_0=ruleRealTerm();

                    state._fsp--;


                            current = this_RealTerm_0;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // InternalSimdaseParser.g:5651:5: this_IntegerTerm_1= ruleIntegerTerm
                    {
                     
                            newCompositeNode(grammarAccess.getNumAltAccess().getIntegerTermParserRuleCall_1()); 
                        
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_IntegerTerm_1=ruleIntegerTerm();

                    state._fsp--;


                            current = this_IntegerTerm_1;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 3 :
                    // InternalSimdaseParser.g:5661:5: this_SignedConstant_2= ruleSignedConstant
                    {
                     
                            newCompositeNode(grammarAccess.getNumAltAccess().getSignedConstantParserRuleCall_2()); 
                        
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_SignedConstant_2=ruleSignedConstant();

                    state._fsp--;


                            current = this_SignedConstant_2;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 4 :
                    // InternalSimdaseParser.g:5671:5: this_ConstantValue_3= ruleConstantValue
                    {
                     
                            newCompositeNode(grammarAccess.getNumAltAccess().getConstantValueParserRuleCall_3()); 
                        
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_ConstantValue_3=ruleConstantValue();

                    state._fsp--;


                            current = this_ConstantValue_3;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNumAlt"


    // $ANTLR start "entryRuleAppliesToKeywords"
    // InternalSimdaseParser.g:5687:1: entryRuleAppliesToKeywords returns [String current=null] : iv_ruleAppliesToKeywords= ruleAppliesToKeywords EOF ;
    public final String entryRuleAppliesToKeywords() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleAppliesToKeywords = null;


        try {
            // InternalSimdaseParser.g:5688:1: (iv_ruleAppliesToKeywords= ruleAppliesToKeywords EOF )
            // InternalSimdaseParser.g:5689:2: iv_ruleAppliesToKeywords= ruleAppliesToKeywords EOF
            {
             newCompositeNode(grammarAccess.getAppliesToKeywordsRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleAppliesToKeywords=ruleAppliesToKeywords();

            state._fsp--;

             current =iv_ruleAppliesToKeywords.getText(); 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAppliesToKeywords"


    // $ANTLR start "ruleAppliesToKeywords"
    // InternalSimdaseParser.g:5696:1: ruleAppliesToKeywords returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= Applies kw= To ) ;
    public final AntlrDatatypeRuleToken ruleAppliesToKeywords() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;

         enterRule(); 
            
        try {
            // InternalSimdaseParser.g:5700:6: ( (kw= Applies kw= To ) )
            // InternalSimdaseParser.g:5701:1: (kw= Applies kw= To )
            {
            // InternalSimdaseParser.g:5701:1: (kw= Applies kw= To )
            // InternalSimdaseParser.g:5702:2: kw= Applies kw= To
            {
            kw=(Token)match(input,Applies,FollowSets000.FOLLOW_54); 

                    current.merge(kw);
                    newLeafNode(kw, grammarAccess.getAppliesToKeywordsAccess().getAppliesKeyword_0()); 
                
            kw=(Token)match(input,To,FollowSets000.FOLLOW_2); 

                    current.merge(kw);
                    newLeafNode(kw, grammarAccess.getAppliesToKeywordsAccess().getToKeyword_1()); 
                

            }


            }

             leaveRule();
                
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAppliesToKeywords"


    // $ANTLR start "entryRuleInBindingKeywords"
    // InternalSimdaseParser.g:5721:1: entryRuleInBindingKeywords returns [String current=null] : iv_ruleInBindingKeywords= ruleInBindingKeywords EOF ;
    public final String entryRuleInBindingKeywords() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleInBindingKeywords = null;


        try {
            // InternalSimdaseParser.g:5722:1: (iv_ruleInBindingKeywords= ruleInBindingKeywords EOF )
            // InternalSimdaseParser.g:5723:2: iv_ruleInBindingKeywords= ruleInBindingKeywords EOF
            {
             newCompositeNode(grammarAccess.getInBindingKeywordsRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleInBindingKeywords=ruleInBindingKeywords();

            state._fsp--;

             current =iv_ruleInBindingKeywords.getText(); 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInBindingKeywords"


    // $ANTLR start "ruleInBindingKeywords"
    // InternalSimdaseParser.g:5730:1: ruleInBindingKeywords returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= In kw= Binding ) ;
    public final AntlrDatatypeRuleToken ruleInBindingKeywords() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;

         enterRule(); 
            
        try {
            // InternalSimdaseParser.g:5734:6: ( (kw= In kw= Binding ) )
            // InternalSimdaseParser.g:5735:1: (kw= In kw= Binding )
            {
            // InternalSimdaseParser.g:5735:1: (kw= In kw= Binding )
            // InternalSimdaseParser.g:5736:2: kw= In kw= Binding
            {
            kw=(Token)match(input,In,FollowSets000.FOLLOW_55); 

                    current.merge(kw);
                    newLeafNode(kw, grammarAccess.getInBindingKeywordsAccess().getInKeyword_0()); 
                
            kw=(Token)match(input,Binding,FollowSets000.FOLLOW_2); 

                    current.merge(kw);
                    newLeafNode(kw, grammarAccess.getInBindingKeywordsAccess().getBindingKeyword_1()); 
                

            }


            }

             leaveRule();
                
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInBindingKeywords"


    // $ANTLR start "entryRuleInModesKeywords"
    // InternalSimdaseParser.g:5755:1: entryRuleInModesKeywords returns [String current=null] : iv_ruleInModesKeywords= ruleInModesKeywords EOF ;
    public final String entryRuleInModesKeywords() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleInModesKeywords = null;


        try {
            // InternalSimdaseParser.g:5756:1: (iv_ruleInModesKeywords= ruleInModesKeywords EOF )
            // InternalSimdaseParser.g:5757:2: iv_ruleInModesKeywords= ruleInModesKeywords EOF
            {
             newCompositeNode(grammarAccess.getInModesKeywordsRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleInModesKeywords=ruleInModesKeywords();

            state._fsp--;

             current =iv_ruleInModesKeywords.getText(); 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInModesKeywords"


    // $ANTLR start "ruleInModesKeywords"
    // InternalSimdaseParser.g:5764:1: ruleInModesKeywords returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= In kw= Modes ) ;
    public final AntlrDatatypeRuleToken ruleInModesKeywords() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;

         enterRule(); 
            
        try {
            // InternalSimdaseParser.g:5768:6: ( (kw= In kw= Modes ) )
            // InternalSimdaseParser.g:5769:1: (kw= In kw= Modes )
            {
            // InternalSimdaseParser.g:5769:1: (kw= In kw= Modes )
            // InternalSimdaseParser.g:5770:2: kw= In kw= Modes
            {
            kw=(Token)match(input,In,FollowSets000.FOLLOW_56); 

                    current.merge(kw);
                    newLeafNode(kw, grammarAccess.getInModesKeywordsAccess().getInKeyword_0()); 
                
            kw=(Token)match(input,Modes,FollowSets000.FOLLOW_2); 

                    current.merge(kw);
                    newLeafNode(kw, grammarAccess.getInModesKeywordsAccess().getModesKeyword_1()); 
                

            }


            }

             leaveRule();
                
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInModesKeywords"


    // $ANTLR start "entryRuleINTVALUE"
    // InternalSimdaseParser.g:5789:1: entryRuleINTVALUE returns [String current=null] : iv_ruleINTVALUE= ruleINTVALUE EOF ;
    public final String entryRuleINTVALUE() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleINTVALUE = null;


        try {
            // InternalSimdaseParser.g:5790:1: (iv_ruleINTVALUE= ruleINTVALUE EOF )
            // InternalSimdaseParser.g:5791:2: iv_ruleINTVALUE= ruleINTVALUE EOF
            {
             newCompositeNode(grammarAccess.getINTVALUERule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleINTVALUE=ruleINTVALUE();

            state._fsp--;

             current =iv_ruleINTVALUE.getText(); 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleINTVALUE"


    // $ANTLR start "ruleINTVALUE"
    // InternalSimdaseParser.g:5798:1: ruleINTVALUE returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : this_INTEGER_LIT_0= RULE_INTEGER_LIT ;
    public final AntlrDatatypeRuleToken ruleINTVALUE() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_INTEGER_LIT_0=null;

         enterRule(); 
            
        try {
            // InternalSimdaseParser.g:5802:6: (this_INTEGER_LIT_0= RULE_INTEGER_LIT )
            // InternalSimdaseParser.g:5803:5: this_INTEGER_LIT_0= RULE_INTEGER_LIT
            {
            this_INTEGER_LIT_0=(Token)match(input,RULE_INTEGER_LIT,FollowSets000.FOLLOW_2); 

            		current.merge(this_INTEGER_LIT_0);
                
             
                newLeafNode(this_INTEGER_LIT_0, grammarAccess.getINTVALUEAccess().getINTEGER_LITTerminalRuleCall()); 
                

            }

             leaveRule();
                
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleINTVALUE"


    // $ANTLR start "entryRuleQPREF"
    // InternalSimdaseParser.g:5820:1: entryRuleQPREF returns [String current=null] : iv_ruleQPREF= ruleQPREF EOF ;
    public final String entryRuleQPREF() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleQPREF = null;


        try {
            // InternalSimdaseParser.g:5821:1: (iv_ruleQPREF= ruleQPREF EOF )
            // InternalSimdaseParser.g:5822:2: iv_ruleQPREF= ruleQPREF EOF
            {
             newCompositeNode(grammarAccess.getQPREFRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleQPREF=ruleQPREF();

            state._fsp--;

             current =iv_ruleQPREF.getText(); 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQPREF"


    // $ANTLR start "ruleQPREF"
    // InternalSimdaseParser.g:5829:1: ruleQPREF returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_ID_0= RULE_ID (kw= ColonColon this_ID_2= RULE_ID )? ) ;
    public final AntlrDatatypeRuleToken ruleQPREF() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_ID_0=null;
        Token kw=null;
        Token this_ID_2=null;

         enterRule(); 
            
        try {
            // InternalSimdaseParser.g:5833:6: ( (this_ID_0= RULE_ID (kw= ColonColon this_ID_2= RULE_ID )? ) )
            // InternalSimdaseParser.g:5834:1: (this_ID_0= RULE_ID (kw= ColonColon this_ID_2= RULE_ID )? )
            {
            // InternalSimdaseParser.g:5834:1: (this_ID_0= RULE_ID (kw= ColonColon this_ID_2= RULE_ID )? )
            // InternalSimdaseParser.g:5834:6: this_ID_0= RULE_ID (kw= ColonColon this_ID_2= RULE_ID )?
            {
            this_ID_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_57); 

            		current.merge(this_ID_0);
                
             
                newLeafNode(this_ID_0, grammarAccess.getQPREFAccess().getIDTerminalRuleCall_0()); 
                
            // InternalSimdaseParser.g:5841:1: (kw= ColonColon this_ID_2= RULE_ID )?
            int alt58=2;
            int LA58_0 = input.LA(1);

            if ( (LA58_0==ColonColon) ) {
                alt58=1;
            }
            switch (alt58) {
                case 1 :
                    // InternalSimdaseParser.g:5842:2: kw= ColonColon this_ID_2= RULE_ID
                    {
                    kw=(Token)match(input,ColonColon,FollowSets000.FOLLOW_39); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getQPREFAccess().getColonColonKeyword_1_0()); 
                        
                    this_ID_2=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_2); 

                    		current.merge(this_ID_2);
                        
                     
                        newLeafNode(this_ID_2, grammarAccess.getQPREFAccess().getIDTerminalRuleCall_1_1()); 
                        

                    }
                    break;

            }


            }


            }

             leaveRule();
                
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQPREF"


    // $ANTLR start "entryRuleQCREF"
    // InternalSimdaseParser.g:5862:1: entryRuleQCREF returns [String current=null] : iv_ruleQCREF= ruleQCREF EOF ;
    public final String entryRuleQCREF() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleQCREF = null;


        try {
            // InternalSimdaseParser.g:5863:1: (iv_ruleQCREF= ruleQCREF EOF )
            // InternalSimdaseParser.g:5864:2: iv_ruleQCREF= ruleQCREF EOF
            {
             newCompositeNode(grammarAccess.getQCREFRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleQCREF=ruleQCREF();

            state._fsp--;

             current =iv_ruleQCREF.getText(); 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQCREF"


    // $ANTLR start "ruleQCREF"
    // InternalSimdaseParser.g:5871:1: ruleQCREF returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : ( (this_ID_0= RULE_ID kw= ColonColon )* this_ID_2= RULE_ID (kw= FullStop this_ID_4= RULE_ID )? ) ;
    public final AntlrDatatypeRuleToken ruleQCREF() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_ID_0=null;
        Token kw=null;
        Token this_ID_2=null;
        Token this_ID_4=null;

         enterRule(); 
            
        try {
            // InternalSimdaseParser.g:5875:6: ( ( (this_ID_0= RULE_ID kw= ColonColon )* this_ID_2= RULE_ID (kw= FullStop this_ID_4= RULE_ID )? ) )
            // InternalSimdaseParser.g:5876:1: ( (this_ID_0= RULE_ID kw= ColonColon )* this_ID_2= RULE_ID (kw= FullStop this_ID_4= RULE_ID )? )
            {
            // InternalSimdaseParser.g:5876:1: ( (this_ID_0= RULE_ID kw= ColonColon )* this_ID_2= RULE_ID (kw= FullStop this_ID_4= RULE_ID )? )
            // InternalSimdaseParser.g:5876:2: (this_ID_0= RULE_ID kw= ColonColon )* this_ID_2= RULE_ID (kw= FullStop this_ID_4= RULE_ID )?
            {
            // InternalSimdaseParser.g:5876:2: (this_ID_0= RULE_ID kw= ColonColon )*
            loop59:
            do {
                int alt59=2;
                int LA59_0 = input.LA(1);

                if ( (LA59_0==RULE_ID) ) {
                    int LA59_1 = input.LA(2);

                    if ( (LA59_1==ColonColon) ) {
                        alt59=1;
                    }


                }


                switch (alt59) {
            	case 1 :
            	    // InternalSimdaseParser.g:5876:7: this_ID_0= RULE_ID kw= ColonColon
            	    {
            	    this_ID_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_58); 

            	    		current.merge(this_ID_0);
            	        
            	     
            	        newLeafNode(this_ID_0, grammarAccess.getQCREFAccess().getIDTerminalRuleCall_0_0()); 
            	        
            	    kw=(Token)match(input,ColonColon,FollowSets000.FOLLOW_39); 

            	            current.merge(kw);
            	            newLeafNode(kw, grammarAccess.getQCREFAccess().getColonColonKeyword_0_1()); 
            	        

            	    }
            	    break;

            	default :
            	    break loop59;
                }
            } while (true);

            this_ID_2=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_59); 

            		current.merge(this_ID_2);
                
             
                newLeafNode(this_ID_2, grammarAccess.getQCREFAccess().getIDTerminalRuleCall_1()); 
                
            // InternalSimdaseParser.g:5896:1: (kw= FullStop this_ID_4= RULE_ID )?
            int alt60=2;
            int LA60_0 = input.LA(1);

            if ( (LA60_0==FullStop) ) {
                alt60=1;
            }
            switch (alt60) {
                case 1 :
                    // InternalSimdaseParser.g:5897:2: kw= FullStop this_ID_4= RULE_ID
                    {
                    kw=(Token)match(input,FullStop,FollowSets000.FOLLOW_39); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getQCREFAccess().getFullStopKeyword_2_0()); 
                        
                    this_ID_4=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_2); 

                    		current.merge(this_ID_4);
                        
                     
                        newLeafNode(this_ID_4, grammarAccess.getQCREFAccess().getIDTerminalRuleCall_2_1()); 
                        

                    }
                    break;

            }


            }


            }

             leaveRule();
                
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQCREF"


    // $ANTLR start "entryRuleSTAR"
    // InternalSimdaseParser.g:5917:1: entryRuleSTAR returns [String current=null] : iv_ruleSTAR= ruleSTAR EOF ;
    public final String entryRuleSTAR() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleSTAR = null;


        try {
            // InternalSimdaseParser.g:5918:1: (iv_ruleSTAR= ruleSTAR EOF )
            // InternalSimdaseParser.g:5919:2: iv_ruleSTAR= ruleSTAR EOF
            {
             newCompositeNode(grammarAccess.getSTARRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleSTAR=ruleSTAR();

            state._fsp--;

             current =iv_ruleSTAR.getText(); 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSTAR"


    // $ANTLR start "ruleSTAR"
    // InternalSimdaseParser.g:5926:1: ruleSTAR returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : kw= Asterisk ;
    public final AntlrDatatypeRuleToken ruleSTAR() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;

         enterRule(); 
            
        try {
            // InternalSimdaseParser.g:5930:6: (kw= Asterisk )
            // InternalSimdaseParser.g:5932:2: kw= Asterisk
            {
            kw=(Token)match(input,Asterisk,FollowSets000.FOLLOW_2); 

                    current.merge(kw);
                    newLeafNode(kw, grammarAccess.getSTARAccess().getAsteriskKeyword()); 
                

            }

             leaveRule();
                
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSTAR"

    // Delegated rules


    protected DFA43 dfa43 = new DFA43(this);
    static final String dfa_1s = "\25\uffff";
    static final String dfa_2s = "\10\uffff\1\17\1\21\1\23\3\uffff\1\17\1\uffff\1\21\3\uffff\1\23";
    static final String dfa_3s = "\1\37\5\uffff\2\142\3\50\3\uffff\1\50\1\uffff\1\50\1\uffff\1\154\1\uffff\1\50";
    static final String dfa_4s = "\1\154\5\uffff\4\154\1\131\3\uffff\1\131\1\uffff\1\131\1\uffff\1\154\1\uffff\1\131";
    static final String dfa_5s = "\1\uffff\1\1\1\2\1\3\1\4\1\5\5\uffff\1\11\1\12\1\6\1\uffff\1\7\1\uffff\1\10\1\uffff\1\13\1\uffff";
    static final String dfa_6s = "\25\uffff}>";
    static final String[] dfa_7s = {
            "\1\3\1\uffff\1\2\11\uffff\1\4\10\uffff\1\14\7\uffff\1\14\23\uffff\1\13\2\uffff\1\6\1\uffff\1\7\6\uffff\1\1\5\uffff\1\11\5\uffff\1\10\2\uffff\1\5\1\12",
            "",
            "",
            "",
            "",
            "",
            "\1\11\5\uffff\1\10\3\uffff\1\15",
            "\1\11\5\uffff\1\10\3\uffff\1\15",
            "\1\17\33\uffff\1\15\6\uffff\1\17\5\uffff\1\17\2\uffff\1\17\4\uffff\1\17\22\uffff\1\16",
            "\1\21\33\uffff\1\15\6\uffff\1\21\5\uffff\1\21\2\uffff\1\21\4\uffff\1\21\22\uffff\1\20",
            "\1\23\33\uffff\1\15\1\22\5\uffff\1\23\5\uffff\1\23\2\uffff\1\23\4\uffff\1\23",
            "",
            "",
            "",
            "\1\17\33\uffff\1\15\6\uffff\1\17\5\uffff\1\17\2\uffff\1\17\4\uffff\1\17",
            "",
            "\1\21\33\uffff\1\15\6\uffff\1\21\5\uffff\1\21\2\uffff\1\21\4\uffff\1\21",
            "",
            "\1\24",
            "",
            "\1\23\33\uffff\1\15\6\uffff\1\23\5\uffff\1\23\2\uffff\1\23\4\uffff\1\23"
    };

    static final short[] dfa_1 = DFA.unpackEncodedString(dfa_1s);
    static final short[] dfa_2 = DFA.unpackEncodedString(dfa_2s);
    static final char[] dfa_3 = DFA.unpackEncodedStringToUnsignedChars(dfa_3s);
    static final char[] dfa_4 = DFA.unpackEncodedStringToUnsignedChars(dfa_4s);
    static final short[] dfa_5 = DFA.unpackEncodedString(dfa_5s);
    static final short[] dfa_6 = DFA.unpackEncodedString(dfa_6s);
    static final short[][] dfa_7 = unpackEncodedStringArray(dfa_7s);

    class DFA43 extends DFA {

        public DFA43(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 43;
            this.eot = dfa_1;
            this.eof = dfa_2;
            this.min = dfa_3;
            this.max = dfa_4;
            this.accept = dfa_5;
            this.special = dfa_6;
            this.transition = dfa_7;
        }
        public String getDescription() {
            return "4448:1: (this_RecordTerm_0= ruleRecordTerm | this_ReferenceTerm_1= ruleReferenceTerm | this_ComponentClassifierTerm_2= ruleComponentClassifierTerm | this_ComputedTerm_3= ruleComputedTerm | this_StringTerm_4= ruleStringTerm | this_NumericRangeTerm_5= ruleNumericRangeTerm | this_RealTerm_6= ruleRealTerm | this_IntegerTerm_7= ruleIntegerTerm | this_ListTerm_8= ruleListTerm | this_BooleanLiteral_9= ruleBooleanLiteral | this_LiteralorReferenceTerm_10= ruleLiteralorReferenceTerm )";
        }
    }
 

    
    private static class FollowSets000 {
        public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x010000B054000002L,0x0000000000000001L});
        public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000100L});
        public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x1010000000000000L});
        public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000000000L,0x0000000002000000L});
        public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000000000L,0x0000000800200000L});
        public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x8C80000800000000L,0x00000008C0210002L});
        public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000000000L,0x0000000100000000L});
        public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000000000L,0x0000080200000000L});
        public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000000000L,0x0000000200000000L});
        public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x4000000000000000L});
        public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0040000000000000L});
        public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0004000000000000L});
        public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000620000000000L});
        public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0002000000000000L});
        public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000000000L,0x0000080000000000L});
        public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000000000000L,0x0000000001000000L});
        public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000000000000L,0x0000000010000000L});
        public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000000000000L,0x0000080020000000L});
        public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000000000000L,0x0000000020000000L});
        public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000000000002L,0x0000000000100000L});
        public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x000190412BFFFFF0L,0x0000100200000000L});
        public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000000000000L,0x0000000200100000L});
        public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000400L});
        public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000000000000000L,0x0000000800000000L});
        public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000000000000002L,0x0000000000280000L});
        public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000000000000002L,0x0000000000848000L});
        public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000008L});
        public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0000000000000000L,0x0000000000020000L});
        public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x1C90000800000000L,0x00000008C0214002L});
        public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x0200000000000002L});
        public static final BitSet FOLLOW_33 = new BitSet(new long[]{0x0000000000000002L,0x0000000000002000L});
        public static final BitSet FOLLOW_34 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000004L});
        public static final BitSet FOLLOW_35 = new BitSet(new long[]{0x0000000000000002L,0x000000000C0002C0L});
        public static final BitSet FOLLOW_36 = new BitSet(new long[]{0x2000000000000000L,0x0000000000000100L});
        public static final BitSet FOLLOW_37 = new BitSet(new long[]{0x1010080680000000L,0x0000190410290000L});
        public static final BitSet FOLLOW_38 = new BitSet(new long[]{0x0000010000000000L,0x0000000002100800L});
        public static final BitSet FOLLOW_39 = new BitSet(new long[]{0x0000000000000000L,0x0000100000000000L});
        public static final BitSet FOLLOW_40 = new BitSet(new long[]{0x0000000000000000L,0x0000000002100800L});
        public static final BitSet FOLLOW_41 = new BitSet(new long[]{0x0000000000000000L,0x0000000000010000L});
        public static final BitSet FOLLOW_42 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000800L});
        public static final BitSet FOLLOW_43 = new BitSet(new long[]{0x0000000000000000L,0x0000000000120000L});
        public static final BitSet FOLLOW_44 = new BitSet(new long[]{0x0000000000000000L,0x0000100020000000L});
        public static final BitSet FOLLOW_45 = new BitSet(new long[]{0x1010080680000000L,0x00001904102B0000L});
        public static final BitSet FOLLOW_46 = new BitSet(new long[]{0x0000000000000002L,0x0000000010400000L});
        public static final BitSet FOLLOW_47 = new BitSet(new long[]{0x0000000000000000L,0x0000000400000000L});
        public static final BitSet FOLLOW_48 = new BitSet(new long[]{0x0000000000000000L,0x0000000020000010L});
        public static final BitSet FOLLOW_49 = new BitSet(new long[]{0x0000000000000000L,0x0000110400280000L});
        public static final BitSet FOLLOW_50 = new BitSet(new long[]{0x0000000000000002L,0x0000100000000000L});
        public static final BitSet FOLLOW_51 = new BitSet(new long[]{0x0000000000000000L,0x0000010000000000L});
        public static final BitSet FOLLOW_52 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000010L});
        public static final BitSet FOLLOW_53 = new BitSet(new long[]{0x0008000000000002L});
        public static final BitSet FOLLOW_54 = new BitSet(new long[]{0x0000000000000000L,0x0000000000001000L});
        public static final BitSet FOLLOW_55 = new BitSet(new long[]{0x0000040000000000L});
        public static final BitSet FOLLOW_56 = new BitSet(new long[]{0x0020000000000000L});
        public static final BitSet FOLLOW_57 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000020L});
        public static final BitSet FOLLOW_58 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000020L});
        public static final BitSet FOLLOW_59 = new BitSet(new long[]{0x0000000000000002L,0x0000000000400000L});
    }


}