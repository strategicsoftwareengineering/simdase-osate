/**
 */
package edu.clemson.simdase.simdase.impl;

import edu.clemson.simdase.simdase.CostProperties;
import edu.clemson.simdase.simdase.CostStatement;
import edu.clemson.simdase.simdase.SimdasePackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Cost Properties</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.clemson.simdase.simdase.impl.CostPropertiesImpl#getCostStatements <em>Cost Statements</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CostPropertiesImpl extends SIMDASEStatementImpl implements CostProperties
{
  /**
   * The cached value of the '{@link #getCostStatements() <em>Cost Statements</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCostStatements()
   * @generated
   * @ordered
   */
  protected EList<CostStatement> costStatements;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected CostPropertiesImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return SimdasePackage.Literals.COST_PROPERTIES;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<CostStatement> getCostStatements()
  {
    if (costStatements == null)
    {
      costStatements = new EObjectContainmentEList<CostStatement>(CostStatement.class, this, SimdasePackage.COST_PROPERTIES__COST_STATEMENTS);
    }
    return costStatements;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case SimdasePackage.COST_PROPERTIES__COST_STATEMENTS:
        return ((InternalEList<?>)getCostStatements()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case SimdasePackage.COST_PROPERTIES__COST_STATEMENTS:
        return getCostStatements();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case SimdasePackage.COST_PROPERTIES__COST_STATEMENTS:
        getCostStatements().clear();
        getCostStatements().addAll((Collection<? extends CostStatement>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case SimdasePackage.COST_PROPERTIES__COST_STATEMENTS:
        getCostStatements().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case SimdasePackage.COST_PROPERTIES__COST_STATEMENTS:
        return costStatements != null && !costStatements.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //CostPropertiesImpl
