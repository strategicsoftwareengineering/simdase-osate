package edu.clemson.simdase.parsing;

import org.osate.aadl2.AnnexLibrary;
import org.osate.aadl2.AnnexSubclause;
import org.osate.aadl2.modelsupport.errorreporting.ParseErrorReporter;
import org.osate.annexsupport.AnnexParseUtil;
import org.osate.annexsupport.AnnexParser;

import com.google.inject.Injector;
import edu.clemson.simdase.parser.antlr.SimdaseParser;
import edu.clemson.simdase.services.SimdaseGrammarAccess;
import edu.clemson.simdase.ui.internal.SimdaseActivator;

// Based on EMV2AnnexParser from Error Model annex
public class SimdaseAnnexParser implements AnnexParser {
    private SimdaseParser parser;

    protected SimdaseParser getParser() {
        if (parser == null) {
            Injector injector = SimdaseActivator.getInstance().getInjector(
            		SimdaseActivator.EDU_CLEMSON_SIMDASE_SIMDASE);
            parser = injector.getInstance(SimdaseParser.class);
        }
        return parser;
    }

    protected SimdaseGrammarAccess getGrammarAccess() {
        return getParser().getGrammarAccess();
    }

    public AnnexLibrary parseAnnexLibrary(String annexName, String source, String filename,
            int line, int column, ParseErrorReporter errReporter) {
        return (AnnexLibrary) AnnexParseUtil.parse(getParser(),source, getGrammarAccess().getSimdaseLibraryRule(), filename, line,
                column, errReporter);
    }

    public AnnexSubclause parseAnnexSubclause(String annexName, String source, String filename,
            int line, int column, ParseErrorReporter errReporter) {
    	return (AnnexSubclause) AnnexParseUtil.parse(getParser(),source,getGrammarAccess().getSimdaseSubclauseRule(),filename,line,column, errReporter);
    }


}