/**
 */
package edu.clemson.simdase.simdase.impl;

import edu.clemson.simdase.simdase.BooleanStatementLvl4;
import edu.clemson.simdase.simdase.SimdasePackage;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Boolean Statement Lvl4</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class BooleanStatementLvl4Impl extends MinimalEObjectImpl.Container implements BooleanStatementLvl4
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected BooleanStatementLvl4Impl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return SimdasePackage.Literals.BOOLEAN_STATEMENT_LVL4;
  }

} //BooleanStatementLvl4Impl
