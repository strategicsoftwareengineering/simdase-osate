/**
 */
package edu.clemson.simdase.simdase;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Bool T</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.clemson.simdase.simdase.SimdasePackage#getBoolT()
 * @model
 * @generated
 */
public interface BoolT extends BooleanStatementLvl8
{
} // BoolT
