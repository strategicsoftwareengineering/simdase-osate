/**
 */
package edu.clemson.simdase.simdase;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>With Not</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.clemson.simdase.simdase.WithNot#getStatement <em>Statement</em>}</li>
 * </ul>
 *
 * @see edu.clemson.simdase.simdase.SimdasePackage#getWithNot()
 * @model
 * @generated
 */
public interface WithNot extends BooleanStatementLvl4
{
  /**
   * Returns the value of the '<em><b>Statement</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Statement</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Statement</em>' containment reference.
   * @see #setStatement(BooleanStatementLvl4)
   * @see edu.clemson.simdase.simdase.SimdasePackage#getWithNot_Statement()
   * @model containment="true"
   * @generated
   */
  BooleanStatementLvl4 getStatement();

  /**
   * Sets the value of the '{@link edu.clemson.simdase.simdase.WithNot#getStatement <em>Statement</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Statement</em>' containment reference.
   * @see #getStatement()
   * @generated
   */
  void setStatement(BooleanStatementLvl4 value);

} // WithNot
