package edu.clemson.simdase.ui;

import org.apache.log4j.Logger;
import org.osate.core.OsateCorePlugin;
import org.osgi.framework.BundleContext;

import com.google.inject.Injector;

import edu.clemson.simdase.ui.internal.SimdaseActivator;

public class MySimdaseActivator extends SimdaseActivator {
	@Override
	public void start(BundleContext context) throws Exception {
		super.start(context);
		try {
			registerInjectorFor(EDU_CLEMSON_SIMDASE_SIMDASE);

		} catch (Exception e) {
			Logger.getLogger(getClass()).error(e.getMessage(), e);
			throw e;
		}
	}

	@Override
	public Injector getInjector(String languageName) {
		return OsateCorePlugin.getDefault().getInjector(languageName);
	}

	protected void registerInjectorFor(String language) throws Exception {
		OsateCorePlugin.getDefault().registerInjectorFor(language, createInjector(language));
	}
}
