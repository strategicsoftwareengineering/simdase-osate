/**
 */
package edu.clemson.simdase.simdase.impl;

import edu.clemson.simdase.simdase.SimdasePackage;
import edu.clemson.simdase.simdase.StatementLvl4;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Statement Lvl4</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class StatementLvl4Impl extends MinimalEObjectImpl.Container implements StatementLvl4
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected StatementLvl4Impl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return SimdasePackage.Literals.STATEMENT_LVL4;
  }

} //StatementLvl4Impl
