
/*
 * generated by Xtext
 */
lexer grammar InternalSimdaseLexer;


@header {
package edu.clemson.simdase.ui.contentassist.antlr.lexer;

// Hack: Use our own Lexer superclass by means of import. 
// Currently there is no other way to specify the superclass for the lexer.
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.Lexer;
}




Using_externally_available_software : ('U'|'u')('S'|'s')('I'|'i')('N'|'n')('G'|'g')'_'('E'|'e')('X'|'x')('T'|'t')('E'|'e')('R'|'r')('N'|'n')('A'|'a')('L'|'l')('L'|'l')('Y'|'y')'_'('A'|'a')('V'|'v')('A'|'a')('I'|'i')('L'|'l')('A'|'a')('B'|'b')('L'|'l')('E'|'e')'_'('S'|'s')('O'|'o')('F'|'f')('T'|'t')('W'|'w')('A'|'a')('R'|'r')('E'|'e');

Developing_acquisition_strategy : ('D'|'d')('E'|'e')('V'|'v')('E'|'e')('L'|'l')('O'|'o')('P'|'p')('I'|'i')('N'|'n')('G'|'g')'_'('A'|'a')('C'|'c')('Q'|'q')('U'|'u')('I'|'i')('S'|'s')('I'|'i')('T'|'t')('I'|'i')('O'|'o')('N'|'n')'_'('S'|'s')('T'|'t')('R'|'r')('A'|'a')('T'|'t')('E'|'e')('G'|'g')('Y'|'y');

Organizational_risk_management : ('O'|'o')('R'|'r')('G'|'g')('A'|'a')('N'|'n')('I'|'i')('Z'|'z')('A'|'a')('T'|'t')('I'|'i')('O'|'o')('N'|'n')('A'|'a')('L'|'l')'_'('R'|'r')('I'|'i')('S'|'s')('K'|'k')'_'('M'|'m')('A'|'a')('N'|'n')('A'|'a')('G'|'g')('E'|'e')('M'|'m')('E'|'e')('N'|'n')('T'|'t');

Understanding_relevant_domains : ('U'|'u')('N'|'n')('D'|'d')('E'|'e')('R'|'r')('S'|'s')('T'|'t')('A'|'a')('N'|'n')('D'|'d')('I'|'i')('N'|'n')('G'|'g')'_'('R'|'r')('E'|'e')('L'|'l')('E'|'e')('V'|'v')('A'|'a')('N'|'n')('T'|'t')'_'('D'|'d')('O'|'o')('M'|'m')('A'|'a')('I'|'i')('N'|'n')('S'|'s');

Customer_interface_management : ('C'|'c')('U'|'u')('S'|'s')('T'|'t')('O'|'o')('M'|'m')('E'|'e')('R'|'r')'_'('I'|'i')('N'|'n')('T'|'t')('E'|'e')('R'|'r')('F'|'f')('A'|'a')('C'|'c')('E'|'e')'_'('M'|'m')('A'|'a')('N'|'n')('A'|'a')('G'|'g')('E'|'e')('M'|'m')('E'|'e')('N'|'n')('T'|'t');

Launching_institutionalizing : ('L'|'l')('A'|'a')('U'|'u')('N'|'n')('C'|'c')('H'|'h')('I'|'i')('N'|'n')('G'|'g')'_'('I'|'i')('N'|'n')('S'|'s')('T'|'t')('I'|'i')('T'|'t')('U'|'u')('T'|'t')('I'|'i')('O'|'o')('N'|'n')('A'|'a')('L'|'l')('I'|'i')('Z'|'z')('I'|'i')('N'|'n')('G'|'g');

Software_system_integration : ('S'|'s')('O'|'o')('F'|'f')('T'|'t')('W'|'w')('A'|'a')('R'|'r')('E'|'e')'_'('S'|'s')('Y'|'y')('S'|'s')('T'|'t')('E'|'e')('M'|'m')'_'('I'|'i')('N'|'n')('T'|'t')('E'|'e')('G'|'g')('R'|'r')('A'|'a')('T'|'t')('I'|'i')('O'|'o')('N'|'n');

Technical_risk_management : ('T'|'t')('E'|'e')('C'|'c')('H'|'h')('N'|'n')('I'|'i')('C'|'c')('A'|'a')('L'|'l')'_'('R'|'r')('I'|'i')('S'|'s')('K'|'k')'_'('M'|'m')('A'|'a')('N'|'n')('A'|'a')('G'|'g')('E'|'e')('M'|'m')('E'|'e')('N'|'n')('T'|'t');

Configuration_management : ('C'|'c')('O'|'o')('N'|'n')('F'|'f')('I'|'i')('G'|'g')('U'|'u')('R'|'r')('A'|'a')('T'|'t')('I'|'i')('O'|'o')('N'|'n')'_'('M'|'m')('A'|'a')('N'|'n')('A'|'a')('G'|'g')('E'|'e')('M'|'m')('E'|'e')('N'|'n')('T'|'t');

Requirements_engineering : ('R'|'r')('E'|'e')('Q'|'q')('U'|'u')('I'|'i')('R'|'r')('E'|'e')('M'|'m')('E'|'e')('N'|'n')('T'|'t')('S'|'s')'_'('E'|'e')('N'|'n')('G'|'g')('I'|'i')('N'|'n')('E'|'e')('E'|'e')('R'|'r')('I'|'i')('N'|'n')('G'|'g');

Structuring_organization : ('S'|'s')('T'|'t')('R'|'r')('U'|'u')('C'|'c')('T'|'t')('U'|'u')('R'|'r')('I'|'i')('N'|'n')('G'|'g')'_'('O'|'o')('R'|'r')('G'|'g')('A'|'a')('N'|'n')('I'|'i')('Z'|'z')('A'|'a')('T'|'t')('I'|'i')('O'|'o')('N'|'n');

Architecture_definition : ('A'|'a')('R'|'r')('C'|'c')('H'|'h')('I'|'i')('T'|'t')('E'|'e')('C'|'c')('T'|'t')('U'|'u')('R'|'r')('E'|'e')'_'('D'|'d')('E'|'e')('F'|'f')('I'|'i')('N'|'n')('I'|'i')('T'|'t')('I'|'i')('O'|'o')('N'|'n');

Architecture_evaluation : ('A'|'a')('R'|'r')('C'|'c')('H'|'h')('I'|'i')('T'|'t')('E'|'e')('C'|'c')('T'|'t')('U'|'u')('R'|'r')('E'|'e')'_'('E'|'e')('V'|'v')('A'|'a')('L'|'l')('U'|'u')('A'|'a')('T'|'t')('I'|'i')('O'|'o')('N'|'n');

Organizational_planning : ('O'|'o')('R'|'r')('G'|'g')('A'|'a')('N'|'n')('I'|'i')('Z'|'z')('A'|'a')('T'|'t')('I'|'i')('O'|'o')('N'|'n')('A'|'a')('L'|'l')'_'('P'|'p')('L'|'l')('A'|'a')('N'|'n')('N'|'n')('I'|'i')('N'|'n')('G'|'g');

Building_business_case : ('B'|'b')('U'|'u')('I'|'i')('L'|'l')('D'|'d')('I'|'i')('N'|'n')('G'|'g')'_'('B'|'b')('U'|'u')('S'|'s')('I'|'i')('N'|'n')('E'|'e')('S'|'s')('S'|'s')'_'('C'|'c')('A'|'a')('S'|'s')('E'|'e');

Mining_existing_assets : ('M'|'m')('I'|'i')('N'|'n')('I'|'i')('N'|'n')('G'|'g')'_'('E'|'e')('X'|'x')('I'|'i')('S'|'s')('T'|'t')('I'|'i')('N'|'n')('G'|'g')'_'('A'|'a')('S'|'s')('S'|'s')('E'|'e')('T'|'t')('S'|'s');

Technology_forecasting : ('T'|'t')('E'|'e')('C'|'c')('H'|'h')('N'|'n')('O'|'o')('L'|'l')('O'|'o')('G'|'g')('Y'|'y')'_'('F'|'f')('O'|'o')('R'|'r')('E'|'e')('C'|'c')('A'|'a')('S'|'s')('T'|'t')('I'|'i')('N'|'n')('G'|'g');

Component_development : ('C'|'c')('O'|'o')('M'|'m')('P'|'p')('O'|'o')('N'|'n')('E'|'e')('N'|'n')('T'|'t')'_'('D'|'d')('E'|'e')('V'|'v')('E'|'e')('L'|'l')('O'|'o')('P'|'p')('M'|'m')('E'|'e')('N'|'n')('T'|'t');

Measurement_tracking : ('M'|'m')('E'|'e')('A'|'a')('S'|'s')('U'|'u')('R'|'r')('E'|'e')('M'|'m')('E'|'e')('N'|'n')('T'|'t')'_'('T'|'t')('R'|'r')('A'|'a')('C'|'c')('K'|'k')('I'|'i')('N'|'n')('G'|'g');

Commission_analysis : ('C'|'c')('O'|'o')('M'|'m')('M'|'m')('I'|'i')('S'|'s')('S'|'s')('I'|'i')('O'|'o')('N'|'n')'_'('A'|'a')('N'|'n')('A'|'a')('L'|'l')('Y'|'y')('S'|'s')('I'|'i')('S'|'s');

Process_discipline : ('P'|'p')('R'|'r')('O'|'o')('C'|'c')('E'|'e')('S'|'s')('S'|'s')'_'('D'|'d')('I'|'i')('S'|'s')('C'|'c')('I'|'i')('P'|'p')('L'|'l')('I'|'i')('N'|'n')('E'|'e');

Technical_planning : ('T'|'t')('E'|'e')('C'|'c')('H'|'h')('N'|'n')('I'|'i')('C'|'c')('A'|'a')('L'|'l')'_'('P'|'p')('L'|'l')('A'|'a')('N'|'n')('N'|'n')('I'|'i')('N'|'n')('G'|'g');

Active_variants : ('A'|'a')('C'|'c')('T'|'t')('I'|'i')('V'|'v')('E'|'e')'_'('V'|'v')('A'|'a')('R'|'r')('I'|'i')('A'|'a')('N'|'n')('T'|'t')('S'|'s');

Market_analysis : ('M'|'m')('A'|'a')('R'|'r')('K'|'k')('E'|'e')('T'|'t')'_'('A'|'a')('N'|'n')('A'|'a')('L'|'l')('Y'|'y')('S'|'s')('I'|'i')('S'|'s');

Scaling_factor : ('S'|'s')('C'|'c')('A'|'a')('L'|'l')('I'|'i')('N'|'n')('G'|'g')'_'('F'|'f')('A'|'a')('C'|'c')('T'|'t')('O'|'o')('R'|'r');

Tool_support : ('T'|'t')('O'|'o')('O'|'o')('L'|'l')'_'('S'|'s')('U'|'u')('P'|'p')('P'|'p')('O'|'o')('R'|'r')('T'|'t');

Is_adaptive : ('I'|'i')('S'|'s')'_'('A'|'a')('D'|'d')('A'|'a')('P'|'p')('T'|'t')('I'|'i')('V'|'v')('E'|'e');

Classifier : ('C'|'c')('L'|'l')('A'|'a')('S'|'s')('S'|'s')('I'|'i')('F'|'f')('I'|'i')('E'|'e')('R'|'r');

Operations : ('O'|'o')('P'|'p')('E'|'e')('R'|'r')('A'|'a')('T'|'t')('I'|'i')('O'|'o')('N'|'n')('S'|'s');

Reference : ('R'|'r')('E'|'e')('F'|'f')('E'|'e')('R'|'r')('E'|'e')('N'|'n')('C'|'c')('E'|'e');

Constant : ('C'|'c')('O'|'o')('N'|'n')('S'|'s')('T'|'t')('A'|'a')('N'|'n')('T'|'t');

Random : ('R'|'r')('A'|'a')('N'|'n')('D'|'d')('O'|'o')('M'|'m')'('')';

Time_max : ('T'|'t')('I'|'i')('M'|'m')('E'|'e')'_'('M'|'m')('A'|'a')('X'|'x');

Time_min : ('T'|'t')('I'|'i')('M'|'m')('E'|'e')'_'('M'|'m')('I'|'i')('N'|'n');

Training : ('T'|'t')('R'|'r')('A'|'a')('I'|'i')('N'|'n')('I'|'i')('N'|'n')('G'|'g');

Variants : ('V'|'v')('A'|'a')('R'|'r')('I'|'i')('A'|'a')('N'|'n')('T'|'t')('S'|'s');

Applies : ('A'|'a')('P'|'p')('P'|'p')('L'|'l')('I'|'i')('E'|'e')('S'|'s');

Average : ('A'|'a')('V'|'v')('E'|'e')('R'|'r')('A'|'a')('G'|'g')('E'|'e');

Binding : ('B'|'b')('I'|'i')('N'|'n')('D'|'d')('I'|'i')('N'|'n')('G'|'g');

Compute : ('C'|'c')('O'|'o')('M'|'m')('P'|'p')('U'|'u')('T'|'t')('E'|'e');

Funding : ('F'|'f')('U'|'u')('N'|'n')('D'|'d')('I'|'i')('N'|'n')('G'|'g');

Maximum : ('M'|'m')('A'|'a')('X'|'x')('I'|'i')('M'|'m')('U'|'u')('M'|'m');

Minimum : ('M'|'m')('I'|'i')('N'|'n')('I'|'i')('M'|'m')('U'|'u')('M'|'m');

Scoping : ('S'|'s')('C'|'c')('O'|'o')('P'|'p')('I'|'i')('N'|'n')('G'|'g');

Testing : ('T'|'t')('E'|'e')('S'|'s')('T'|'t')('I'|'i')('N'|'n')('G'|'g');

Result : ('R'|'r')('E'|'e')('S'|'s')('U'|'u')('L'|'l')('T'|'t');

Taking : ('T'|'t')('A'|'a')('K'|'k')('I'|'i')('N'|'n')('G'|'g');

Delta : ('D'|'d')('E'|'e')('L'|'l')('T'|'t')('A'|'a');

False : ('F'|'f')('A'|'a')('L'|'l')('S'|'s')('E'|'e');

Modes : ('M'|'m')('O'|'o')('D'|'d')('E'|'e')('S'|'s');

Times : ('T'|'t')('I'|'i')('M'|'m')('E'|'e')('S'|'s');

Cos : ('C'|'c')('O'|'o')('S'|'s')'(';

Cost : ('C'|'c')('O'|'o')('S'|'s')('T'|'t');

Else : ('E'|'e')('L'|'l')('S'|'s')('E'|'e');

Sin : ('S'|'s')('I'|'i')('N'|'n')'(';

Tan : ('T'|'t')('A'|'a')('N'|'n')'(';

True : ('T'|'t')('R'|'r')('U'|'u')('E'|'e');

PlusSignEqualsSignGreaterThanSign : '+''=''>';

For : ('F'|'f')('O'|'o')('R'|'r');

If : ('I'|'i')('F'|'f')'(';

Run : ('R'|'r')('U'|'u')('N'|'n');

S : ('S'|'s')'('')';

AmpersandAmpersand : '&''&';

AsteriskAsterisk : '*''*';

FullStopFullStop : '.''.';

ColonColon : ':'':';

LessThanSignEqualsSign : '<''=';

EqualsSignEqualsSign : '=''=';

EqualsSignGreaterThanSign : '=''>';

GreaterThanSignEqualsSign : '>''=';

At : ('A'|'a')('T'|'t');

In : ('I'|'i')('N'|'n');

To : ('T'|'t')('O'|'o');

VerticalLineVerticalLine : '|''|';

ExclamationMark : '!';

PercentSign : '%';

LeftParenthesis : '(';

RightParenthesis : ')';

Asterisk : '*';

PlusSign : '+';

Comma : ',';

HyphenMinus : '-';

FullStop : '.';

Solidus : '/';

Colon : ':';

Semicolon : ';';

LessThanSign : '<';

GreaterThanSign : '>';

LeftSquareBracket : '[';

RightSquareBracket : ']';

I : ('I'|'i');

T : ('T'|'t');

LeftCurlyBracket : '{';

RightCurlyBracket : '}';



RULE_FLOAT : RULE_INTEGER_LIT '.' RULE_INTEGER_LIT;

RULE_SL_COMMENT : '--' ~(('\n'|'\r'))* ('\r'? '\n')?;

fragment RULE_EXPONENT : ('e'|'E') ('+'|'-')? RULE_DIGIT+;

fragment RULE_INT_EXPONENT : ('e'|'E') '+'? RULE_DIGIT+;

RULE_REAL_LIT : RULE_DIGIT+ ('_' RULE_DIGIT+)* '.' RULE_DIGIT+ ('_' RULE_DIGIT+)* RULE_EXPONENT?;

RULE_INTEGER_LIT : RULE_DIGIT+ ('_' RULE_DIGIT+)* ('#' RULE_BASED_INTEGER '#' RULE_INT_EXPONENT?|RULE_INT_EXPONENT?);

fragment RULE_DIGIT : '0'..'9';

fragment RULE_EXTENDED_DIGIT : ('0'..'9'|'a'..'f'|'A'..'F');

fragment RULE_BASED_INTEGER : RULE_EXTENDED_DIGIT ('_'? RULE_EXTENDED_DIGIT)*;

RULE_STRING : ('"' ('\\' ('b'|'t'|'n'|'f'|'r'|'u'|'"'|'\''|'\\')|~(('\\'|'"')))* '"'|'\'' ('\\' ('b'|'t'|'n'|'f'|'r'|'u'|'"'|'\''|'\\')|~(('\\'|'\'')))* '\'');

RULE_ID : ('a'..'z'|'A'..'Z') ('_'? ('a'..'z'|'A'..'Z'|'0'..'9'))*;

RULE_WS : (' '|'\t'|'\r'|'\n')+;



