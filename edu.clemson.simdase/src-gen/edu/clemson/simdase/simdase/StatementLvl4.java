/**
 */
package edu.clemson.simdase.simdase;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Statement Lvl4</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.clemson.simdase.simdase.SimdasePackage#getStatementLvl4()
 * @model
 * @generated
 */
public interface StatementLvl4 extends EObject
{
} // StatementLvl4
