/**
 */
package edu.clemson.simdase.simdase;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Active Variants</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.clemson.simdase.simdase.ActiveVariants#getVariants <em>Variants</em>}</li>
 *   <li>{@link edu.clemson.simdase.simdase.ActiveVariants#getFirst <em>First</em>}</li>
 *   <li>{@link edu.clemson.simdase.simdase.ActiveVariants#getRest <em>Rest</em>}</li>
 * </ul>
 *
 * @see edu.clemson.simdase.simdase.SimdasePackage#getActiveVariants()
 * @model
 * @generated
 */
public interface ActiveVariants extends SIMDASEStatement
{
  /**
   * Returns the value of the '<em><b>Variants</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Variants</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Variants</em>' containment reference.
   * @see #setVariants(ActiveVariants)
   * @see edu.clemson.simdase.simdase.SimdasePackage#getActiveVariants_Variants()
   * @model containment="true"
   * @generated
   */
  ActiveVariants getVariants();

  /**
   * Sets the value of the '{@link edu.clemson.simdase.simdase.ActiveVariants#getVariants <em>Variants</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Variants</em>' containment reference.
   * @see #getVariants()
   * @generated
   */
  void setVariants(ActiveVariants value);

  /**
   * Returns the value of the '<em><b>First</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>First</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>First</em>' containment reference.
   * @see #setFirst(ActiveVariant)
   * @see edu.clemson.simdase.simdase.SimdasePackage#getActiveVariants_First()
   * @model containment="true"
   * @generated
   */
  ActiveVariant getFirst();

  /**
   * Sets the value of the '{@link edu.clemson.simdase.simdase.ActiveVariants#getFirst <em>First</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>First</em>' containment reference.
   * @see #getFirst()
   * @generated
   */
  void setFirst(ActiveVariant value);

  /**
   * Returns the value of the '<em><b>Rest</b></em>' containment reference list.
   * The list contents are of type {@link edu.clemson.simdase.simdase.ActiveVariant}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Rest</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Rest</em>' containment reference list.
   * @see edu.clemson.simdase.simdase.SimdasePackage#getActiveVariants_Rest()
   * @model containment="true"
   * @generated
   */
  EList<ActiveVariant> getRest();

} // ActiveVariants
