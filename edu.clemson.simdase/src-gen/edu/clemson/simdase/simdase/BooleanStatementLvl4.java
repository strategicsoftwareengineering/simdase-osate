/**
 */
package edu.clemson.simdase.simdase;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Boolean Statement Lvl4</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.clemson.simdase.simdase.SimdasePackage#getBooleanStatementLvl4()
 * @model
 * @generated
 */
public interface BooleanStatementLvl4 extends EObject
{
} // BooleanStatementLvl4
