/**
 */
package edu.clemson.simdase.simdase.impl;

import edu.clemson.simdase.simdase.ActiveVariant;
import edu.clemson.simdase.simdase.ActiveVariants;
import edu.clemson.simdase.simdase.BoolBoolean;
import edu.clemson.simdase.simdase.BoolCos;
import edu.clemson.simdase.simdase.BoolI;
import edu.clemson.simdase.simdase.BoolInteger;
import edu.clemson.simdase.simdase.BoolParen;
import edu.clemson.simdase.simdase.BoolRandom;
import edu.clemson.simdase.simdase.BoolScalingFactor;
import edu.clemson.simdase.simdase.BoolSin;
import edu.clemson.simdase.simdase.BoolT;
import edu.clemson.simdase.simdase.BoolTan;
import edu.clemson.simdase.simdase.BooleanStatement;
import edu.clemson.simdase.simdase.BooleanStatementLvl2;
import edu.clemson.simdase.simdase.BooleanStatementLvl3;
import edu.clemson.simdase.simdase.BooleanStatementLvl4;
import edu.clemson.simdase.simdase.BooleanStatementLvl5;
import edu.clemson.simdase.simdase.BooleanStatementLvl6;
import edu.clemson.simdase.simdase.BooleanStatementLvl7;
import edu.clemson.simdase.simdase.BooleanStatementLvl8;
import edu.clemson.simdase.simdase.Contract;
import edu.clemson.simdase.simdase.Cos;
import edu.clemson.simdase.simdase.CostProperties;
import edu.clemson.simdase.simdase.CostStatement;
import edu.clemson.simdase.simdase.CostValue;
import edu.clemson.simdase.simdase.EmployeeStatement;
import edu.clemson.simdase.simdase.FixedCost;
import edu.clemson.simdase.simdase.HourlyCost;
import edu.clemson.simdase.simdase.I;
import edu.clemson.simdase.simdase.If;
import edu.clemson.simdase.simdase.IsAdaptiveStatement;
import edu.clemson.simdase.simdase.Paren;
import edu.clemson.simdase.simdase.Random;
import edu.clemson.simdase.simdase.ReEvaluateAveraging;
import edu.clemson.simdase.simdase.SIMDASEStatement;
import edu.clemson.simdase.simdase.ScalingFactor;
import edu.clemson.simdase.simdase.SimdaseContract;
import edu.clemson.simdase.simdase.SimdaseContractLibrary;
import edu.clemson.simdase.simdase.SimdaseContractSubclause;
import edu.clemson.simdase.simdase.SimdaseFactory;
import edu.clemson.simdase.simdase.SimdaseLibrary;
import edu.clemson.simdase.simdase.SimdasePackage;
import edu.clemson.simdase.simdase.SimdaseSubclause;
import edu.clemson.simdase.simdase.Sin;
import edu.clemson.simdase.simdase.Statement;
import edu.clemson.simdase.simdase.StatementLvl2;
import edu.clemson.simdase.simdase.StatementLvl3;
import edu.clemson.simdase.simdase.StatementLvl4;
import edu.clemson.simdase.simdase.StatementScalingFactor;
import edu.clemson.simdase.simdase.T;
import edu.clemson.simdase.simdase.TMax;
import edu.clemson.simdase.simdase.TMin;
import edu.clemson.simdase.simdase.Tan;
import edu.clemson.simdase.simdase.Variant;
import edu.clemson.simdase.simdase.VariantNames;
import edu.clemson.simdase.simdase.Variants;
import edu.clemson.simdase.simdase.WithNot;
import edu.clemson.simdase.simdase.WithoutNot;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class SimdaseFactoryImpl extends EFactoryImpl implements SimdaseFactory
{
  /**
   * Creates the default factory implementation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static SimdaseFactory init()
  {
    try
    {
      SimdaseFactory theSimdaseFactory = (SimdaseFactory)EPackage.Registry.INSTANCE.getEFactory(SimdasePackage.eNS_URI);
      if (theSimdaseFactory != null)
      {
        return theSimdaseFactory;
      }
    }
    catch (Exception exception)
    {
      EcorePlugin.INSTANCE.log(exception);
    }
    return new SimdaseFactoryImpl();
  }

  /**
   * Creates an instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SimdaseFactoryImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EObject create(EClass eClass)
  {
    switch (eClass.getClassifierID())
    {
      case SimdasePackage.SIMDASE_LIBRARY: return createSimdaseLibrary();
      case SimdasePackage.SIMDASE_SUBCLAUSE: return createSimdaseSubclause();
      case SimdasePackage.CONTRACT: return createContract();
      case SimdasePackage.SIMDASE_STATEMENT: return createSIMDASEStatement();
      case SimdasePackage.VARIANTS: return createVariants();
      case SimdasePackage.VARIANT: return createVariant();
      case SimdasePackage.ACTIVE_VARIANTS: return createActiveVariants();
      case SimdasePackage.ACTIVE_VARIANT: return createActiveVariant();
      case SimdasePackage.VARIANT_NAMES: return createVariantNames();
      case SimdasePackage.COST_PROPERTIES: return createCostProperties();
      case SimdasePackage.COST_STATEMENT: return createCostStatement();
      case SimdasePackage.COST_VALUE: return createCostValue();
      case SimdasePackage.EMPLOYEE_STATEMENT: return createEmployeeStatement();
      case SimdasePackage.BOOLEAN: return createBoolean();
      case SimdasePackage.NUMBER: return createNumber();
      case SimdasePackage.STATEMENT: return createStatement();
      case SimdasePackage.STATEMENT_LVL2: return createStatementLvl2();
      case SimdasePackage.STATEMENT_LVL3: return createStatementLvl3();
      case SimdasePackage.STATEMENT_LVL4: return createStatementLvl4();
      case SimdasePackage.BOOLEAN_STATEMENT: return createBooleanStatement();
      case SimdasePackage.BOOLEAN_STATEMENT_LVL2: return createBooleanStatementLvl2();
      case SimdasePackage.BOOLEAN_STATEMENT_LVL3: return createBooleanStatementLvl3();
      case SimdasePackage.BOOLEAN_STATEMENT_LVL4: return createBooleanStatementLvl4();
      case SimdasePackage.BOOLEAN_STATEMENT_LVL5: return createBooleanStatementLvl5();
      case SimdasePackage.BOOLEAN_STATEMENT_LVL6: return createBooleanStatementLvl6();
      case SimdasePackage.BOOLEAN_STATEMENT_LVL7: return createBooleanStatementLvl7();
      case SimdasePackage.BOOLEAN_STATEMENT_LVL8: return createBooleanStatementLvl8();
      case SimdasePackage.SIMDASE_CONTRACT_LIBRARY: return createSimdaseContractLibrary();
      case SimdasePackage.SIMDASE_CONTRACT_SUBCLAUSE: return createSimdaseContractSubclause();
      case SimdasePackage.SIMDASE_CONTRACT: return createSimdaseContract();
      case SimdasePackage.IS_ADAPTIVE_STATEMENT: return createIsAdaptiveStatement();
      case SimdasePackage.TMAX: return createTMax();
      case SimdasePackage.TMIN: return createTMin();
      case SimdasePackage.SCALING_FACTOR: return createScalingFactor();
      case SimdasePackage.RE_EVALUATE_AVERAGING: return createReEvaluateAveraging();
      case SimdasePackage.HOURLY_COST: return createHourlyCost();
      case SimdasePackage.FIXED_COST: return createFixedCost();
      case SimdasePackage.INTEGER: return createInteger();
      case SimdasePackage.T: return createT();
      case SimdasePackage.I: return createI();
      case SimdasePackage.SIN: return createSin();
      case SimdasePackage.COS: return createCos();
      case SimdasePackage.TAN: return createTan();
      case SimdasePackage.PAREN: return createParen();
      case SimdasePackage.IF: return createIf();
      case SimdasePackage.RANDOM: return createRandom();
      case SimdasePackage.STATEMENT_SCALING_FACTOR: return createStatementScalingFactor();
      case SimdasePackage.WITH_NOT: return createWithNot();
      case SimdasePackage.WITHOUT_NOT: return createWithoutNot();
      case SimdasePackage.BOOL_BOOLEAN: return createBoolBoolean();
      case SimdasePackage.BOOL_T: return createBoolT();
      case SimdasePackage.BOOL_I: return createBoolI();
      case SimdasePackage.BOOL_INTEGER: return createBoolInteger();
      case SimdasePackage.BOOL_SIN: return createBoolSin();
      case SimdasePackage.BOOL_COS: return createBoolCos();
      case SimdasePackage.BOOL_TAN: return createBoolTan();
      case SimdasePackage.BOOL_PAREN: return createBoolParen();
      case SimdasePackage.BOOL_RANDOM: return createBoolRandom();
      case SimdasePackage.BOOL_SCALING_FACTOR: return createBoolScalingFactor();
      default:
        throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SimdaseLibrary createSimdaseLibrary()
  {
    SimdaseLibraryImpl simdaseLibrary = new SimdaseLibraryImpl();
    return simdaseLibrary;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SimdaseSubclause createSimdaseSubclause()
  {
    SimdaseSubclauseImpl simdaseSubclause = new SimdaseSubclauseImpl();
    return simdaseSubclause;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Contract createContract()
  {
    ContractImpl contract = new ContractImpl();
    return contract;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SIMDASEStatement createSIMDASEStatement()
  {
    SIMDASEStatementImpl simdaseStatement = new SIMDASEStatementImpl();
    return simdaseStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Variants createVariants()
  {
    VariantsImpl variants = new VariantsImpl();
    return variants;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Variant createVariant()
  {
    VariantImpl variant = new VariantImpl();
    return variant;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ActiveVariants createActiveVariants()
  {
    ActiveVariantsImpl activeVariants = new ActiveVariantsImpl();
    return activeVariants;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ActiveVariant createActiveVariant()
  {
    ActiveVariantImpl activeVariant = new ActiveVariantImpl();
    return activeVariant;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public VariantNames createVariantNames()
  {
    VariantNamesImpl variantNames = new VariantNamesImpl();
    return variantNames;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CostProperties createCostProperties()
  {
    CostPropertiesImpl costProperties = new CostPropertiesImpl();
    return costProperties;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CostStatement createCostStatement()
  {
    CostStatementImpl costStatement = new CostStatementImpl();
    return costStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CostValue createCostValue()
  {
    CostValueImpl costValue = new CostValueImpl();
    return costValue;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EmployeeStatement createEmployeeStatement()
  {
    EmployeeStatementImpl employeeStatement = new EmployeeStatementImpl();
    return employeeStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public edu.clemson.simdase.simdase.Boolean createBoolean()
  {
    BooleanImpl boolean_ = new BooleanImpl();
    return boolean_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public edu.clemson.simdase.simdase.Number createNumber()
  {
    NumberImpl number = new NumberImpl();
    return number;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Statement createStatement()
  {
    StatementImpl statement = new StatementImpl();
    return statement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public StatementLvl2 createStatementLvl2()
  {
    StatementLvl2Impl statementLvl2 = new StatementLvl2Impl();
    return statementLvl2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public StatementLvl3 createStatementLvl3()
  {
    StatementLvl3Impl statementLvl3 = new StatementLvl3Impl();
    return statementLvl3;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public StatementLvl4 createStatementLvl4()
  {
    StatementLvl4Impl statementLvl4 = new StatementLvl4Impl();
    return statementLvl4;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BooleanStatement createBooleanStatement()
  {
    BooleanStatementImpl booleanStatement = new BooleanStatementImpl();
    return booleanStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BooleanStatementLvl2 createBooleanStatementLvl2()
  {
    BooleanStatementLvl2Impl booleanStatementLvl2 = new BooleanStatementLvl2Impl();
    return booleanStatementLvl2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BooleanStatementLvl3 createBooleanStatementLvl3()
  {
    BooleanStatementLvl3Impl booleanStatementLvl3 = new BooleanStatementLvl3Impl();
    return booleanStatementLvl3;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BooleanStatementLvl4 createBooleanStatementLvl4()
  {
    BooleanStatementLvl4Impl booleanStatementLvl4 = new BooleanStatementLvl4Impl();
    return booleanStatementLvl4;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BooleanStatementLvl5 createBooleanStatementLvl5()
  {
    BooleanStatementLvl5Impl booleanStatementLvl5 = new BooleanStatementLvl5Impl();
    return booleanStatementLvl5;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BooleanStatementLvl6 createBooleanStatementLvl6()
  {
    BooleanStatementLvl6Impl booleanStatementLvl6 = new BooleanStatementLvl6Impl();
    return booleanStatementLvl6;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BooleanStatementLvl7 createBooleanStatementLvl7()
  {
    BooleanStatementLvl7Impl booleanStatementLvl7 = new BooleanStatementLvl7Impl();
    return booleanStatementLvl7;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BooleanStatementLvl8 createBooleanStatementLvl8()
  {
    BooleanStatementLvl8Impl booleanStatementLvl8 = new BooleanStatementLvl8Impl();
    return booleanStatementLvl8;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SimdaseContractLibrary createSimdaseContractLibrary()
  {
    SimdaseContractLibraryImpl simdaseContractLibrary = new SimdaseContractLibraryImpl();
    return simdaseContractLibrary;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SimdaseContractSubclause createSimdaseContractSubclause()
  {
    SimdaseContractSubclauseImpl simdaseContractSubclause = new SimdaseContractSubclauseImpl();
    return simdaseContractSubclause;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SimdaseContract createSimdaseContract()
  {
    SimdaseContractImpl simdaseContract = new SimdaseContractImpl();
    return simdaseContract;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IsAdaptiveStatement createIsAdaptiveStatement()
  {
    IsAdaptiveStatementImpl isAdaptiveStatement = new IsAdaptiveStatementImpl();
    return isAdaptiveStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TMax createTMax()
  {
    TMaxImpl tMax = new TMaxImpl();
    return tMax;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TMin createTMin()
  {
    TMinImpl tMin = new TMinImpl();
    return tMin;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ScalingFactor createScalingFactor()
  {
    ScalingFactorImpl scalingFactor = new ScalingFactorImpl();
    return scalingFactor;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ReEvaluateAveraging createReEvaluateAveraging()
  {
    ReEvaluateAveragingImpl reEvaluateAveraging = new ReEvaluateAveragingImpl();
    return reEvaluateAveraging;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public HourlyCost createHourlyCost()
  {
    HourlyCostImpl hourlyCost = new HourlyCostImpl();
    return hourlyCost;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FixedCost createFixedCost()
  {
    FixedCostImpl fixedCost = new FixedCostImpl();
    return fixedCost;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public edu.clemson.simdase.simdase.Integer createInteger()
  {
    IntegerImpl integer = new IntegerImpl();
    return integer;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public T createT()
  {
    TImpl t = new TImpl();
    return t;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public I createI()
  {
    IImpl i = new IImpl();
    return i;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Sin createSin()
  {
    SinImpl sin = new SinImpl();
    return sin;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Cos createCos()
  {
    CosImpl cos = new CosImpl();
    return cos;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Tan createTan()
  {
    TanImpl tan = new TanImpl();
    return tan;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Paren createParen()
  {
    ParenImpl paren = new ParenImpl();
    return paren;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public If createIf()
  {
    IfImpl if_ = new IfImpl();
    return if_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Random createRandom()
  {
    RandomImpl random = new RandomImpl();
    return random;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public StatementScalingFactor createStatementScalingFactor()
  {
    StatementScalingFactorImpl statementScalingFactor = new StatementScalingFactorImpl();
    return statementScalingFactor;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public WithNot createWithNot()
  {
    WithNotImpl withNot = new WithNotImpl();
    return withNot;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public WithoutNot createWithoutNot()
  {
    WithoutNotImpl withoutNot = new WithoutNotImpl();
    return withoutNot;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BoolBoolean createBoolBoolean()
  {
    BoolBooleanImpl boolBoolean = new BoolBooleanImpl();
    return boolBoolean;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BoolT createBoolT()
  {
    BoolTImpl boolT = new BoolTImpl();
    return boolT;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BoolI createBoolI()
  {
    BoolIImpl boolI = new BoolIImpl();
    return boolI;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BoolInteger createBoolInteger()
  {
    BoolIntegerImpl boolInteger = new BoolIntegerImpl();
    return boolInteger;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BoolSin createBoolSin()
  {
    BoolSinImpl boolSin = new BoolSinImpl();
    return boolSin;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BoolCos createBoolCos()
  {
    BoolCosImpl boolCos = new BoolCosImpl();
    return boolCos;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BoolTan createBoolTan()
  {
    BoolTanImpl boolTan = new BoolTanImpl();
    return boolTan;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BoolParen createBoolParen()
  {
    BoolParenImpl boolParen = new BoolParenImpl();
    return boolParen;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BoolRandom createBoolRandom()
  {
    BoolRandomImpl boolRandom = new BoolRandomImpl();
    return boolRandom;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BoolScalingFactor createBoolScalingFactor()
  {
    BoolScalingFactorImpl boolScalingFactor = new BoolScalingFactorImpl();
    return boolScalingFactor;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SimdasePackage getSimdasePackage()
  {
    return (SimdasePackage)getEPackage();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @deprecated
   * @generated
   */
  @Deprecated
  public static SimdasePackage getPackage()
  {
    return SimdasePackage.eINSTANCE;
  }

} //SimdaseFactoryImpl
