package edu.clemson.simdase.analysis.handlers;

import java.util.ArrayList;
import java.util.List;

import org.osate.aadl2.ComponentImplementation;

public class Node {
	private ComponentImplementation component;
	private Integer componentLevel;
	private Integer timePeriod;
	private SimdaseAnnex simdaseAnnex;
	private List<Node> children;
	private List<List<Node>> variants;
	
	public ComponentImplementation getComponent() {
		return component;
	}
	
	public void setComponent(ComponentImplementation component) {
		this.component = component;
	}
	
	public Integer getComponentLevel() {
		return componentLevel;
	}

	public void setComponentLevel(Integer componentLevel) {
		this.componentLevel = componentLevel;
	}
	
	public Integer getTimePeriod() {
		return timePeriod;
	}

	public void setTimePeriod(Integer timePeriod) {
		this.timePeriod = timePeriod;
	}

	public SimdaseAnnex getSimdaseAnnex() {
		return simdaseAnnex;
	}
	
	public void setSimdaseAnnex(SimdaseAnnex simdaseAnnex, String type) {
		if(this.simdaseAnnex == null) {
			this.simdaseAnnex = simdaseAnnex;
		} else {
			this.simdaseAnnex.setActiveVariants(simdaseAnnex.getActiveVariants());
			this.simdaseAnnex.setIsAdaptive(simdaseAnnex.getIsAdaptive());
			this.simdaseAnnex.setIsVariationPoint(simdaseAnnex.getIsVariationPoint());
			this.simdaseAnnex.setReevaluateNTimes(simdaseAnnex.getReevaluateNTimes());
			this.simdaseAnnex.setScalingFactor(simdaseAnnex.getScalingFactor());
			this.simdaseAnnex.setTimePeriodEnd(simdaseAnnex.getTimePeriodEnd());
			this.simdaseAnnex.setTimePeriodStart(simdaseAnnex.getTimePeriodStart());
			this.simdaseAnnex.setUseWhichResult(simdaseAnnex.getUseWhichResult());
			this.simdaseAnnex.setVariants(simdaseAnnex.getVariants());
			for(String cost : this.simdaseAnnex.getEmployeeCosts().keySet()) {
				if(type.equals("average")) {
					List<CostRow.EmployeeCost> currentCosts = this.simdaseAnnex.getEmployeeCosts().get(cost);
					List<CostRow.EmployeeCost> newCosts = simdaseAnnex.getEmployeeCosts().get(cost);
					for(int i = 0; i < currentCosts.size(); ++i) {
						for(int j = 0; j < newCosts.size(); ++j) {
							if(currentCosts.get(i).getEmployeeType().equals(newCosts.get(j).getEmployeeType())) {
								currentCosts.get(i).setCostPerEmployee(currentCosts.get(i).getCostPerEmployee().add(newCosts.get(j).getCostPerEmployee()));
								break;
							}
						}
					}
					this.simdaseAnnex.getEmployeeCosts().put(cost, currentCosts);
				} else if(type.equals("minimum")) {
					List<CostRow.EmployeeCost> currentCosts = this.simdaseAnnex.getEmployeeCosts().get(cost);
					List<CostRow.EmployeeCost> newCosts = simdaseAnnex.getEmployeeCosts().get(cost);
					for(int i = 0; i < currentCosts.size(); ++i) {
						for(int j = 0; j < newCosts.size(); ++j) {
							if(currentCosts.get(i).getEmployeeType().equals(newCosts.get(j).getEmployeeType())) {
								if(currentCosts.get(i).getCostPerEmployee().compareTo(newCosts.get(j).getCostPerEmployee()) > 0) {
									currentCosts.get(i).setCostPerEmployee(newCosts.get(j).getCostPerEmployee());
								}
								break;
							}
						}
					}
					this.simdaseAnnex.getEmployeeCosts().put(cost, currentCosts);
				} else if(type.equals("maximum")) {
					List<CostRow.EmployeeCost> currentCosts = this.simdaseAnnex.getEmployeeCosts().get(cost);
					List<CostRow.EmployeeCost> newCosts = simdaseAnnex.getEmployeeCosts().get(cost);
					for(int i = 0; i < currentCosts.size(); ++i) {
						for(int j = 0; j < newCosts.size(); ++j) {
							if(currentCosts.get(i).getEmployeeType().equals(newCosts.get(j).getEmployeeType())) {
								if(currentCosts.get(i).getCostPerEmployee().compareTo(newCosts.get(j).getCostPerEmployee()) < 0) {
									currentCosts.get(i).setCostPerEmployee(newCosts.get(j).getCostPerEmployee());
								}
								break;
							}
						}
					}
					this.simdaseAnnex.getEmployeeCosts().put(cost, currentCosts);
				}
			}
		}
	}
	
	public List<Node> getChildren() {
		if(children == null) {
			this.children = new ArrayList<Node>();
		}
		return children;
	}
	
	public void setChildren(List<Node> children) {
		this.children = children;
	}
	
	public List<List<Node>> getVariants() {
		if(variants == null) {
			this.variants = new ArrayList<List<Node>>();
		}
		return variants;
	}
	
	public void setVariants(List<List<Node>> variants) {
		this.variants = variants;
	}
	
	public void mergeCosts(Node childNode) {
		if(this.getSimdaseAnnex() == null) {
			this.setSimdaseAnnex(childNode.getSimdaseAnnex(), null);
		} else {
			if(childNode.getSimdaseAnnex() != null) {
				for(String key : childNode.getSimdaseAnnex().getEmployeeCosts().keySet()) {
					if(this.getSimdaseAnnex().getEmployeeCosts().containsKey(key)) {
						this.getSimdaseAnnex().getEmployeeCosts().get(key).addAll(childNode.getSimdaseAnnex().getEmployeeCosts().get(key));
					} else {
						this.getSimdaseAnnex().getEmployeeCosts().put(key, childNode.getSimdaseAnnex().getEmployeeCosts().get(key));
					}
				}
			}
		}
	}
}
