/**
 */
package edu.clemson.simdase.simdase.impl;

import edu.clemson.simdase.simdase.SimdasePackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Number</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.clemson.simdase.simdase.impl.NumberImpl#getNegative <em>Negative</em>}</li>
 *   <li>{@link edu.clemson.simdase.simdase.impl.NumberImpl#getFloatValue <em>Float Value</em>}</li>
 * </ul>
 *
 * @generated
 */
public class NumberImpl extends MinimalEObjectImpl.Container implements edu.clemson.simdase.simdase.Number
{
  /**
   * The default value of the '{@link #getNegative() <em>Negative</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNegative()
   * @generated
   * @ordered
   */
  protected static final String NEGATIVE_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getNegative() <em>Negative</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNegative()
   * @generated
   * @ordered
   */
  protected String negative = NEGATIVE_EDEFAULT;

  /**
   * The default value of the '{@link #getFloatValue() <em>Float Value</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFloatValue()
   * @generated
   * @ordered
   */
  protected static final String FLOAT_VALUE_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getFloatValue() <em>Float Value</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFloatValue()
   * @generated
   * @ordered
   */
  protected String floatValue = FLOAT_VALUE_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected NumberImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return SimdasePackage.Literals.NUMBER;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getNegative()
  {
    return negative;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setNegative(String newNegative)
  {
    String oldNegative = negative;
    negative = newNegative;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SimdasePackage.NUMBER__NEGATIVE, oldNegative, negative));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getFloatValue()
  {
    return floatValue;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setFloatValue(String newFloatValue)
  {
    String oldFloatValue = floatValue;
    floatValue = newFloatValue;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SimdasePackage.NUMBER__FLOAT_VALUE, oldFloatValue, floatValue));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case SimdasePackage.NUMBER__NEGATIVE:
        return getNegative();
      case SimdasePackage.NUMBER__FLOAT_VALUE:
        return getFloatValue();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case SimdasePackage.NUMBER__NEGATIVE:
        setNegative((String)newValue);
        return;
      case SimdasePackage.NUMBER__FLOAT_VALUE:
        setFloatValue((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case SimdasePackage.NUMBER__NEGATIVE:
        setNegative(NEGATIVE_EDEFAULT);
        return;
      case SimdasePackage.NUMBER__FLOAT_VALUE:
        setFloatValue(FLOAT_VALUE_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case SimdasePackage.NUMBER__NEGATIVE:
        return NEGATIVE_EDEFAULT == null ? negative != null : !NEGATIVE_EDEFAULT.equals(negative);
      case SimdasePackage.NUMBER__FLOAT_VALUE:
        return FLOAT_VALUE_EDEFAULT == null ? floatValue != null : !FLOAT_VALUE_EDEFAULT.equals(floatValue);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (negative: ");
    result.append(negative);
    result.append(", floatValue: ");
    result.append(floatValue);
    result.append(')');
    return result.toString();
  }

} //NumberImpl
