/**
 */
package edu.clemson.simdase.simdase;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.osate.aadl2.Aadl2Package;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see edu.clemson.simdase.simdase.SimdaseFactory
 * @model kind="package"
 * @generated
 */
public interface SimdasePackage extends EPackage
{
  /**
   * The package name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNAME = "simdase";

  /**
   * The package namespace URI.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_URI = "http://www.ethantmcgee.com/SIMDASE";

  /**
   * The package namespace name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_PREFIX = "simdase";

  /**
   * The singleton instance of the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  SimdasePackage eINSTANCE = edu.clemson.simdase.simdase.impl.SimdasePackageImpl.init();

  /**
   * The meta object id for the '{@link edu.clemson.simdase.simdase.impl.SimdaseLibraryImpl <em>Library</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.simdase.simdase.impl.SimdaseLibraryImpl
   * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getSimdaseLibrary()
   * @generated
   */
  int SIMDASE_LIBRARY = 0;

  /**
   * The feature id for the '<em><b>Owned Element</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMDASE_LIBRARY__OWNED_ELEMENT = Aadl2Package.ANNEX_LIBRARY__OWNED_ELEMENT;

  /**
   * The feature id for the '<em><b>Owned Comment</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMDASE_LIBRARY__OWNED_COMMENT = Aadl2Package.ANNEX_LIBRARY__OWNED_COMMENT;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMDASE_LIBRARY__NAME = Aadl2Package.ANNEX_LIBRARY__NAME;

  /**
   * The feature id for the '<em><b>Qualified Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMDASE_LIBRARY__QUALIFIED_NAME = Aadl2Package.ANNEX_LIBRARY__QUALIFIED_NAME;

  /**
   * The feature id for the '<em><b>Owned Property Association</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMDASE_LIBRARY__OWNED_PROPERTY_ASSOCIATION = Aadl2Package.ANNEX_LIBRARY__OWNED_PROPERTY_ASSOCIATION;

  /**
   * The number of structural features of the '<em>Library</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMDASE_LIBRARY_FEATURE_COUNT = Aadl2Package.ANNEX_LIBRARY_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link edu.clemson.simdase.simdase.impl.SimdaseSubclauseImpl <em>Subclause</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.simdase.simdase.impl.SimdaseSubclauseImpl
   * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getSimdaseSubclause()
   * @generated
   */
  int SIMDASE_SUBCLAUSE = 1;

  /**
   * The feature id for the '<em><b>Owned Element</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMDASE_SUBCLAUSE__OWNED_ELEMENT = Aadl2Package.ANNEX_SUBCLAUSE__OWNED_ELEMENT;

  /**
   * The feature id for the '<em><b>Owned Comment</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMDASE_SUBCLAUSE__OWNED_COMMENT = Aadl2Package.ANNEX_SUBCLAUSE__OWNED_COMMENT;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMDASE_SUBCLAUSE__NAME = Aadl2Package.ANNEX_SUBCLAUSE__NAME;

  /**
   * The feature id for the '<em><b>Qualified Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMDASE_SUBCLAUSE__QUALIFIED_NAME = Aadl2Package.ANNEX_SUBCLAUSE__QUALIFIED_NAME;

  /**
   * The feature id for the '<em><b>Owned Property Association</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMDASE_SUBCLAUSE__OWNED_PROPERTY_ASSOCIATION = Aadl2Package.ANNEX_SUBCLAUSE__OWNED_PROPERTY_ASSOCIATION;

  /**
   * The feature id for the '<em><b>In Mode</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMDASE_SUBCLAUSE__IN_MODE = Aadl2Package.ANNEX_SUBCLAUSE__IN_MODE;

  /**
   * The number of structural features of the '<em>Subclause</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMDASE_SUBCLAUSE_FEATURE_COUNT = Aadl2Package.ANNEX_SUBCLAUSE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link edu.clemson.simdase.simdase.impl.ContractImpl <em>Contract</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.simdase.simdase.impl.ContractImpl
   * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getContract()
   * @generated
   */
  int CONTRACT = 2;

  /**
   * The number of structural features of the '<em>Contract</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONTRACT_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link edu.clemson.simdase.simdase.impl.SIMDASEStatementImpl <em>SIMDASE Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.simdase.simdase.impl.SIMDASEStatementImpl
   * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getSIMDASEStatement()
   * @generated
   */
  int SIMDASE_STATEMENT = 3;

  /**
   * The number of structural features of the '<em>SIMDASE Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMDASE_STATEMENT_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link edu.clemson.simdase.simdase.impl.VariantsImpl <em>Variants</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.simdase.simdase.impl.VariantsImpl
   * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getVariants()
   * @generated
   */
  int VARIANTS = 4;

  /**
   * The feature id for the '<em><b>Variants</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VARIANTS__VARIANTS = SIMDASE_STATEMENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>First</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VARIANTS__FIRST = SIMDASE_STATEMENT_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Rest</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VARIANTS__REST = SIMDASE_STATEMENT_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Variants</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VARIANTS_FEATURE_COUNT = SIMDASE_STATEMENT_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link edu.clemson.simdase.simdase.impl.VariantImpl <em>Variant</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.simdase.simdase.impl.VariantImpl
   * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getVariant()
   * @generated
   */
  int VARIANT = 5;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VARIANT__NAME = 0;

  /**
   * The feature id for the '<em><b>Variants</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VARIANT__VARIANTS = 1;

  /**
   * The number of structural features of the '<em>Variant</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VARIANT_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link edu.clemson.simdase.simdase.impl.ActiveVariantsImpl <em>Active Variants</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.simdase.simdase.impl.ActiveVariantsImpl
   * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getActiveVariants()
   * @generated
   */
  int ACTIVE_VARIANTS = 6;

  /**
   * The feature id for the '<em><b>Variants</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACTIVE_VARIANTS__VARIANTS = SIMDASE_STATEMENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>First</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACTIVE_VARIANTS__FIRST = SIMDASE_STATEMENT_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Rest</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACTIVE_VARIANTS__REST = SIMDASE_STATEMENT_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Active Variants</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACTIVE_VARIANTS_FEATURE_COUNT = SIMDASE_STATEMENT_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link edu.clemson.simdase.simdase.impl.ActiveVariantImpl <em>Active Variant</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.simdase.simdase.impl.ActiveVariantImpl
   * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getActiveVariant()
   * @generated
   */
  int ACTIVE_VARIANT = 7;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACTIVE_VARIANT__NAME = 0;

  /**
   * The feature id for the '<em><b>Variants</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACTIVE_VARIANT__VARIANTS = 1;

  /**
   * The number of structural features of the '<em>Active Variant</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACTIVE_VARIANT_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link edu.clemson.simdase.simdase.impl.VariantNamesImpl <em>Variant Names</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.simdase.simdase.impl.VariantNamesImpl
   * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getVariantNames()
   * @generated
   */
  int VARIANT_NAMES = 8;

  /**
   * The feature id for the '<em><b>First</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VARIANT_NAMES__FIRST = 0;

  /**
   * The feature id for the '<em><b>Rest</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VARIANT_NAMES__REST = 1;

  /**
   * The number of structural features of the '<em>Variant Names</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VARIANT_NAMES_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link edu.clemson.simdase.simdase.impl.CostPropertiesImpl <em>Cost Properties</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.simdase.simdase.impl.CostPropertiesImpl
   * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getCostProperties()
   * @generated
   */
  int COST_PROPERTIES = 9;

  /**
   * The feature id for the '<em><b>Cost Statements</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COST_PROPERTIES__COST_STATEMENTS = SIMDASE_STATEMENT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Cost Properties</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COST_PROPERTIES_FEATURE_COUNT = SIMDASE_STATEMENT_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link edu.clemson.simdase.simdase.impl.CostStatementImpl <em>Cost Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.simdase.simdase.impl.CostStatementImpl
   * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getCostStatement()
   * @generated
   */
  int COST_STATEMENT = 10;

  /**
   * The feature id for the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COST_STATEMENT__TYPE = 0;

  /**
   * The feature id for the '<em><b>Value</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COST_STATEMENT__VALUE = 1;

  /**
   * The number of structural features of the '<em>Cost Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COST_STATEMENT_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link edu.clemson.simdase.simdase.impl.CostValueImpl <em>Cost Value</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.simdase.simdase.impl.CostValueImpl
   * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getCostValue()
   * @generated
   */
  int COST_VALUE = 11;

  /**
   * The feature id for the '<em><b>First</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COST_VALUE__FIRST = 0;

  /**
   * The feature id for the '<em><b>Rest</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COST_VALUE__REST = 1;

  /**
   * The number of structural features of the '<em>Cost Value</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COST_VALUE_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link edu.clemson.simdase.simdase.impl.EmployeeStatementImpl <em>Employee Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.simdase.simdase.impl.EmployeeStatementImpl
   * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getEmployeeStatement()
   * @generated
   */
  int EMPLOYEE_STATEMENT = 12;

  /**
   * The number of structural features of the '<em>Employee Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EMPLOYEE_STATEMENT_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link edu.clemson.simdase.simdase.impl.BooleanImpl <em>Boolean</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.simdase.simdase.impl.BooleanImpl
   * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getBoolean()
   * @generated
   */
  int BOOLEAN = 13;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN__VALUE = 0;

  /**
   * The number of structural features of the '<em>Boolean</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link edu.clemson.simdase.simdase.impl.NumberImpl <em>Number</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.simdase.simdase.impl.NumberImpl
   * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getNumber()
   * @generated
   */
  int NUMBER = 14;

  /**
   * The feature id for the '<em><b>Negative</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NUMBER__NEGATIVE = 0;

  /**
   * The feature id for the '<em><b>Float Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NUMBER__FLOAT_VALUE = 1;

  /**
   * The number of structural features of the '<em>Number</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NUMBER_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link edu.clemson.simdase.simdase.impl.StatementImpl <em>Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.simdase.simdase.impl.StatementImpl
   * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getStatement()
   * @generated
   */
  int STATEMENT = 15;

  /**
   * The feature id for the '<em><b>Statement</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STATEMENT__STATEMENT = 0;

  /**
   * The feature id for the '<em><b>Op</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STATEMENT__OP = 1;

  /**
   * The feature id for the '<em><b>Rest</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STATEMENT__REST = 2;

  /**
   * The number of structural features of the '<em>Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STATEMENT_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link edu.clemson.simdase.simdase.impl.StatementLvl2Impl <em>Statement Lvl2</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.simdase.simdase.impl.StatementLvl2Impl
   * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getStatementLvl2()
   * @generated
   */
  int STATEMENT_LVL2 = 16;

  /**
   * The feature id for the '<em><b>Statement</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STATEMENT_LVL2__STATEMENT = 0;

  /**
   * The feature id for the '<em><b>Op</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STATEMENT_LVL2__OP = 1;

  /**
   * The feature id for the '<em><b>Rest</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STATEMENT_LVL2__REST = 2;

  /**
   * The number of structural features of the '<em>Statement Lvl2</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STATEMENT_LVL2_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link edu.clemson.simdase.simdase.impl.StatementLvl3Impl <em>Statement Lvl3</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.simdase.simdase.impl.StatementLvl3Impl
   * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getStatementLvl3()
   * @generated
   */
  int STATEMENT_LVL3 = 17;

  /**
   * The feature id for the '<em><b>Statement</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STATEMENT_LVL3__STATEMENT = 0;

  /**
   * The feature id for the '<em><b>Op</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STATEMENT_LVL3__OP = 1;

  /**
   * The feature id for the '<em><b>Rest</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STATEMENT_LVL3__REST = 2;

  /**
   * The number of structural features of the '<em>Statement Lvl3</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STATEMENT_LVL3_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link edu.clemson.simdase.simdase.impl.StatementLvl4Impl <em>Statement Lvl4</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.simdase.simdase.impl.StatementLvl4Impl
   * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getStatementLvl4()
   * @generated
   */
  int STATEMENT_LVL4 = 18;

  /**
   * The number of structural features of the '<em>Statement Lvl4</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STATEMENT_LVL4_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link edu.clemson.simdase.simdase.impl.BooleanStatementImpl <em>Boolean Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.simdase.simdase.impl.BooleanStatementImpl
   * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getBooleanStatement()
   * @generated
   */
  int BOOLEAN_STATEMENT = 19;

  /**
   * The feature id for the '<em><b>Statement</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_STATEMENT__STATEMENT = 0;

  /**
   * The feature id for the '<em><b>Op</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_STATEMENT__OP = 1;

  /**
   * The feature id for the '<em><b>Rest</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_STATEMENT__REST = 2;

  /**
   * The number of structural features of the '<em>Boolean Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_STATEMENT_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link edu.clemson.simdase.simdase.impl.BooleanStatementLvl2Impl <em>Boolean Statement Lvl2</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.simdase.simdase.impl.BooleanStatementLvl2Impl
   * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getBooleanStatementLvl2()
   * @generated
   */
  int BOOLEAN_STATEMENT_LVL2 = 20;

  /**
   * The feature id for the '<em><b>Statement</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_STATEMENT_LVL2__STATEMENT = 0;

  /**
   * The feature id for the '<em><b>Op</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_STATEMENT_LVL2__OP = 1;

  /**
   * The feature id for the '<em><b>Rest</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_STATEMENT_LVL2__REST = 2;

  /**
   * The number of structural features of the '<em>Boolean Statement Lvl2</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_STATEMENT_LVL2_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link edu.clemson.simdase.simdase.impl.BooleanStatementLvl3Impl <em>Boolean Statement Lvl3</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.simdase.simdase.impl.BooleanStatementLvl3Impl
   * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getBooleanStatementLvl3()
   * @generated
   */
  int BOOLEAN_STATEMENT_LVL3 = 21;

  /**
   * The feature id for the '<em><b>Statement</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_STATEMENT_LVL3__STATEMENT = 0;

  /**
   * The feature id for the '<em><b>Op</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_STATEMENT_LVL3__OP = 1;

  /**
   * The feature id for the '<em><b>Rest</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_STATEMENT_LVL3__REST = 2;

  /**
   * The number of structural features of the '<em>Boolean Statement Lvl3</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_STATEMENT_LVL3_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link edu.clemson.simdase.simdase.impl.BooleanStatementLvl4Impl <em>Boolean Statement Lvl4</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.simdase.simdase.impl.BooleanStatementLvl4Impl
   * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getBooleanStatementLvl4()
   * @generated
   */
  int BOOLEAN_STATEMENT_LVL4 = 22;

  /**
   * The number of structural features of the '<em>Boolean Statement Lvl4</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_STATEMENT_LVL4_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link edu.clemson.simdase.simdase.impl.BooleanStatementLvl5Impl <em>Boolean Statement Lvl5</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.simdase.simdase.impl.BooleanStatementLvl5Impl
   * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getBooleanStatementLvl5()
   * @generated
   */
  int BOOLEAN_STATEMENT_LVL5 = 23;

  /**
   * The feature id for the '<em><b>Statement</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_STATEMENT_LVL5__STATEMENT = 0;

  /**
   * The feature id for the '<em><b>Op</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_STATEMENT_LVL5__OP = 1;

  /**
   * The feature id for the '<em><b>Rest</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_STATEMENT_LVL5__REST = 2;

  /**
   * The number of structural features of the '<em>Boolean Statement Lvl5</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_STATEMENT_LVL5_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link edu.clemson.simdase.simdase.impl.BooleanStatementLvl6Impl <em>Boolean Statement Lvl6</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.simdase.simdase.impl.BooleanStatementLvl6Impl
   * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getBooleanStatementLvl6()
   * @generated
   */
  int BOOLEAN_STATEMENT_LVL6 = 24;

  /**
   * The feature id for the '<em><b>Statement</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_STATEMENT_LVL6__STATEMENT = 0;

  /**
   * The feature id for the '<em><b>Op</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_STATEMENT_LVL6__OP = 1;

  /**
   * The feature id for the '<em><b>Rest</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_STATEMENT_LVL6__REST = 2;

  /**
   * The number of structural features of the '<em>Boolean Statement Lvl6</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_STATEMENT_LVL6_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link edu.clemson.simdase.simdase.impl.BooleanStatementLvl7Impl <em>Boolean Statement Lvl7</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.simdase.simdase.impl.BooleanStatementLvl7Impl
   * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getBooleanStatementLvl7()
   * @generated
   */
  int BOOLEAN_STATEMENT_LVL7 = 25;

  /**
   * The feature id for the '<em><b>Statement</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_STATEMENT_LVL7__STATEMENT = 0;

  /**
   * The feature id for the '<em><b>Op</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_STATEMENT_LVL7__OP = 1;

  /**
   * The feature id for the '<em><b>Rest</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_STATEMENT_LVL7__REST = 2;

  /**
   * The number of structural features of the '<em>Boolean Statement Lvl7</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_STATEMENT_LVL7_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link edu.clemson.simdase.simdase.impl.BooleanStatementLvl8Impl <em>Boolean Statement Lvl8</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.simdase.simdase.impl.BooleanStatementLvl8Impl
   * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getBooleanStatementLvl8()
   * @generated
   */
  int BOOLEAN_STATEMENT_LVL8 = 26;

  /**
   * The number of structural features of the '<em>Boolean Statement Lvl8</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_STATEMENT_LVL8_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link edu.clemson.simdase.simdase.impl.SimdaseContractLibraryImpl <em>Contract Library</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.simdase.simdase.impl.SimdaseContractLibraryImpl
   * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getSimdaseContractLibrary()
   * @generated
   */
  int SIMDASE_CONTRACT_LIBRARY = 27;

  /**
   * The feature id for the '<em><b>Owned Element</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMDASE_CONTRACT_LIBRARY__OWNED_ELEMENT = SIMDASE_LIBRARY__OWNED_ELEMENT;

  /**
   * The feature id for the '<em><b>Owned Comment</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMDASE_CONTRACT_LIBRARY__OWNED_COMMENT = SIMDASE_LIBRARY__OWNED_COMMENT;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMDASE_CONTRACT_LIBRARY__NAME = SIMDASE_LIBRARY__NAME;

  /**
   * The feature id for the '<em><b>Qualified Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMDASE_CONTRACT_LIBRARY__QUALIFIED_NAME = SIMDASE_LIBRARY__QUALIFIED_NAME;

  /**
   * The feature id for the '<em><b>Owned Property Association</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMDASE_CONTRACT_LIBRARY__OWNED_PROPERTY_ASSOCIATION = SIMDASE_LIBRARY__OWNED_PROPERTY_ASSOCIATION;

  /**
   * The feature id for the '<em><b>Contract</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMDASE_CONTRACT_LIBRARY__CONTRACT = SIMDASE_LIBRARY_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Contract Library</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMDASE_CONTRACT_LIBRARY_FEATURE_COUNT = SIMDASE_LIBRARY_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link edu.clemson.simdase.simdase.impl.SimdaseContractSubclauseImpl <em>Contract Subclause</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.simdase.simdase.impl.SimdaseContractSubclauseImpl
   * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getSimdaseContractSubclause()
   * @generated
   */
  int SIMDASE_CONTRACT_SUBCLAUSE = 28;

  /**
   * The feature id for the '<em><b>Owned Element</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMDASE_CONTRACT_SUBCLAUSE__OWNED_ELEMENT = SIMDASE_SUBCLAUSE__OWNED_ELEMENT;

  /**
   * The feature id for the '<em><b>Owned Comment</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMDASE_CONTRACT_SUBCLAUSE__OWNED_COMMENT = SIMDASE_SUBCLAUSE__OWNED_COMMENT;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMDASE_CONTRACT_SUBCLAUSE__NAME = SIMDASE_SUBCLAUSE__NAME;

  /**
   * The feature id for the '<em><b>Qualified Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMDASE_CONTRACT_SUBCLAUSE__QUALIFIED_NAME = SIMDASE_SUBCLAUSE__QUALIFIED_NAME;

  /**
   * The feature id for the '<em><b>Owned Property Association</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMDASE_CONTRACT_SUBCLAUSE__OWNED_PROPERTY_ASSOCIATION = SIMDASE_SUBCLAUSE__OWNED_PROPERTY_ASSOCIATION;

  /**
   * The feature id for the '<em><b>In Mode</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMDASE_CONTRACT_SUBCLAUSE__IN_MODE = SIMDASE_SUBCLAUSE__IN_MODE;

  /**
   * The feature id for the '<em><b>Contract</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMDASE_CONTRACT_SUBCLAUSE__CONTRACT = SIMDASE_SUBCLAUSE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Contract Subclause</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMDASE_CONTRACT_SUBCLAUSE_FEATURE_COUNT = SIMDASE_SUBCLAUSE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link edu.clemson.simdase.simdase.impl.SimdaseContractImpl <em>Contract</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.simdase.simdase.impl.SimdaseContractImpl
   * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getSimdaseContract()
   * @generated
   */
  int SIMDASE_CONTRACT = 29;

  /**
   * The feature id for the '<em><b>Statement</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMDASE_CONTRACT__STATEMENT = CONTRACT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Contract</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMDASE_CONTRACT_FEATURE_COUNT = CONTRACT_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link edu.clemson.simdase.simdase.impl.IsAdaptiveStatementImpl <em>Is Adaptive Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.simdase.simdase.impl.IsAdaptiveStatementImpl
   * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getIsAdaptiveStatement()
   * @generated
   */
  int IS_ADAPTIVE_STATEMENT = 30;

  /**
   * The feature id for the '<em><b>Value</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IS_ADAPTIVE_STATEMENT__VALUE = SIMDASE_STATEMENT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Is Adaptive Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IS_ADAPTIVE_STATEMENT_FEATURE_COUNT = SIMDASE_STATEMENT_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link edu.clemson.simdase.simdase.impl.TMaxImpl <em>TMax</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.simdase.simdase.impl.TMaxImpl
   * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getTMax()
   * @generated
   */
  int TMAX = 31;

  /**
   * The feature id for the '<em><b>Value</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TMAX__VALUE = SIMDASE_STATEMENT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>TMax</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TMAX_FEATURE_COUNT = SIMDASE_STATEMENT_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link edu.clemson.simdase.simdase.impl.TMinImpl <em>TMin</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.simdase.simdase.impl.TMinImpl
   * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getTMin()
   * @generated
   */
  int TMIN = 32;

  /**
   * The feature id for the '<em><b>Value</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TMIN__VALUE = SIMDASE_STATEMENT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>TMin</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TMIN_FEATURE_COUNT = SIMDASE_STATEMENT_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link edu.clemson.simdase.simdase.impl.ScalingFactorImpl <em>Scaling Factor</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.simdase.simdase.impl.ScalingFactorImpl
   * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getScalingFactor()
   * @generated
   */
  int SCALING_FACTOR = 33;

  /**
   * The feature id for the '<em><b>Statement</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SCALING_FACTOR__STATEMENT = SIMDASE_STATEMENT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Scaling Factor</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SCALING_FACTOR_FEATURE_COUNT = SIMDASE_STATEMENT_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link edu.clemson.simdase.simdase.impl.ReEvaluateAveragingImpl <em>Re Evaluate Averaging</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.simdase.simdase.impl.ReEvaluateAveragingImpl
   * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getReEvaluateAveraging()
   * @generated
   */
  int RE_EVALUATE_AVERAGING = 34;

  /**
   * The feature id for the '<em><b>Count</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RE_EVALUATE_AVERAGING__COUNT = SIMDASE_STATEMENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RE_EVALUATE_AVERAGING__TYPE = SIMDASE_STATEMENT_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Re Evaluate Averaging</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RE_EVALUATE_AVERAGING_FEATURE_COUNT = SIMDASE_STATEMENT_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link edu.clemson.simdase.simdase.impl.HourlyCostImpl <em>Hourly Cost</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.simdase.simdase.impl.HourlyCostImpl
   * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getHourlyCost()
   * @generated
   */
  int HOURLY_COST = 35;

  /**
   * The feature id for the '<em><b>Count</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int HOURLY_COST__COUNT = EMPLOYEE_STATEMENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Employee Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int HOURLY_COST__EMPLOYEE_TYPE = EMPLOYEE_STATEMENT_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Cost</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int HOURLY_COST__COST = EMPLOYEE_STATEMENT_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Hourly Cost</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int HOURLY_COST_FEATURE_COUNT = EMPLOYEE_STATEMENT_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link edu.clemson.simdase.simdase.impl.FixedCostImpl <em>Fixed Cost</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.simdase.simdase.impl.FixedCostImpl
   * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getFixedCost()
   * @generated
   */
  int FIXED_COST = 36;

  /**
   * The feature id for the '<em><b>Cost</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FIXED_COST__COST = EMPLOYEE_STATEMENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Cost Reason</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FIXED_COST__COST_REASON = EMPLOYEE_STATEMENT_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Period</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FIXED_COST__PERIOD = EMPLOYEE_STATEMENT_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Fixed Cost</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FIXED_COST_FEATURE_COUNT = EMPLOYEE_STATEMENT_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link edu.clemson.simdase.simdase.impl.IntegerImpl <em>Integer</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.simdase.simdase.impl.IntegerImpl
   * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getInteger()
   * @generated
   */
  int INTEGER = 37;

  /**
   * The feature id for the '<em><b>Value</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER__VALUE = STATEMENT_LVL4_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Integer</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_FEATURE_COUNT = STATEMENT_LVL4_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link edu.clemson.simdase.simdase.impl.TImpl <em>T</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.simdase.simdase.impl.TImpl
   * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getT()
   * @generated
   */
  int T = 38;

  /**
   * The number of structural features of the '<em>T</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int T_FEATURE_COUNT = STATEMENT_LVL4_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link edu.clemson.simdase.simdase.impl.IImpl <em>I</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.simdase.simdase.impl.IImpl
   * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getI()
   * @generated
   */
  int I = 39;

  /**
   * The number of structural features of the '<em>I</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int I_FEATURE_COUNT = STATEMENT_LVL4_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link edu.clemson.simdase.simdase.impl.SinImpl <em>Sin</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.simdase.simdase.impl.SinImpl
   * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getSin()
   * @generated
   */
  int SIN = 40;

  /**
   * The feature id for the '<em><b>Statement</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIN__STATEMENT = STATEMENT_LVL4_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Sin</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIN_FEATURE_COUNT = STATEMENT_LVL4_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link edu.clemson.simdase.simdase.impl.CosImpl <em>Cos</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.simdase.simdase.impl.CosImpl
   * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getCos()
   * @generated
   */
  int COS = 41;

  /**
   * The feature id for the '<em><b>Statement</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COS__STATEMENT = STATEMENT_LVL4_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Cos</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COS_FEATURE_COUNT = STATEMENT_LVL4_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link edu.clemson.simdase.simdase.impl.TanImpl <em>Tan</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.simdase.simdase.impl.TanImpl
   * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getTan()
   * @generated
   */
  int TAN = 42;

  /**
   * The feature id for the '<em><b>Statement</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TAN__STATEMENT = STATEMENT_LVL4_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Tan</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TAN_FEATURE_COUNT = STATEMENT_LVL4_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link edu.clemson.simdase.simdase.impl.ParenImpl <em>Paren</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.simdase.simdase.impl.ParenImpl
   * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getParen()
   * @generated
   */
  int PAREN = 43;

  /**
   * The feature id for the '<em><b>Statement</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PAREN__STATEMENT = STATEMENT_LVL4_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Paren</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PAREN_FEATURE_COUNT = STATEMENT_LVL4_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link edu.clemson.simdase.simdase.impl.IfImpl <em>If</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.simdase.simdase.impl.IfImpl
   * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getIf()
   * @generated
   */
  int IF = 44;

  /**
   * The feature id for the '<em><b>Boolean Statement</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IF__BOOLEAN_STATEMENT = STATEMENT_LVL4_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>True Statement</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IF__TRUE_STATEMENT = STATEMENT_LVL4_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>False Statement</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IF__FALSE_STATEMENT = STATEMENT_LVL4_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>If</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IF_FEATURE_COUNT = STATEMENT_LVL4_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link edu.clemson.simdase.simdase.impl.RandomImpl <em>Random</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.simdase.simdase.impl.RandomImpl
   * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getRandom()
   * @generated
   */
  int RANDOM = 45;

  /**
   * The number of structural features of the '<em>Random</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RANDOM_FEATURE_COUNT = STATEMENT_LVL4_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link edu.clemson.simdase.simdase.impl.StatementScalingFactorImpl <em>Statement Scaling Factor</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.simdase.simdase.impl.StatementScalingFactorImpl
   * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getStatementScalingFactor()
   * @generated
   */
  int STATEMENT_SCALING_FACTOR = 46;

  /**
   * The number of structural features of the '<em>Statement Scaling Factor</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STATEMENT_SCALING_FACTOR_FEATURE_COUNT = STATEMENT_LVL4_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link edu.clemson.simdase.simdase.impl.WithNotImpl <em>With Not</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.simdase.simdase.impl.WithNotImpl
   * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getWithNot()
   * @generated
   */
  int WITH_NOT = 47;

  /**
   * The feature id for the '<em><b>Statement</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int WITH_NOT__STATEMENT = BOOLEAN_STATEMENT_LVL4_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>With Not</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int WITH_NOT_FEATURE_COUNT = BOOLEAN_STATEMENT_LVL4_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link edu.clemson.simdase.simdase.impl.WithoutNotImpl <em>Without Not</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.simdase.simdase.impl.WithoutNotImpl
   * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getWithoutNot()
   * @generated
   */
  int WITHOUT_NOT = 48;

  /**
   * The feature id for the '<em><b>Statement</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int WITHOUT_NOT__STATEMENT = BOOLEAN_STATEMENT_LVL4_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Without Not</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int WITHOUT_NOT_FEATURE_COUNT = BOOLEAN_STATEMENT_LVL4_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link edu.clemson.simdase.simdase.impl.BoolBooleanImpl <em>Bool Boolean</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.simdase.simdase.impl.BoolBooleanImpl
   * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getBoolBoolean()
   * @generated
   */
  int BOOL_BOOLEAN = 49;

  /**
   * The feature id for the '<em><b>Value</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOL_BOOLEAN__VALUE = BOOLEAN_STATEMENT_LVL8_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Bool Boolean</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOL_BOOLEAN_FEATURE_COUNT = BOOLEAN_STATEMENT_LVL8_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link edu.clemson.simdase.simdase.impl.BoolTImpl <em>Bool T</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.simdase.simdase.impl.BoolTImpl
   * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getBoolT()
   * @generated
   */
  int BOOL_T = 50;

  /**
   * The number of structural features of the '<em>Bool T</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOL_T_FEATURE_COUNT = BOOLEAN_STATEMENT_LVL8_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link edu.clemson.simdase.simdase.impl.BoolIImpl <em>Bool I</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.simdase.simdase.impl.BoolIImpl
   * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getBoolI()
   * @generated
   */
  int BOOL_I = 51;

  /**
   * The number of structural features of the '<em>Bool I</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOL_I_FEATURE_COUNT = BOOLEAN_STATEMENT_LVL8_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link edu.clemson.simdase.simdase.impl.BoolIntegerImpl <em>Bool Integer</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.simdase.simdase.impl.BoolIntegerImpl
   * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getBoolInteger()
   * @generated
   */
  int BOOL_INTEGER = 52;

  /**
   * The feature id for the '<em><b>Value</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOL_INTEGER__VALUE = BOOLEAN_STATEMENT_LVL8_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Bool Integer</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOL_INTEGER_FEATURE_COUNT = BOOLEAN_STATEMENT_LVL8_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link edu.clemson.simdase.simdase.impl.BoolSinImpl <em>Bool Sin</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.simdase.simdase.impl.BoolSinImpl
   * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getBoolSin()
   * @generated
   */
  int BOOL_SIN = 53;

  /**
   * The feature id for the '<em><b>Statement</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOL_SIN__STATEMENT = BOOLEAN_STATEMENT_LVL8_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Bool Sin</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOL_SIN_FEATURE_COUNT = BOOLEAN_STATEMENT_LVL8_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link edu.clemson.simdase.simdase.impl.BoolCosImpl <em>Bool Cos</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.simdase.simdase.impl.BoolCosImpl
   * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getBoolCos()
   * @generated
   */
  int BOOL_COS = 54;

  /**
   * The feature id for the '<em><b>Statement</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOL_COS__STATEMENT = BOOLEAN_STATEMENT_LVL8_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Bool Cos</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOL_COS_FEATURE_COUNT = BOOLEAN_STATEMENT_LVL8_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link edu.clemson.simdase.simdase.impl.BoolTanImpl <em>Bool Tan</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.simdase.simdase.impl.BoolTanImpl
   * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getBoolTan()
   * @generated
   */
  int BOOL_TAN = 55;

  /**
   * The feature id for the '<em><b>Statement</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOL_TAN__STATEMENT = BOOLEAN_STATEMENT_LVL8_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Bool Tan</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOL_TAN_FEATURE_COUNT = BOOLEAN_STATEMENT_LVL8_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link edu.clemson.simdase.simdase.impl.BoolParenImpl <em>Bool Paren</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.simdase.simdase.impl.BoolParenImpl
   * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getBoolParen()
   * @generated
   */
  int BOOL_PAREN = 56;

  /**
   * The feature id for the '<em><b>Statement</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOL_PAREN__STATEMENT = BOOLEAN_STATEMENT_LVL8_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Bool Paren</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOL_PAREN_FEATURE_COUNT = BOOLEAN_STATEMENT_LVL8_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link edu.clemson.simdase.simdase.impl.BoolRandomImpl <em>Bool Random</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.simdase.simdase.impl.BoolRandomImpl
   * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getBoolRandom()
   * @generated
   */
  int BOOL_RANDOM = 57;

  /**
   * The number of structural features of the '<em>Bool Random</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOL_RANDOM_FEATURE_COUNT = BOOLEAN_STATEMENT_LVL8_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link edu.clemson.simdase.simdase.impl.BoolScalingFactorImpl <em>Bool Scaling Factor</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.simdase.simdase.impl.BoolScalingFactorImpl
   * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getBoolScalingFactor()
   * @generated
   */
  int BOOL_SCALING_FACTOR = 58;

  /**
   * The number of structural features of the '<em>Bool Scaling Factor</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOL_SCALING_FACTOR_FEATURE_COUNT = BOOLEAN_STATEMENT_LVL8_FEATURE_COUNT + 0;


  /**
   * Returns the meta object for class '{@link edu.clemson.simdase.simdase.SimdaseLibrary <em>Library</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Library</em>'.
   * @see edu.clemson.simdase.simdase.SimdaseLibrary
   * @generated
   */
  EClass getSimdaseLibrary();

  /**
   * Returns the meta object for class '{@link edu.clemson.simdase.simdase.SimdaseSubclause <em>Subclause</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Subclause</em>'.
   * @see edu.clemson.simdase.simdase.SimdaseSubclause
   * @generated
   */
  EClass getSimdaseSubclause();

  /**
   * Returns the meta object for class '{@link edu.clemson.simdase.simdase.Contract <em>Contract</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Contract</em>'.
   * @see edu.clemson.simdase.simdase.Contract
   * @generated
   */
  EClass getContract();

  /**
   * Returns the meta object for class '{@link edu.clemson.simdase.simdase.SIMDASEStatement <em>SIMDASE Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>SIMDASE Statement</em>'.
   * @see edu.clemson.simdase.simdase.SIMDASEStatement
   * @generated
   */
  EClass getSIMDASEStatement();

  /**
   * Returns the meta object for class '{@link edu.clemson.simdase.simdase.Variants <em>Variants</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Variants</em>'.
   * @see edu.clemson.simdase.simdase.Variants
   * @generated
   */
  EClass getVariants();

  /**
   * Returns the meta object for the containment reference '{@link edu.clemson.simdase.simdase.Variants#getVariants <em>Variants</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Variants</em>'.
   * @see edu.clemson.simdase.simdase.Variants#getVariants()
   * @see #getVariants()
   * @generated
   */
  EReference getVariants_Variants();

  /**
   * Returns the meta object for the containment reference '{@link edu.clemson.simdase.simdase.Variants#getFirst <em>First</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>First</em>'.
   * @see edu.clemson.simdase.simdase.Variants#getFirst()
   * @see #getVariants()
   * @generated
   */
  EReference getVariants_First();

  /**
   * Returns the meta object for the containment reference list '{@link edu.clemson.simdase.simdase.Variants#getRest <em>Rest</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Rest</em>'.
   * @see edu.clemson.simdase.simdase.Variants#getRest()
   * @see #getVariants()
   * @generated
   */
  EReference getVariants_Rest();

  /**
   * Returns the meta object for class '{@link edu.clemson.simdase.simdase.Variant <em>Variant</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Variant</em>'.
   * @see edu.clemson.simdase.simdase.Variant
   * @generated
   */
  EClass getVariant();

  /**
   * Returns the meta object for the attribute '{@link edu.clemson.simdase.simdase.Variant#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see edu.clemson.simdase.simdase.Variant#getName()
   * @see #getVariant()
   * @generated
   */
  EAttribute getVariant_Name();

  /**
   * Returns the meta object for the containment reference '{@link edu.clemson.simdase.simdase.Variant#getVariants <em>Variants</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Variants</em>'.
   * @see edu.clemson.simdase.simdase.Variant#getVariants()
   * @see #getVariant()
   * @generated
   */
  EReference getVariant_Variants();

  /**
   * Returns the meta object for class '{@link edu.clemson.simdase.simdase.ActiveVariants <em>Active Variants</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Active Variants</em>'.
   * @see edu.clemson.simdase.simdase.ActiveVariants
   * @generated
   */
  EClass getActiveVariants();

  /**
   * Returns the meta object for the containment reference '{@link edu.clemson.simdase.simdase.ActiveVariants#getVariants <em>Variants</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Variants</em>'.
   * @see edu.clemson.simdase.simdase.ActiveVariants#getVariants()
   * @see #getActiveVariants()
   * @generated
   */
  EReference getActiveVariants_Variants();

  /**
   * Returns the meta object for the containment reference '{@link edu.clemson.simdase.simdase.ActiveVariants#getFirst <em>First</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>First</em>'.
   * @see edu.clemson.simdase.simdase.ActiveVariants#getFirst()
   * @see #getActiveVariants()
   * @generated
   */
  EReference getActiveVariants_First();

  /**
   * Returns the meta object for the containment reference list '{@link edu.clemson.simdase.simdase.ActiveVariants#getRest <em>Rest</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Rest</em>'.
   * @see edu.clemson.simdase.simdase.ActiveVariants#getRest()
   * @see #getActiveVariants()
   * @generated
   */
  EReference getActiveVariants_Rest();

  /**
   * Returns the meta object for class '{@link edu.clemson.simdase.simdase.ActiveVariant <em>Active Variant</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Active Variant</em>'.
   * @see edu.clemson.simdase.simdase.ActiveVariant
   * @generated
   */
  EClass getActiveVariant();

  /**
   * Returns the meta object for the attribute '{@link edu.clemson.simdase.simdase.ActiveVariant#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see edu.clemson.simdase.simdase.ActiveVariant#getName()
   * @see #getActiveVariant()
   * @generated
   */
  EAttribute getActiveVariant_Name();

  /**
   * Returns the meta object for the containment reference '{@link edu.clemson.simdase.simdase.ActiveVariant#getVariants <em>Variants</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Variants</em>'.
   * @see edu.clemson.simdase.simdase.ActiveVariant#getVariants()
   * @see #getActiveVariant()
   * @generated
   */
  EReference getActiveVariant_Variants();

  /**
   * Returns the meta object for class '{@link edu.clemson.simdase.simdase.VariantNames <em>Variant Names</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Variant Names</em>'.
   * @see edu.clemson.simdase.simdase.VariantNames
   * @generated
   */
  EClass getVariantNames();

  /**
   * Returns the meta object for the attribute '{@link edu.clemson.simdase.simdase.VariantNames#getFirst <em>First</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>First</em>'.
   * @see edu.clemson.simdase.simdase.VariantNames#getFirst()
   * @see #getVariantNames()
   * @generated
   */
  EAttribute getVariantNames_First();

  /**
   * Returns the meta object for the attribute list '{@link edu.clemson.simdase.simdase.VariantNames#getRest <em>Rest</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Rest</em>'.
   * @see edu.clemson.simdase.simdase.VariantNames#getRest()
   * @see #getVariantNames()
   * @generated
   */
  EAttribute getVariantNames_Rest();

  /**
   * Returns the meta object for class '{@link edu.clemson.simdase.simdase.CostProperties <em>Cost Properties</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Cost Properties</em>'.
   * @see edu.clemson.simdase.simdase.CostProperties
   * @generated
   */
  EClass getCostProperties();

  /**
   * Returns the meta object for the containment reference list '{@link edu.clemson.simdase.simdase.CostProperties#getCostStatements <em>Cost Statements</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Cost Statements</em>'.
   * @see edu.clemson.simdase.simdase.CostProperties#getCostStatements()
   * @see #getCostProperties()
   * @generated
   */
  EReference getCostProperties_CostStatements();

  /**
   * Returns the meta object for class '{@link edu.clemson.simdase.simdase.CostStatement <em>Cost Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Cost Statement</em>'.
   * @see edu.clemson.simdase.simdase.CostStatement
   * @generated
   */
  EClass getCostStatement();

  /**
   * Returns the meta object for the attribute '{@link edu.clemson.simdase.simdase.CostStatement#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Type</em>'.
   * @see edu.clemson.simdase.simdase.CostStatement#getType()
   * @see #getCostStatement()
   * @generated
   */
  EAttribute getCostStatement_Type();

  /**
   * Returns the meta object for the containment reference '{@link edu.clemson.simdase.simdase.CostStatement#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Value</em>'.
   * @see edu.clemson.simdase.simdase.CostStatement#getValue()
   * @see #getCostStatement()
   * @generated
   */
  EReference getCostStatement_Value();

  /**
   * Returns the meta object for class '{@link edu.clemson.simdase.simdase.CostValue <em>Cost Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Cost Value</em>'.
   * @see edu.clemson.simdase.simdase.CostValue
   * @generated
   */
  EClass getCostValue();

  /**
   * Returns the meta object for the containment reference '{@link edu.clemson.simdase.simdase.CostValue#getFirst <em>First</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>First</em>'.
   * @see edu.clemson.simdase.simdase.CostValue#getFirst()
   * @see #getCostValue()
   * @generated
   */
  EReference getCostValue_First();

  /**
   * Returns the meta object for the containment reference list '{@link edu.clemson.simdase.simdase.CostValue#getRest <em>Rest</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Rest</em>'.
   * @see edu.clemson.simdase.simdase.CostValue#getRest()
   * @see #getCostValue()
   * @generated
   */
  EReference getCostValue_Rest();

  /**
   * Returns the meta object for class '{@link edu.clemson.simdase.simdase.EmployeeStatement <em>Employee Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Employee Statement</em>'.
   * @see edu.clemson.simdase.simdase.EmployeeStatement
   * @generated
   */
  EClass getEmployeeStatement();

  /**
   * Returns the meta object for class '{@link edu.clemson.simdase.simdase.Boolean <em>Boolean</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Boolean</em>'.
   * @see edu.clemson.simdase.simdase.Boolean
   * @generated
   */
  EClass getBoolean();

  /**
   * Returns the meta object for the attribute '{@link edu.clemson.simdase.simdase.Boolean#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see edu.clemson.simdase.simdase.Boolean#getValue()
   * @see #getBoolean()
   * @generated
   */
  EAttribute getBoolean_Value();

  /**
   * Returns the meta object for class '{@link edu.clemson.simdase.simdase.Number <em>Number</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Number</em>'.
   * @see edu.clemson.simdase.simdase.Number
   * @generated
   */
  EClass getNumber();

  /**
   * Returns the meta object for the attribute '{@link edu.clemson.simdase.simdase.Number#getNegative <em>Negative</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Negative</em>'.
   * @see edu.clemson.simdase.simdase.Number#getNegative()
   * @see #getNumber()
   * @generated
   */
  EAttribute getNumber_Negative();

  /**
   * Returns the meta object for the attribute '{@link edu.clemson.simdase.simdase.Number#getFloatValue <em>Float Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Float Value</em>'.
   * @see edu.clemson.simdase.simdase.Number#getFloatValue()
   * @see #getNumber()
   * @generated
   */
  EAttribute getNumber_FloatValue();

  /**
   * Returns the meta object for class '{@link edu.clemson.simdase.simdase.Statement <em>Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Statement</em>'.
   * @see edu.clemson.simdase.simdase.Statement
   * @generated
   */
  EClass getStatement();

  /**
   * Returns the meta object for the containment reference '{@link edu.clemson.simdase.simdase.Statement#getStatement <em>Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Statement</em>'.
   * @see edu.clemson.simdase.simdase.Statement#getStatement()
   * @see #getStatement()
   * @generated
   */
  EReference getStatement_Statement();

  /**
   * Returns the meta object for the attribute list '{@link edu.clemson.simdase.simdase.Statement#getOp <em>Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Op</em>'.
   * @see edu.clemson.simdase.simdase.Statement#getOp()
   * @see #getStatement()
   * @generated
   */
  EAttribute getStatement_Op();

  /**
   * Returns the meta object for the containment reference list '{@link edu.clemson.simdase.simdase.Statement#getRest <em>Rest</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Rest</em>'.
   * @see edu.clemson.simdase.simdase.Statement#getRest()
   * @see #getStatement()
   * @generated
   */
  EReference getStatement_Rest();

  /**
   * Returns the meta object for class '{@link edu.clemson.simdase.simdase.StatementLvl2 <em>Statement Lvl2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Statement Lvl2</em>'.
   * @see edu.clemson.simdase.simdase.StatementLvl2
   * @generated
   */
  EClass getStatementLvl2();

  /**
   * Returns the meta object for the containment reference '{@link edu.clemson.simdase.simdase.StatementLvl2#getStatement <em>Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Statement</em>'.
   * @see edu.clemson.simdase.simdase.StatementLvl2#getStatement()
   * @see #getStatementLvl2()
   * @generated
   */
  EReference getStatementLvl2_Statement();

  /**
   * Returns the meta object for the attribute list '{@link edu.clemson.simdase.simdase.StatementLvl2#getOp <em>Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Op</em>'.
   * @see edu.clemson.simdase.simdase.StatementLvl2#getOp()
   * @see #getStatementLvl2()
   * @generated
   */
  EAttribute getStatementLvl2_Op();

  /**
   * Returns the meta object for the containment reference list '{@link edu.clemson.simdase.simdase.StatementLvl2#getRest <em>Rest</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Rest</em>'.
   * @see edu.clemson.simdase.simdase.StatementLvl2#getRest()
   * @see #getStatementLvl2()
   * @generated
   */
  EReference getStatementLvl2_Rest();

  /**
   * Returns the meta object for class '{@link edu.clemson.simdase.simdase.StatementLvl3 <em>Statement Lvl3</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Statement Lvl3</em>'.
   * @see edu.clemson.simdase.simdase.StatementLvl3
   * @generated
   */
  EClass getStatementLvl3();

  /**
   * Returns the meta object for the containment reference '{@link edu.clemson.simdase.simdase.StatementLvl3#getStatement <em>Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Statement</em>'.
   * @see edu.clemson.simdase.simdase.StatementLvl3#getStatement()
   * @see #getStatementLvl3()
   * @generated
   */
  EReference getStatementLvl3_Statement();

  /**
   * Returns the meta object for the attribute list '{@link edu.clemson.simdase.simdase.StatementLvl3#getOp <em>Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Op</em>'.
   * @see edu.clemson.simdase.simdase.StatementLvl3#getOp()
   * @see #getStatementLvl3()
   * @generated
   */
  EAttribute getStatementLvl3_Op();

  /**
   * Returns the meta object for the containment reference list '{@link edu.clemson.simdase.simdase.StatementLvl3#getRest <em>Rest</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Rest</em>'.
   * @see edu.clemson.simdase.simdase.StatementLvl3#getRest()
   * @see #getStatementLvl3()
   * @generated
   */
  EReference getStatementLvl3_Rest();

  /**
   * Returns the meta object for class '{@link edu.clemson.simdase.simdase.StatementLvl4 <em>Statement Lvl4</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Statement Lvl4</em>'.
   * @see edu.clemson.simdase.simdase.StatementLvl4
   * @generated
   */
  EClass getStatementLvl4();

  /**
   * Returns the meta object for class '{@link edu.clemson.simdase.simdase.BooleanStatement <em>Boolean Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Boolean Statement</em>'.
   * @see edu.clemson.simdase.simdase.BooleanStatement
   * @generated
   */
  EClass getBooleanStatement();

  /**
   * Returns the meta object for the containment reference '{@link edu.clemson.simdase.simdase.BooleanStatement#getStatement <em>Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Statement</em>'.
   * @see edu.clemson.simdase.simdase.BooleanStatement#getStatement()
   * @see #getBooleanStatement()
   * @generated
   */
  EReference getBooleanStatement_Statement();

  /**
   * Returns the meta object for the attribute list '{@link edu.clemson.simdase.simdase.BooleanStatement#getOp <em>Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Op</em>'.
   * @see edu.clemson.simdase.simdase.BooleanStatement#getOp()
   * @see #getBooleanStatement()
   * @generated
   */
  EAttribute getBooleanStatement_Op();

  /**
   * Returns the meta object for the containment reference list '{@link edu.clemson.simdase.simdase.BooleanStatement#getRest <em>Rest</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Rest</em>'.
   * @see edu.clemson.simdase.simdase.BooleanStatement#getRest()
   * @see #getBooleanStatement()
   * @generated
   */
  EReference getBooleanStatement_Rest();

  /**
   * Returns the meta object for class '{@link edu.clemson.simdase.simdase.BooleanStatementLvl2 <em>Boolean Statement Lvl2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Boolean Statement Lvl2</em>'.
   * @see edu.clemson.simdase.simdase.BooleanStatementLvl2
   * @generated
   */
  EClass getBooleanStatementLvl2();

  /**
   * Returns the meta object for the containment reference '{@link edu.clemson.simdase.simdase.BooleanStatementLvl2#getStatement <em>Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Statement</em>'.
   * @see edu.clemson.simdase.simdase.BooleanStatementLvl2#getStatement()
   * @see #getBooleanStatementLvl2()
   * @generated
   */
  EReference getBooleanStatementLvl2_Statement();

  /**
   * Returns the meta object for the attribute list '{@link edu.clemson.simdase.simdase.BooleanStatementLvl2#getOp <em>Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Op</em>'.
   * @see edu.clemson.simdase.simdase.BooleanStatementLvl2#getOp()
   * @see #getBooleanStatementLvl2()
   * @generated
   */
  EAttribute getBooleanStatementLvl2_Op();

  /**
   * Returns the meta object for the containment reference list '{@link edu.clemson.simdase.simdase.BooleanStatementLvl2#getRest <em>Rest</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Rest</em>'.
   * @see edu.clemson.simdase.simdase.BooleanStatementLvl2#getRest()
   * @see #getBooleanStatementLvl2()
   * @generated
   */
  EReference getBooleanStatementLvl2_Rest();

  /**
   * Returns the meta object for class '{@link edu.clemson.simdase.simdase.BooleanStatementLvl3 <em>Boolean Statement Lvl3</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Boolean Statement Lvl3</em>'.
   * @see edu.clemson.simdase.simdase.BooleanStatementLvl3
   * @generated
   */
  EClass getBooleanStatementLvl3();

  /**
   * Returns the meta object for the containment reference '{@link edu.clemson.simdase.simdase.BooleanStatementLvl3#getStatement <em>Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Statement</em>'.
   * @see edu.clemson.simdase.simdase.BooleanStatementLvl3#getStatement()
   * @see #getBooleanStatementLvl3()
   * @generated
   */
  EReference getBooleanStatementLvl3_Statement();

  /**
   * Returns the meta object for the attribute list '{@link edu.clemson.simdase.simdase.BooleanStatementLvl3#getOp <em>Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Op</em>'.
   * @see edu.clemson.simdase.simdase.BooleanStatementLvl3#getOp()
   * @see #getBooleanStatementLvl3()
   * @generated
   */
  EAttribute getBooleanStatementLvl3_Op();

  /**
   * Returns the meta object for the containment reference list '{@link edu.clemson.simdase.simdase.BooleanStatementLvl3#getRest <em>Rest</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Rest</em>'.
   * @see edu.clemson.simdase.simdase.BooleanStatementLvl3#getRest()
   * @see #getBooleanStatementLvl3()
   * @generated
   */
  EReference getBooleanStatementLvl3_Rest();

  /**
   * Returns the meta object for class '{@link edu.clemson.simdase.simdase.BooleanStatementLvl4 <em>Boolean Statement Lvl4</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Boolean Statement Lvl4</em>'.
   * @see edu.clemson.simdase.simdase.BooleanStatementLvl4
   * @generated
   */
  EClass getBooleanStatementLvl4();

  /**
   * Returns the meta object for class '{@link edu.clemson.simdase.simdase.BooleanStatementLvl5 <em>Boolean Statement Lvl5</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Boolean Statement Lvl5</em>'.
   * @see edu.clemson.simdase.simdase.BooleanStatementLvl5
   * @generated
   */
  EClass getBooleanStatementLvl5();

  /**
   * Returns the meta object for the containment reference '{@link edu.clemson.simdase.simdase.BooleanStatementLvl5#getStatement <em>Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Statement</em>'.
   * @see edu.clemson.simdase.simdase.BooleanStatementLvl5#getStatement()
   * @see #getBooleanStatementLvl5()
   * @generated
   */
  EReference getBooleanStatementLvl5_Statement();

  /**
   * Returns the meta object for the attribute list '{@link edu.clemson.simdase.simdase.BooleanStatementLvl5#getOp <em>Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Op</em>'.
   * @see edu.clemson.simdase.simdase.BooleanStatementLvl5#getOp()
   * @see #getBooleanStatementLvl5()
   * @generated
   */
  EAttribute getBooleanStatementLvl5_Op();

  /**
   * Returns the meta object for the containment reference list '{@link edu.clemson.simdase.simdase.BooleanStatementLvl5#getRest <em>Rest</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Rest</em>'.
   * @see edu.clemson.simdase.simdase.BooleanStatementLvl5#getRest()
   * @see #getBooleanStatementLvl5()
   * @generated
   */
  EReference getBooleanStatementLvl5_Rest();

  /**
   * Returns the meta object for class '{@link edu.clemson.simdase.simdase.BooleanStatementLvl6 <em>Boolean Statement Lvl6</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Boolean Statement Lvl6</em>'.
   * @see edu.clemson.simdase.simdase.BooleanStatementLvl6
   * @generated
   */
  EClass getBooleanStatementLvl6();

  /**
   * Returns the meta object for the containment reference '{@link edu.clemson.simdase.simdase.BooleanStatementLvl6#getStatement <em>Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Statement</em>'.
   * @see edu.clemson.simdase.simdase.BooleanStatementLvl6#getStatement()
   * @see #getBooleanStatementLvl6()
   * @generated
   */
  EReference getBooleanStatementLvl6_Statement();

  /**
   * Returns the meta object for the attribute list '{@link edu.clemson.simdase.simdase.BooleanStatementLvl6#getOp <em>Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Op</em>'.
   * @see edu.clemson.simdase.simdase.BooleanStatementLvl6#getOp()
   * @see #getBooleanStatementLvl6()
   * @generated
   */
  EAttribute getBooleanStatementLvl6_Op();

  /**
   * Returns the meta object for the containment reference list '{@link edu.clemson.simdase.simdase.BooleanStatementLvl6#getRest <em>Rest</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Rest</em>'.
   * @see edu.clemson.simdase.simdase.BooleanStatementLvl6#getRest()
   * @see #getBooleanStatementLvl6()
   * @generated
   */
  EReference getBooleanStatementLvl6_Rest();

  /**
   * Returns the meta object for class '{@link edu.clemson.simdase.simdase.BooleanStatementLvl7 <em>Boolean Statement Lvl7</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Boolean Statement Lvl7</em>'.
   * @see edu.clemson.simdase.simdase.BooleanStatementLvl7
   * @generated
   */
  EClass getBooleanStatementLvl7();

  /**
   * Returns the meta object for the containment reference '{@link edu.clemson.simdase.simdase.BooleanStatementLvl7#getStatement <em>Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Statement</em>'.
   * @see edu.clemson.simdase.simdase.BooleanStatementLvl7#getStatement()
   * @see #getBooleanStatementLvl7()
   * @generated
   */
  EReference getBooleanStatementLvl7_Statement();

  /**
   * Returns the meta object for the attribute list '{@link edu.clemson.simdase.simdase.BooleanStatementLvl7#getOp <em>Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Op</em>'.
   * @see edu.clemson.simdase.simdase.BooleanStatementLvl7#getOp()
   * @see #getBooleanStatementLvl7()
   * @generated
   */
  EAttribute getBooleanStatementLvl7_Op();

  /**
   * Returns the meta object for the containment reference list '{@link edu.clemson.simdase.simdase.BooleanStatementLvl7#getRest <em>Rest</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Rest</em>'.
   * @see edu.clemson.simdase.simdase.BooleanStatementLvl7#getRest()
   * @see #getBooleanStatementLvl7()
   * @generated
   */
  EReference getBooleanStatementLvl7_Rest();

  /**
   * Returns the meta object for class '{@link edu.clemson.simdase.simdase.BooleanStatementLvl8 <em>Boolean Statement Lvl8</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Boolean Statement Lvl8</em>'.
   * @see edu.clemson.simdase.simdase.BooleanStatementLvl8
   * @generated
   */
  EClass getBooleanStatementLvl8();

  /**
   * Returns the meta object for class '{@link edu.clemson.simdase.simdase.SimdaseContractLibrary <em>Contract Library</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Contract Library</em>'.
   * @see edu.clemson.simdase.simdase.SimdaseContractLibrary
   * @generated
   */
  EClass getSimdaseContractLibrary();

  /**
   * Returns the meta object for the containment reference '{@link edu.clemson.simdase.simdase.SimdaseContractLibrary#getContract <em>Contract</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Contract</em>'.
   * @see edu.clemson.simdase.simdase.SimdaseContractLibrary#getContract()
   * @see #getSimdaseContractLibrary()
   * @generated
   */
  EReference getSimdaseContractLibrary_Contract();

  /**
   * Returns the meta object for class '{@link edu.clemson.simdase.simdase.SimdaseContractSubclause <em>Contract Subclause</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Contract Subclause</em>'.
   * @see edu.clemson.simdase.simdase.SimdaseContractSubclause
   * @generated
   */
  EClass getSimdaseContractSubclause();

  /**
   * Returns the meta object for the containment reference '{@link edu.clemson.simdase.simdase.SimdaseContractSubclause#getContract <em>Contract</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Contract</em>'.
   * @see edu.clemson.simdase.simdase.SimdaseContractSubclause#getContract()
   * @see #getSimdaseContractSubclause()
   * @generated
   */
  EReference getSimdaseContractSubclause_Contract();

  /**
   * Returns the meta object for class '{@link edu.clemson.simdase.simdase.SimdaseContract <em>Contract</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Contract</em>'.
   * @see edu.clemson.simdase.simdase.SimdaseContract
   * @generated
   */
  EClass getSimdaseContract();

  /**
   * Returns the meta object for the containment reference list '{@link edu.clemson.simdase.simdase.SimdaseContract#getStatement <em>Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Statement</em>'.
   * @see edu.clemson.simdase.simdase.SimdaseContract#getStatement()
   * @see #getSimdaseContract()
   * @generated
   */
  EReference getSimdaseContract_Statement();

  /**
   * Returns the meta object for class '{@link edu.clemson.simdase.simdase.IsAdaptiveStatement <em>Is Adaptive Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Is Adaptive Statement</em>'.
   * @see edu.clemson.simdase.simdase.IsAdaptiveStatement
   * @generated
   */
  EClass getIsAdaptiveStatement();

  /**
   * Returns the meta object for the containment reference '{@link edu.clemson.simdase.simdase.IsAdaptiveStatement#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Value</em>'.
   * @see edu.clemson.simdase.simdase.IsAdaptiveStatement#getValue()
   * @see #getIsAdaptiveStatement()
   * @generated
   */
  EReference getIsAdaptiveStatement_Value();

  /**
   * Returns the meta object for class '{@link edu.clemson.simdase.simdase.TMax <em>TMax</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TMax</em>'.
   * @see edu.clemson.simdase.simdase.TMax
   * @generated
   */
  EClass getTMax();

  /**
   * Returns the meta object for the containment reference '{@link edu.clemson.simdase.simdase.TMax#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Value</em>'.
   * @see edu.clemson.simdase.simdase.TMax#getValue()
   * @see #getTMax()
   * @generated
   */
  EReference getTMax_Value();

  /**
   * Returns the meta object for class '{@link edu.clemson.simdase.simdase.TMin <em>TMin</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>TMin</em>'.
   * @see edu.clemson.simdase.simdase.TMin
   * @generated
   */
  EClass getTMin();

  /**
   * Returns the meta object for the containment reference '{@link edu.clemson.simdase.simdase.TMin#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Value</em>'.
   * @see edu.clemson.simdase.simdase.TMin#getValue()
   * @see #getTMin()
   * @generated
   */
  EReference getTMin_Value();

  /**
   * Returns the meta object for class '{@link edu.clemson.simdase.simdase.ScalingFactor <em>Scaling Factor</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Scaling Factor</em>'.
   * @see edu.clemson.simdase.simdase.ScalingFactor
   * @generated
   */
  EClass getScalingFactor();

  /**
   * Returns the meta object for the containment reference '{@link edu.clemson.simdase.simdase.ScalingFactor#getStatement <em>Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Statement</em>'.
   * @see edu.clemson.simdase.simdase.ScalingFactor#getStatement()
   * @see #getScalingFactor()
   * @generated
   */
  EReference getScalingFactor_Statement();

  /**
   * Returns the meta object for class '{@link edu.clemson.simdase.simdase.ReEvaluateAveraging <em>Re Evaluate Averaging</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Re Evaluate Averaging</em>'.
   * @see edu.clemson.simdase.simdase.ReEvaluateAveraging
   * @generated
   */
  EClass getReEvaluateAveraging();

  /**
   * Returns the meta object for the containment reference '{@link edu.clemson.simdase.simdase.ReEvaluateAveraging#getCount <em>Count</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Count</em>'.
   * @see edu.clemson.simdase.simdase.ReEvaluateAveraging#getCount()
   * @see #getReEvaluateAveraging()
   * @generated
   */
  EReference getReEvaluateAveraging_Count();

  /**
   * Returns the meta object for the attribute '{@link edu.clemson.simdase.simdase.ReEvaluateAveraging#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Type</em>'.
   * @see edu.clemson.simdase.simdase.ReEvaluateAveraging#getType()
   * @see #getReEvaluateAveraging()
   * @generated
   */
  EAttribute getReEvaluateAveraging_Type();

  /**
   * Returns the meta object for class '{@link edu.clemson.simdase.simdase.HourlyCost <em>Hourly Cost</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Hourly Cost</em>'.
   * @see edu.clemson.simdase.simdase.HourlyCost
   * @generated
   */
  EClass getHourlyCost();

  /**
   * Returns the meta object for the containment reference '{@link edu.clemson.simdase.simdase.HourlyCost#getCount <em>Count</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Count</em>'.
   * @see edu.clemson.simdase.simdase.HourlyCost#getCount()
   * @see #getHourlyCost()
   * @generated
   */
  EReference getHourlyCost_Count();

  /**
   * Returns the meta object for the attribute '{@link edu.clemson.simdase.simdase.HourlyCost#getEmployeeType <em>Employee Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Employee Type</em>'.
   * @see edu.clemson.simdase.simdase.HourlyCost#getEmployeeType()
   * @see #getHourlyCost()
   * @generated
   */
  EAttribute getHourlyCost_EmployeeType();

  /**
   * Returns the meta object for the containment reference '{@link edu.clemson.simdase.simdase.HourlyCost#getCost <em>Cost</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Cost</em>'.
   * @see edu.clemson.simdase.simdase.HourlyCost#getCost()
   * @see #getHourlyCost()
   * @generated
   */
  EReference getHourlyCost_Cost();

  /**
   * Returns the meta object for class '{@link edu.clemson.simdase.simdase.FixedCost <em>Fixed Cost</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Fixed Cost</em>'.
   * @see edu.clemson.simdase.simdase.FixedCost
   * @generated
   */
  EClass getFixedCost();

  /**
   * Returns the meta object for the containment reference '{@link edu.clemson.simdase.simdase.FixedCost#getCost <em>Cost</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Cost</em>'.
   * @see edu.clemson.simdase.simdase.FixedCost#getCost()
   * @see #getFixedCost()
   * @generated
   */
  EReference getFixedCost_Cost();

  /**
   * Returns the meta object for the attribute '{@link edu.clemson.simdase.simdase.FixedCost#getCostReason <em>Cost Reason</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Cost Reason</em>'.
   * @see edu.clemson.simdase.simdase.FixedCost#getCostReason()
   * @see #getFixedCost()
   * @generated
   */
  EAttribute getFixedCost_CostReason();

  /**
   * Returns the meta object for the containment reference '{@link edu.clemson.simdase.simdase.FixedCost#getPeriod <em>Period</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Period</em>'.
   * @see edu.clemson.simdase.simdase.FixedCost#getPeriod()
   * @see #getFixedCost()
   * @generated
   */
  EReference getFixedCost_Period();

  /**
   * Returns the meta object for class '{@link edu.clemson.simdase.simdase.Integer <em>Integer</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Integer</em>'.
   * @see edu.clemson.simdase.simdase.Integer
   * @generated
   */
  EClass getInteger();

  /**
   * Returns the meta object for the containment reference '{@link edu.clemson.simdase.simdase.Integer#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Value</em>'.
   * @see edu.clemson.simdase.simdase.Integer#getValue()
   * @see #getInteger()
   * @generated
   */
  EReference getInteger_Value();

  /**
   * Returns the meta object for class '{@link edu.clemson.simdase.simdase.T <em>T</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>T</em>'.
   * @see edu.clemson.simdase.simdase.T
   * @generated
   */
  EClass getT();

  /**
   * Returns the meta object for class '{@link edu.clemson.simdase.simdase.I <em>I</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>I</em>'.
   * @see edu.clemson.simdase.simdase.I
   * @generated
   */
  EClass getI();

  /**
   * Returns the meta object for class '{@link edu.clemson.simdase.simdase.Sin <em>Sin</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Sin</em>'.
   * @see edu.clemson.simdase.simdase.Sin
   * @generated
   */
  EClass getSin();

  /**
   * Returns the meta object for the containment reference '{@link edu.clemson.simdase.simdase.Sin#getStatement <em>Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Statement</em>'.
   * @see edu.clemson.simdase.simdase.Sin#getStatement()
   * @see #getSin()
   * @generated
   */
  EReference getSin_Statement();

  /**
   * Returns the meta object for class '{@link edu.clemson.simdase.simdase.Cos <em>Cos</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Cos</em>'.
   * @see edu.clemson.simdase.simdase.Cos
   * @generated
   */
  EClass getCos();

  /**
   * Returns the meta object for the containment reference '{@link edu.clemson.simdase.simdase.Cos#getStatement <em>Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Statement</em>'.
   * @see edu.clemson.simdase.simdase.Cos#getStatement()
   * @see #getCos()
   * @generated
   */
  EReference getCos_Statement();

  /**
   * Returns the meta object for class '{@link edu.clemson.simdase.simdase.Tan <em>Tan</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Tan</em>'.
   * @see edu.clemson.simdase.simdase.Tan
   * @generated
   */
  EClass getTan();

  /**
   * Returns the meta object for the containment reference '{@link edu.clemson.simdase.simdase.Tan#getStatement <em>Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Statement</em>'.
   * @see edu.clemson.simdase.simdase.Tan#getStatement()
   * @see #getTan()
   * @generated
   */
  EReference getTan_Statement();

  /**
   * Returns the meta object for class '{@link edu.clemson.simdase.simdase.Paren <em>Paren</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Paren</em>'.
   * @see edu.clemson.simdase.simdase.Paren
   * @generated
   */
  EClass getParen();

  /**
   * Returns the meta object for the containment reference '{@link edu.clemson.simdase.simdase.Paren#getStatement <em>Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Statement</em>'.
   * @see edu.clemson.simdase.simdase.Paren#getStatement()
   * @see #getParen()
   * @generated
   */
  EReference getParen_Statement();

  /**
   * Returns the meta object for class '{@link edu.clemson.simdase.simdase.If <em>If</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>If</em>'.
   * @see edu.clemson.simdase.simdase.If
   * @generated
   */
  EClass getIf();

  /**
   * Returns the meta object for the containment reference '{@link edu.clemson.simdase.simdase.If#getBooleanStatement <em>Boolean Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Boolean Statement</em>'.
   * @see edu.clemson.simdase.simdase.If#getBooleanStatement()
   * @see #getIf()
   * @generated
   */
  EReference getIf_BooleanStatement();

  /**
   * Returns the meta object for the containment reference '{@link edu.clemson.simdase.simdase.If#getTrueStatement <em>True Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>True Statement</em>'.
   * @see edu.clemson.simdase.simdase.If#getTrueStatement()
   * @see #getIf()
   * @generated
   */
  EReference getIf_TrueStatement();

  /**
   * Returns the meta object for the containment reference '{@link edu.clemson.simdase.simdase.If#getFalseStatement <em>False Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>False Statement</em>'.
   * @see edu.clemson.simdase.simdase.If#getFalseStatement()
   * @see #getIf()
   * @generated
   */
  EReference getIf_FalseStatement();

  /**
   * Returns the meta object for class '{@link edu.clemson.simdase.simdase.Random <em>Random</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Random</em>'.
   * @see edu.clemson.simdase.simdase.Random
   * @generated
   */
  EClass getRandom();

  /**
   * Returns the meta object for class '{@link edu.clemson.simdase.simdase.StatementScalingFactor <em>Statement Scaling Factor</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Statement Scaling Factor</em>'.
   * @see edu.clemson.simdase.simdase.StatementScalingFactor
   * @generated
   */
  EClass getStatementScalingFactor();

  /**
   * Returns the meta object for class '{@link edu.clemson.simdase.simdase.WithNot <em>With Not</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>With Not</em>'.
   * @see edu.clemson.simdase.simdase.WithNot
   * @generated
   */
  EClass getWithNot();

  /**
   * Returns the meta object for the containment reference '{@link edu.clemson.simdase.simdase.WithNot#getStatement <em>Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Statement</em>'.
   * @see edu.clemson.simdase.simdase.WithNot#getStatement()
   * @see #getWithNot()
   * @generated
   */
  EReference getWithNot_Statement();

  /**
   * Returns the meta object for class '{@link edu.clemson.simdase.simdase.WithoutNot <em>Without Not</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Without Not</em>'.
   * @see edu.clemson.simdase.simdase.WithoutNot
   * @generated
   */
  EClass getWithoutNot();

  /**
   * Returns the meta object for the containment reference '{@link edu.clemson.simdase.simdase.WithoutNot#getStatement <em>Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Statement</em>'.
   * @see edu.clemson.simdase.simdase.WithoutNot#getStatement()
   * @see #getWithoutNot()
   * @generated
   */
  EReference getWithoutNot_Statement();

  /**
   * Returns the meta object for class '{@link edu.clemson.simdase.simdase.BoolBoolean <em>Bool Boolean</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Bool Boolean</em>'.
   * @see edu.clemson.simdase.simdase.BoolBoolean
   * @generated
   */
  EClass getBoolBoolean();

  /**
   * Returns the meta object for the containment reference '{@link edu.clemson.simdase.simdase.BoolBoolean#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Value</em>'.
   * @see edu.clemson.simdase.simdase.BoolBoolean#getValue()
   * @see #getBoolBoolean()
   * @generated
   */
  EReference getBoolBoolean_Value();

  /**
   * Returns the meta object for class '{@link edu.clemson.simdase.simdase.BoolT <em>Bool T</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Bool T</em>'.
   * @see edu.clemson.simdase.simdase.BoolT
   * @generated
   */
  EClass getBoolT();

  /**
   * Returns the meta object for class '{@link edu.clemson.simdase.simdase.BoolI <em>Bool I</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Bool I</em>'.
   * @see edu.clemson.simdase.simdase.BoolI
   * @generated
   */
  EClass getBoolI();

  /**
   * Returns the meta object for class '{@link edu.clemson.simdase.simdase.BoolInteger <em>Bool Integer</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Bool Integer</em>'.
   * @see edu.clemson.simdase.simdase.BoolInteger
   * @generated
   */
  EClass getBoolInteger();

  /**
   * Returns the meta object for the containment reference '{@link edu.clemson.simdase.simdase.BoolInteger#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Value</em>'.
   * @see edu.clemson.simdase.simdase.BoolInteger#getValue()
   * @see #getBoolInteger()
   * @generated
   */
  EReference getBoolInteger_Value();

  /**
   * Returns the meta object for class '{@link edu.clemson.simdase.simdase.BoolSin <em>Bool Sin</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Bool Sin</em>'.
   * @see edu.clemson.simdase.simdase.BoolSin
   * @generated
   */
  EClass getBoolSin();

  /**
   * Returns the meta object for the containment reference '{@link edu.clemson.simdase.simdase.BoolSin#getStatement <em>Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Statement</em>'.
   * @see edu.clemson.simdase.simdase.BoolSin#getStatement()
   * @see #getBoolSin()
   * @generated
   */
  EReference getBoolSin_Statement();

  /**
   * Returns the meta object for class '{@link edu.clemson.simdase.simdase.BoolCos <em>Bool Cos</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Bool Cos</em>'.
   * @see edu.clemson.simdase.simdase.BoolCos
   * @generated
   */
  EClass getBoolCos();

  /**
   * Returns the meta object for the containment reference '{@link edu.clemson.simdase.simdase.BoolCos#getStatement <em>Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Statement</em>'.
   * @see edu.clemson.simdase.simdase.BoolCos#getStatement()
   * @see #getBoolCos()
   * @generated
   */
  EReference getBoolCos_Statement();

  /**
   * Returns the meta object for class '{@link edu.clemson.simdase.simdase.BoolTan <em>Bool Tan</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Bool Tan</em>'.
   * @see edu.clemson.simdase.simdase.BoolTan
   * @generated
   */
  EClass getBoolTan();

  /**
   * Returns the meta object for the containment reference '{@link edu.clemson.simdase.simdase.BoolTan#getStatement <em>Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Statement</em>'.
   * @see edu.clemson.simdase.simdase.BoolTan#getStatement()
   * @see #getBoolTan()
   * @generated
   */
  EReference getBoolTan_Statement();

  /**
   * Returns the meta object for class '{@link edu.clemson.simdase.simdase.BoolParen <em>Bool Paren</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Bool Paren</em>'.
   * @see edu.clemson.simdase.simdase.BoolParen
   * @generated
   */
  EClass getBoolParen();

  /**
   * Returns the meta object for the containment reference '{@link edu.clemson.simdase.simdase.BoolParen#getStatement <em>Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Statement</em>'.
   * @see edu.clemson.simdase.simdase.BoolParen#getStatement()
   * @see #getBoolParen()
   * @generated
   */
  EReference getBoolParen_Statement();

  /**
   * Returns the meta object for class '{@link edu.clemson.simdase.simdase.BoolRandom <em>Bool Random</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Bool Random</em>'.
   * @see edu.clemson.simdase.simdase.BoolRandom
   * @generated
   */
  EClass getBoolRandom();

  /**
   * Returns the meta object for class '{@link edu.clemson.simdase.simdase.BoolScalingFactor <em>Bool Scaling Factor</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Bool Scaling Factor</em>'.
   * @see edu.clemson.simdase.simdase.BoolScalingFactor
   * @generated
   */
  EClass getBoolScalingFactor();

  /**
   * Returns the factory that creates the instances of the model.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the factory that creates the instances of the model.
   * @generated
   */
  SimdaseFactory getSimdaseFactory();

  /**
   * <!-- begin-user-doc -->
   * Defines literals for the meta objects that represent
   * <ul>
   *   <li>each class,</li>
   *   <li>each feature of each class,</li>
   *   <li>each enum,</li>
   *   <li>and each data type</li>
   * </ul>
   * <!-- end-user-doc -->
   * @generated
   */
  interface Literals
  {
    /**
     * The meta object literal for the '{@link edu.clemson.simdase.simdase.impl.SimdaseLibraryImpl <em>Library</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.simdase.simdase.impl.SimdaseLibraryImpl
     * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getSimdaseLibrary()
     * @generated
     */
    EClass SIMDASE_LIBRARY = eINSTANCE.getSimdaseLibrary();

    /**
     * The meta object literal for the '{@link edu.clemson.simdase.simdase.impl.SimdaseSubclauseImpl <em>Subclause</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.simdase.simdase.impl.SimdaseSubclauseImpl
     * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getSimdaseSubclause()
     * @generated
     */
    EClass SIMDASE_SUBCLAUSE = eINSTANCE.getSimdaseSubclause();

    /**
     * The meta object literal for the '{@link edu.clemson.simdase.simdase.impl.ContractImpl <em>Contract</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.simdase.simdase.impl.ContractImpl
     * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getContract()
     * @generated
     */
    EClass CONTRACT = eINSTANCE.getContract();

    /**
     * The meta object literal for the '{@link edu.clemson.simdase.simdase.impl.SIMDASEStatementImpl <em>SIMDASE Statement</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.simdase.simdase.impl.SIMDASEStatementImpl
     * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getSIMDASEStatement()
     * @generated
     */
    EClass SIMDASE_STATEMENT = eINSTANCE.getSIMDASEStatement();

    /**
     * The meta object literal for the '{@link edu.clemson.simdase.simdase.impl.VariantsImpl <em>Variants</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.simdase.simdase.impl.VariantsImpl
     * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getVariants()
     * @generated
     */
    EClass VARIANTS = eINSTANCE.getVariants();

    /**
     * The meta object literal for the '<em><b>Variants</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference VARIANTS__VARIANTS = eINSTANCE.getVariants_Variants();

    /**
     * The meta object literal for the '<em><b>First</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference VARIANTS__FIRST = eINSTANCE.getVariants_First();

    /**
     * The meta object literal for the '<em><b>Rest</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference VARIANTS__REST = eINSTANCE.getVariants_Rest();

    /**
     * The meta object literal for the '{@link edu.clemson.simdase.simdase.impl.VariantImpl <em>Variant</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.simdase.simdase.impl.VariantImpl
     * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getVariant()
     * @generated
     */
    EClass VARIANT = eINSTANCE.getVariant();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute VARIANT__NAME = eINSTANCE.getVariant_Name();

    /**
     * The meta object literal for the '<em><b>Variants</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference VARIANT__VARIANTS = eINSTANCE.getVariant_Variants();

    /**
     * The meta object literal for the '{@link edu.clemson.simdase.simdase.impl.ActiveVariantsImpl <em>Active Variants</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.simdase.simdase.impl.ActiveVariantsImpl
     * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getActiveVariants()
     * @generated
     */
    EClass ACTIVE_VARIANTS = eINSTANCE.getActiveVariants();

    /**
     * The meta object literal for the '<em><b>Variants</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ACTIVE_VARIANTS__VARIANTS = eINSTANCE.getActiveVariants_Variants();

    /**
     * The meta object literal for the '<em><b>First</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ACTIVE_VARIANTS__FIRST = eINSTANCE.getActiveVariants_First();

    /**
     * The meta object literal for the '<em><b>Rest</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ACTIVE_VARIANTS__REST = eINSTANCE.getActiveVariants_Rest();

    /**
     * The meta object literal for the '{@link edu.clemson.simdase.simdase.impl.ActiveVariantImpl <em>Active Variant</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.simdase.simdase.impl.ActiveVariantImpl
     * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getActiveVariant()
     * @generated
     */
    EClass ACTIVE_VARIANT = eINSTANCE.getActiveVariant();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ACTIVE_VARIANT__NAME = eINSTANCE.getActiveVariant_Name();

    /**
     * The meta object literal for the '<em><b>Variants</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ACTIVE_VARIANT__VARIANTS = eINSTANCE.getActiveVariant_Variants();

    /**
     * The meta object literal for the '{@link edu.clemson.simdase.simdase.impl.VariantNamesImpl <em>Variant Names</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.simdase.simdase.impl.VariantNamesImpl
     * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getVariantNames()
     * @generated
     */
    EClass VARIANT_NAMES = eINSTANCE.getVariantNames();

    /**
     * The meta object literal for the '<em><b>First</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute VARIANT_NAMES__FIRST = eINSTANCE.getVariantNames_First();

    /**
     * The meta object literal for the '<em><b>Rest</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute VARIANT_NAMES__REST = eINSTANCE.getVariantNames_Rest();

    /**
     * The meta object literal for the '{@link edu.clemson.simdase.simdase.impl.CostPropertiesImpl <em>Cost Properties</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.simdase.simdase.impl.CostPropertiesImpl
     * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getCostProperties()
     * @generated
     */
    EClass COST_PROPERTIES = eINSTANCE.getCostProperties();

    /**
     * The meta object literal for the '<em><b>Cost Statements</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference COST_PROPERTIES__COST_STATEMENTS = eINSTANCE.getCostProperties_CostStatements();

    /**
     * The meta object literal for the '{@link edu.clemson.simdase.simdase.impl.CostStatementImpl <em>Cost Statement</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.simdase.simdase.impl.CostStatementImpl
     * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getCostStatement()
     * @generated
     */
    EClass COST_STATEMENT = eINSTANCE.getCostStatement();

    /**
     * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute COST_STATEMENT__TYPE = eINSTANCE.getCostStatement_Type();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference COST_STATEMENT__VALUE = eINSTANCE.getCostStatement_Value();

    /**
     * The meta object literal for the '{@link edu.clemson.simdase.simdase.impl.CostValueImpl <em>Cost Value</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.simdase.simdase.impl.CostValueImpl
     * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getCostValue()
     * @generated
     */
    EClass COST_VALUE = eINSTANCE.getCostValue();

    /**
     * The meta object literal for the '<em><b>First</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference COST_VALUE__FIRST = eINSTANCE.getCostValue_First();

    /**
     * The meta object literal for the '<em><b>Rest</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference COST_VALUE__REST = eINSTANCE.getCostValue_Rest();

    /**
     * The meta object literal for the '{@link edu.clemson.simdase.simdase.impl.EmployeeStatementImpl <em>Employee Statement</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.simdase.simdase.impl.EmployeeStatementImpl
     * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getEmployeeStatement()
     * @generated
     */
    EClass EMPLOYEE_STATEMENT = eINSTANCE.getEmployeeStatement();

    /**
     * The meta object literal for the '{@link edu.clemson.simdase.simdase.impl.BooleanImpl <em>Boolean</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.simdase.simdase.impl.BooleanImpl
     * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getBoolean()
     * @generated
     */
    EClass BOOLEAN = eINSTANCE.getBoolean();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BOOLEAN__VALUE = eINSTANCE.getBoolean_Value();

    /**
     * The meta object literal for the '{@link edu.clemson.simdase.simdase.impl.NumberImpl <em>Number</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.simdase.simdase.impl.NumberImpl
     * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getNumber()
     * @generated
     */
    EClass NUMBER = eINSTANCE.getNumber();

    /**
     * The meta object literal for the '<em><b>Negative</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute NUMBER__NEGATIVE = eINSTANCE.getNumber_Negative();

    /**
     * The meta object literal for the '<em><b>Float Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute NUMBER__FLOAT_VALUE = eINSTANCE.getNumber_FloatValue();

    /**
     * The meta object literal for the '{@link edu.clemson.simdase.simdase.impl.StatementImpl <em>Statement</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.simdase.simdase.impl.StatementImpl
     * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getStatement()
     * @generated
     */
    EClass STATEMENT = eINSTANCE.getStatement();

    /**
     * The meta object literal for the '<em><b>Statement</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference STATEMENT__STATEMENT = eINSTANCE.getStatement_Statement();

    /**
     * The meta object literal for the '<em><b>Op</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute STATEMENT__OP = eINSTANCE.getStatement_Op();

    /**
     * The meta object literal for the '<em><b>Rest</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference STATEMENT__REST = eINSTANCE.getStatement_Rest();

    /**
     * The meta object literal for the '{@link edu.clemson.simdase.simdase.impl.StatementLvl2Impl <em>Statement Lvl2</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.simdase.simdase.impl.StatementLvl2Impl
     * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getStatementLvl2()
     * @generated
     */
    EClass STATEMENT_LVL2 = eINSTANCE.getStatementLvl2();

    /**
     * The meta object literal for the '<em><b>Statement</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference STATEMENT_LVL2__STATEMENT = eINSTANCE.getStatementLvl2_Statement();

    /**
     * The meta object literal for the '<em><b>Op</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute STATEMENT_LVL2__OP = eINSTANCE.getStatementLvl2_Op();

    /**
     * The meta object literal for the '<em><b>Rest</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference STATEMENT_LVL2__REST = eINSTANCE.getStatementLvl2_Rest();

    /**
     * The meta object literal for the '{@link edu.clemson.simdase.simdase.impl.StatementLvl3Impl <em>Statement Lvl3</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.simdase.simdase.impl.StatementLvl3Impl
     * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getStatementLvl3()
     * @generated
     */
    EClass STATEMENT_LVL3 = eINSTANCE.getStatementLvl3();

    /**
     * The meta object literal for the '<em><b>Statement</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference STATEMENT_LVL3__STATEMENT = eINSTANCE.getStatementLvl3_Statement();

    /**
     * The meta object literal for the '<em><b>Op</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute STATEMENT_LVL3__OP = eINSTANCE.getStatementLvl3_Op();

    /**
     * The meta object literal for the '<em><b>Rest</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference STATEMENT_LVL3__REST = eINSTANCE.getStatementLvl3_Rest();

    /**
     * The meta object literal for the '{@link edu.clemson.simdase.simdase.impl.StatementLvl4Impl <em>Statement Lvl4</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.simdase.simdase.impl.StatementLvl4Impl
     * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getStatementLvl4()
     * @generated
     */
    EClass STATEMENT_LVL4 = eINSTANCE.getStatementLvl4();

    /**
     * The meta object literal for the '{@link edu.clemson.simdase.simdase.impl.BooleanStatementImpl <em>Boolean Statement</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.simdase.simdase.impl.BooleanStatementImpl
     * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getBooleanStatement()
     * @generated
     */
    EClass BOOLEAN_STATEMENT = eINSTANCE.getBooleanStatement();

    /**
     * The meta object literal for the '<em><b>Statement</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BOOLEAN_STATEMENT__STATEMENT = eINSTANCE.getBooleanStatement_Statement();

    /**
     * The meta object literal for the '<em><b>Op</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BOOLEAN_STATEMENT__OP = eINSTANCE.getBooleanStatement_Op();

    /**
     * The meta object literal for the '<em><b>Rest</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BOOLEAN_STATEMENT__REST = eINSTANCE.getBooleanStatement_Rest();

    /**
     * The meta object literal for the '{@link edu.clemson.simdase.simdase.impl.BooleanStatementLvl2Impl <em>Boolean Statement Lvl2</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.simdase.simdase.impl.BooleanStatementLvl2Impl
     * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getBooleanStatementLvl2()
     * @generated
     */
    EClass BOOLEAN_STATEMENT_LVL2 = eINSTANCE.getBooleanStatementLvl2();

    /**
     * The meta object literal for the '<em><b>Statement</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BOOLEAN_STATEMENT_LVL2__STATEMENT = eINSTANCE.getBooleanStatementLvl2_Statement();

    /**
     * The meta object literal for the '<em><b>Op</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BOOLEAN_STATEMENT_LVL2__OP = eINSTANCE.getBooleanStatementLvl2_Op();

    /**
     * The meta object literal for the '<em><b>Rest</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BOOLEAN_STATEMENT_LVL2__REST = eINSTANCE.getBooleanStatementLvl2_Rest();

    /**
     * The meta object literal for the '{@link edu.clemson.simdase.simdase.impl.BooleanStatementLvl3Impl <em>Boolean Statement Lvl3</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.simdase.simdase.impl.BooleanStatementLvl3Impl
     * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getBooleanStatementLvl3()
     * @generated
     */
    EClass BOOLEAN_STATEMENT_LVL3 = eINSTANCE.getBooleanStatementLvl3();

    /**
     * The meta object literal for the '<em><b>Statement</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BOOLEAN_STATEMENT_LVL3__STATEMENT = eINSTANCE.getBooleanStatementLvl3_Statement();

    /**
     * The meta object literal for the '<em><b>Op</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BOOLEAN_STATEMENT_LVL3__OP = eINSTANCE.getBooleanStatementLvl3_Op();

    /**
     * The meta object literal for the '<em><b>Rest</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BOOLEAN_STATEMENT_LVL3__REST = eINSTANCE.getBooleanStatementLvl3_Rest();

    /**
     * The meta object literal for the '{@link edu.clemson.simdase.simdase.impl.BooleanStatementLvl4Impl <em>Boolean Statement Lvl4</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.simdase.simdase.impl.BooleanStatementLvl4Impl
     * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getBooleanStatementLvl4()
     * @generated
     */
    EClass BOOLEAN_STATEMENT_LVL4 = eINSTANCE.getBooleanStatementLvl4();

    /**
     * The meta object literal for the '{@link edu.clemson.simdase.simdase.impl.BooleanStatementLvl5Impl <em>Boolean Statement Lvl5</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.simdase.simdase.impl.BooleanStatementLvl5Impl
     * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getBooleanStatementLvl5()
     * @generated
     */
    EClass BOOLEAN_STATEMENT_LVL5 = eINSTANCE.getBooleanStatementLvl5();

    /**
     * The meta object literal for the '<em><b>Statement</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BOOLEAN_STATEMENT_LVL5__STATEMENT = eINSTANCE.getBooleanStatementLvl5_Statement();

    /**
     * The meta object literal for the '<em><b>Op</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BOOLEAN_STATEMENT_LVL5__OP = eINSTANCE.getBooleanStatementLvl5_Op();

    /**
     * The meta object literal for the '<em><b>Rest</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BOOLEAN_STATEMENT_LVL5__REST = eINSTANCE.getBooleanStatementLvl5_Rest();

    /**
     * The meta object literal for the '{@link edu.clemson.simdase.simdase.impl.BooleanStatementLvl6Impl <em>Boolean Statement Lvl6</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.simdase.simdase.impl.BooleanStatementLvl6Impl
     * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getBooleanStatementLvl6()
     * @generated
     */
    EClass BOOLEAN_STATEMENT_LVL6 = eINSTANCE.getBooleanStatementLvl6();

    /**
     * The meta object literal for the '<em><b>Statement</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BOOLEAN_STATEMENT_LVL6__STATEMENT = eINSTANCE.getBooleanStatementLvl6_Statement();

    /**
     * The meta object literal for the '<em><b>Op</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BOOLEAN_STATEMENT_LVL6__OP = eINSTANCE.getBooleanStatementLvl6_Op();

    /**
     * The meta object literal for the '<em><b>Rest</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BOOLEAN_STATEMENT_LVL6__REST = eINSTANCE.getBooleanStatementLvl6_Rest();

    /**
     * The meta object literal for the '{@link edu.clemson.simdase.simdase.impl.BooleanStatementLvl7Impl <em>Boolean Statement Lvl7</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.simdase.simdase.impl.BooleanStatementLvl7Impl
     * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getBooleanStatementLvl7()
     * @generated
     */
    EClass BOOLEAN_STATEMENT_LVL7 = eINSTANCE.getBooleanStatementLvl7();

    /**
     * The meta object literal for the '<em><b>Statement</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BOOLEAN_STATEMENT_LVL7__STATEMENT = eINSTANCE.getBooleanStatementLvl7_Statement();

    /**
     * The meta object literal for the '<em><b>Op</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BOOLEAN_STATEMENT_LVL7__OP = eINSTANCE.getBooleanStatementLvl7_Op();

    /**
     * The meta object literal for the '<em><b>Rest</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BOOLEAN_STATEMENT_LVL7__REST = eINSTANCE.getBooleanStatementLvl7_Rest();

    /**
     * The meta object literal for the '{@link edu.clemson.simdase.simdase.impl.BooleanStatementLvl8Impl <em>Boolean Statement Lvl8</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.simdase.simdase.impl.BooleanStatementLvl8Impl
     * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getBooleanStatementLvl8()
     * @generated
     */
    EClass BOOLEAN_STATEMENT_LVL8 = eINSTANCE.getBooleanStatementLvl8();

    /**
     * The meta object literal for the '{@link edu.clemson.simdase.simdase.impl.SimdaseContractLibraryImpl <em>Contract Library</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.simdase.simdase.impl.SimdaseContractLibraryImpl
     * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getSimdaseContractLibrary()
     * @generated
     */
    EClass SIMDASE_CONTRACT_LIBRARY = eINSTANCE.getSimdaseContractLibrary();

    /**
     * The meta object literal for the '<em><b>Contract</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SIMDASE_CONTRACT_LIBRARY__CONTRACT = eINSTANCE.getSimdaseContractLibrary_Contract();

    /**
     * The meta object literal for the '{@link edu.clemson.simdase.simdase.impl.SimdaseContractSubclauseImpl <em>Contract Subclause</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.simdase.simdase.impl.SimdaseContractSubclauseImpl
     * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getSimdaseContractSubclause()
     * @generated
     */
    EClass SIMDASE_CONTRACT_SUBCLAUSE = eINSTANCE.getSimdaseContractSubclause();

    /**
     * The meta object literal for the '<em><b>Contract</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SIMDASE_CONTRACT_SUBCLAUSE__CONTRACT = eINSTANCE.getSimdaseContractSubclause_Contract();

    /**
     * The meta object literal for the '{@link edu.clemson.simdase.simdase.impl.SimdaseContractImpl <em>Contract</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.simdase.simdase.impl.SimdaseContractImpl
     * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getSimdaseContract()
     * @generated
     */
    EClass SIMDASE_CONTRACT = eINSTANCE.getSimdaseContract();

    /**
     * The meta object literal for the '<em><b>Statement</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SIMDASE_CONTRACT__STATEMENT = eINSTANCE.getSimdaseContract_Statement();

    /**
     * The meta object literal for the '{@link edu.clemson.simdase.simdase.impl.IsAdaptiveStatementImpl <em>Is Adaptive Statement</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.simdase.simdase.impl.IsAdaptiveStatementImpl
     * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getIsAdaptiveStatement()
     * @generated
     */
    EClass IS_ADAPTIVE_STATEMENT = eINSTANCE.getIsAdaptiveStatement();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference IS_ADAPTIVE_STATEMENT__VALUE = eINSTANCE.getIsAdaptiveStatement_Value();

    /**
     * The meta object literal for the '{@link edu.clemson.simdase.simdase.impl.TMaxImpl <em>TMax</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.simdase.simdase.impl.TMaxImpl
     * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getTMax()
     * @generated
     */
    EClass TMAX = eINSTANCE.getTMax();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference TMAX__VALUE = eINSTANCE.getTMax_Value();

    /**
     * The meta object literal for the '{@link edu.clemson.simdase.simdase.impl.TMinImpl <em>TMin</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.simdase.simdase.impl.TMinImpl
     * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getTMin()
     * @generated
     */
    EClass TMIN = eINSTANCE.getTMin();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference TMIN__VALUE = eINSTANCE.getTMin_Value();

    /**
     * The meta object literal for the '{@link edu.clemson.simdase.simdase.impl.ScalingFactorImpl <em>Scaling Factor</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.simdase.simdase.impl.ScalingFactorImpl
     * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getScalingFactor()
     * @generated
     */
    EClass SCALING_FACTOR = eINSTANCE.getScalingFactor();

    /**
     * The meta object literal for the '<em><b>Statement</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SCALING_FACTOR__STATEMENT = eINSTANCE.getScalingFactor_Statement();

    /**
     * The meta object literal for the '{@link edu.clemson.simdase.simdase.impl.ReEvaluateAveragingImpl <em>Re Evaluate Averaging</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.simdase.simdase.impl.ReEvaluateAveragingImpl
     * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getReEvaluateAveraging()
     * @generated
     */
    EClass RE_EVALUATE_AVERAGING = eINSTANCE.getReEvaluateAveraging();

    /**
     * The meta object literal for the '<em><b>Count</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference RE_EVALUATE_AVERAGING__COUNT = eINSTANCE.getReEvaluateAveraging_Count();

    /**
     * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute RE_EVALUATE_AVERAGING__TYPE = eINSTANCE.getReEvaluateAveraging_Type();

    /**
     * The meta object literal for the '{@link edu.clemson.simdase.simdase.impl.HourlyCostImpl <em>Hourly Cost</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.simdase.simdase.impl.HourlyCostImpl
     * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getHourlyCost()
     * @generated
     */
    EClass HOURLY_COST = eINSTANCE.getHourlyCost();

    /**
     * The meta object literal for the '<em><b>Count</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference HOURLY_COST__COUNT = eINSTANCE.getHourlyCost_Count();

    /**
     * The meta object literal for the '<em><b>Employee Type</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute HOURLY_COST__EMPLOYEE_TYPE = eINSTANCE.getHourlyCost_EmployeeType();

    /**
     * The meta object literal for the '<em><b>Cost</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference HOURLY_COST__COST = eINSTANCE.getHourlyCost_Cost();

    /**
     * The meta object literal for the '{@link edu.clemson.simdase.simdase.impl.FixedCostImpl <em>Fixed Cost</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.simdase.simdase.impl.FixedCostImpl
     * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getFixedCost()
     * @generated
     */
    EClass FIXED_COST = eINSTANCE.getFixedCost();

    /**
     * The meta object literal for the '<em><b>Cost</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference FIXED_COST__COST = eINSTANCE.getFixedCost_Cost();

    /**
     * The meta object literal for the '<em><b>Cost Reason</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute FIXED_COST__COST_REASON = eINSTANCE.getFixedCost_CostReason();

    /**
     * The meta object literal for the '<em><b>Period</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference FIXED_COST__PERIOD = eINSTANCE.getFixedCost_Period();

    /**
     * The meta object literal for the '{@link edu.clemson.simdase.simdase.impl.IntegerImpl <em>Integer</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.simdase.simdase.impl.IntegerImpl
     * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getInteger()
     * @generated
     */
    EClass INTEGER = eINSTANCE.getInteger();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference INTEGER__VALUE = eINSTANCE.getInteger_Value();

    /**
     * The meta object literal for the '{@link edu.clemson.simdase.simdase.impl.TImpl <em>T</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.simdase.simdase.impl.TImpl
     * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getT()
     * @generated
     */
    EClass T = eINSTANCE.getT();

    /**
     * The meta object literal for the '{@link edu.clemson.simdase.simdase.impl.IImpl <em>I</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.simdase.simdase.impl.IImpl
     * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getI()
     * @generated
     */
    EClass I = eINSTANCE.getI();

    /**
     * The meta object literal for the '{@link edu.clemson.simdase.simdase.impl.SinImpl <em>Sin</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.simdase.simdase.impl.SinImpl
     * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getSin()
     * @generated
     */
    EClass SIN = eINSTANCE.getSin();

    /**
     * The meta object literal for the '<em><b>Statement</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SIN__STATEMENT = eINSTANCE.getSin_Statement();

    /**
     * The meta object literal for the '{@link edu.clemson.simdase.simdase.impl.CosImpl <em>Cos</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.simdase.simdase.impl.CosImpl
     * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getCos()
     * @generated
     */
    EClass COS = eINSTANCE.getCos();

    /**
     * The meta object literal for the '<em><b>Statement</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference COS__STATEMENT = eINSTANCE.getCos_Statement();

    /**
     * The meta object literal for the '{@link edu.clemson.simdase.simdase.impl.TanImpl <em>Tan</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.simdase.simdase.impl.TanImpl
     * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getTan()
     * @generated
     */
    EClass TAN = eINSTANCE.getTan();

    /**
     * The meta object literal for the '<em><b>Statement</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference TAN__STATEMENT = eINSTANCE.getTan_Statement();

    /**
     * The meta object literal for the '{@link edu.clemson.simdase.simdase.impl.ParenImpl <em>Paren</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.simdase.simdase.impl.ParenImpl
     * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getParen()
     * @generated
     */
    EClass PAREN = eINSTANCE.getParen();

    /**
     * The meta object literal for the '<em><b>Statement</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PAREN__STATEMENT = eINSTANCE.getParen_Statement();

    /**
     * The meta object literal for the '{@link edu.clemson.simdase.simdase.impl.IfImpl <em>If</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.simdase.simdase.impl.IfImpl
     * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getIf()
     * @generated
     */
    EClass IF = eINSTANCE.getIf();

    /**
     * The meta object literal for the '<em><b>Boolean Statement</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference IF__BOOLEAN_STATEMENT = eINSTANCE.getIf_BooleanStatement();

    /**
     * The meta object literal for the '<em><b>True Statement</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference IF__TRUE_STATEMENT = eINSTANCE.getIf_TrueStatement();

    /**
     * The meta object literal for the '<em><b>False Statement</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference IF__FALSE_STATEMENT = eINSTANCE.getIf_FalseStatement();

    /**
     * The meta object literal for the '{@link edu.clemson.simdase.simdase.impl.RandomImpl <em>Random</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.simdase.simdase.impl.RandomImpl
     * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getRandom()
     * @generated
     */
    EClass RANDOM = eINSTANCE.getRandom();

    /**
     * The meta object literal for the '{@link edu.clemson.simdase.simdase.impl.StatementScalingFactorImpl <em>Statement Scaling Factor</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.simdase.simdase.impl.StatementScalingFactorImpl
     * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getStatementScalingFactor()
     * @generated
     */
    EClass STATEMENT_SCALING_FACTOR = eINSTANCE.getStatementScalingFactor();

    /**
     * The meta object literal for the '{@link edu.clemson.simdase.simdase.impl.WithNotImpl <em>With Not</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.simdase.simdase.impl.WithNotImpl
     * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getWithNot()
     * @generated
     */
    EClass WITH_NOT = eINSTANCE.getWithNot();

    /**
     * The meta object literal for the '<em><b>Statement</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference WITH_NOT__STATEMENT = eINSTANCE.getWithNot_Statement();

    /**
     * The meta object literal for the '{@link edu.clemson.simdase.simdase.impl.WithoutNotImpl <em>Without Not</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.simdase.simdase.impl.WithoutNotImpl
     * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getWithoutNot()
     * @generated
     */
    EClass WITHOUT_NOT = eINSTANCE.getWithoutNot();

    /**
     * The meta object literal for the '<em><b>Statement</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference WITHOUT_NOT__STATEMENT = eINSTANCE.getWithoutNot_Statement();

    /**
     * The meta object literal for the '{@link edu.clemson.simdase.simdase.impl.BoolBooleanImpl <em>Bool Boolean</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.simdase.simdase.impl.BoolBooleanImpl
     * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getBoolBoolean()
     * @generated
     */
    EClass BOOL_BOOLEAN = eINSTANCE.getBoolBoolean();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BOOL_BOOLEAN__VALUE = eINSTANCE.getBoolBoolean_Value();

    /**
     * The meta object literal for the '{@link edu.clemson.simdase.simdase.impl.BoolTImpl <em>Bool T</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.simdase.simdase.impl.BoolTImpl
     * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getBoolT()
     * @generated
     */
    EClass BOOL_T = eINSTANCE.getBoolT();

    /**
     * The meta object literal for the '{@link edu.clemson.simdase.simdase.impl.BoolIImpl <em>Bool I</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.simdase.simdase.impl.BoolIImpl
     * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getBoolI()
     * @generated
     */
    EClass BOOL_I = eINSTANCE.getBoolI();

    /**
     * The meta object literal for the '{@link edu.clemson.simdase.simdase.impl.BoolIntegerImpl <em>Bool Integer</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.simdase.simdase.impl.BoolIntegerImpl
     * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getBoolInteger()
     * @generated
     */
    EClass BOOL_INTEGER = eINSTANCE.getBoolInteger();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BOOL_INTEGER__VALUE = eINSTANCE.getBoolInteger_Value();

    /**
     * The meta object literal for the '{@link edu.clemson.simdase.simdase.impl.BoolSinImpl <em>Bool Sin</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.simdase.simdase.impl.BoolSinImpl
     * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getBoolSin()
     * @generated
     */
    EClass BOOL_SIN = eINSTANCE.getBoolSin();

    /**
     * The meta object literal for the '<em><b>Statement</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BOOL_SIN__STATEMENT = eINSTANCE.getBoolSin_Statement();

    /**
     * The meta object literal for the '{@link edu.clemson.simdase.simdase.impl.BoolCosImpl <em>Bool Cos</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.simdase.simdase.impl.BoolCosImpl
     * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getBoolCos()
     * @generated
     */
    EClass BOOL_COS = eINSTANCE.getBoolCos();

    /**
     * The meta object literal for the '<em><b>Statement</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BOOL_COS__STATEMENT = eINSTANCE.getBoolCos_Statement();

    /**
     * The meta object literal for the '{@link edu.clemson.simdase.simdase.impl.BoolTanImpl <em>Bool Tan</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.simdase.simdase.impl.BoolTanImpl
     * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getBoolTan()
     * @generated
     */
    EClass BOOL_TAN = eINSTANCE.getBoolTan();

    /**
     * The meta object literal for the '<em><b>Statement</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BOOL_TAN__STATEMENT = eINSTANCE.getBoolTan_Statement();

    /**
     * The meta object literal for the '{@link edu.clemson.simdase.simdase.impl.BoolParenImpl <em>Bool Paren</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.simdase.simdase.impl.BoolParenImpl
     * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getBoolParen()
     * @generated
     */
    EClass BOOL_PAREN = eINSTANCE.getBoolParen();

    /**
     * The meta object literal for the '<em><b>Statement</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BOOL_PAREN__STATEMENT = eINSTANCE.getBoolParen_Statement();

    /**
     * The meta object literal for the '{@link edu.clemson.simdase.simdase.impl.BoolRandomImpl <em>Bool Random</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.simdase.simdase.impl.BoolRandomImpl
     * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getBoolRandom()
     * @generated
     */
    EClass BOOL_RANDOM = eINSTANCE.getBoolRandom();

    /**
     * The meta object literal for the '{@link edu.clemson.simdase.simdase.impl.BoolScalingFactorImpl <em>Bool Scaling Factor</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.simdase.simdase.impl.BoolScalingFactorImpl
     * @see edu.clemson.simdase.simdase.impl.SimdasePackageImpl#getBoolScalingFactor()
     * @generated
     */
    EClass BOOL_SCALING_FACTOR = eINSTANCE.getBoolScalingFactor();

  }

} //SimdasePackage
