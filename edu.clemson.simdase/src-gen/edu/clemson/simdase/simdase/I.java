/**
 */
package edu.clemson.simdase.simdase;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>I</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.clemson.simdase.simdase.SimdasePackage#getI()
 * @model
 * @generated
 */
public interface I extends StatementLvl4
{
} // I
