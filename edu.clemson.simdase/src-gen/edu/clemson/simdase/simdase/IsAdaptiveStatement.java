/**
 */
package edu.clemson.simdase.simdase;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Is Adaptive Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.clemson.simdase.simdase.IsAdaptiveStatement#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see edu.clemson.simdase.simdase.SimdasePackage#getIsAdaptiveStatement()
 * @model
 * @generated
 */
public interface IsAdaptiveStatement extends SIMDASEStatement
{
  /**
   * Returns the value of the '<em><b>Value</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Value</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Value</em>' containment reference.
   * @see #setValue(edu.clemson.simdase.simdase.Boolean)
   * @see edu.clemson.simdase.simdase.SimdasePackage#getIsAdaptiveStatement_Value()
   * @model containment="true"
   * @generated
   */
  edu.clemson.simdase.simdase.Boolean getValue();

  /**
   * Sets the value of the '{@link edu.clemson.simdase.simdase.IsAdaptiveStatement#getValue <em>Value</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Value</em>' containment reference.
   * @see #getValue()
   * @generated
   */
  void setValue(edu.clemson.simdase.simdase.Boolean value);

} // IsAdaptiveStatement
