/**
 */
package edu.clemson.simdase.simdase;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Hourly Cost</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.clemson.simdase.simdase.HourlyCost#getCount <em>Count</em>}</li>
 *   <li>{@link edu.clemson.simdase.simdase.HourlyCost#getEmployeeType <em>Employee Type</em>}</li>
 *   <li>{@link edu.clemson.simdase.simdase.HourlyCost#getCost <em>Cost</em>}</li>
 * </ul>
 *
 * @see edu.clemson.simdase.simdase.SimdasePackage#getHourlyCost()
 * @model
 * @generated
 */
public interface HourlyCost extends EmployeeStatement
{
  /**
   * Returns the value of the '<em><b>Count</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Count</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Count</em>' containment reference.
   * @see #setCount(edu.clemson.simdase.simdase.Number)
   * @see edu.clemson.simdase.simdase.SimdasePackage#getHourlyCost_Count()
   * @model containment="true"
   * @generated
   */
  edu.clemson.simdase.simdase.Number getCount();

  /**
   * Sets the value of the '{@link edu.clemson.simdase.simdase.HourlyCost#getCount <em>Count</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Count</em>' containment reference.
   * @see #getCount()
   * @generated
   */
  void setCount(edu.clemson.simdase.simdase.Number value);

  /**
   * Returns the value of the '<em><b>Employee Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Employee Type</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Employee Type</em>' attribute.
   * @see #setEmployeeType(String)
   * @see edu.clemson.simdase.simdase.SimdasePackage#getHourlyCost_EmployeeType()
   * @model
   * @generated
   */
  String getEmployeeType();

  /**
   * Sets the value of the '{@link edu.clemson.simdase.simdase.HourlyCost#getEmployeeType <em>Employee Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Employee Type</em>' attribute.
   * @see #getEmployeeType()
   * @generated
   */
  void setEmployeeType(String value);

  /**
   * Returns the value of the '<em><b>Cost</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Cost</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Cost</em>' containment reference.
   * @see #setCost(Statement)
   * @see edu.clemson.simdase.simdase.SimdasePackage#getHourlyCost_Cost()
   * @model containment="true"
   * @generated
   */
  Statement getCost();

  /**
   * Sets the value of the '{@link edu.clemson.simdase.simdase.HourlyCost#getCost <em>Cost</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Cost</em>' containment reference.
   * @see #getCost()
   * @generated
   */
  void setCost(Statement value);

} // HourlyCost
