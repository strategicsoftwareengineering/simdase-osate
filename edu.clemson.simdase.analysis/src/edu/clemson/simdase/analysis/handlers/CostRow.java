package edu.clemson.simdase.analysis.handlers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.Map;

import edu.clemson.simdase.analysis.handlers.CostRow.EmployeeCost;

public class CostRow {
	public static class EmployeeCost {
		private BigDecimal employeeQuantity;
		private BigDecimal costPerEmployee = BigDecimal.ZERO;
		private String employeeType;
		
		private BigDecimal fixedCost = BigDecimal.ZERO;
		private String fixedCostReason;
		
		public BigDecimal getEmployeeQuantity() {
			return employeeQuantity;
		}
		
		public void setEmployeeQuantity(BigDecimal employeeQuantity) {
			this.employeeQuantity = employeeQuantity;
		}
		
		public BigDecimal getCostPerEmployee() {
			return costPerEmployee;
		}
		
		public void setCostPerEmployee(BigDecimal costPerEmployee) {
			this.costPerEmployee = costPerEmployee;
		}

		public String getEmployeeType() {
			return employeeType;
		}

		public void setEmployeeType(String employeeType) {
			this.employeeType = employeeType;
		}
		
		public BigDecimal getFixedCost() {
			return fixedCost;
		}
		
		public void setFixedCost(BigDecimal fixedCost) {
			this.fixedCost = fixedCost;
		}
		
		public String getFixedCostReason() {
			return fixedCostReason;
		}
		
		public void setFixedCostReason(String fixedCostReason) {
			this.fixedCostReason = fixedCostReason;
		}
		
		public String toString() {
			if(fixedCost != null && fixedCost.compareTo(BigDecimal.ZERO) == 0) {
				return employeeQuantity + " " + employeeType + " at " + costPerEmployee;
			} else {
				return fixedCost + " for " + fixedCostReason;
			}
		}
	}
	
	private String componentName;
	private Integer i;
	private Integer t;
	
	private List<EmployeeCost> architectureDefinitionCost;
	private List<EmployeeCost> architectureEvaluationCost;
	private List<EmployeeCost> componentDevelopmentCost;
	private List<EmployeeCost> miningExistingAssetsCost;
	private List<EmployeeCost> requirementsEngineeringCost;
	private List<EmployeeCost> softwareSystemIntegrationCost;
	private List<EmployeeCost> testingCost;
	private List<EmployeeCost> understandingRelevantDomainsCost;
	private List<EmployeeCost> usingExternallyAvailableSoftwareCost;
	private List<EmployeeCost> configurationManagementCost;
	private List<EmployeeCost> commissionAnalysisCost;
	private List<EmployeeCost> measurementTrackingCost;
	private List<EmployeeCost> processDisciplineCost;
	private List<EmployeeCost> scopingCost;
	private List<EmployeeCost> technicalPlanningCost;
	private List<EmployeeCost> technicalRiskManagementCost;
	private List<EmployeeCost> toolSupportCost;
	private List<EmployeeCost> buildingBusinessCaseCost;
	private List<EmployeeCost> customerInterfaceManagementCost;
	private List<EmployeeCost> developingAcquisitionStrategyCost;
	private List<EmployeeCost> fundingCost;
	private List<EmployeeCost> launchingInstitutionalizingCost;
	private List<EmployeeCost> marketAnalysisCost;
	private List<EmployeeCost> operationsCost;
	private List<EmployeeCost> organizationalPlanningCost;
	private List<EmployeeCost> organizationalRiskManagementCost;
	private List<EmployeeCost> structuringOrganizationCost;
	private List<EmployeeCost> technologyForecastingCost;
	private List<EmployeeCost> trainingCost;
	private Map<String, List<EmployeeCost>> otherCosts;
	
	public String getComponentName() {
		return componentName;
	}
	
	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}
	
	public Integer getI() {
		return i;
	}
	
	public void setI(Integer i) {
		this.i = i;
	}
	
	public Integer getT() {
		return t;
	}
	
	public void setT(Integer t) {
		this.t = t;
	}
	
	private String listToString(List<EmployeeCost> list) {
		String str = "";
		boolean first = true;
		for(EmployeeCost ec : list) {
			str += (!first ? "," : "") + ec.toString(); 
			first = false;
		}
		return str;
	}
	
	public List<EmployeeCost> getArchitectureDefinitionCost() {
		if(architectureDefinitionCost == null) {
			architectureDefinitionCost = new ArrayList<EmployeeCost>();
		}
		return architectureDefinitionCost.stream().filter(x -> (x.costPerEmployee != null && x.costPerEmployee.doubleValue() > 0.0) || x.fixedCost.doubleValue() > 0.0).collect(Collectors.toList());
	}
	
	public String getArchitectureDefinitionCostAsString() {
		return listToString(getArchitectureDefinitionCost());
	}
	
	public void setArchitectureDefinitionCost(List<EmployeeCost> architectureDefinitionCost) {
		this.architectureDefinitionCost = architectureDefinitionCost;
	}
	
	public List<EmployeeCost> getArchitectureEvaluationCost() {
		if(architectureEvaluationCost == null) {
			architectureEvaluationCost = new ArrayList<EmployeeCost>();
		}
		return architectureEvaluationCost.stream().filter(x -> (x.costPerEmployee != null && x.costPerEmployee.doubleValue() > 0.0) || x.fixedCost.doubleValue() > 0.0).collect(Collectors.toList());
	}
	
	public String getArchitectureEvaluationCostAsString() {
		return listToString(getArchitectureEvaluationCost());
	}
	
	public void setArchitectureEvaluationCost(List<EmployeeCost> architectureEvaluationCost) {
		this.architectureEvaluationCost = architectureEvaluationCost;
	}
	
	public List<EmployeeCost> getComponentDevelopmentCost() {
		if(componentDevelopmentCost == null) {
			componentDevelopmentCost = new ArrayList<EmployeeCost>();
		}
		return componentDevelopmentCost.stream().filter(x -> (x.costPerEmployee != null && x.costPerEmployee.doubleValue() > 0.0) || x.fixedCost.doubleValue() > 0.0).collect(Collectors.toList());
	}
	
	public String getComponentDevelopmentCostAsString() {
		return listToString(getComponentDevelopmentCost());
	}
	
	public void setComponentDevelopmentCost(List<EmployeeCost> componentDevelopmentCost) {
		this.componentDevelopmentCost = componentDevelopmentCost;
	}	
	
	public List<EmployeeCost> getMiningExistingAssetsCost() {
		if(miningExistingAssetsCost == null) {
			miningExistingAssetsCost = new ArrayList<EmployeeCost>();
		}
		return miningExistingAssetsCost.stream().filter(x -> (x.costPerEmployee != null && x.costPerEmployee.doubleValue() > 0.0) || x.fixedCost.doubleValue() > 0.0).collect(Collectors.toList());
	}
	
	public String getMiningExistingAssetsCostAsString() {
		return listToString(getMiningExistingAssetsCost());
	}
	
	public void setMiningExistingAssetsCost(List<EmployeeCost> miningExistingAssetsCost) {
		this.miningExistingAssetsCost = miningExistingAssetsCost;
	}
	
	public List<EmployeeCost> getRequirementsEngineeringCost() {
		if(requirementsEngineeringCost == null) {
			requirementsEngineeringCost = new ArrayList<EmployeeCost>();
		}
		return requirementsEngineeringCost.stream().filter(x -> (x.costPerEmployee != null && x.costPerEmployee.doubleValue() > 0.0) || x.fixedCost.doubleValue() > 0.0).collect(Collectors.toList());
	}
	
	public String getRequirementsEngineeringCostAsString() {
		return listToString(getRequirementsEngineeringCost());
	}
	
	public void setRequirementsEngineeringCost(List<EmployeeCost> requirementsEngineeringCost) {
		this.requirementsEngineeringCost = requirementsEngineeringCost;
	}
	
	public List<EmployeeCost> getSoftwareSystemIntegrationCost() {
		if(softwareSystemIntegrationCost == null) {
			softwareSystemIntegrationCost = new ArrayList<EmployeeCost>();
		}
		return softwareSystemIntegrationCost.stream().filter(x -> (x.costPerEmployee != null && x.costPerEmployee.doubleValue() > 0.0) || x.fixedCost.doubleValue() > 0.0).collect(Collectors.toList());
	}
	
	public String getSoftwareSystemIntegrationCostAsString() {
		return listToString(getSoftwareSystemIntegrationCost());
	}
	
	public void setSoftwareSystemIntegrationCost(List<EmployeeCost> softwareSystemIntegrationCost) {
		this.softwareSystemIntegrationCost = softwareSystemIntegrationCost;
	}
	
	public List<EmployeeCost> getTestingCost() {
		if(testingCost == null) {
			testingCost = new ArrayList<EmployeeCost>();
		}
		return testingCost.stream().filter(x -> (x.costPerEmployee != null && x.costPerEmployee.doubleValue() > 0.0) || x.fixedCost.doubleValue() > 0.0).collect(Collectors.toList());
	}
	
	public String getTestingCostAsString() {
		return listToString(getTestingCost());
	}
	
	public void setTestingCost(List<EmployeeCost> testingCost) {
		this.testingCost = testingCost;
	}
	
	public List<EmployeeCost> getUnderstandingRelevantDomainsCost() {
		if(understandingRelevantDomainsCost == null) {
			understandingRelevantDomainsCost = new ArrayList<EmployeeCost>();
		}
		return understandingRelevantDomainsCost.stream().filter(x -> (x.costPerEmployee != null && x.costPerEmployee.doubleValue() > 0.0) || x.fixedCost.doubleValue() > 0.0).collect(Collectors.toList());
	}
	
	public String getUnderstandingRelevantDomainsCostAsString() {
		return listToString(getUnderstandingRelevantDomainsCost());
	}
	
	public void setUnderstandingRelevantDomainsCost(List<EmployeeCost> understandingRelevantDomainsCost) {
		this.understandingRelevantDomainsCost = understandingRelevantDomainsCost;
	}
	
	public List<EmployeeCost> getUsingExternallyAvailableSoftwareCost() {
		if(usingExternallyAvailableSoftwareCost == null) {
			usingExternallyAvailableSoftwareCost = new ArrayList<EmployeeCost>();
		}
		return usingExternallyAvailableSoftwareCost.stream().filter(x -> (x.costPerEmployee != null && x.costPerEmployee.doubleValue() > 0.0) || x.fixedCost.doubleValue() > 0.0).collect(Collectors.toList());
	}
	
	public String getUsingExternallyAvailableSoftwareCostAsString() {
		return listToString(getUsingExternallyAvailableSoftwareCost());
	}
	
	public void setUsingExternallyAvailableSoftwareCost(List<EmployeeCost> usingExternallyAvailableSoftwareCost) {
		this.usingExternallyAvailableSoftwareCost = usingExternallyAvailableSoftwareCost;
	}
	
	public List<EmployeeCost> getConfigurationManagementCost() {
		if(configurationManagementCost == null) {
			configurationManagementCost = new ArrayList<EmployeeCost>();
		}
		return configurationManagementCost.stream().filter(x -> (x.costPerEmployee != null && x.costPerEmployee.doubleValue() > 0.0) || x.fixedCost.doubleValue() > 0.0).collect(Collectors.toList());
	}
	
	public String getConfigurationManagementCostAsString() {
		return listToString(getConfigurationManagementCost());
	}
	
	public void setConfigurationManagementCost(List<EmployeeCost> configurationManagementCost) {
		this.configurationManagementCost = configurationManagementCost;
	}
	
	public List<EmployeeCost> getCommissionAnalysisCost() {
		if(commissionAnalysisCost == null) {
			commissionAnalysisCost = new ArrayList<EmployeeCost>();
		}
		return commissionAnalysisCost.stream().filter(x -> (x.costPerEmployee != null && x.costPerEmployee.doubleValue() > 0.0) || x.fixedCost.doubleValue() > 0.0).collect(Collectors.toList());
	}
	
	public String getCommissionAnalysisCostAsString() {
		return listToString(getCommissionAnalysisCost());
	}
	
	public void setCommissionAnalysisCost(List<EmployeeCost> commissionAnalysisCost) {
		this.commissionAnalysisCost = commissionAnalysisCost;
	}
	
	public List<EmployeeCost> getMeasurementTrackingCost() {
		if(measurementTrackingCost == null) {
			measurementTrackingCost = new ArrayList<EmployeeCost>();
		}
		return measurementTrackingCost.stream().filter(x -> (x.costPerEmployee != null && x.costPerEmployee.doubleValue() > 0.0) || x.fixedCost.doubleValue() > 0.0).collect(Collectors.toList());
	}
	
	public String getMeasurementTrackingCostAsString() {
		return listToString(getMeasurementTrackingCost());
	}
	
	public void setMeasurementTrackingCost(List<EmployeeCost> measurementTrackingCost) {
		this.measurementTrackingCost = measurementTrackingCost;
	}
	
	public List<EmployeeCost> getProcessDisciplineCost() {
		if(processDisciplineCost == null) {
			processDisciplineCost = new ArrayList<EmployeeCost>();
		}
		return processDisciplineCost.stream().filter(x -> (x.costPerEmployee != null && x.costPerEmployee.doubleValue() > 0.0) || x.fixedCost.doubleValue() > 0.0).collect(Collectors.toList());
	}
	
	public String getProcessDisciplineCostAsString() {
		return listToString(getProcessDisciplineCost());
	}
	
	public void setProcessDisciplineCost(List<EmployeeCost> processDisciplineCost) {
		this.processDisciplineCost = processDisciplineCost;
	}
	
	public List<EmployeeCost> getScopingCost() {
		if(scopingCost == null) {
			scopingCost = new ArrayList<EmployeeCost>();
		}
		return scopingCost.stream().filter(x -> (x.costPerEmployee != null && x.costPerEmployee.doubleValue() > 0.0) || x.fixedCost.doubleValue() > 0.0).collect(Collectors.toList());
	}
	
	public String getScopingCostAsString() {
		return listToString(getScopingCost());
	}
	
	public void setScopingCost(List<EmployeeCost> scopingCost) {
		this.scopingCost = scopingCost;
	}
	
	public List<EmployeeCost> getTechnicalPlanningCost() {
		if(technicalPlanningCost == null) {
			technicalPlanningCost = new ArrayList<EmployeeCost>();
		}
		return technicalPlanningCost.stream().filter(x -> (x.costPerEmployee != null && x.costPerEmployee.doubleValue() > 0.0) || x.fixedCost.doubleValue() > 0.0).collect(Collectors.toList());
	}
	
	public String getTechnicalPlanningCostAsString() {
		return listToString(getTechnicalPlanningCost());
	}
	
	public void setTechnicalPlanningCost(List<EmployeeCost> technicalPlanningCost) {
		this.technicalPlanningCost = technicalPlanningCost;
	}
	
	public List<EmployeeCost> getTechnicalRiskManagementCost() {
		if(technicalRiskManagementCost == null) {
			technicalRiskManagementCost = new ArrayList<EmployeeCost>();
		}
		return technicalRiskManagementCost.stream().filter(x -> (x.costPerEmployee != null && x.costPerEmployee.doubleValue() > 0.0) || x.fixedCost.doubleValue() > 0.0).collect(Collectors.toList());
	}
	
	public String getTechnicalRiskManagementCostAsString() {
		return listToString(getTechnicalRiskManagementCost());
	}
	
	public void setTechnicalRiskManagementCost(List<EmployeeCost> technicalRiskManagementCost) {
		this.technicalRiskManagementCost = technicalRiskManagementCost;
	}
	
	public List<EmployeeCost> getToolSupportCost() {
		if(toolSupportCost == null) {
			toolSupportCost = new ArrayList<EmployeeCost>();
		}
		return toolSupportCost.stream().filter(x -> (x.costPerEmployee != null && x.costPerEmployee.doubleValue() > 0.0) || x.fixedCost.doubleValue() > 0.0).collect(Collectors.toList());
	}
	
	public String getToolSupportCostAsString() {
		return listToString(getToolSupportCost());
	}
	
	public void setToolSupportCost(List<EmployeeCost> toolSupportCost) {
		this.toolSupportCost = toolSupportCost;
	}
	
	public List<EmployeeCost> getBuildingBusinessCaseCost() {
		if(buildingBusinessCaseCost == null) {
			buildingBusinessCaseCost = new ArrayList<EmployeeCost>();
		}
		return buildingBusinessCaseCost.stream().filter(x -> (x.costPerEmployee != null && x.costPerEmployee.doubleValue() > 0.0) || x.fixedCost.doubleValue() > 0.0).collect(Collectors.toList());
	}
	
	public String getBuildingBusinessCaseCostAsString() {
		return listToString(getBuildingBusinessCaseCost());
	}
	
	public void setBuildingBusinessCaseCost(List<EmployeeCost> buildingBusinessCaseCost) {
		this.buildingBusinessCaseCost = buildingBusinessCaseCost;
	}
	
	public List<EmployeeCost> getCustomerInterfaceManagementCost() {
		if(customerInterfaceManagementCost == null) {
			customerInterfaceManagementCost = new ArrayList<EmployeeCost>();
		}
		return customerInterfaceManagementCost.stream().filter(x -> (x.costPerEmployee != null && x.costPerEmployee.doubleValue() > 0.0) || x.fixedCost.doubleValue() > 0.0).collect(Collectors.toList());
	}
	
	public String getCustomerInterfaceManagementCostAsString() {
		return listToString(getCustomerInterfaceManagementCost());
	}
	
	public void setCustomerInterfaceManagementCost(List<EmployeeCost> customerInterfaceManagementCost) {
		this.customerInterfaceManagementCost = customerInterfaceManagementCost;
	}
	
	public List<EmployeeCost> getDevelopingAcquisitionStrategyCost() {
		if(developingAcquisitionStrategyCost == null) {
			developingAcquisitionStrategyCost = new ArrayList<EmployeeCost>();
		}
		return developingAcquisitionStrategyCost.stream().filter(x -> (x.costPerEmployee != null && x.costPerEmployee.doubleValue() > 0.0) || x.fixedCost.doubleValue() > 0.0).collect(Collectors.toList());
	}
	
	public String getDevelopingAcquisitionStrategyCostAsString() {
		return listToString(getDevelopingAcquisitionStrategyCost());
	}
	
	public void setDevelopingAcquisitionStrategyCost(List<EmployeeCost> developingAcquisitionStrategyCost) {
		this.developingAcquisitionStrategyCost = developingAcquisitionStrategyCost;
	}
	
	public List<EmployeeCost> getFundingCost() {
		if(fundingCost == null) {
			fundingCost = new ArrayList<EmployeeCost>();
		}
		return fundingCost.stream().filter(x -> (x.costPerEmployee != null && x.costPerEmployee.doubleValue() > 0.0) || x.fixedCost.doubleValue() > 0.0).collect(Collectors.toList());
	}
	
	public String getFundingCostAsString() {
		return listToString(getFundingCost());
	}
	
	public void setFundingCost(List<EmployeeCost> fundingCost) {
		this.fundingCost = fundingCost;
	}
	
	public List<EmployeeCost> getLaunchingInstitutionalizingCost() {
		if(launchingInstitutionalizingCost == null) {
			launchingInstitutionalizingCost = new ArrayList<EmployeeCost>();
		}
		return launchingInstitutionalizingCost.stream().filter(x -> (x.costPerEmployee != null && x.costPerEmployee.doubleValue() > 0.0) || x.fixedCost.doubleValue() > 0.0).collect(Collectors.toList());
	}
	
	public String getLaunchingInstitutionalizingCostAsString() {
		return listToString(getLaunchingInstitutionalizingCost());
	}
	
	public void setLaunchingInstitutionalizingCost(List<EmployeeCost> launchingInstitutionalizingCost) {
		this.launchingInstitutionalizingCost = launchingInstitutionalizingCost;
	}
	
	public List<EmployeeCost> getMarketAnalysisCost() {
		if(marketAnalysisCost == null) {
			marketAnalysisCost = new ArrayList<EmployeeCost>();
		}
		return marketAnalysisCost.stream().filter(x -> (x.costPerEmployee != null && x.costPerEmployee.doubleValue() > 0.0) || x.fixedCost.doubleValue() > 0.0).collect(Collectors.toList());
	}
	
	public String getMarketAnalysisCostAsString() {
		return listToString(getMarketAnalysisCost());
	}
	
	public void setMarketAnalysisCost(List<EmployeeCost> marketAnalysisCost) {
		this.marketAnalysisCost = marketAnalysisCost;
	}
	
	public List<EmployeeCost> getOperationsCost() {
		if(operationsCost == null) {
			operationsCost = new ArrayList<EmployeeCost>();
		}
		return operationsCost.stream().filter(x -> (x.costPerEmployee != null && x.costPerEmployee.doubleValue() > 0.0) || x.fixedCost.doubleValue() > 0.0).collect(Collectors.toList());
	}
	
	public String getOperationsCostAsString() {
		return listToString(getOperationsCost());
	}
	
	public void setOperationsCost(List<EmployeeCost> operationsCost) {
		this.operationsCost = operationsCost;
	}
	
	public List<EmployeeCost> getOrganizationalPlanningCost() {
		if(organizationalPlanningCost == null) {
			organizationalPlanningCost = new ArrayList<EmployeeCost>();
		}
		return organizationalPlanningCost.stream().filter(x -> (x.costPerEmployee != null && x.costPerEmployee.doubleValue() > 0.0) || x.fixedCost.doubleValue() > 0.0).collect(Collectors.toList());
	}
	
	public String getOrganizationalPlanningCostAsString() {
		return listToString(getOrganizationalPlanningCost());
	}
	
	public void setOrganizationalPlanningCost(List<EmployeeCost> organizationalPlanningCost) {
		this.organizationalPlanningCost = organizationalPlanningCost;
	}
	
	public List<EmployeeCost> getOrganizationalRiskManagementCost() {
		if(organizationalRiskManagementCost == null) {
			organizationalRiskManagementCost = new ArrayList<EmployeeCost>();
		}
		return organizationalRiskManagementCost.stream().filter(x -> (x.costPerEmployee != null && x.costPerEmployee.doubleValue() > 0.0) || x.fixedCost.doubleValue() > 0.0).collect(Collectors.toList());
	}
	
	public String getOrganizationalRiskManagementCostAsString() {
		return listToString(getOrganizationalRiskManagementCost());
	}
	
	public void setOrganizationalRiskManagementCost(List<EmployeeCost> organizationalRiskManagementCost) {
		this.organizationalRiskManagementCost = organizationalRiskManagementCost;
	}
	
	public List<EmployeeCost> getStructuringOrganizationCost() {
		if(structuringOrganizationCost == null) {
			structuringOrganizationCost = new ArrayList<EmployeeCost>();
		}
		return structuringOrganizationCost.stream().filter(x -> (x.costPerEmployee != null && x.costPerEmployee.doubleValue() > 0.0) || x.fixedCost.doubleValue() > 0.0).collect(Collectors.toList());
	}
	
	public String getStructuringOrganizationCostAsString() {
		return listToString(getStructuringOrganizationCost());
	}
	
	public void setStructuringOrganizationCost(List<EmployeeCost> structuringOrganizationCost) {
		this.structuringOrganizationCost = structuringOrganizationCost;
	}
	
	public List<EmployeeCost> getTechnologyForecastingCost() {
		if(technologyForecastingCost == null) {
			technologyForecastingCost = new ArrayList<EmployeeCost>();
		}
		return technologyForecastingCost.stream().filter(x -> (x.costPerEmployee != null && x.costPerEmployee.doubleValue() > 0.0) || x.fixedCost.doubleValue() > 0.0).collect(Collectors.toList());
	}
	
	public String getTechnologyForecastingCostAsString() {
		return listToString(getTechnologyForecastingCost());
	}
	
	public void setTechnologyForecastingCost(List<EmployeeCost> technologyForecastingCost) {
		this.technologyForecastingCost = technologyForecastingCost;
	}
	
	public List<EmployeeCost> getTrainingCost() {
		if(trainingCost == null) {
			trainingCost = new ArrayList<EmployeeCost>();
		}
		return trainingCost.stream().filter(x -> (x.costPerEmployee != null && x.costPerEmployee.doubleValue() > 0.0) || x.fixedCost.doubleValue() > 0.0).collect(Collectors.toList());
	}
	
	public String getTrainingCostAsString() {
		return listToString(getTrainingCost());
	}
	
	public void setTrainingCost(List<EmployeeCost> trainingCost) {
		this.trainingCost = trainingCost;
	}
	
	public Map<String, List<EmployeeCost>> getOtherCosts() {
		if(otherCosts == null) {
			otherCosts = new HashMap<String, List<EmployeeCost>>();
		}
		return otherCosts;
	}
	
	public String getOtherCostAsString(String key) {
		if(getOtherCosts().containsKey(key)) {
			return listToString(getOtherCosts().get(key).stream().filter(x -> (x.costPerEmployee != null && x.costPerEmployee.doubleValue() > 0.0) || x.fixedCost.doubleValue() > 0.0).collect(Collectors.toList()));
		} else {
			return "";
		}
	}
	
	public String toCSVString() {
		String str = "";
		str += componentName + ",";
		str += i.toString() + ",";
		str += t.toString() + ",";
		str += "\"" + getArchitectureDefinitionCostAsString() + "\",";
		str += "\"" + getArchitectureEvaluationCostAsString() + "\",";
		str += "\"" + getComponentDevelopmentCostAsString() + "\",";
		str += "\"" + getMiningExistingAssetsCostAsString() + "\",";
		str += "\"" + getRequirementsEngineeringCostAsString() + "\",";
		str += "\"" + getSoftwareSystemIntegrationCostAsString() + "\",";
		str += "\"" + getTestingCostAsString() + "\",";
		str += "\"" + getUnderstandingRelevantDomainsCostAsString() + "\",";
		str += "\"" + getUsingExternallyAvailableSoftwareCostAsString() + "\",";
		str += "\"" + getConfigurationManagementCostAsString() + "\",";
		str += "\"" + getCommissionAnalysisCostAsString() + "\",";
		str += "\"" + getMeasurementTrackingCostAsString() + "\",";
		str += "\"" + getProcessDisciplineCostAsString() + "\",";
		str += "\"" + getScopingCostAsString() + "\",";
		str += "\"" + getTechnicalPlanningCostAsString() + "\",";
		str += "\"" + getTechnicalRiskManagementCostAsString() + "\",";
		str += "\"" + getToolSupportCostAsString() + "\",";
		str += "\"" + getBuildingBusinessCaseCostAsString() + "\",";
		str += "\"" + getCustomerInterfaceManagementCostAsString() + "\",";
		str += "\"" + getDevelopingAcquisitionStrategyCostAsString() + "\",";
		str += "\"" + getFundingCostAsString() + "\",";
		str += "\"" + getLaunchingInstitutionalizingCostAsString() + "\",";
		str += "\"" + getMarketAnalysisCostAsString() + "\",";
		str += "\"" + getOperationsCostAsString() + "\",";
		str += "\"" + getOrganizationalPlanningCostAsString() + "\",";
		str += "\"" + getOrganizationalRiskManagementCostAsString() + "\",";
		str += "\"" + getStructuringOrganizationCostAsString() + "\",";
		str += "\"" + getTechnologyForecastingCostAsString() + "\",";
		str += "\"" + getTrainingCostAsString() + "\"";
		return str;
	}
}
