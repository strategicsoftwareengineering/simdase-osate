/**
 */
package edu.clemson.simdase.simdase;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>If</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.clemson.simdase.simdase.If#getBooleanStatement <em>Boolean Statement</em>}</li>
 *   <li>{@link edu.clemson.simdase.simdase.If#getTrueStatement <em>True Statement</em>}</li>
 *   <li>{@link edu.clemson.simdase.simdase.If#getFalseStatement <em>False Statement</em>}</li>
 * </ul>
 *
 * @see edu.clemson.simdase.simdase.SimdasePackage#getIf()
 * @model
 * @generated
 */
public interface If extends StatementLvl4
{
  /**
   * Returns the value of the '<em><b>Boolean Statement</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Boolean Statement</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Boolean Statement</em>' containment reference.
   * @see #setBooleanStatement(BooleanStatement)
   * @see edu.clemson.simdase.simdase.SimdasePackage#getIf_BooleanStatement()
   * @model containment="true"
   * @generated
   */
  BooleanStatement getBooleanStatement();

  /**
   * Sets the value of the '{@link edu.clemson.simdase.simdase.If#getBooleanStatement <em>Boolean Statement</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Boolean Statement</em>' containment reference.
   * @see #getBooleanStatement()
   * @generated
   */
  void setBooleanStatement(BooleanStatement value);

  /**
   * Returns the value of the '<em><b>True Statement</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>True Statement</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>True Statement</em>' containment reference.
   * @see #setTrueStatement(Statement)
   * @see edu.clemson.simdase.simdase.SimdasePackage#getIf_TrueStatement()
   * @model containment="true"
   * @generated
   */
  Statement getTrueStatement();

  /**
   * Sets the value of the '{@link edu.clemson.simdase.simdase.If#getTrueStatement <em>True Statement</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>True Statement</em>' containment reference.
   * @see #getTrueStatement()
   * @generated
   */
  void setTrueStatement(Statement value);

  /**
   * Returns the value of the '<em><b>False Statement</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>False Statement</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>False Statement</em>' containment reference.
   * @see #setFalseStatement(Statement)
   * @see edu.clemson.simdase.simdase.SimdasePackage#getIf_FalseStatement()
   * @model containment="true"
   * @generated
   */
  Statement getFalseStatement();

  /**
   * Sets the value of the '{@link edu.clemson.simdase.simdase.If#getFalseStatement <em>False Statement</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>False Statement</em>' containment reference.
   * @see #getFalseStatement()
   * @generated
   */
  void setFalseStatement(Statement value);

} // If
