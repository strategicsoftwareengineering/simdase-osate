/*
 * generated by Xtext
 */
package edu.clemson.simdase.parser.antlr;

import java.io.InputStream;
import org.eclipse.xtext.parser.antlr.IAntlrTokenFileProvider;

public class SimdaseAntlrTokenFileProvider implements IAntlrTokenFileProvider {
	
	@Override
	public InputStream getAntlrTokenFile() {
		ClassLoader classLoader = getClass().getClassLoader();
    	return classLoader.getResourceAsStream("edu/clemson/simdase/parser/antlr/internal/InternalSimdaseParser.tokens");
	}
}
