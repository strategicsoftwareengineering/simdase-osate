/**
 */
package edu.clemson.simdase.simdase.impl;

import edu.clemson.simdase.simdase.SimdasePackage;
import edu.clemson.simdase.simdase.T;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>T</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class TImpl extends StatementLvl4Impl implements T
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected TImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return SimdasePackage.Literals.T;
  }

} //TImpl
