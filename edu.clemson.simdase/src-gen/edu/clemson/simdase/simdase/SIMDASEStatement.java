/**
 */
package edu.clemson.simdase.simdase;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SIMDASE Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.clemson.simdase.simdase.SimdasePackage#getSIMDASEStatement()
 * @model
 * @generated
 */
public interface SIMDASEStatement extends EObject
{
} // SIMDASEStatement
