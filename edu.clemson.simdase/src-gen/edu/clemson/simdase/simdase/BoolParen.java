/**
 */
package edu.clemson.simdase.simdase;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Bool Paren</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.clemson.simdase.simdase.BoolParen#getStatement <em>Statement</em>}</li>
 * </ul>
 *
 * @see edu.clemson.simdase.simdase.SimdasePackage#getBoolParen()
 * @model
 * @generated
 */
public interface BoolParen extends BooleanStatementLvl8
{
  /**
   * Returns the value of the '<em><b>Statement</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Statement</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Statement</em>' containment reference.
   * @see #setStatement(BooleanStatement)
   * @see edu.clemson.simdase.simdase.SimdasePackage#getBoolParen_Statement()
   * @model containment="true"
   * @generated
   */
  BooleanStatement getStatement();

  /**
   * Sets the value of the '{@link edu.clemson.simdase.simdase.BoolParen#getStatement <em>Statement</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Statement</em>' containment reference.
   * @see #getStatement()
   * @generated
   */
  void setStatement(BooleanStatement value);

} // BoolParen
