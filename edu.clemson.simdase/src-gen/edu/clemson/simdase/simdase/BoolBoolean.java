/**
 */
package edu.clemson.simdase.simdase;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Bool Boolean</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.clemson.simdase.simdase.BoolBoolean#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see edu.clemson.simdase.simdase.SimdasePackage#getBoolBoolean()
 * @model
 * @generated
 */
public interface BoolBoolean extends BooleanStatementLvl8
{
  /**
   * Returns the value of the '<em><b>Value</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Value</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Value</em>' containment reference.
   * @see #setValue(edu.clemson.simdase.simdase.Boolean)
   * @see edu.clemson.simdase.simdase.SimdasePackage#getBoolBoolean_Value()
   * @model containment="true"
   * @generated
   */
  edu.clemson.simdase.simdase.Boolean getValue();

  /**
   * Sets the value of the '{@link edu.clemson.simdase.simdase.BoolBoolean#getValue <em>Value</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Value</em>' containment reference.
   * @see #getValue()
   * @generated
   */
  void setValue(edu.clemson.simdase.simdase.Boolean value);

} // BoolBoolean
