/**
 */
package edu.clemson.simdase.simdase.util;

import edu.clemson.simdase.simdase.ActiveVariant;
import edu.clemson.simdase.simdase.ActiveVariants;
import edu.clemson.simdase.simdase.BoolBoolean;
import edu.clemson.simdase.simdase.BoolCos;
import edu.clemson.simdase.simdase.BoolI;
import edu.clemson.simdase.simdase.BoolInteger;
import edu.clemson.simdase.simdase.BoolParen;
import edu.clemson.simdase.simdase.BoolRandom;
import edu.clemson.simdase.simdase.BoolScalingFactor;
import edu.clemson.simdase.simdase.BoolSin;
import edu.clemson.simdase.simdase.BoolT;
import edu.clemson.simdase.simdase.BoolTan;
import edu.clemson.simdase.simdase.BooleanStatement;
import edu.clemson.simdase.simdase.BooleanStatementLvl2;
import edu.clemson.simdase.simdase.BooleanStatementLvl3;
import edu.clemson.simdase.simdase.BooleanStatementLvl4;
import edu.clemson.simdase.simdase.BooleanStatementLvl5;
import edu.clemson.simdase.simdase.BooleanStatementLvl6;
import edu.clemson.simdase.simdase.BooleanStatementLvl7;
import edu.clemson.simdase.simdase.BooleanStatementLvl8;
import edu.clemson.simdase.simdase.Contract;
import edu.clemson.simdase.simdase.Cos;
import edu.clemson.simdase.simdase.CostProperties;
import edu.clemson.simdase.simdase.CostStatement;
import edu.clemson.simdase.simdase.CostValue;
import edu.clemson.simdase.simdase.EmployeeStatement;
import edu.clemson.simdase.simdase.FixedCost;
import edu.clemson.simdase.simdase.HourlyCost;
import edu.clemson.simdase.simdase.I;
import edu.clemson.simdase.simdase.If;
import edu.clemson.simdase.simdase.IsAdaptiveStatement;
import edu.clemson.simdase.simdase.Paren;
import edu.clemson.simdase.simdase.Random;
import edu.clemson.simdase.simdase.ReEvaluateAveraging;
import edu.clemson.simdase.simdase.SIMDASEStatement;
import edu.clemson.simdase.simdase.ScalingFactor;
import edu.clemson.simdase.simdase.SimdaseContract;
import edu.clemson.simdase.simdase.SimdaseContractLibrary;
import edu.clemson.simdase.simdase.SimdaseContractSubclause;
import edu.clemson.simdase.simdase.SimdaseLibrary;
import edu.clemson.simdase.simdase.SimdasePackage;
import edu.clemson.simdase.simdase.SimdaseSubclause;
import edu.clemson.simdase.simdase.Sin;
import edu.clemson.simdase.simdase.Statement;
import edu.clemson.simdase.simdase.StatementLvl2;
import edu.clemson.simdase.simdase.StatementLvl3;
import edu.clemson.simdase.simdase.StatementLvl4;
import edu.clemson.simdase.simdase.StatementScalingFactor;
import edu.clemson.simdase.simdase.T;
import edu.clemson.simdase.simdase.TMax;
import edu.clemson.simdase.simdase.TMin;
import edu.clemson.simdase.simdase.Tan;
import edu.clemson.simdase.simdase.Variant;
import edu.clemson.simdase.simdase.VariantNames;
import edu.clemson.simdase.simdase.Variants;
import edu.clemson.simdase.simdase.WithNot;
import edu.clemson.simdase.simdase.WithoutNot;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

import org.osate.aadl2.AnnexLibrary;
import org.osate.aadl2.AnnexSubclause;
import org.osate.aadl2.Element;
import org.osate.aadl2.ModalElement;
import org.osate.aadl2.NamedElement;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see edu.clemson.simdase.simdase.SimdasePackage
 * @generated
 */
public class SimdaseSwitch<T1> extends Switch<T1>
{
  /**
   * The cached model package
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected static SimdasePackage modelPackage;

  /**
   * Creates an instance of the switch.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SimdaseSwitch()
  {
    if (modelPackage == null)
    {
      modelPackage = SimdasePackage.eINSTANCE;
    }
  }

  /**
   * Checks whether this is a switch for the given package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param ePackage the package in question.
   * @return whether this is a switch for the given package.
   * @generated
   */
  @Override
  protected boolean isSwitchFor(EPackage ePackage)
  {
    return ePackage == modelPackage;
  }

  /**
   * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the first non-null result returned by a <code>caseXXX</code> call.
   * @generated
   */
  @Override
  protected T1 doSwitch(int classifierID, EObject theEObject)
  {
    switch (classifierID)
    {
      case SimdasePackage.SIMDASE_LIBRARY:
      {
        SimdaseLibrary simdaseLibrary = (SimdaseLibrary)theEObject;
        T1 result = caseSimdaseLibrary(simdaseLibrary);
        if (result == null) result = caseAnnexLibrary(simdaseLibrary);
        if (result == null) result = caseNamedElement(simdaseLibrary);
        if (result == null) result = caseElement(simdaseLibrary);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SimdasePackage.SIMDASE_SUBCLAUSE:
      {
        SimdaseSubclause simdaseSubclause = (SimdaseSubclause)theEObject;
        T1 result = caseSimdaseSubclause(simdaseSubclause);
        if (result == null) result = caseAnnexSubclause(simdaseSubclause);
        if (result == null) result = caseModalElement(simdaseSubclause);
        if (result == null) result = caseNamedElement(simdaseSubclause);
        if (result == null) result = caseElement(simdaseSubclause);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SimdasePackage.CONTRACT:
      {
        Contract contract = (Contract)theEObject;
        T1 result = caseContract(contract);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SimdasePackage.SIMDASE_STATEMENT:
      {
        SIMDASEStatement simdaseStatement = (SIMDASEStatement)theEObject;
        T1 result = caseSIMDASEStatement(simdaseStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SimdasePackage.VARIANTS:
      {
        Variants variants = (Variants)theEObject;
        T1 result = caseVariants(variants);
        if (result == null) result = caseSIMDASEStatement(variants);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SimdasePackage.VARIANT:
      {
        Variant variant = (Variant)theEObject;
        T1 result = caseVariant(variant);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SimdasePackage.ACTIVE_VARIANTS:
      {
        ActiveVariants activeVariants = (ActiveVariants)theEObject;
        T1 result = caseActiveVariants(activeVariants);
        if (result == null) result = caseSIMDASEStatement(activeVariants);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SimdasePackage.ACTIVE_VARIANT:
      {
        ActiveVariant activeVariant = (ActiveVariant)theEObject;
        T1 result = caseActiveVariant(activeVariant);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SimdasePackage.VARIANT_NAMES:
      {
        VariantNames variantNames = (VariantNames)theEObject;
        T1 result = caseVariantNames(variantNames);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SimdasePackage.COST_PROPERTIES:
      {
        CostProperties costProperties = (CostProperties)theEObject;
        T1 result = caseCostProperties(costProperties);
        if (result == null) result = caseSIMDASEStatement(costProperties);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SimdasePackage.COST_STATEMENT:
      {
        CostStatement costStatement = (CostStatement)theEObject;
        T1 result = caseCostStatement(costStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SimdasePackage.COST_VALUE:
      {
        CostValue costValue = (CostValue)theEObject;
        T1 result = caseCostValue(costValue);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SimdasePackage.EMPLOYEE_STATEMENT:
      {
        EmployeeStatement employeeStatement = (EmployeeStatement)theEObject;
        T1 result = caseEmployeeStatement(employeeStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SimdasePackage.BOOLEAN:
      {
        edu.clemson.simdase.simdase.Boolean boolean_ = (edu.clemson.simdase.simdase.Boolean)theEObject;
        T1 result = caseBoolean(boolean_);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SimdasePackage.NUMBER:
      {
        edu.clemson.simdase.simdase.Number number = (edu.clemson.simdase.simdase.Number)theEObject;
        T1 result = caseNumber(number);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SimdasePackage.STATEMENT:
      {
        Statement statement = (Statement)theEObject;
        T1 result = caseStatement(statement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SimdasePackage.STATEMENT_LVL2:
      {
        StatementLvl2 statementLvl2 = (StatementLvl2)theEObject;
        T1 result = caseStatementLvl2(statementLvl2);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SimdasePackage.STATEMENT_LVL3:
      {
        StatementLvl3 statementLvl3 = (StatementLvl3)theEObject;
        T1 result = caseStatementLvl3(statementLvl3);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SimdasePackage.STATEMENT_LVL4:
      {
        StatementLvl4 statementLvl4 = (StatementLvl4)theEObject;
        T1 result = caseStatementLvl4(statementLvl4);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SimdasePackage.BOOLEAN_STATEMENT:
      {
        BooleanStatement booleanStatement = (BooleanStatement)theEObject;
        T1 result = caseBooleanStatement(booleanStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SimdasePackage.BOOLEAN_STATEMENT_LVL2:
      {
        BooleanStatementLvl2 booleanStatementLvl2 = (BooleanStatementLvl2)theEObject;
        T1 result = caseBooleanStatementLvl2(booleanStatementLvl2);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SimdasePackage.BOOLEAN_STATEMENT_LVL3:
      {
        BooleanStatementLvl3 booleanStatementLvl3 = (BooleanStatementLvl3)theEObject;
        T1 result = caseBooleanStatementLvl3(booleanStatementLvl3);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SimdasePackage.BOOLEAN_STATEMENT_LVL4:
      {
        BooleanStatementLvl4 booleanStatementLvl4 = (BooleanStatementLvl4)theEObject;
        T1 result = caseBooleanStatementLvl4(booleanStatementLvl4);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SimdasePackage.BOOLEAN_STATEMENT_LVL5:
      {
        BooleanStatementLvl5 booleanStatementLvl5 = (BooleanStatementLvl5)theEObject;
        T1 result = caseBooleanStatementLvl5(booleanStatementLvl5);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SimdasePackage.BOOLEAN_STATEMENT_LVL6:
      {
        BooleanStatementLvl6 booleanStatementLvl6 = (BooleanStatementLvl6)theEObject;
        T1 result = caseBooleanStatementLvl6(booleanStatementLvl6);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SimdasePackage.BOOLEAN_STATEMENT_LVL7:
      {
        BooleanStatementLvl7 booleanStatementLvl7 = (BooleanStatementLvl7)theEObject;
        T1 result = caseBooleanStatementLvl7(booleanStatementLvl7);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SimdasePackage.BOOLEAN_STATEMENT_LVL8:
      {
        BooleanStatementLvl8 booleanStatementLvl8 = (BooleanStatementLvl8)theEObject;
        T1 result = caseBooleanStatementLvl8(booleanStatementLvl8);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SimdasePackage.SIMDASE_CONTRACT_LIBRARY:
      {
        SimdaseContractLibrary simdaseContractLibrary = (SimdaseContractLibrary)theEObject;
        T1 result = caseSimdaseContractLibrary(simdaseContractLibrary);
        if (result == null) result = caseSimdaseLibrary(simdaseContractLibrary);
        if (result == null) result = caseAnnexLibrary(simdaseContractLibrary);
        if (result == null) result = caseNamedElement(simdaseContractLibrary);
        if (result == null) result = caseElement(simdaseContractLibrary);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SimdasePackage.SIMDASE_CONTRACT_SUBCLAUSE:
      {
        SimdaseContractSubclause simdaseContractSubclause = (SimdaseContractSubclause)theEObject;
        T1 result = caseSimdaseContractSubclause(simdaseContractSubclause);
        if (result == null) result = caseSimdaseSubclause(simdaseContractSubclause);
        if (result == null) result = caseAnnexSubclause(simdaseContractSubclause);
        if (result == null) result = caseModalElement(simdaseContractSubclause);
        if (result == null) result = caseNamedElement(simdaseContractSubclause);
        if (result == null) result = caseElement(simdaseContractSubclause);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SimdasePackage.SIMDASE_CONTRACT:
      {
        SimdaseContract simdaseContract = (SimdaseContract)theEObject;
        T1 result = caseSimdaseContract(simdaseContract);
        if (result == null) result = caseContract(simdaseContract);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SimdasePackage.IS_ADAPTIVE_STATEMENT:
      {
        IsAdaptiveStatement isAdaptiveStatement = (IsAdaptiveStatement)theEObject;
        T1 result = caseIsAdaptiveStatement(isAdaptiveStatement);
        if (result == null) result = caseSIMDASEStatement(isAdaptiveStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SimdasePackage.TMAX:
      {
        TMax tMax = (TMax)theEObject;
        T1 result = caseTMax(tMax);
        if (result == null) result = caseSIMDASEStatement(tMax);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SimdasePackage.TMIN:
      {
        TMin tMin = (TMin)theEObject;
        T1 result = caseTMin(tMin);
        if (result == null) result = caseSIMDASEStatement(tMin);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SimdasePackage.SCALING_FACTOR:
      {
        ScalingFactor scalingFactor = (ScalingFactor)theEObject;
        T1 result = caseScalingFactor(scalingFactor);
        if (result == null) result = caseSIMDASEStatement(scalingFactor);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SimdasePackage.RE_EVALUATE_AVERAGING:
      {
        ReEvaluateAveraging reEvaluateAveraging = (ReEvaluateAveraging)theEObject;
        T1 result = caseReEvaluateAveraging(reEvaluateAveraging);
        if (result == null) result = caseSIMDASEStatement(reEvaluateAveraging);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SimdasePackage.HOURLY_COST:
      {
        HourlyCost hourlyCost = (HourlyCost)theEObject;
        T1 result = caseHourlyCost(hourlyCost);
        if (result == null) result = caseEmployeeStatement(hourlyCost);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SimdasePackage.FIXED_COST:
      {
        FixedCost fixedCost = (FixedCost)theEObject;
        T1 result = caseFixedCost(fixedCost);
        if (result == null) result = caseEmployeeStatement(fixedCost);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SimdasePackage.INTEGER:
      {
        edu.clemson.simdase.simdase.Integer integer = (edu.clemson.simdase.simdase.Integer)theEObject;
        T1 result = caseInteger(integer);
        if (result == null) result = caseStatementLvl4(integer);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SimdasePackage.T:
      {
        T t = (T)theEObject;
        T1 result = caseT(t);
        if (result == null) result = caseStatementLvl4(t);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SimdasePackage.I:
      {
        I i = (I)theEObject;
        T1 result = caseI(i);
        if (result == null) result = caseStatementLvl4(i);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SimdasePackage.SIN:
      {
        Sin sin = (Sin)theEObject;
        T1 result = caseSin(sin);
        if (result == null) result = caseStatementLvl4(sin);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SimdasePackage.COS:
      {
        Cos cos = (Cos)theEObject;
        T1 result = caseCos(cos);
        if (result == null) result = caseStatementLvl4(cos);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SimdasePackage.TAN:
      {
        Tan tan = (Tan)theEObject;
        T1 result = caseTan(tan);
        if (result == null) result = caseStatementLvl4(tan);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SimdasePackage.PAREN:
      {
        Paren paren = (Paren)theEObject;
        T1 result = caseParen(paren);
        if (result == null) result = caseStatementLvl4(paren);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SimdasePackage.IF:
      {
        If if_ = (If)theEObject;
        T1 result = caseIf(if_);
        if (result == null) result = caseStatementLvl4(if_);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SimdasePackage.RANDOM:
      {
        Random random = (Random)theEObject;
        T1 result = caseRandom(random);
        if (result == null) result = caseStatementLvl4(random);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SimdasePackage.STATEMENT_SCALING_FACTOR:
      {
        StatementScalingFactor statementScalingFactor = (StatementScalingFactor)theEObject;
        T1 result = caseStatementScalingFactor(statementScalingFactor);
        if (result == null) result = caseStatementLvl4(statementScalingFactor);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SimdasePackage.WITH_NOT:
      {
        WithNot withNot = (WithNot)theEObject;
        T1 result = caseWithNot(withNot);
        if (result == null) result = caseBooleanStatementLvl4(withNot);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SimdasePackage.WITHOUT_NOT:
      {
        WithoutNot withoutNot = (WithoutNot)theEObject;
        T1 result = caseWithoutNot(withoutNot);
        if (result == null) result = caseBooleanStatementLvl4(withoutNot);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SimdasePackage.BOOL_BOOLEAN:
      {
        BoolBoolean boolBoolean = (BoolBoolean)theEObject;
        T1 result = caseBoolBoolean(boolBoolean);
        if (result == null) result = caseBooleanStatementLvl8(boolBoolean);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SimdasePackage.BOOL_T:
      {
        BoolT boolT = (BoolT)theEObject;
        T1 result = caseBoolT(boolT);
        if (result == null) result = caseBooleanStatementLvl8(boolT);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SimdasePackage.BOOL_I:
      {
        BoolI boolI = (BoolI)theEObject;
        T1 result = caseBoolI(boolI);
        if (result == null) result = caseBooleanStatementLvl8(boolI);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SimdasePackage.BOOL_INTEGER:
      {
        BoolInteger boolInteger = (BoolInteger)theEObject;
        T1 result = caseBoolInteger(boolInteger);
        if (result == null) result = caseBooleanStatementLvl8(boolInteger);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SimdasePackage.BOOL_SIN:
      {
        BoolSin boolSin = (BoolSin)theEObject;
        T1 result = caseBoolSin(boolSin);
        if (result == null) result = caseBooleanStatementLvl8(boolSin);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SimdasePackage.BOOL_COS:
      {
        BoolCos boolCos = (BoolCos)theEObject;
        T1 result = caseBoolCos(boolCos);
        if (result == null) result = caseBooleanStatementLvl8(boolCos);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SimdasePackage.BOOL_TAN:
      {
        BoolTan boolTan = (BoolTan)theEObject;
        T1 result = caseBoolTan(boolTan);
        if (result == null) result = caseBooleanStatementLvl8(boolTan);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SimdasePackage.BOOL_PAREN:
      {
        BoolParen boolParen = (BoolParen)theEObject;
        T1 result = caseBoolParen(boolParen);
        if (result == null) result = caseBooleanStatementLvl8(boolParen);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SimdasePackage.BOOL_RANDOM:
      {
        BoolRandom boolRandom = (BoolRandom)theEObject;
        T1 result = caseBoolRandom(boolRandom);
        if (result == null) result = caseBooleanStatementLvl8(boolRandom);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SimdasePackage.BOOL_SCALING_FACTOR:
      {
        BoolScalingFactor boolScalingFactor = (BoolScalingFactor)theEObject;
        T1 result = caseBoolScalingFactor(boolScalingFactor);
        if (result == null) result = caseBooleanStatementLvl8(boolScalingFactor);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      default: return defaultCase(theEObject);
    }
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Library</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Library</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T1 caseSimdaseLibrary(SimdaseLibrary object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Subclause</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Subclause</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T1 caseSimdaseSubclause(SimdaseSubclause object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Contract</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Contract</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T1 caseContract(Contract object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>SIMDASE Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>SIMDASE Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T1 caseSIMDASEStatement(SIMDASEStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Variants</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Variants</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T1 caseVariants(Variants object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Variant</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Variant</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T1 caseVariant(Variant object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Active Variants</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Active Variants</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T1 caseActiveVariants(ActiveVariants object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Active Variant</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Active Variant</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T1 caseActiveVariant(ActiveVariant object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Variant Names</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Variant Names</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T1 caseVariantNames(VariantNames object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Cost Properties</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Cost Properties</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T1 caseCostProperties(CostProperties object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Cost Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Cost Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T1 caseCostStatement(CostStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Cost Value</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Cost Value</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T1 caseCostValue(CostValue object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Employee Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Employee Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T1 caseEmployeeStatement(EmployeeStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Boolean</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Boolean</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T1 caseBoolean(edu.clemson.simdase.simdase.Boolean object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Number</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Number</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T1 caseNumber(edu.clemson.simdase.simdase.Number object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T1 caseStatement(Statement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Statement Lvl2</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Statement Lvl2</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T1 caseStatementLvl2(StatementLvl2 object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Statement Lvl3</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Statement Lvl3</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T1 caseStatementLvl3(StatementLvl3 object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Statement Lvl4</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Statement Lvl4</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T1 caseStatementLvl4(StatementLvl4 object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Boolean Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Boolean Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T1 caseBooleanStatement(BooleanStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Boolean Statement Lvl2</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Boolean Statement Lvl2</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T1 caseBooleanStatementLvl2(BooleanStatementLvl2 object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Boolean Statement Lvl3</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Boolean Statement Lvl3</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T1 caseBooleanStatementLvl3(BooleanStatementLvl3 object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Boolean Statement Lvl4</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Boolean Statement Lvl4</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T1 caseBooleanStatementLvl4(BooleanStatementLvl4 object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Boolean Statement Lvl5</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Boolean Statement Lvl5</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T1 caseBooleanStatementLvl5(BooleanStatementLvl5 object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Boolean Statement Lvl6</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Boolean Statement Lvl6</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T1 caseBooleanStatementLvl6(BooleanStatementLvl6 object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Boolean Statement Lvl7</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Boolean Statement Lvl7</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T1 caseBooleanStatementLvl7(BooleanStatementLvl7 object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Boolean Statement Lvl8</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Boolean Statement Lvl8</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T1 caseBooleanStatementLvl8(BooleanStatementLvl8 object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Contract Library</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Contract Library</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T1 caseSimdaseContractLibrary(SimdaseContractLibrary object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Contract Subclause</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Contract Subclause</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T1 caseSimdaseContractSubclause(SimdaseContractSubclause object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Contract</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Contract</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T1 caseSimdaseContract(SimdaseContract object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Is Adaptive Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Is Adaptive Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T1 caseIsAdaptiveStatement(IsAdaptiveStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>TMax</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>TMax</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T1 caseTMax(TMax object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>TMin</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>TMin</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T1 caseTMin(TMin object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Scaling Factor</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Scaling Factor</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T1 caseScalingFactor(ScalingFactor object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Re Evaluate Averaging</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Re Evaluate Averaging</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T1 caseReEvaluateAveraging(ReEvaluateAveraging object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Hourly Cost</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Hourly Cost</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T1 caseHourlyCost(HourlyCost object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Fixed Cost</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Fixed Cost</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T1 caseFixedCost(FixedCost object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Integer</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Integer</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T1 caseInteger(edu.clemson.simdase.simdase.Integer object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>T</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>T</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T1 caseT(T object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>I</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>I</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T1 caseI(I object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Sin</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Sin</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T1 caseSin(Sin object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Cos</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Cos</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T1 caseCos(Cos object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Tan</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Tan</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T1 caseTan(Tan object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Paren</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Paren</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T1 caseParen(Paren object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>If</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>If</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T1 caseIf(If object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Random</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Random</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T1 caseRandom(Random object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Statement Scaling Factor</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Statement Scaling Factor</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T1 caseStatementScalingFactor(StatementScalingFactor object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>With Not</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>With Not</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T1 caseWithNot(WithNot object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Without Not</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Without Not</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T1 caseWithoutNot(WithoutNot object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Bool Boolean</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Bool Boolean</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T1 caseBoolBoolean(BoolBoolean object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Bool T</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Bool T</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T1 caseBoolT(BoolT object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Bool I</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Bool I</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T1 caseBoolI(BoolI object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Bool Integer</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Bool Integer</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T1 caseBoolInteger(BoolInteger object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Bool Sin</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Bool Sin</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T1 caseBoolSin(BoolSin object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Bool Cos</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Bool Cos</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T1 caseBoolCos(BoolCos object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Bool Tan</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Bool Tan</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T1 caseBoolTan(BoolTan object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Bool Paren</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Bool Paren</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T1 caseBoolParen(BoolParen object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Bool Random</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Bool Random</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T1 caseBoolRandom(BoolRandom object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Bool Scaling Factor</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Bool Scaling Factor</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T1 caseBoolScalingFactor(BoolScalingFactor object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Element</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Element</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T1 caseElement(Element object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Named Element</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Named Element</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T1 caseNamedElement(NamedElement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Annex Library</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Annex Library</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T1 caseAnnexLibrary(AnnexLibrary object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Modal Element</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Modal Element</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T1 caseModalElement(ModalElement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Annex Subclause</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Annex Subclause</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T1 caseAnnexSubclause(AnnexSubclause object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch, but this is the last case anyway.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject)
   * @generated
   */
  @Override
  public T1 defaultCase(EObject object)
  {
    return null;
  }

} //SimdaseSwitch
