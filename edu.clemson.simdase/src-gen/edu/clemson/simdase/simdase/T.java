/**
 */
package edu.clemson.simdase.simdase;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>T</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.clemson.simdase.simdase.SimdasePackage#getT()
 * @model
 * @generated
 */
public interface T extends StatementLvl4
{
} // T
