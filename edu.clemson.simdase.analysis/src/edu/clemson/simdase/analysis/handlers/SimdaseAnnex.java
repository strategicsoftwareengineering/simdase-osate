package edu.clemson.simdase.analysis.handlers;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SimdaseAnnex {
	private Boolean isAdaptive;
	private Boolean isVariationPoint;
	private BigDecimal timePeriodStart;
	private BigDecimal timePeriodEnd;
	private BigDecimal scalingFactor;
	private BigDecimal reevaluateNTimes = BigDecimal.ONE;
	private String useWhichResult = "average";
	private Map<String, List<String>> variants;
	private Map<String, List<String>> activeVariants;
	private Map<String, List<CostRow.EmployeeCost>> employeeCosts;
	
	public Boolean getIsAdaptive() {
		return isAdaptive;
	}
	
	public void setIsAdaptive(Boolean isAdaptive) {
		this.isAdaptive = isAdaptive;
	}
	
	public Boolean getIsVariationPoint() {
		return isVariationPoint;
	}
	
	public void setIsVariationPoint(Boolean isVariationPoint) {
		this.isVariationPoint = isVariationPoint;
	}

	public BigDecimal getTimePeriodStart() {
		return timePeriodStart;
	}

	public void setTimePeriodStart(BigDecimal timePeriodStart) {
		this.timePeriodStart = timePeriodStart;
	}

	public BigDecimal getTimePeriodEnd() {
		return timePeriodEnd;
	}

	public void setTimePeriodEnd(BigDecimal timePeriodEnd) {
		this.timePeriodEnd = timePeriodEnd;
	}

	public BigDecimal getScalingFactor() {
		return scalingFactor;
	}

	public void setScalingFactor(BigDecimal scalingFactor) {
		this.scalingFactor = scalingFactor;
	}

	public Map<String, List<String>> getVariants() {
		if(variants == null) {
			variants = new HashMap<String, List<String>>();
		}
		return variants;
	}

	public void setVariants(Map<String, List<String>> variants) {
		this.variants = variants;
	}

	public Map<String, List<String>> getActiveVariants() {
		if(activeVariants == null) {
			activeVariants = new HashMap<String, List<String>>();
		}
		return activeVariants;
	}

	public void setActiveVariants(Map<String, List<String>> activeVariants) {
		this.activeVariants = activeVariants;
	}

	public Map<String, List<CostRow.EmployeeCost>> getEmployeeCosts() {
		if(employeeCosts == null) {
			employeeCosts = new HashMap<String, List<CostRow.EmployeeCost>>();
		}
		return employeeCosts;
	}

	public void setEmployeeCosts(Map<String, List<CostRow.EmployeeCost>> employeeCosts) {
		this.employeeCosts = employeeCosts;
	}

	public BigDecimal getReevaluateNTimes() {
		return reevaluateNTimes;
	}

	public void setReevaluateNTimes(BigDecimal reevaluateNTimes) {
		this.reevaluateNTimes = reevaluateNTimes;
	}

	public String getUseWhichResult() {
		return useWhichResult;
	}

	public void setUseWhichResult(String useWhichResult) {
		this.useWhichResult = useWhichResult;
	}
}
