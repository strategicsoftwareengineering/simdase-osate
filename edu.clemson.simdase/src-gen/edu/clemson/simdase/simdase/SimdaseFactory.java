/**
 */
package edu.clemson.simdase.simdase;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see edu.clemson.simdase.simdase.SimdasePackage
 * @generated
 */
public interface SimdaseFactory extends EFactory
{
  /**
   * The singleton instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  SimdaseFactory eINSTANCE = edu.clemson.simdase.simdase.impl.SimdaseFactoryImpl.init();

  /**
   * Returns a new object of class '<em>Library</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Library</em>'.
   * @generated
   */
  SimdaseLibrary createSimdaseLibrary();

  /**
   * Returns a new object of class '<em>Subclause</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Subclause</em>'.
   * @generated
   */
  SimdaseSubclause createSimdaseSubclause();

  /**
   * Returns a new object of class '<em>Contract</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Contract</em>'.
   * @generated
   */
  Contract createContract();

  /**
   * Returns a new object of class '<em>SIMDASE Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>SIMDASE Statement</em>'.
   * @generated
   */
  SIMDASEStatement createSIMDASEStatement();

  /**
   * Returns a new object of class '<em>Variants</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Variants</em>'.
   * @generated
   */
  Variants createVariants();

  /**
   * Returns a new object of class '<em>Variant</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Variant</em>'.
   * @generated
   */
  Variant createVariant();

  /**
   * Returns a new object of class '<em>Active Variants</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Active Variants</em>'.
   * @generated
   */
  ActiveVariants createActiveVariants();

  /**
   * Returns a new object of class '<em>Active Variant</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Active Variant</em>'.
   * @generated
   */
  ActiveVariant createActiveVariant();

  /**
   * Returns a new object of class '<em>Variant Names</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Variant Names</em>'.
   * @generated
   */
  VariantNames createVariantNames();

  /**
   * Returns a new object of class '<em>Cost Properties</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Cost Properties</em>'.
   * @generated
   */
  CostProperties createCostProperties();

  /**
   * Returns a new object of class '<em>Cost Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Cost Statement</em>'.
   * @generated
   */
  CostStatement createCostStatement();

  /**
   * Returns a new object of class '<em>Cost Value</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Cost Value</em>'.
   * @generated
   */
  CostValue createCostValue();

  /**
   * Returns a new object of class '<em>Employee Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Employee Statement</em>'.
   * @generated
   */
  EmployeeStatement createEmployeeStatement();

  /**
   * Returns a new object of class '<em>Boolean</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Boolean</em>'.
   * @generated
   */
  Boolean createBoolean();

  /**
   * Returns a new object of class '<em>Number</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Number</em>'.
   * @generated
   */
  Number createNumber();

  /**
   * Returns a new object of class '<em>Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Statement</em>'.
   * @generated
   */
  Statement createStatement();

  /**
   * Returns a new object of class '<em>Statement Lvl2</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Statement Lvl2</em>'.
   * @generated
   */
  StatementLvl2 createStatementLvl2();

  /**
   * Returns a new object of class '<em>Statement Lvl3</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Statement Lvl3</em>'.
   * @generated
   */
  StatementLvl3 createStatementLvl3();

  /**
   * Returns a new object of class '<em>Statement Lvl4</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Statement Lvl4</em>'.
   * @generated
   */
  StatementLvl4 createStatementLvl4();

  /**
   * Returns a new object of class '<em>Boolean Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Boolean Statement</em>'.
   * @generated
   */
  BooleanStatement createBooleanStatement();

  /**
   * Returns a new object of class '<em>Boolean Statement Lvl2</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Boolean Statement Lvl2</em>'.
   * @generated
   */
  BooleanStatementLvl2 createBooleanStatementLvl2();

  /**
   * Returns a new object of class '<em>Boolean Statement Lvl3</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Boolean Statement Lvl3</em>'.
   * @generated
   */
  BooleanStatementLvl3 createBooleanStatementLvl3();

  /**
   * Returns a new object of class '<em>Boolean Statement Lvl4</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Boolean Statement Lvl4</em>'.
   * @generated
   */
  BooleanStatementLvl4 createBooleanStatementLvl4();

  /**
   * Returns a new object of class '<em>Boolean Statement Lvl5</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Boolean Statement Lvl5</em>'.
   * @generated
   */
  BooleanStatementLvl5 createBooleanStatementLvl5();

  /**
   * Returns a new object of class '<em>Boolean Statement Lvl6</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Boolean Statement Lvl6</em>'.
   * @generated
   */
  BooleanStatementLvl6 createBooleanStatementLvl6();

  /**
   * Returns a new object of class '<em>Boolean Statement Lvl7</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Boolean Statement Lvl7</em>'.
   * @generated
   */
  BooleanStatementLvl7 createBooleanStatementLvl7();

  /**
   * Returns a new object of class '<em>Boolean Statement Lvl8</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Boolean Statement Lvl8</em>'.
   * @generated
   */
  BooleanStatementLvl8 createBooleanStatementLvl8();

  /**
   * Returns a new object of class '<em>Contract Library</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Contract Library</em>'.
   * @generated
   */
  SimdaseContractLibrary createSimdaseContractLibrary();

  /**
   * Returns a new object of class '<em>Contract Subclause</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Contract Subclause</em>'.
   * @generated
   */
  SimdaseContractSubclause createSimdaseContractSubclause();

  /**
   * Returns a new object of class '<em>Contract</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Contract</em>'.
   * @generated
   */
  SimdaseContract createSimdaseContract();

  /**
   * Returns a new object of class '<em>Is Adaptive Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Is Adaptive Statement</em>'.
   * @generated
   */
  IsAdaptiveStatement createIsAdaptiveStatement();

  /**
   * Returns a new object of class '<em>TMax</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>TMax</em>'.
   * @generated
   */
  TMax createTMax();

  /**
   * Returns a new object of class '<em>TMin</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>TMin</em>'.
   * @generated
   */
  TMin createTMin();

  /**
   * Returns a new object of class '<em>Scaling Factor</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Scaling Factor</em>'.
   * @generated
   */
  ScalingFactor createScalingFactor();

  /**
   * Returns a new object of class '<em>Re Evaluate Averaging</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Re Evaluate Averaging</em>'.
   * @generated
   */
  ReEvaluateAveraging createReEvaluateAveraging();

  /**
   * Returns a new object of class '<em>Hourly Cost</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Hourly Cost</em>'.
   * @generated
   */
  HourlyCost createHourlyCost();

  /**
   * Returns a new object of class '<em>Fixed Cost</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Fixed Cost</em>'.
   * @generated
   */
  FixedCost createFixedCost();

  /**
   * Returns a new object of class '<em>Integer</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Integer</em>'.
   * @generated
   */
  Integer createInteger();

  /**
   * Returns a new object of class '<em>T</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>T</em>'.
   * @generated
   */
  T createT();

  /**
   * Returns a new object of class '<em>I</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>I</em>'.
   * @generated
   */
  I createI();

  /**
   * Returns a new object of class '<em>Sin</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Sin</em>'.
   * @generated
   */
  Sin createSin();

  /**
   * Returns a new object of class '<em>Cos</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Cos</em>'.
   * @generated
   */
  Cos createCos();

  /**
   * Returns a new object of class '<em>Tan</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Tan</em>'.
   * @generated
   */
  Tan createTan();

  /**
   * Returns a new object of class '<em>Paren</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Paren</em>'.
   * @generated
   */
  Paren createParen();

  /**
   * Returns a new object of class '<em>If</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>If</em>'.
   * @generated
   */
  If createIf();

  /**
   * Returns a new object of class '<em>Random</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Random</em>'.
   * @generated
   */
  Random createRandom();

  /**
   * Returns a new object of class '<em>Statement Scaling Factor</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Statement Scaling Factor</em>'.
   * @generated
   */
  StatementScalingFactor createStatementScalingFactor();

  /**
   * Returns a new object of class '<em>With Not</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>With Not</em>'.
   * @generated
   */
  WithNot createWithNot();

  /**
   * Returns a new object of class '<em>Without Not</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Without Not</em>'.
   * @generated
   */
  WithoutNot createWithoutNot();

  /**
   * Returns a new object of class '<em>Bool Boolean</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Bool Boolean</em>'.
   * @generated
   */
  BoolBoolean createBoolBoolean();

  /**
   * Returns a new object of class '<em>Bool T</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Bool T</em>'.
   * @generated
   */
  BoolT createBoolT();

  /**
   * Returns a new object of class '<em>Bool I</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Bool I</em>'.
   * @generated
   */
  BoolI createBoolI();

  /**
   * Returns a new object of class '<em>Bool Integer</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Bool Integer</em>'.
   * @generated
   */
  BoolInteger createBoolInteger();

  /**
   * Returns a new object of class '<em>Bool Sin</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Bool Sin</em>'.
   * @generated
   */
  BoolSin createBoolSin();

  /**
   * Returns a new object of class '<em>Bool Cos</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Bool Cos</em>'.
   * @generated
   */
  BoolCos createBoolCos();

  /**
   * Returns a new object of class '<em>Bool Tan</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Bool Tan</em>'.
   * @generated
   */
  BoolTan createBoolTan();

  /**
   * Returns a new object of class '<em>Bool Paren</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Bool Paren</em>'.
   * @generated
   */
  BoolParen createBoolParen();

  /**
   * Returns a new object of class '<em>Bool Random</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Bool Random</em>'.
   * @generated
   */
  BoolRandom createBoolRandom();

  /**
   * Returns a new object of class '<em>Bool Scaling Factor</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Bool Scaling Factor</em>'.
   * @generated
   */
  BoolScalingFactor createBoolScalingFactor();

  /**
   * Returns the package supported by this factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the package supported by this factory.
   * @generated
   */
  SimdasePackage getSimdasePackage();

} //SimdaseFactory
