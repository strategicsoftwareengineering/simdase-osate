/**
 */
package edu.clemson.simdase.simdase.impl;

import edu.clemson.simdase.simdase.BooleanStatementLvl3;
import edu.clemson.simdase.simdase.BooleanStatementLvl4;
import edu.clemson.simdase.simdase.SimdasePackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EDataTypeEList;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Boolean Statement Lvl3</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.clemson.simdase.simdase.impl.BooleanStatementLvl3Impl#getStatement <em>Statement</em>}</li>
 *   <li>{@link edu.clemson.simdase.simdase.impl.BooleanStatementLvl3Impl#getOp <em>Op</em>}</li>
 *   <li>{@link edu.clemson.simdase.simdase.impl.BooleanStatementLvl3Impl#getRest <em>Rest</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BooleanStatementLvl3Impl extends MinimalEObjectImpl.Container implements BooleanStatementLvl3
{
  /**
   * The cached value of the '{@link #getStatement() <em>Statement</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getStatement()
   * @generated
   * @ordered
   */
  protected BooleanStatementLvl4 statement;

  /**
   * The cached value of the '{@link #getOp() <em>Op</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOp()
   * @generated
   * @ordered
   */
  protected EList<String> op;

  /**
   * The cached value of the '{@link #getRest() <em>Rest</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRest()
   * @generated
   * @ordered
   */
  protected EList<BooleanStatementLvl4> rest;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected BooleanStatementLvl3Impl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return SimdasePackage.Literals.BOOLEAN_STATEMENT_LVL3;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BooleanStatementLvl4 getStatement()
  {
    return statement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetStatement(BooleanStatementLvl4 newStatement, NotificationChain msgs)
  {
    BooleanStatementLvl4 oldStatement = statement;
    statement = newStatement;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SimdasePackage.BOOLEAN_STATEMENT_LVL3__STATEMENT, oldStatement, newStatement);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setStatement(BooleanStatementLvl4 newStatement)
  {
    if (newStatement != statement)
    {
      NotificationChain msgs = null;
      if (statement != null)
        msgs = ((InternalEObject)statement).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SimdasePackage.BOOLEAN_STATEMENT_LVL3__STATEMENT, null, msgs);
      if (newStatement != null)
        msgs = ((InternalEObject)newStatement).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SimdasePackage.BOOLEAN_STATEMENT_LVL3__STATEMENT, null, msgs);
      msgs = basicSetStatement(newStatement, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SimdasePackage.BOOLEAN_STATEMENT_LVL3__STATEMENT, newStatement, newStatement));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<String> getOp()
  {
    if (op == null)
    {
      op = new EDataTypeEList<String>(String.class, this, SimdasePackage.BOOLEAN_STATEMENT_LVL3__OP);
    }
    return op;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<BooleanStatementLvl4> getRest()
  {
    if (rest == null)
    {
      rest = new EObjectContainmentEList<BooleanStatementLvl4>(BooleanStatementLvl4.class, this, SimdasePackage.BOOLEAN_STATEMENT_LVL3__REST);
    }
    return rest;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case SimdasePackage.BOOLEAN_STATEMENT_LVL3__STATEMENT:
        return basicSetStatement(null, msgs);
      case SimdasePackage.BOOLEAN_STATEMENT_LVL3__REST:
        return ((InternalEList<?>)getRest()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case SimdasePackage.BOOLEAN_STATEMENT_LVL3__STATEMENT:
        return getStatement();
      case SimdasePackage.BOOLEAN_STATEMENT_LVL3__OP:
        return getOp();
      case SimdasePackage.BOOLEAN_STATEMENT_LVL3__REST:
        return getRest();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case SimdasePackage.BOOLEAN_STATEMENT_LVL3__STATEMENT:
        setStatement((BooleanStatementLvl4)newValue);
        return;
      case SimdasePackage.BOOLEAN_STATEMENT_LVL3__OP:
        getOp().clear();
        getOp().addAll((Collection<? extends String>)newValue);
        return;
      case SimdasePackage.BOOLEAN_STATEMENT_LVL3__REST:
        getRest().clear();
        getRest().addAll((Collection<? extends BooleanStatementLvl4>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case SimdasePackage.BOOLEAN_STATEMENT_LVL3__STATEMENT:
        setStatement((BooleanStatementLvl4)null);
        return;
      case SimdasePackage.BOOLEAN_STATEMENT_LVL3__OP:
        getOp().clear();
        return;
      case SimdasePackage.BOOLEAN_STATEMENT_LVL3__REST:
        getRest().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case SimdasePackage.BOOLEAN_STATEMENT_LVL3__STATEMENT:
        return statement != null;
      case SimdasePackage.BOOLEAN_STATEMENT_LVL3__OP:
        return op != null && !op.isEmpty();
      case SimdasePackage.BOOLEAN_STATEMENT_LVL3__REST:
        return rest != null && !rest.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (op: ");
    result.append(op);
    result.append(')');
    return result.toString();
  }

} //BooleanStatementLvl3Impl
