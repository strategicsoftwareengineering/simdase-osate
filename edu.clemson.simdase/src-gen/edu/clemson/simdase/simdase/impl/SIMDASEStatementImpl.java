/**
 */
package edu.clemson.simdase.simdase.impl;

import edu.clemson.simdase.simdase.SIMDASEStatement;
import edu.clemson.simdase.simdase.SimdasePackage;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SIMDASE Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class SIMDASEStatementImpl extends MinimalEObjectImpl.Container implements SIMDASEStatement
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected SIMDASEStatementImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return SimdasePackage.Literals.SIMDASE_STATEMENT;
  }

} //SIMDASEStatementImpl
