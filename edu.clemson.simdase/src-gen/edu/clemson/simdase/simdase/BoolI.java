/**
 */
package edu.clemson.simdase.simdase;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Bool I</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.clemson.simdase.simdase.SimdasePackage#getBoolI()
 * @model
 * @generated
 */
public interface BoolI extends BooleanStatementLvl8
{
} // BoolI
