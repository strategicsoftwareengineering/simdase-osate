/**
 */
package edu.clemson.simdase.simdase.impl;

import edu.clemson.simdase.simdase.Random;
import edu.clemson.simdase.simdase.SimdasePackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Random</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class RandomImpl extends StatementLvl4Impl implements Random
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected RandomImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return SimdasePackage.Literals.RANDOM;
  }

} //RandomImpl
