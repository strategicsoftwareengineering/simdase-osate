/**
 */
package edu.clemson.simdase.simdase.impl;

import edu.clemson.simdase.simdase.HourlyCost;
import edu.clemson.simdase.simdase.SimdasePackage;
import edu.clemson.simdase.simdase.Statement;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Hourly Cost</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.clemson.simdase.simdase.impl.HourlyCostImpl#getCount <em>Count</em>}</li>
 *   <li>{@link edu.clemson.simdase.simdase.impl.HourlyCostImpl#getEmployeeType <em>Employee Type</em>}</li>
 *   <li>{@link edu.clemson.simdase.simdase.impl.HourlyCostImpl#getCost <em>Cost</em>}</li>
 * </ul>
 *
 * @generated
 */
public class HourlyCostImpl extends EmployeeStatementImpl implements HourlyCost
{
  /**
   * The cached value of the '{@link #getCount() <em>Count</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCount()
   * @generated
   * @ordered
   */
  protected edu.clemson.simdase.simdase.Number count;

  /**
   * The default value of the '{@link #getEmployeeType() <em>Employee Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getEmployeeType()
   * @generated
   * @ordered
   */
  protected static final String EMPLOYEE_TYPE_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getEmployeeType() <em>Employee Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getEmployeeType()
   * @generated
   * @ordered
   */
  protected String employeeType = EMPLOYEE_TYPE_EDEFAULT;

  /**
   * The cached value of the '{@link #getCost() <em>Cost</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCost()
   * @generated
   * @ordered
   */
  protected Statement cost;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected HourlyCostImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return SimdasePackage.Literals.HOURLY_COST;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public edu.clemson.simdase.simdase.Number getCount()
  {
    return count;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetCount(edu.clemson.simdase.simdase.Number newCount, NotificationChain msgs)
  {
    edu.clemson.simdase.simdase.Number oldCount = count;
    count = newCount;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SimdasePackage.HOURLY_COST__COUNT, oldCount, newCount);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCount(edu.clemson.simdase.simdase.Number newCount)
  {
    if (newCount != count)
    {
      NotificationChain msgs = null;
      if (count != null)
        msgs = ((InternalEObject)count).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SimdasePackage.HOURLY_COST__COUNT, null, msgs);
      if (newCount != null)
        msgs = ((InternalEObject)newCount).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SimdasePackage.HOURLY_COST__COUNT, null, msgs);
      msgs = basicSetCount(newCount, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SimdasePackage.HOURLY_COST__COUNT, newCount, newCount));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getEmployeeType()
  {
    return employeeType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setEmployeeType(String newEmployeeType)
  {
    String oldEmployeeType = employeeType;
    employeeType = newEmployeeType;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SimdasePackage.HOURLY_COST__EMPLOYEE_TYPE, oldEmployeeType, employeeType));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Statement getCost()
  {
    return cost;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetCost(Statement newCost, NotificationChain msgs)
  {
    Statement oldCost = cost;
    cost = newCost;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SimdasePackage.HOURLY_COST__COST, oldCost, newCost);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCost(Statement newCost)
  {
    if (newCost != cost)
    {
      NotificationChain msgs = null;
      if (cost != null)
        msgs = ((InternalEObject)cost).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SimdasePackage.HOURLY_COST__COST, null, msgs);
      if (newCost != null)
        msgs = ((InternalEObject)newCost).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SimdasePackage.HOURLY_COST__COST, null, msgs);
      msgs = basicSetCost(newCost, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SimdasePackage.HOURLY_COST__COST, newCost, newCost));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case SimdasePackage.HOURLY_COST__COUNT:
        return basicSetCount(null, msgs);
      case SimdasePackage.HOURLY_COST__COST:
        return basicSetCost(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case SimdasePackage.HOURLY_COST__COUNT:
        return getCount();
      case SimdasePackage.HOURLY_COST__EMPLOYEE_TYPE:
        return getEmployeeType();
      case SimdasePackage.HOURLY_COST__COST:
        return getCost();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case SimdasePackage.HOURLY_COST__COUNT:
        setCount((edu.clemson.simdase.simdase.Number)newValue);
        return;
      case SimdasePackage.HOURLY_COST__EMPLOYEE_TYPE:
        setEmployeeType((String)newValue);
        return;
      case SimdasePackage.HOURLY_COST__COST:
        setCost((Statement)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case SimdasePackage.HOURLY_COST__COUNT:
        setCount((edu.clemson.simdase.simdase.Number)null);
        return;
      case SimdasePackage.HOURLY_COST__EMPLOYEE_TYPE:
        setEmployeeType(EMPLOYEE_TYPE_EDEFAULT);
        return;
      case SimdasePackage.HOURLY_COST__COST:
        setCost((Statement)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case SimdasePackage.HOURLY_COST__COUNT:
        return count != null;
      case SimdasePackage.HOURLY_COST__EMPLOYEE_TYPE:
        return EMPLOYEE_TYPE_EDEFAULT == null ? employeeType != null : !EMPLOYEE_TYPE_EDEFAULT.equals(employeeType);
      case SimdasePackage.HOURLY_COST__COST:
        return cost != null;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (employeeType: ");
    result.append(employeeType);
    result.append(')');
    return result.toString();
  }

} //HourlyCostImpl
