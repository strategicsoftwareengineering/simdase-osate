/**
 */
package edu.clemson.simdase.simdase;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Bool Random</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.clemson.simdase.simdase.SimdasePackage#getBoolRandom()
 * @model
 * @generated
 */
public interface BoolRandom extends BooleanStatementLvl8
{
} // BoolRandom
