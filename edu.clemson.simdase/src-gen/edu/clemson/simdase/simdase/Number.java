/**
 */
package edu.clemson.simdase.simdase;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Number</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.clemson.simdase.simdase.Number#getNegative <em>Negative</em>}</li>
 *   <li>{@link edu.clemson.simdase.simdase.Number#getFloatValue <em>Float Value</em>}</li>
 * </ul>
 *
 * @see edu.clemson.simdase.simdase.SimdasePackage#getNumber()
 * @model
 * @generated
 */
public interface Number extends EObject
{
  /**
   * Returns the value of the '<em><b>Negative</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Negative</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Negative</em>' attribute.
   * @see #setNegative(String)
   * @see edu.clemson.simdase.simdase.SimdasePackage#getNumber_Negative()
   * @model
   * @generated
   */
  String getNegative();

  /**
   * Sets the value of the '{@link edu.clemson.simdase.simdase.Number#getNegative <em>Negative</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Negative</em>' attribute.
   * @see #getNegative()
   * @generated
   */
  void setNegative(String value);

  /**
   * Returns the value of the '<em><b>Float Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Float Value</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Float Value</em>' attribute.
   * @see #setFloatValue(String)
   * @see edu.clemson.simdase.simdase.SimdasePackage#getNumber_FloatValue()
   * @model
   * @generated
   */
  String getFloatValue();

  /**
   * Sets the value of the '{@link edu.clemson.simdase.simdase.Number#getFloatValue <em>Float Value</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Float Value</em>' attribute.
   * @see #getFloatValue()
   * @generated
   */
  void setFloatValue(String value);

} // Number
