/**
 */
package edu.clemson.simdase.simdase.impl;

import edu.clemson.simdase.simdase.SimdasePackage;
import edu.clemson.simdase.simdase.SimdaseSubclause;

import org.eclipse.emf.ecore.EClass;

import org.osate.aadl2.impl.AnnexSubclauseImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Subclause</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class SimdaseSubclauseImpl extends AnnexSubclauseImpl implements SimdaseSubclause
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected SimdaseSubclauseImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return SimdasePackage.Literals.SIMDASE_SUBCLAUSE;
  }

} //SimdaseSubclauseImpl
