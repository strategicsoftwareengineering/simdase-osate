/**
 */
package edu.clemson.simdase.simdase.impl;

import edu.clemson.simdase.simdase.FixedCost;
import edu.clemson.simdase.simdase.SimdasePackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Fixed Cost</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.clemson.simdase.simdase.impl.FixedCostImpl#getCost <em>Cost</em>}</li>
 *   <li>{@link edu.clemson.simdase.simdase.impl.FixedCostImpl#getCostReason <em>Cost Reason</em>}</li>
 *   <li>{@link edu.clemson.simdase.simdase.impl.FixedCostImpl#getPeriod <em>Period</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FixedCostImpl extends EmployeeStatementImpl implements FixedCost
{
  /**
   * The cached value of the '{@link #getCost() <em>Cost</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCost()
   * @generated
   * @ordered
   */
  protected edu.clemson.simdase.simdase.Number cost;

  /**
   * The default value of the '{@link #getCostReason() <em>Cost Reason</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCostReason()
   * @generated
   * @ordered
   */
  protected static final String COST_REASON_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getCostReason() <em>Cost Reason</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCostReason()
   * @generated
   * @ordered
   */
  protected String costReason = COST_REASON_EDEFAULT;

  /**
   * The cached value of the '{@link #getPeriod() <em>Period</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPeriod()
   * @generated
   * @ordered
   */
  protected edu.clemson.simdase.simdase.Number period;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected FixedCostImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return SimdasePackage.Literals.FIXED_COST;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public edu.clemson.simdase.simdase.Number getCost()
  {
    return cost;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetCost(edu.clemson.simdase.simdase.Number newCost, NotificationChain msgs)
  {
    edu.clemson.simdase.simdase.Number oldCost = cost;
    cost = newCost;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SimdasePackage.FIXED_COST__COST, oldCost, newCost);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCost(edu.clemson.simdase.simdase.Number newCost)
  {
    if (newCost != cost)
    {
      NotificationChain msgs = null;
      if (cost != null)
        msgs = ((InternalEObject)cost).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SimdasePackage.FIXED_COST__COST, null, msgs);
      if (newCost != null)
        msgs = ((InternalEObject)newCost).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SimdasePackage.FIXED_COST__COST, null, msgs);
      msgs = basicSetCost(newCost, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SimdasePackage.FIXED_COST__COST, newCost, newCost));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getCostReason()
  {
    return costReason;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCostReason(String newCostReason)
  {
    String oldCostReason = costReason;
    costReason = newCostReason;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SimdasePackage.FIXED_COST__COST_REASON, oldCostReason, costReason));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public edu.clemson.simdase.simdase.Number getPeriod()
  {
    return period;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetPeriod(edu.clemson.simdase.simdase.Number newPeriod, NotificationChain msgs)
  {
    edu.clemson.simdase.simdase.Number oldPeriod = period;
    period = newPeriod;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SimdasePackage.FIXED_COST__PERIOD, oldPeriod, newPeriod);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setPeriod(edu.clemson.simdase.simdase.Number newPeriod)
  {
    if (newPeriod != period)
    {
      NotificationChain msgs = null;
      if (period != null)
        msgs = ((InternalEObject)period).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SimdasePackage.FIXED_COST__PERIOD, null, msgs);
      if (newPeriod != null)
        msgs = ((InternalEObject)newPeriod).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SimdasePackage.FIXED_COST__PERIOD, null, msgs);
      msgs = basicSetPeriod(newPeriod, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SimdasePackage.FIXED_COST__PERIOD, newPeriod, newPeriod));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case SimdasePackage.FIXED_COST__COST:
        return basicSetCost(null, msgs);
      case SimdasePackage.FIXED_COST__PERIOD:
        return basicSetPeriod(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case SimdasePackage.FIXED_COST__COST:
        return getCost();
      case SimdasePackage.FIXED_COST__COST_REASON:
        return getCostReason();
      case SimdasePackage.FIXED_COST__PERIOD:
        return getPeriod();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case SimdasePackage.FIXED_COST__COST:
        setCost((edu.clemson.simdase.simdase.Number)newValue);
        return;
      case SimdasePackage.FIXED_COST__COST_REASON:
        setCostReason((String)newValue);
        return;
      case SimdasePackage.FIXED_COST__PERIOD:
        setPeriod((edu.clemson.simdase.simdase.Number)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case SimdasePackage.FIXED_COST__COST:
        setCost((edu.clemson.simdase.simdase.Number)null);
        return;
      case SimdasePackage.FIXED_COST__COST_REASON:
        setCostReason(COST_REASON_EDEFAULT);
        return;
      case SimdasePackage.FIXED_COST__PERIOD:
        setPeriod((edu.clemson.simdase.simdase.Number)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case SimdasePackage.FIXED_COST__COST:
        return cost != null;
      case SimdasePackage.FIXED_COST__COST_REASON:
        return COST_REASON_EDEFAULT == null ? costReason != null : !COST_REASON_EDEFAULT.equals(costReason);
      case SimdasePackage.FIXED_COST__PERIOD:
        return period != null;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (costReason: ");
    result.append(costReason);
    result.append(')');
    return result.toString();
  }

} //FixedCostImpl
