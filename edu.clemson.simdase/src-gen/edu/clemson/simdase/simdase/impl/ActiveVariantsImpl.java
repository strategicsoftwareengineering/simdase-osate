/**
 */
package edu.clemson.simdase.simdase.impl;

import edu.clemson.simdase.simdase.ActiveVariant;
import edu.clemson.simdase.simdase.ActiveVariants;
import edu.clemson.simdase.simdase.SimdasePackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Active Variants</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.clemson.simdase.simdase.impl.ActiveVariantsImpl#getVariants <em>Variants</em>}</li>
 *   <li>{@link edu.clemson.simdase.simdase.impl.ActiveVariantsImpl#getFirst <em>First</em>}</li>
 *   <li>{@link edu.clemson.simdase.simdase.impl.ActiveVariantsImpl#getRest <em>Rest</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ActiveVariantsImpl extends SIMDASEStatementImpl implements ActiveVariants
{
  /**
   * The cached value of the '{@link #getVariants() <em>Variants</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVariants()
   * @generated
   * @ordered
   */
  protected ActiveVariants variants;

  /**
   * The cached value of the '{@link #getFirst() <em>First</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFirst()
   * @generated
   * @ordered
   */
  protected ActiveVariant first;

  /**
   * The cached value of the '{@link #getRest() <em>Rest</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRest()
   * @generated
   * @ordered
   */
  protected EList<ActiveVariant> rest;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ActiveVariantsImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return SimdasePackage.Literals.ACTIVE_VARIANTS;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ActiveVariants getVariants()
  {
    return variants;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetVariants(ActiveVariants newVariants, NotificationChain msgs)
  {
    ActiveVariants oldVariants = variants;
    variants = newVariants;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SimdasePackage.ACTIVE_VARIANTS__VARIANTS, oldVariants, newVariants);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setVariants(ActiveVariants newVariants)
  {
    if (newVariants != variants)
    {
      NotificationChain msgs = null;
      if (variants != null)
        msgs = ((InternalEObject)variants).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SimdasePackage.ACTIVE_VARIANTS__VARIANTS, null, msgs);
      if (newVariants != null)
        msgs = ((InternalEObject)newVariants).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SimdasePackage.ACTIVE_VARIANTS__VARIANTS, null, msgs);
      msgs = basicSetVariants(newVariants, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SimdasePackage.ACTIVE_VARIANTS__VARIANTS, newVariants, newVariants));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ActiveVariant getFirst()
  {
    return first;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetFirst(ActiveVariant newFirst, NotificationChain msgs)
  {
    ActiveVariant oldFirst = first;
    first = newFirst;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SimdasePackage.ACTIVE_VARIANTS__FIRST, oldFirst, newFirst);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setFirst(ActiveVariant newFirst)
  {
    if (newFirst != first)
    {
      NotificationChain msgs = null;
      if (first != null)
        msgs = ((InternalEObject)first).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SimdasePackage.ACTIVE_VARIANTS__FIRST, null, msgs);
      if (newFirst != null)
        msgs = ((InternalEObject)newFirst).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SimdasePackage.ACTIVE_VARIANTS__FIRST, null, msgs);
      msgs = basicSetFirst(newFirst, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SimdasePackage.ACTIVE_VARIANTS__FIRST, newFirst, newFirst));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<ActiveVariant> getRest()
  {
    if (rest == null)
    {
      rest = new EObjectContainmentEList<ActiveVariant>(ActiveVariant.class, this, SimdasePackage.ACTIVE_VARIANTS__REST);
    }
    return rest;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case SimdasePackage.ACTIVE_VARIANTS__VARIANTS:
        return basicSetVariants(null, msgs);
      case SimdasePackage.ACTIVE_VARIANTS__FIRST:
        return basicSetFirst(null, msgs);
      case SimdasePackage.ACTIVE_VARIANTS__REST:
        return ((InternalEList<?>)getRest()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case SimdasePackage.ACTIVE_VARIANTS__VARIANTS:
        return getVariants();
      case SimdasePackage.ACTIVE_VARIANTS__FIRST:
        return getFirst();
      case SimdasePackage.ACTIVE_VARIANTS__REST:
        return getRest();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case SimdasePackage.ACTIVE_VARIANTS__VARIANTS:
        setVariants((ActiveVariants)newValue);
        return;
      case SimdasePackage.ACTIVE_VARIANTS__FIRST:
        setFirst((ActiveVariant)newValue);
        return;
      case SimdasePackage.ACTIVE_VARIANTS__REST:
        getRest().clear();
        getRest().addAll((Collection<? extends ActiveVariant>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case SimdasePackage.ACTIVE_VARIANTS__VARIANTS:
        setVariants((ActiveVariants)null);
        return;
      case SimdasePackage.ACTIVE_VARIANTS__FIRST:
        setFirst((ActiveVariant)null);
        return;
      case SimdasePackage.ACTIVE_VARIANTS__REST:
        getRest().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case SimdasePackage.ACTIVE_VARIANTS__VARIANTS:
        return variants != null;
      case SimdasePackage.ACTIVE_VARIANTS__FIRST:
        return first != null;
      case SimdasePackage.ACTIVE_VARIANTS__REST:
        return rest != null && !rest.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //ActiveVariantsImpl
