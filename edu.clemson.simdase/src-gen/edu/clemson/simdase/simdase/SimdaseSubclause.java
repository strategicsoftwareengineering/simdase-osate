/**
 */
package edu.clemson.simdase.simdase;

import org.eclipse.emf.ecore.EObject;

import org.osate.aadl2.AnnexSubclause;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Subclause</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.clemson.simdase.simdase.SimdasePackage#getSimdaseSubclause()
 * @model
 * @generated
 */
public interface SimdaseSubclause extends EObject, AnnexSubclause
{
} // SimdaseSubclause
