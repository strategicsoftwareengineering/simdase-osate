/**
 */
package edu.clemson.simdase.simdase;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Statement Scaling Factor</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.clemson.simdase.simdase.SimdasePackage#getStatementScalingFactor()
 * @model
 * @generated
 */
public interface StatementScalingFactor extends StatementLvl4
{
} // StatementScalingFactor
