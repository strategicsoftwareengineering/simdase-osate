/**
 */
package edu.clemson.simdase.simdase;

import org.eclipse.emf.ecore.EObject;

import org.osate.aadl2.AnnexLibrary;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Library</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.clemson.simdase.simdase.SimdasePackage#getSimdaseLibrary()
 * @model
 * @generated
 */
public interface SimdaseLibrary extends EObject, AnnexLibrary
{
} // SimdaseLibrary
