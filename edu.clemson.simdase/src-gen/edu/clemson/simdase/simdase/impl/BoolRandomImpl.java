/**
 */
package edu.clemson.simdase.simdase.impl;

import edu.clemson.simdase.simdase.BoolRandom;
import edu.clemson.simdase.simdase.SimdasePackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Bool Random</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class BoolRandomImpl extends BooleanStatementLvl8Impl implements BoolRandom
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected BoolRandomImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return SimdasePackage.Literals.BOOL_RANDOM;
  }

} //BoolRandomImpl
