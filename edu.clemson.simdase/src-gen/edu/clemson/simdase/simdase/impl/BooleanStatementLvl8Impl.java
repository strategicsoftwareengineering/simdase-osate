/**
 */
package edu.clemson.simdase.simdase.impl;

import edu.clemson.simdase.simdase.BooleanStatementLvl8;
import edu.clemson.simdase.simdase.SimdasePackage;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Boolean Statement Lvl8</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class BooleanStatementLvl8Impl extends MinimalEObjectImpl.Container implements BooleanStatementLvl8
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected BooleanStatementLvl8Impl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return SimdasePackage.Literals.BOOLEAN_STATEMENT_LVL8;
  }

} //BooleanStatementLvl8Impl
