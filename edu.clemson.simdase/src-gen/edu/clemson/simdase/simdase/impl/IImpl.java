/**
 */
package edu.clemson.simdase.simdase.impl;

import edu.clemson.simdase.simdase.I;
import edu.clemson.simdase.simdase.SimdasePackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>I</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class IImpl extends StatementLvl4Impl implements I
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected IImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return SimdasePackage.Literals.I;
  }

} //IImpl
