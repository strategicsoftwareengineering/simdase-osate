/**
 */
package edu.clemson.simdase.simdase;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Variants</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.clemson.simdase.simdase.Variants#getVariants <em>Variants</em>}</li>
 *   <li>{@link edu.clemson.simdase.simdase.Variants#getFirst <em>First</em>}</li>
 *   <li>{@link edu.clemson.simdase.simdase.Variants#getRest <em>Rest</em>}</li>
 * </ul>
 *
 * @see edu.clemson.simdase.simdase.SimdasePackage#getVariants()
 * @model
 * @generated
 */
public interface Variants extends SIMDASEStatement
{
  /**
   * Returns the value of the '<em><b>Variants</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Variants</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Variants</em>' containment reference.
   * @see #setVariants(Variants)
   * @see edu.clemson.simdase.simdase.SimdasePackage#getVariants_Variants()
   * @model containment="true"
   * @generated
   */
  Variants getVariants();

  /**
   * Sets the value of the '{@link edu.clemson.simdase.simdase.Variants#getVariants <em>Variants</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Variants</em>' containment reference.
   * @see #getVariants()
   * @generated
   */
  void setVariants(Variants value);

  /**
   * Returns the value of the '<em><b>First</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>First</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>First</em>' containment reference.
   * @see #setFirst(Variant)
   * @see edu.clemson.simdase.simdase.SimdasePackage#getVariants_First()
   * @model containment="true"
   * @generated
   */
  Variant getFirst();

  /**
   * Sets the value of the '{@link edu.clemson.simdase.simdase.Variants#getFirst <em>First</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>First</em>' containment reference.
   * @see #getFirst()
   * @generated
   */
  void setFirst(Variant value);

  /**
   * Returns the value of the '<em><b>Rest</b></em>' containment reference list.
   * The list contents are of type {@link edu.clemson.simdase.simdase.Variant}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Rest</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Rest</em>' containment reference list.
   * @see edu.clemson.simdase.simdase.SimdasePackage#getVariants_Rest()
   * @model containment="true"
   * @generated
   */
  EList<Variant> getRest();

} // Variants
