/**
 */
package edu.clemson.simdase.simdase.impl;

import edu.clemson.simdase.simdase.EmployeeStatement;
import edu.clemson.simdase.simdase.SimdasePackage;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Employee Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class EmployeeStatementImpl extends MinimalEObjectImpl.Container implements EmployeeStatement
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected EmployeeStatementImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return SimdasePackage.Literals.EMPLOYEE_STATEMENT;
  }

} //EmployeeStatementImpl
