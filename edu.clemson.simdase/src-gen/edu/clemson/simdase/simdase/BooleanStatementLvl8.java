/**
 */
package edu.clemson.simdase.simdase;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Boolean Statement Lvl8</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.clemson.simdase.simdase.SimdasePackage#getBooleanStatementLvl8()
 * @model
 * @generated
 */
public interface BooleanStatementLvl8 extends EObject
{
} // BooleanStatementLvl8
