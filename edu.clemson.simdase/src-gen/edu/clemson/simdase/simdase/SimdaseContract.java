/**
 */
package edu.clemson.simdase.simdase;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Contract</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.clemson.simdase.simdase.SimdaseContract#getStatement <em>Statement</em>}</li>
 * </ul>
 *
 * @see edu.clemson.simdase.simdase.SimdasePackage#getSimdaseContract()
 * @model
 * @generated
 */
public interface SimdaseContract extends Contract
{
  /**
   * Returns the value of the '<em><b>Statement</b></em>' containment reference list.
   * The list contents are of type {@link edu.clemson.simdase.simdase.SIMDASEStatement}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Statement</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Statement</em>' containment reference list.
   * @see edu.clemson.simdase.simdase.SimdasePackage#getSimdaseContract_Statement()
   * @model containment="true"
   * @generated
   */
  EList<SIMDASEStatement> getStatement();

} // SimdaseContract
