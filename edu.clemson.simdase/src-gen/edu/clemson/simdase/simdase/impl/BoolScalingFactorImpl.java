/**
 */
package edu.clemson.simdase.simdase.impl;

import edu.clemson.simdase.simdase.BoolScalingFactor;
import edu.clemson.simdase.simdase.SimdasePackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Bool Scaling Factor</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class BoolScalingFactorImpl extends BooleanStatementLvl8Impl implements BoolScalingFactor
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected BoolScalingFactorImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return SimdasePackage.Literals.BOOL_SCALING_FACTOR;
  }

} //BoolScalingFactorImpl
