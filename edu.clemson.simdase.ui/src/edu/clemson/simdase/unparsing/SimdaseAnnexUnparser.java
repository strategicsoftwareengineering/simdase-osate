package edu.clemson.simdase.unparsing;

import org.eclipse.xtext.serializer.ISerializer;
import org.osate.aadl2.AnnexLibrary;
import org.osate.aadl2.AnnexSubclause;
import org.osate.annexsupport.AnnexUnparser;

import com.google.inject.Inject;
import com.google.inject.Injector;

import edu.clemson.simdase.serializer.SimdaseSerializer;
import edu.clemson.simdase.ui.internal.SimdaseActivator;

public class SimdaseAnnexUnparser implements AnnexUnparser {
    @Inject private ISerializer serializer;

    protected ISerializer getSerializer() {
        if (serializer == null) {
            Injector injector = SimdaseActivator.getInstance().getInjector(
            		SimdaseActivator.EDU_CLEMSON_SIMDASE_SIMDASE);
            serializer = injector.getInstance(SimdaseSerializer.class);
        }
        return serializer;
    }

	@Override
	public String unparseAnnexLibrary(AnnexLibrary library, String indent) {
		return indent + getSerializer().serialize(library);
	}

	@Override
	public String unparseAnnexSubclause(AnnexSubclause subclause, String indent) {
		return indent + getSerializer().serialize(subclause);
	}
}