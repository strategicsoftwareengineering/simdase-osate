# Structured Intuitive Model for Dynamic Adaptive System Economics (SIMDASE) OSATE Edition

This repository holds the plugin edition for the SIMDASE cost estimation tooling for the OSATE IDE.

## Installation

In order to use the contents of this repository, go to the "Install New Software" menu within OSATE. Add a new update site with the url "https://bitbucket.org/strategicsoftwareengineering/simdase-update-site/raw/master/site.xml". Select Simdase from the list of install candidates.

## Contributions

Contributions are accepted provided there is a genuine, demonstratable need for the addition.  In general, nice to haves are not accepted.  If you are unsure, please contact the [primary investigator](mailto:etmcgee@g.clemson.edu) before making the pull request.  Pull requests will be merged after they have been reviewed.

## License

Copyright 2017 Ethan McGee

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
