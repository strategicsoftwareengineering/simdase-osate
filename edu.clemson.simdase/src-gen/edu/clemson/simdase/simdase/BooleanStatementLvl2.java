/**
 */
package edu.clemson.simdase.simdase;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Boolean Statement Lvl2</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.clemson.simdase.simdase.BooleanStatementLvl2#getStatement <em>Statement</em>}</li>
 *   <li>{@link edu.clemson.simdase.simdase.BooleanStatementLvl2#getOp <em>Op</em>}</li>
 *   <li>{@link edu.clemson.simdase.simdase.BooleanStatementLvl2#getRest <em>Rest</em>}</li>
 * </ul>
 *
 * @see edu.clemson.simdase.simdase.SimdasePackage#getBooleanStatementLvl2()
 * @model
 * @generated
 */
public interface BooleanStatementLvl2 extends EObject
{
  /**
   * Returns the value of the '<em><b>Statement</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Statement</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Statement</em>' containment reference.
   * @see #setStatement(BooleanStatementLvl3)
   * @see edu.clemson.simdase.simdase.SimdasePackage#getBooleanStatementLvl2_Statement()
   * @model containment="true"
   * @generated
   */
  BooleanStatementLvl3 getStatement();

  /**
   * Sets the value of the '{@link edu.clemson.simdase.simdase.BooleanStatementLvl2#getStatement <em>Statement</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Statement</em>' containment reference.
   * @see #getStatement()
   * @generated
   */
  void setStatement(BooleanStatementLvl3 value);

  /**
   * Returns the value of the '<em><b>Op</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Op</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Op</em>' attribute list.
   * @see edu.clemson.simdase.simdase.SimdasePackage#getBooleanStatementLvl2_Op()
   * @model unique="false"
   * @generated
   */
  EList<String> getOp();

  /**
   * Returns the value of the '<em><b>Rest</b></em>' containment reference list.
   * The list contents are of type {@link edu.clemson.simdase.simdase.BooleanStatementLvl3}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Rest</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Rest</em>' containment reference list.
   * @see edu.clemson.simdase.simdase.SimdasePackage#getBooleanStatementLvl2_Rest()
   * @model containment="true"
   * @generated
   */
  EList<BooleanStatementLvl3> getRest();

} // BooleanStatementLvl2
