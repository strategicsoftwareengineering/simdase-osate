/**
 */
package edu.clemson.simdase.simdase.impl;

import edu.clemson.simdase.simdase.BoolI;
import edu.clemson.simdase.simdase.SimdasePackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Bool I</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class BoolIImpl extends BooleanStatementLvl8Impl implements BoolI
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected BoolIImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return SimdasePackage.Literals.BOOL_I;
  }

} //BoolIImpl
