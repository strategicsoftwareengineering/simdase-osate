/**
 */
package edu.clemson.simdase.simdase.impl;

import edu.clemson.simdase.simdase.BooleanStatement;
import edu.clemson.simdase.simdase.If;
import edu.clemson.simdase.simdase.SimdasePackage;
import edu.clemson.simdase.simdase.Statement;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>If</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.clemson.simdase.simdase.impl.IfImpl#getBooleanStatement <em>Boolean Statement</em>}</li>
 *   <li>{@link edu.clemson.simdase.simdase.impl.IfImpl#getTrueStatement <em>True Statement</em>}</li>
 *   <li>{@link edu.clemson.simdase.simdase.impl.IfImpl#getFalseStatement <em>False Statement</em>}</li>
 * </ul>
 *
 * @generated
 */
public class IfImpl extends StatementLvl4Impl implements If
{
  /**
   * The cached value of the '{@link #getBooleanStatement() <em>Boolean Statement</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBooleanStatement()
   * @generated
   * @ordered
   */
  protected BooleanStatement booleanStatement;

  /**
   * The cached value of the '{@link #getTrueStatement() <em>True Statement</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTrueStatement()
   * @generated
   * @ordered
   */
  protected Statement trueStatement;

  /**
   * The cached value of the '{@link #getFalseStatement() <em>False Statement</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFalseStatement()
   * @generated
   * @ordered
   */
  protected Statement falseStatement;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected IfImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return SimdasePackage.Literals.IF;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BooleanStatement getBooleanStatement()
  {
    return booleanStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetBooleanStatement(BooleanStatement newBooleanStatement, NotificationChain msgs)
  {
    BooleanStatement oldBooleanStatement = booleanStatement;
    booleanStatement = newBooleanStatement;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SimdasePackage.IF__BOOLEAN_STATEMENT, oldBooleanStatement, newBooleanStatement);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBooleanStatement(BooleanStatement newBooleanStatement)
  {
    if (newBooleanStatement != booleanStatement)
    {
      NotificationChain msgs = null;
      if (booleanStatement != null)
        msgs = ((InternalEObject)booleanStatement).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SimdasePackage.IF__BOOLEAN_STATEMENT, null, msgs);
      if (newBooleanStatement != null)
        msgs = ((InternalEObject)newBooleanStatement).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SimdasePackage.IF__BOOLEAN_STATEMENT, null, msgs);
      msgs = basicSetBooleanStatement(newBooleanStatement, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SimdasePackage.IF__BOOLEAN_STATEMENT, newBooleanStatement, newBooleanStatement));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Statement getTrueStatement()
  {
    return trueStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetTrueStatement(Statement newTrueStatement, NotificationChain msgs)
  {
    Statement oldTrueStatement = trueStatement;
    trueStatement = newTrueStatement;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SimdasePackage.IF__TRUE_STATEMENT, oldTrueStatement, newTrueStatement);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTrueStatement(Statement newTrueStatement)
  {
    if (newTrueStatement != trueStatement)
    {
      NotificationChain msgs = null;
      if (trueStatement != null)
        msgs = ((InternalEObject)trueStatement).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SimdasePackage.IF__TRUE_STATEMENT, null, msgs);
      if (newTrueStatement != null)
        msgs = ((InternalEObject)newTrueStatement).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SimdasePackage.IF__TRUE_STATEMENT, null, msgs);
      msgs = basicSetTrueStatement(newTrueStatement, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SimdasePackage.IF__TRUE_STATEMENT, newTrueStatement, newTrueStatement));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Statement getFalseStatement()
  {
    return falseStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetFalseStatement(Statement newFalseStatement, NotificationChain msgs)
  {
    Statement oldFalseStatement = falseStatement;
    falseStatement = newFalseStatement;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SimdasePackage.IF__FALSE_STATEMENT, oldFalseStatement, newFalseStatement);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setFalseStatement(Statement newFalseStatement)
  {
    if (newFalseStatement != falseStatement)
    {
      NotificationChain msgs = null;
      if (falseStatement != null)
        msgs = ((InternalEObject)falseStatement).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SimdasePackage.IF__FALSE_STATEMENT, null, msgs);
      if (newFalseStatement != null)
        msgs = ((InternalEObject)newFalseStatement).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SimdasePackage.IF__FALSE_STATEMENT, null, msgs);
      msgs = basicSetFalseStatement(newFalseStatement, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SimdasePackage.IF__FALSE_STATEMENT, newFalseStatement, newFalseStatement));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case SimdasePackage.IF__BOOLEAN_STATEMENT:
        return basicSetBooleanStatement(null, msgs);
      case SimdasePackage.IF__TRUE_STATEMENT:
        return basicSetTrueStatement(null, msgs);
      case SimdasePackage.IF__FALSE_STATEMENT:
        return basicSetFalseStatement(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case SimdasePackage.IF__BOOLEAN_STATEMENT:
        return getBooleanStatement();
      case SimdasePackage.IF__TRUE_STATEMENT:
        return getTrueStatement();
      case SimdasePackage.IF__FALSE_STATEMENT:
        return getFalseStatement();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case SimdasePackage.IF__BOOLEAN_STATEMENT:
        setBooleanStatement((BooleanStatement)newValue);
        return;
      case SimdasePackage.IF__TRUE_STATEMENT:
        setTrueStatement((Statement)newValue);
        return;
      case SimdasePackage.IF__FALSE_STATEMENT:
        setFalseStatement((Statement)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case SimdasePackage.IF__BOOLEAN_STATEMENT:
        setBooleanStatement((BooleanStatement)null);
        return;
      case SimdasePackage.IF__TRUE_STATEMENT:
        setTrueStatement((Statement)null);
        return;
      case SimdasePackage.IF__FALSE_STATEMENT:
        setFalseStatement((Statement)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case SimdasePackage.IF__BOOLEAN_STATEMENT:
        return booleanStatement != null;
      case SimdasePackage.IF__TRUE_STATEMENT:
        return trueStatement != null;
      case SimdasePackage.IF__FALSE_STATEMENT:
        return falseStatement != null;
    }
    return super.eIsSet(featureID);
  }

} //IfImpl
