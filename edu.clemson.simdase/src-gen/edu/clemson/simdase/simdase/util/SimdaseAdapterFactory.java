/**
 */
package edu.clemson.simdase.simdase.util;

import edu.clemson.simdase.simdase.ActiveVariant;
import edu.clemson.simdase.simdase.ActiveVariants;
import edu.clemson.simdase.simdase.BoolBoolean;
import edu.clemson.simdase.simdase.BoolCos;
import edu.clemson.simdase.simdase.BoolI;
import edu.clemson.simdase.simdase.BoolInteger;
import edu.clemson.simdase.simdase.BoolParen;
import edu.clemson.simdase.simdase.BoolRandom;
import edu.clemson.simdase.simdase.BoolScalingFactor;
import edu.clemson.simdase.simdase.BoolSin;
import edu.clemson.simdase.simdase.BoolT;
import edu.clemson.simdase.simdase.BoolTan;
import edu.clemson.simdase.simdase.BooleanStatement;
import edu.clemson.simdase.simdase.BooleanStatementLvl2;
import edu.clemson.simdase.simdase.BooleanStatementLvl3;
import edu.clemson.simdase.simdase.BooleanStatementLvl4;
import edu.clemson.simdase.simdase.BooleanStatementLvl5;
import edu.clemson.simdase.simdase.BooleanStatementLvl6;
import edu.clemson.simdase.simdase.BooleanStatementLvl7;
import edu.clemson.simdase.simdase.BooleanStatementLvl8;
import edu.clemson.simdase.simdase.Contract;
import edu.clemson.simdase.simdase.Cos;
import edu.clemson.simdase.simdase.CostProperties;
import edu.clemson.simdase.simdase.CostStatement;
import edu.clemson.simdase.simdase.CostValue;
import edu.clemson.simdase.simdase.EmployeeStatement;
import edu.clemson.simdase.simdase.FixedCost;
import edu.clemson.simdase.simdase.HourlyCost;
import edu.clemson.simdase.simdase.I;
import edu.clemson.simdase.simdase.If;
import edu.clemson.simdase.simdase.IsAdaptiveStatement;
import edu.clemson.simdase.simdase.Paren;
import edu.clemson.simdase.simdase.Random;
import edu.clemson.simdase.simdase.ReEvaluateAveraging;
import edu.clemson.simdase.simdase.SIMDASEStatement;
import edu.clemson.simdase.simdase.ScalingFactor;
import edu.clemson.simdase.simdase.SimdaseContract;
import edu.clemson.simdase.simdase.SimdaseContractLibrary;
import edu.clemson.simdase.simdase.SimdaseContractSubclause;
import edu.clemson.simdase.simdase.SimdaseLibrary;
import edu.clemson.simdase.simdase.SimdasePackage;
import edu.clemson.simdase.simdase.SimdaseSubclause;
import edu.clemson.simdase.simdase.Sin;
import edu.clemson.simdase.simdase.Statement;
import edu.clemson.simdase.simdase.StatementLvl2;
import edu.clemson.simdase.simdase.StatementLvl3;
import edu.clemson.simdase.simdase.StatementLvl4;
import edu.clemson.simdase.simdase.StatementScalingFactor;
import edu.clemson.simdase.simdase.T;
import edu.clemson.simdase.simdase.TMax;
import edu.clemson.simdase.simdase.TMin;
import edu.clemson.simdase.simdase.Tan;
import edu.clemson.simdase.simdase.Variant;
import edu.clemson.simdase.simdase.VariantNames;
import edu.clemson.simdase.simdase.Variants;
import edu.clemson.simdase.simdase.WithNot;
import edu.clemson.simdase.simdase.WithoutNot;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

import org.osate.aadl2.AnnexLibrary;
import org.osate.aadl2.AnnexSubclause;
import org.osate.aadl2.Element;
import org.osate.aadl2.ModalElement;
import org.osate.aadl2.NamedElement;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see edu.clemson.simdase.simdase.SimdasePackage
 * @generated
 */
public class SimdaseAdapterFactory extends AdapterFactoryImpl
{
  /**
   * The cached model package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected static SimdasePackage modelPackage;

  /**
   * Creates an instance of the adapter factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SimdaseAdapterFactory()
  {
    if (modelPackage == null)
    {
      modelPackage = SimdasePackage.eINSTANCE;
    }
  }

  /**
   * Returns whether this factory is applicable for the type of the object.
   * <!-- begin-user-doc -->
   * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
   * <!-- end-user-doc -->
   * @return whether this factory is applicable for the type of the object.
   * @generated
   */
  @Override
  public boolean isFactoryForType(Object object)
  {
    if (object == modelPackage)
    {
      return true;
    }
    if (object instanceof EObject)
    {
      return ((EObject)object).eClass().getEPackage() == modelPackage;
    }
    return false;
  }

  /**
   * The switch that delegates to the <code>createXXX</code> methods.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected SimdaseSwitch<Adapter> modelSwitch =
    new SimdaseSwitch<Adapter>()
    {
      @Override
      public Adapter caseSimdaseLibrary(SimdaseLibrary object)
      {
        return createSimdaseLibraryAdapter();
      }
      @Override
      public Adapter caseSimdaseSubclause(SimdaseSubclause object)
      {
        return createSimdaseSubclauseAdapter();
      }
      @Override
      public Adapter caseContract(Contract object)
      {
        return createContractAdapter();
      }
      @Override
      public Adapter caseSIMDASEStatement(SIMDASEStatement object)
      {
        return createSIMDASEStatementAdapter();
      }
      @Override
      public Adapter caseVariants(Variants object)
      {
        return createVariantsAdapter();
      }
      @Override
      public Adapter caseVariant(Variant object)
      {
        return createVariantAdapter();
      }
      @Override
      public Adapter caseActiveVariants(ActiveVariants object)
      {
        return createActiveVariantsAdapter();
      }
      @Override
      public Adapter caseActiveVariant(ActiveVariant object)
      {
        return createActiveVariantAdapter();
      }
      @Override
      public Adapter caseVariantNames(VariantNames object)
      {
        return createVariantNamesAdapter();
      }
      @Override
      public Adapter caseCostProperties(CostProperties object)
      {
        return createCostPropertiesAdapter();
      }
      @Override
      public Adapter caseCostStatement(CostStatement object)
      {
        return createCostStatementAdapter();
      }
      @Override
      public Adapter caseCostValue(CostValue object)
      {
        return createCostValueAdapter();
      }
      @Override
      public Adapter caseEmployeeStatement(EmployeeStatement object)
      {
        return createEmployeeStatementAdapter();
      }
      @Override
      public Adapter caseBoolean(edu.clemson.simdase.simdase.Boolean object)
      {
        return createBooleanAdapter();
      }
      @Override
      public Adapter caseNumber(edu.clemson.simdase.simdase.Number object)
      {
        return createNumberAdapter();
      }
      @Override
      public Adapter caseStatement(Statement object)
      {
        return createStatementAdapter();
      }
      @Override
      public Adapter caseStatementLvl2(StatementLvl2 object)
      {
        return createStatementLvl2Adapter();
      }
      @Override
      public Adapter caseStatementLvl3(StatementLvl3 object)
      {
        return createStatementLvl3Adapter();
      }
      @Override
      public Adapter caseStatementLvl4(StatementLvl4 object)
      {
        return createStatementLvl4Adapter();
      }
      @Override
      public Adapter caseBooleanStatement(BooleanStatement object)
      {
        return createBooleanStatementAdapter();
      }
      @Override
      public Adapter caseBooleanStatementLvl2(BooleanStatementLvl2 object)
      {
        return createBooleanStatementLvl2Adapter();
      }
      @Override
      public Adapter caseBooleanStatementLvl3(BooleanStatementLvl3 object)
      {
        return createBooleanStatementLvl3Adapter();
      }
      @Override
      public Adapter caseBooleanStatementLvl4(BooleanStatementLvl4 object)
      {
        return createBooleanStatementLvl4Adapter();
      }
      @Override
      public Adapter caseBooleanStatementLvl5(BooleanStatementLvl5 object)
      {
        return createBooleanStatementLvl5Adapter();
      }
      @Override
      public Adapter caseBooleanStatementLvl6(BooleanStatementLvl6 object)
      {
        return createBooleanStatementLvl6Adapter();
      }
      @Override
      public Adapter caseBooleanStatementLvl7(BooleanStatementLvl7 object)
      {
        return createBooleanStatementLvl7Adapter();
      }
      @Override
      public Adapter caseBooleanStatementLvl8(BooleanStatementLvl8 object)
      {
        return createBooleanStatementLvl8Adapter();
      }
      @Override
      public Adapter caseSimdaseContractLibrary(SimdaseContractLibrary object)
      {
        return createSimdaseContractLibraryAdapter();
      }
      @Override
      public Adapter caseSimdaseContractSubclause(SimdaseContractSubclause object)
      {
        return createSimdaseContractSubclauseAdapter();
      }
      @Override
      public Adapter caseSimdaseContract(SimdaseContract object)
      {
        return createSimdaseContractAdapter();
      }
      @Override
      public Adapter caseIsAdaptiveStatement(IsAdaptiveStatement object)
      {
        return createIsAdaptiveStatementAdapter();
      }
      @Override
      public Adapter caseTMax(TMax object)
      {
        return createTMaxAdapter();
      }
      @Override
      public Adapter caseTMin(TMin object)
      {
        return createTMinAdapter();
      }
      @Override
      public Adapter caseScalingFactor(ScalingFactor object)
      {
        return createScalingFactorAdapter();
      }
      @Override
      public Adapter caseReEvaluateAveraging(ReEvaluateAveraging object)
      {
        return createReEvaluateAveragingAdapter();
      }
      @Override
      public Adapter caseHourlyCost(HourlyCost object)
      {
        return createHourlyCostAdapter();
      }
      @Override
      public Adapter caseFixedCost(FixedCost object)
      {
        return createFixedCostAdapter();
      }
      @Override
      public Adapter caseInteger(edu.clemson.simdase.simdase.Integer object)
      {
        return createIntegerAdapter();
      }
      @Override
      public Adapter caseT(T object)
      {
        return createTAdapter();
      }
      @Override
      public Adapter caseI(I object)
      {
        return createIAdapter();
      }
      @Override
      public Adapter caseSin(Sin object)
      {
        return createSinAdapter();
      }
      @Override
      public Adapter caseCos(Cos object)
      {
        return createCosAdapter();
      }
      @Override
      public Adapter caseTan(Tan object)
      {
        return createTanAdapter();
      }
      @Override
      public Adapter caseParen(Paren object)
      {
        return createParenAdapter();
      }
      @Override
      public Adapter caseIf(If object)
      {
        return createIfAdapter();
      }
      @Override
      public Adapter caseRandom(Random object)
      {
        return createRandomAdapter();
      }
      @Override
      public Adapter caseStatementScalingFactor(StatementScalingFactor object)
      {
        return createStatementScalingFactorAdapter();
      }
      @Override
      public Adapter caseWithNot(WithNot object)
      {
        return createWithNotAdapter();
      }
      @Override
      public Adapter caseWithoutNot(WithoutNot object)
      {
        return createWithoutNotAdapter();
      }
      @Override
      public Adapter caseBoolBoolean(BoolBoolean object)
      {
        return createBoolBooleanAdapter();
      }
      @Override
      public Adapter caseBoolT(BoolT object)
      {
        return createBoolTAdapter();
      }
      @Override
      public Adapter caseBoolI(BoolI object)
      {
        return createBoolIAdapter();
      }
      @Override
      public Adapter caseBoolInteger(BoolInteger object)
      {
        return createBoolIntegerAdapter();
      }
      @Override
      public Adapter caseBoolSin(BoolSin object)
      {
        return createBoolSinAdapter();
      }
      @Override
      public Adapter caseBoolCos(BoolCos object)
      {
        return createBoolCosAdapter();
      }
      @Override
      public Adapter caseBoolTan(BoolTan object)
      {
        return createBoolTanAdapter();
      }
      @Override
      public Adapter caseBoolParen(BoolParen object)
      {
        return createBoolParenAdapter();
      }
      @Override
      public Adapter caseBoolRandom(BoolRandom object)
      {
        return createBoolRandomAdapter();
      }
      @Override
      public Adapter caseBoolScalingFactor(BoolScalingFactor object)
      {
        return createBoolScalingFactorAdapter();
      }
      @Override
      public Adapter caseElement(Element object)
      {
        return createElementAdapter();
      }
      @Override
      public Adapter caseNamedElement(NamedElement object)
      {
        return createNamedElementAdapter();
      }
      @Override
      public Adapter caseAnnexLibrary(AnnexLibrary object)
      {
        return createAnnexLibraryAdapter();
      }
      @Override
      public Adapter caseModalElement(ModalElement object)
      {
        return createModalElementAdapter();
      }
      @Override
      public Adapter caseAnnexSubclause(AnnexSubclause object)
      {
        return createAnnexSubclauseAdapter();
      }
      @Override
      public Adapter defaultCase(EObject object)
      {
        return createEObjectAdapter();
      }
    };

  /**
   * Creates an adapter for the <code>target</code>.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param target the object to adapt.
   * @return the adapter for the <code>target</code>.
   * @generated
   */
  @Override
  public Adapter createAdapter(Notifier target)
  {
    return modelSwitch.doSwitch((EObject)target);
  }


  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.simdase.simdase.SimdaseLibrary <em>Library</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.simdase.simdase.SimdaseLibrary
   * @generated
   */
  public Adapter createSimdaseLibraryAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.simdase.simdase.SimdaseSubclause <em>Subclause</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.simdase.simdase.SimdaseSubclause
   * @generated
   */
  public Adapter createSimdaseSubclauseAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.simdase.simdase.Contract <em>Contract</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.simdase.simdase.Contract
   * @generated
   */
  public Adapter createContractAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.simdase.simdase.SIMDASEStatement <em>SIMDASE Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.simdase.simdase.SIMDASEStatement
   * @generated
   */
  public Adapter createSIMDASEStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.simdase.simdase.Variants <em>Variants</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.simdase.simdase.Variants
   * @generated
   */
  public Adapter createVariantsAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.simdase.simdase.Variant <em>Variant</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.simdase.simdase.Variant
   * @generated
   */
  public Adapter createVariantAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.simdase.simdase.ActiveVariants <em>Active Variants</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.simdase.simdase.ActiveVariants
   * @generated
   */
  public Adapter createActiveVariantsAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.simdase.simdase.ActiveVariant <em>Active Variant</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.simdase.simdase.ActiveVariant
   * @generated
   */
  public Adapter createActiveVariantAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.simdase.simdase.VariantNames <em>Variant Names</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.simdase.simdase.VariantNames
   * @generated
   */
  public Adapter createVariantNamesAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.simdase.simdase.CostProperties <em>Cost Properties</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.simdase.simdase.CostProperties
   * @generated
   */
  public Adapter createCostPropertiesAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.simdase.simdase.CostStatement <em>Cost Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.simdase.simdase.CostStatement
   * @generated
   */
  public Adapter createCostStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.simdase.simdase.CostValue <em>Cost Value</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.simdase.simdase.CostValue
   * @generated
   */
  public Adapter createCostValueAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.simdase.simdase.EmployeeStatement <em>Employee Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.simdase.simdase.EmployeeStatement
   * @generated
   */
  public Adapter createEmployeeStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.simdase.simdase.Boolean <em>Boolean</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.simdase.simdase.Boolean
   * @generated
   */
  public Adapter createBooleanAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.simdase.simdase.Number <em>Number</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.simdase.simdase.Number
   * @generated
   */
  public Adapter createNumberAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.simdase.simdase.Statement <em>Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.simdase.simdase.Statement
   * @generated
   */
  public Adapter createStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.simdase.simdase.StatementLvl2 <em>Statement Lvl2</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.simdase.simdase.StatementLvl2
   * @generated
   */
  public Adapter createStatementLvl2Adapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.simdase.simdase.StatementLvl3 <em>Statement Lvl3</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.simdase.simdase.StatementLvl3
   * @generated
   */
  public Adapter createStatementLvl3Adapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.simdase.simdase.StatementLvl4 <em>Statement Lvl4</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.simdase.simdase.StatementLvl4
   * @generated
   */
  public Adapter createStatementLvl4Adapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.simdase.simdase.BooleanStatement <em>Boolean Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.simdase.simdase.BooleanStatement
   * @generated
   */
  public Adapter createBooleanStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.simdase.simdase.BooleanStatementLvl2 <em>Boolean Statement Lvl2</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.simdase.simdase.BooleanStatementLvl2
   * @generated
   */
  public Adapter createBooleanStatementLvl2Adapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.simdase.simdase.BooleanStatementLvl3 <em>Boolean Statement Lvl3</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.simdase.simdase.BooleanStatementLvl3
   * @generated
   */
  public Adapter createBooleanStatementLvl3Adapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.simdase.simdase.BooleanStatementLvl4 <em>Boolean Statement Lvl4</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.simdase.simdase.BooleanStatementLvl4
   * @generated
   */
  public Adapter createBooleanStatementLvl4Adapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.simdase.simdase.BooleanStatementLvl5 <em>Boolean Statement Lvl5</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.simdase.simdase.BooleanStatementLvl5
   * @generated
   */
  public Adapter createBooleanStatementLvl5Adapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.simdase.simdase.BooleanStatementLvl6 <em>Boolean Statement Lvl6</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.simdase.simdase.BooleanStatementLvl6
   * @generated
   */
  public Adapter createBooleanStatementLvl6Adapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.simdase.simdase.BooleanStatementLvl7 <em>Boolean Statement Lvl7</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.simdase.simdase.BooleanStatementLvl7
   * @generated
   */
  public Adapter createBooleanStatementLvl7Adapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.simdase.simdase.BooleanStatementLvl8 <em>Boolean Statement Lvl8</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.simdase.simdase.BooleanStatementLvl8
   * @generated
   */
  public Adapter createBooleanStatementLvl8Adapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.simdase.simdase.SimdaseContractLibrary <em>Contract Library</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.simdase.simdase.SimdaseContractLibrary
   * @generated
   */
  public Adapter createSimdaseContractLibraryAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.simdase.simdase.SimdaseContractSubclause <em>Contract Subclause</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.simdase.simdase.SimdaseContractSubclause
   * @generated
   */
  public Adapter createSimdaseContractSubclauseAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.simdase.simdase.SimdaseContract <em>Contract</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.simdase.simdase.SimdaseContract
   * @generated
   */
  public Adapter createSimdaseContractAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.simdase.simdase.IsAdaptiveStatement <em>Is Adaptive Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.simdase.simdase.IsAdaptiveStatement
   * @generated
   */
  public Adapter createIsAdaptiveStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.simdase.simdase.TMax <em>TMax</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.simdase.simdase.TMax
   * @generated
   */
  public Adapter createTMaxAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.simdase.simdase.TMin <em>TMin</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.simdase.simdase.TMin
   * @generated
   */
  public Adapter createTMinAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.simdase.simdase.ScalingFactor <em>Scaling Factor</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.simdase.simdase.ScalingFactor
   * @generated
   */
  public Adapter createScalingFactorAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.simdase.simdase.ReEvaluateAveraging <em>Re Evaluate Averaging</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.simdase.simdase.ReEvaluateAveraging
   * @generated
   */
  public Adapter createReEvaluateAveragingAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.simdase.simdase.HourlyCost <em>Hourly Cost</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.simdase.simdase.HourlyCost
   * @generated
   */
  public Adapter createHourlyCostAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.simdase.simdase.FixedCost <em>Fixed Cost</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.simdase.simdase.FixedCost
   * @generated
   */
  public Adapter createFixedCostAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.simdase.simdase.Integer <em>Integer</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.simdase.simdase.Integer
   * @generated
   */
  public Adapter createIntegerAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.simdase.simdase.T <em>T</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.simdase.simdase.T
   * @generated
   */
  public Adapter createTAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.simdase.simdase.I <em>I</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.simdase.simdase.I
   * @generated
   */
  public Adapter createIAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.simdase.simdase.Sin <em>Sin</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.simdase.simdase.Sin
   * @generated
   */
  public Adapter createSinAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.simdase.simdase.Cos <em>Cos</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.simdase.simdase.Cos
   * @generated
   */
  public Adapter createCosAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.simdase.simdase.Tan <em>Tan</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.simdase.simdase.Tan
   * @generated
   */
  public Adapter createTanAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.simdase.simdase.Paren <em>Paren</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.simdase.simdase.Paren
   * @generated
   */
  public Adapter createParenAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.simdase.simdase.If <em>If</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.simdase.simdase.If
   * @generated
   */
  public Adapter createIfAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.simdase.simdase.Random <em>Random</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.simdase.simdase.Random
   * @generated
   */
  public Adapter createRandomAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.simdase.simdase.StatementScalingFactor <em>Statement Scaling Factor</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.simdase.simdase.StatementScalingFactor
   * @generated
   */
  public Adapter createStatementScalingFactorAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.simdase.simdase.WithNot <em>With Not</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.simdase.simdase.WithNot
   * @generated
   */
  public Adapter createWithNotAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.simdase.simdase.WithoutNot <em>Without Not</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.simdase.simdase.WithoutNot
   * @generated
   */
  public Adapter createWithoutNotAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.simdase.simdase.BoolBoolean <em>Bool Boolean</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.simdase.simdase.BoolBoolean
   * @generated
   */
  public Adapter createBoolBooleanAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.simdase.simdase.BoolT <em>Bool T</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.simdase.simdase.BoolT
   * @generated
   */
  public Adapter createBoolTAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.simdase.simdase.BoolI <em>Bool I</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.simdase.simdase.BoolI
   * @generated
   */
  public Adapter createBoolIAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.simdase.simdase.BoolInteger <em>Bool Integer</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.simdase.simdase.BoolInteger
   * @generated
   */
  public Adapter createBoolIntegerAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.simdase.simdase.BoolSin <em>Bool Sin</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.simdase.simdase.BoolSin
   * @generated
   */
  public Adapter createBoolSinAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.simdase.simdase.BoolCos <em>Bool Cos</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.simdase.simdase.BoolCos
   * @generated
   */
  public Adapter createBoolCosAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.simdase.simdase.BoolTan <em>Bool Tan</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.simdase.simdase.BoolTan
   * @generated
   */
  public Adapter createBoolTanAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.simdase.simdase.BoolParen <em>Bool Paren</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.simdase.simdase.BoolParen
   * @generated
   */
  public Adapter createBoolParenAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.simdase.simdase.BoolRandom <em>Bool Random</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.simdase.simdase.BoolRandom
   * @generated
   */
  public Adapter createBoolRandomAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.simdase.simdase.BoolScalingFactor <em>Bool Scaling Factor</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.simdase.simdase.BoolScalingFactor
   * @generated
   */
  public Adapter createBoolScalingFactorAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.osate.aadl2.Element <em>Element</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.osate.aadl2.Element
   * @generated
   */
  public Adapter createElementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.osate.aadl2.NamedElement <em>Named Element</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.osate.aadl2.NamedElement
   * @generated
   */
  public Adapter createNamedElementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.osate.aadl2.AnnexLibrary <em>Annex Library</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.osate.aadl2.AnnexLibrary
   * @generated
   */
  public Adapter createAnnexLibraryAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.osate.aadl2.ModalElement <em>Modal Element</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.osate.aadl2.ModalElement
   * @generated
   */
  public Adapter createModalElementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.osate.aadl2.AnnexSubclause <em>Annex Subclause</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.osate.aadl2.AnnexSubclause
   * @generated
   */
  public Adapter createAnnexSubclauseAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for the default case.
   * <!-- begin-user-doc -->
   * This default implementation returns null.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @generated
   */
  public Adapter createEObjectAdapter()
  {
    return null;
  }

} //SimdaseAdapterFactory
