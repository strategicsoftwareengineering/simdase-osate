/**
 */
package edu.clemson.simdase.simdase.impl;

import edu.clemson.simdase.simdase.BoolT;
import edu.clemson.simdase.simdase.SimdasePackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Bool T</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class BoolTImpl extends BooleanStatementLvl8Impl implements BoolT
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected BoolTImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return SimdasePackage.Literals.BOOL_T;
  }

} //BoolTImpl
