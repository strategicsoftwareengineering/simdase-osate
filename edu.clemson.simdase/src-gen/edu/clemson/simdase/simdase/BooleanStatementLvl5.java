/**
 */
package edu.clemson.simdase.simdase;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Boolean Statement Lvl5</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.clemson.simdase.simdase.BooleanStatementLvl5#getStatement <em>Statement</em>}</li>
 *   <li>{@link edu.clemson.simdase.simdase.BooleanStatementLvl5#getOp <em>Op</em>}</li>
 *   <li>{@link edu.clemson.simdase.simdase.BooleanStatementLvl5#getRest <em>Rest</em>}</li>
 * </ul>
 *
 * @see edu.clemson.simdase.simdase.SimdasePackage#getBooleanStatementLvl5()
 * @model
 * @generated
 */
public interface BooleanStatementLvl5 extends EObject
{
  /**
   * Returns the value of the '<em><b>Statement</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Statement</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Statement</em>' containment reference.
   * @see #setStatement(BooleanStatementLvl6)
   * @see edu.clemson.simdase.simdase.SimdasePackage#getBooleanStatementLvl5_Statement()
   * @model containment="true"
   * @generated
   */
  BooleanStatementLvl6 getStatement();

  /**
   * Sets the value of the '{@link edu.clemson.simdase.simdase.BooleanStatementLvl5#getStatement <em>Statement</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Statement</em>' containment reference.
   * @see #getStatement()
   * @generated
   */
  void setStatement(BooleanStatementLvl6 value);

  /**
   * Returns the value of the '<em><b>Op</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Op</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Op</em>' attribute list.
   * @see edu.clemson.simdase.simdase.SimdasePackage#getBooleanStatementLvl5_Op()
   * @model unique="false"
   * @generated
   */
  EList<String> getOp();

  /**
   * Returns the value of the '<em><b>Rest</b></em>' containment reference list.
   * The list contents are of type {@link edu.clemson.simdase.simdase.BooleanStatementLvl6}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Rest</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Rest</em>' containment reference list.
   * @see edu.clemson.simdase.simdase.SimdasePackage#getBooleanStatementLvl5_Rest()
   * @model containment="true"
   * @generated
   */
  EList<BooleanStatementLvl6> getRest();

} // BooleanStatementLvl5
