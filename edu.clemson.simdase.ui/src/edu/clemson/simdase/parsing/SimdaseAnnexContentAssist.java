package edu.clemson.simdase.parsing;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.resource.EObjectAtOffsetHelper;
import org.eclipse.xtext.resource.XtextResource;
import org.osate.aadl2.AnnexLibrary;
import org.osate.aadl2.AnnexSubclause;
import org.osate.aadl2.DefaultAnnexLibrary;
import org.osate.aadl2.DefaultAnnexSubclause;
import org.osate.annexsupport.AnnexContentAssist;
import org.osate.xtext.aadl2.properties.ui.contentassist.PropertiesProposalProvider;

import com.google.inject.Injector;
import edu.clemson.simdase.ui.internal.SimdaseActivator;
import edu.clemson.simdase.ui.contentassist.SimdaseProposalProvider;

public class SimdaseAnnexContentAssist implements AnnexContentAssist {
	final private Injector injector = SimdaseActivator.getInstance().getInjector(
			SimdaseActivator.EDU_CLEMSON_SIMDASE_SIMDASE);


	private PropertiesProposalProvider propPropProv;
	private SimdaseAnnexParser parser;
	private EObjectAtOffsetHelper offsetHelper;

	protected PropertiesProposalProvider getLinkingService() {
		if (propPropProv == null) {
			propPropProv = injector.getInstance(SimdaseProposalProvider.class);
		}
		return propPropProv;
	}
	
	protected EObjectAtOffsetHelper getOffsetHelper() {
		if(offsetHelper == null){
			offsetHelper = injector.getInstance(EObjectAtOffsetHelper.class);
		}
		return offsetHelper;
	}

	@Override
	public List<String> annexCompletionSuggestions(EObject defaultAnnex, int offset) {

		offset = (offset <= 0) ? 0 : offset - 1; //get one character back
		EObjectAtOffsetHelper helper = getOffsetHelper();
		EObject grammerObject = null;
		//EObjectAtOffsetHelper
		if(defaultAnnex instanceof DefaultAnnexLibrary){
			AnnexLibrary annexLib = ((DefaultAnnexLibrary)defaultAnnex).getParsedAnnexLibrary();
			XtextResource resource = (XtextResource)annexLib.eResource();
			grammerObject = helper.resolveContainedElementAt(resource, offset);
		}else if(defaultAnnex instanceof DefaultAnnexSubclause){
			AnnexSubclause annexSub = ((DefaultAnnexSubclause)defaultAnnex).getParsedAnnexSubclause();
			XtextResource resource = (XtextResource)annexSub.eResource();
			grammerObject = helper.resolveContainedElementAt(resource, offset);
		}
		
		List<String> results = new ArrayList<>();
		return results;
	}
}
