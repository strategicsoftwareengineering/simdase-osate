package edu.clemson.simdase.serializer;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.serializer.impl.Serializer;

//import com.google.inject.Guice;
//import com.google.inject.Injector;
import com.google.inject.Inject;
//import com.google.inject.Provider;

import edu.clemson.simdase.services.SimdaseGrammarAccess;
import edu.clemson.simdase.simdase.SimdaseContractLibrary;
import edu.clemson.simdase.simdase.SimdaseContractSubclause;;

@SuppressWarnings("restriction")
public class SimdaseSerializer extends Serializer {
	@Inject
	private SimdaseGrammarAccess grammarAccess;

    @Override
    protected EObject getContext(EObject semanticObject) {
    	EObject result = null;
    	if (semanticObject instanceof SimdaseContractLibrary) {
    		result = grammarAccess.getSimdaseLibraryRule();
    	} else if (semanticObject instanceof SimdaseContractSubclause) {
    		result = grammarAccess.getSimdaseContractRule();
    	} else {
    		result = super.getContext(semanticObject);
    	}
    	return result;
    }
}
