/**
 */
package edu.clemson.simdase.simdase;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Re Evaluate Averaging</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.clemson.simdase.simdase.ReEvaluateAveraging#getCount <em>Count</em>}</li>
 *   <li>{@link edu.clemson.simdase.simdase.ReEvaluateAveraging#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see edu.clemson.simdase.simdase.SimdasePackage#getReEvaluateAveraging()
 * @model
 * @generated
 */
public interface ReEvaluateAveraging extends SIMDASEStatement
{
  /**
   * Returns the value of the '<em><b>Count</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Count</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Count</em>' containment reference.
   * @see #setCount(edu.clemson.simdase.simdase.Number)
   * @see edu.clemson.simdase.simdase.SimdasePackage#getReEvaluateAveraging_Count()
   * @model containment="true"
   * @generated
   */
  edu.clemson.simdase.simdase.Number getCount();

  /**
   * Sets the value of the '{@link edu.clemson.simdase.simdase.ReEvaluateAveraging#getCount <em>Count</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Count</em>' containment reference.
   * @see #getCount()
   * @generated
   */
  void setCount(edu.clemson.simdase.simdase.Number value);

  /**
   * Returns the value of the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Type</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Type</em>' attribute.
   * @see #setType(String)
   * @see edu.clemson.simdase.simdase.SimdasePackage#getReEvaluateAveraging_Type()
   * @model
   * @generated
   */
  String getType();

  /**
   * Sets the value of the '{@link edu.clemson.simdase.simdase.ReEvaluateAveraging#getType <em>Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Type</em>' attribute.
   * @see #getType()
   * @generated
   */
  void setType(String value);

} // ReEvaluateAveraging
