/**
 */
package edu.clemson.simdase.simdase;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Cost Properties</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.clemson.simdase.simdase.CostProperties#getCostStatements <em>Cost Statements</em>}</li>
 * </ul>
 *
 * @see edu.clemson.simdase.simdase.SimdasePackage#getCostProperties()
 * @model
 * @generated
 */
public interface CostProperties extends SIMDASEStatement
{
  /**
   * Returns the value of the '<em><b>Cost Statements</b></em>' containment reference list.
   * The list contents are of type {@link edu.clemson.simdase.simdase.CostStatement}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Cost Statements</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Cost Statements</em>' containment reference list.
   * @see edu.clemson.simdase.simdase.SimdasePackage#getCostProperties_CostStatements()
   * @model containment="true"
   * @generated
   */
  EList<CostStatement> getCostStatements();

} // CostProperties
