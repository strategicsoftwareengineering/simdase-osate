package edu.clemson.simdase.analysis.handlers;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.emf.ecore.EObject;
import org.osate.aadl2.AnnexLibrary;
import org.osate.aadl2.AnnexSubclause;

import edu.clemson.simdase.simdase.ActiveVariant;
import edu.clemson.simdase.simdase.ActiveVariants;
import edu.clemson.simdase.simdase.BoolBoolean;
import edu.clemson.simdase.simdase.BoolCos;
import edu.clemson.simdase.simdase.BoolI;
import edu.clemson.simdase.simdase.BoolInteger;
import edu.clemson.simdase.simdase.BoolParen;
import edu.clemson.simdase.simdase.BoolRandom;
import edu.clemson.simdase.simdase.BoolScalingFactor;
import edu.clemson.simdase.simdase.BoolSin;
import edu.clemson.simdase.simdase.BoolT;
import edu.clemson.simdase.simdase.BoolTan;
import edu.clemson.simdase.simdase.BooleanStatement;
import edu.clemson.simdase.simdase.BooleanStatementLvl2;
import edu.clemson.simdase.simdase.BooleanStatementLvl3;
import edu.clemson.simdase.simdase.BooleanStatementLvl4;
import edu.clemson.simdase.simdase.BooleanStatementLvl5;
import edu.clemson.simdase.simdase.BooleanStatementLvl6;
import edu.clemson.simdase.simdase.BooleanStatementLvl7;
import edu.clemson.simdase.simdase.BooleanStatementLvl8;
import edu.clemson.simdase.simdase.Cos;
import edu.clemson.simdase.simdase.CostProperties;
import edu.clemson.simdase.simdase.CostStatement;
import edu.clemson.simdase.simdase.CostValue;
import edu.clemson.simdase.simdase.EmployeeStatement;
import edu.clemson.simdase.simdase.FixedCost;
import edu.clemson.simdase.simdase.HourlyCost;
import edu.clemson.simdase.simdase.I;
import edu.clemson.simdase.simdase.If;
import edu.clemson.simdase.simdase.IsAdaptiveStatement;
import edu.clemson.simdase.simdase.Paren;
import edu.clemson.simdase.simdase.Random;
import edu.clemson.simdase.simdase.ReEvaluateAveraging;
import edu.clemson.simdase.simdase.SIMDASEStatement;
import edu.clemson.simdase.simdase.ScalingFactor;
import edu.clemson.simdase.simdase.SimdaseContract;
import edu.clemson.simdase.simdase.Sin;
import edu.clemson.simdase.simdase.Statement;
import edu.clemson.simdase.simdase.StatementLvl2;
import edu.clemson.simdase.simdase.StatementLvl3;
import edu.clemson.simdase.simdase.StatementLvl4;
import edu.clemson.simdase.simdase.StatementScalingFactor;
import edu.clemson.simdase.simdase.T;
import edu.clemson.simdase.simdase.TMax;
import edu.clemson.simdase.simdase.TMin;
import edu.clemson.simdase.simdase.Tan;
import edu.clemson.simdase.simdase.Variant;
import edu.clemson.simdase.simdase.VariantNames;
import edu.clemson.simdase.simdase.Variants;
import edu.clemson.simdase.simdase.WithNot;
import edu.clemson.simdase.simdase.WithoutNot;
import edu.clemson.simdase.simdase.impl.SimdaseContractSubclauseImpl;

public class SimdaseEvaluator {

	private BigDecimal level;
	private BigDecimal timePeriod;
	private BigDecimal scalingFactor;
	
	public SimdaseEvaluator() {
		this.level = BigDecimal.ZERO;
		this.timePeriod = BigDecimal.ZERO;
		this.scalingFactor = BigDecimal.ZERO;
	}
	
	public SimdaseEvaluator(BigDecimal level, BigDecimal timePeriod, BigDecimal scalingFactor) {
		this.level = level;
		this.timePeriod = timePeriod;
		this.scalingFactor = scalingFactor;
	}
	
	public SimdaseAnnex evaluateSimdaseLibrary(AnnexLibrary object) {
		return evaluateSimdaseLibrary(object, false);
	}
	
	public SimdaseAnnex evaluateSimdaseLibrary(AnnexLibrary object, boolean light) {
		if(!object.eContents().isEmpty()) {
			for(EObject obj : object.eContents()) {
				if(obj instanceof SimdaseContractSubclauseImpl) {
					SimdaseContract contract = (SimdaseContract) ((SimdaseContractSubclauseImpl)obj).getContract();
					return evaluateSimdaseContract(contract, light);
				}
			}
		}
		return null;
	}

	public SimdaseAnnex evaluateSimdaseSubclause(AnnexSubclause object) {
		return evaluateSimdaseSubclause(object, false);
	}
	
	public SimdaseAnnex evaluateSimdaseSubclause(AnnexSubclause object, boolean light) {
		if(!object.eContents().isEmpty()) {
			for(EObject obj : object.eContents()) {
				if(obj instanceof SimdaseContractSubclauseImpl) {
					SimdaseContract contract = (SimdaseContract) ((SimdaseContractSubclauseImpl)obj).getContract();
					return evaluateSimdaseContract(contract, light);
				}
			}
		}
		return null;
	}

	private SimdaseAnnex evaluateSimdaseContract(SimdaseContract object, boolean light) {
		SimdaseAnnex annex = new SimdaseAnnex();
		for(SIMDASEStatement statement : object.getStatement()) {
			if(statement instanceof IsAdaptiveStatement) {
				annex.setIsAdaptive(evaluateIsAdaptiveStatement((IsAdaptiveStatement)statement));
			} else if (statement instanceof TMax) {
				annex.setTimePeriodEnd(evaluateTMax((TMax)statement));
			} else if (statement instanceof TMin) {
				annex.setTimePeriodStart(evaluateTMin((TMin)statement));
			} else if (statement instanceof ScalingFactor) {
				annex.setScalingFactor(evaluateScalingFactor((ScalingFactor)statement));
			} else if (statement instanceof Variants) {
				annex.setIsVariationPoint(true);
				annex.setVariants(evaluateVariants((Variants)statement));
			} else if (statement instanceof ActiveVariants) {
				annex.setActiveVariants(evaluateActiveVariants((ActiveVariants)statement));
			} else if (!light && statement instanceof CostProperties) {
				annex.setEmployeeCosts(evaluateCostProperties((CostProperties)statement));
			} else if (statement instanceof ReEvaluateAveraging) {
				annex = evaluateReEvaluateAveraging((ReEvaluateAveraging) statement, annex);
			}
		}
		return annex;
	}
	
	public BigDecimal evaluateScalingFactor(ScalingFactor factor) {
		return evaluateStatement(factor.getStatement());
	}
	
	private SimdaseAnnex evaluateReEvaluateAveraging(ReEvaluateAveraging statement, SimdaseAnnex annex) {
		annex.setReevaluateNTimes(evaluateNumber(statement.getCount()));
		annex.setUseWhichResult(statement.getType());
		return annex;
	}

	private Map<String, List<String>> evaluateVariants(Variants object) {
		Map<String, List<String>> map = new HashMap<String, List<String>>();
		if(object.getFirst() == null && object.getVariants() != null) {
			Entry<String, List<String>> entry = evaluateVariant(object.getVariants().getFirst());
			map.put(entry.getKey(), entry.getValue());
			if(object.getVariants().getRest() != null) {
				for(Variant var : object.getVariants().getRest()) {
					entry = evaluateVariant(var);
					map.put(entry.getKey(), entry.getValue());
				}
			}
		} else if(object.getFirst() != null) {
			Entry<String, List<String>> entry = evaluateVariant(object.getFirst());
			map.put(entry.getKey(), entry.getValue());
			if(object.getRest() != null) {
				for(Variant var : object.getRest()) {
					entry = evaluateVariant(var);
					map.put(entry.getKey(), entry.getValue());
				}
			}
		}
		return map;
	}

	private Entry<String, List<String>> evaluateVariant(Variant object) {
		return new AbstractMap.SimpleEntry<String, List<String>>(object.getName(), evaluateVariantNames(object.getVariants()));
	}
	
	private Map<String, List<String>> evaluateActiveVariants(ActiveVariants object) {
		Map<String, List<String>> map = new HashMap<String, List<String>>();
		if(object.getFirst() == null && object.getVariants() != null) {
			Entry<String, List<String>> entry = evaluateActiveVariant(object.getVariants().getFirst());
			map.put(entry.getKey(), entry.getValue());
			if(object.getVariants().getRest() != null) {
				for(ActiveVariant var : object.getVariants().getRest()) {
					entry = evaluateActiveVariant(var);
					map.put(entry.getKey(), entry.getValue());
				}
			}
		} else if(object.getFirst() != null) {
			Entry<String, List<String>> entry = evaluateActiveVariant(object.getFirst());
			map.put(entry.getKey(), entry.getValue());
			if(object.getRest() != null) {
				for(ActiveVariant var : object.getRest()) {
					entry = evaluateActiveVariant(var);
					map.put(entry.getKey(), entry.getValue());
				}
			}
		}
		return map;
	}
	
	private Entry<String, List<String>> evaluateActiveVariant(ActiveVariant object) {
		return new AbstractMap.SimpleEntry<String, List<String>>(object.getName(), evaluateVariantNames(object.getVariants()));
	}
	
	private List<String> evaluateVariantNames(VariantNames object) {
		List<String> values = new ArrayList<String>();
		values.add(object.getFirst());
		for(String value : object.getRest()) {
			values.add(value);
		}
		return values;
	}

	private Map<String, List<CostRow.EmployeeCost>> evaluateCostProperties(CostProperties object) {
		Map<String, List<CostRow.EmployeeCost>> map = new HashMap<String, List<CostRow.EmployeeCost>>();
		for(CostStatement statement : object.getCostStatements()) {
			Entry<String, List<CostRow.EmployeeCost>> entry = evaluateCostStatement(statement);
			map.put(entry.getKey(), entry.getValue());
		}
		return map;
	}

	private Entry<String, List<CostRow.EmployeeCost>> evaluateCostStatement(CostStatement object) {
		return new AbstractMap.SimpleEntry<String, List<CostRow.EmployeeCost>>(object.getType(), evaluateCostValue(object.getValue()));
	}

	private List<CostRow.EmployeeCost> evaluateCostValue(CostValue object) {
		List<CostRow.EmployeeCost> costs = new ArrayList<CostRow.EmployeeCost>();
		CostRow.EmployeeCost cost = evaluateEmployeeStatement(object.getFirst());
		if(cost != null) {
			costs.add(cost);
		}
		for(EmployeeStatement employeeStatement : object.getRest()) {
			CostRow.EmployeeCost next = evaluateEmployeeStatement(employeeStatement);
			if(next != null) {
				costs.add(next);
			}
		}
		return costs;
	}

	private CostRow.EmployeeCost evaluateEmployeeStatement(EmployeeStatement object) {
		if(object instanceof HourlyCost) {
			HourlyCost hourlyCost = (HourlyCost)object;
			CostRow.EmployeeCost cost = new CostRow.EmployeeCost();
			cost.setEmployeeQuantity(evaluateNumber(hourlyCost.getCount()));
			cost.setEmployeeType(hourlyCost.getEmployeeType());
			cost.setCostPerEmployee(evaluateStatement(hourlyCost.getCost()));
			return cost;
		} else if (object instanceof FixedCost) {
			FixedCost fixedCost = (FixedCost)object;
			if(evaluateNumber(fixedCost.getPeriod()).doubleValue() == timePeriod.doubleValue()) {
				CostRow.EmployeeCost cost = new CostRow.EmployeeCost();
				cost.setFixedCost(evaluateNumber(fixedCost.getCost()));
				cost.setFixedCostReason(fixedCost.getCostReason());
				return cost;
			}
		}
		return null;
	}

	private BigDecimal evaluateStatement(Statement object) {
		BigDecimal start = evaluateStatementLvl2(object.getStatement());
		for(int i = 0; i < object.getOp().size(); ++i) {
			switch(object.getOp().get(i)) {
				case "+":
					start = start.add(evaluateStatementLvl2(object.getRest().get(i)));
					break;
				case "-":
					start = start.subtract(evaluateStatementLvl2(object.getRest().get(i)));
					break;
			}
		}
		return start;
	}

	private BigDecimal evaluateStatementLvl2(StatementLvl2 object) {
		BigDecimal start = evaluateStatementLvl3(object.getStatement());
		for(int i = 0; i < object.getOp().size(); ++i) {
			switch(object.getOp().get(i)) {
				case "*":
					start = start.multiply(evaluateStatementLvl3(object.getRest().get(i)));
					break;
				case "/":
					MathContext mc = new MathContext(2, RoundingMode.HALF_UP);
					start = start.divide(evaluateStatementLvl3(object.getRest().get(i)), mc);
					break;
				case "%":
					start = start.remainder(evaluateStatementLvl3(object.getRest().get(i)));
					break;
			}
		}
		return start;
	}

	private BigDecimal evaluateStatementLvl3(StatementLvl3 object) {
		BigDecimal start = evaluateStatementLvl4(object.getStatement());
		for(int i = 0; i < object.getOp().size(); ++i) {
			switch(object.getOp().get(i)) {
				case "**":
					start = start.pow(evaluateStatementLvl4(object.getRest().get(i)).intValue());
					break;
			}
		}
		return start;
	}

	private BigDecimal evaluateStatementLvl4(StatementLvl4 object) {
		if(object instanceof edu.clemson.simdase.simdase.Integer) {
			 return evaluateNumber(((edu.clemson.simdase.simdase.Integer)object).getValue());
		} else if (object instanceof T) {
			return timePeriod;
		} else if (object instanceof I) {
			return level; 
		} else if (object instanceof Sin) {
			 BigDecimal paramValue = evaluateStatement(((Sin)object).getStatement());
			 return BigDecimal.valueOf(Math.sin(paramValue.doubleValue()));
		} else if (object instanceof Cos) {
			BigDecimal paramValue = evaluateStatement(((Cos)object).getStatement());
			return BigDecimal.valueOf(Math.cos(paramValue.doubleValue()));
		} else if (object instanceof Tan) {
			BigDecimal paramValue = evaluateStatement(((Tan)object).getStatement());
			return BigDecimal.valueOf(Math.tan(paramValue.doubleValue()));
		} else if (object instanceof Paren) {
			 return evaluateStatement(((Paren)object).getStatement());
		} else if (object instanceof If) {
			 Boolean truthValue = evaluateBooleanStatement(((If)object).getBooleanStatement());
			 if(truthValue) {
				 return evaluateStatement(((If)object).getTrueStatement());
			 } else {
				 return evaluateStatement(((If)object).getFalseStatement());
			 }
		} else if (object instanceof Random) {
			 return BigDecimal.valueOf(Math.random());
		} else if (object instanceof StatementScalingFactor) {
			return scalingFactor;
		}
		return BigDecimal.ZERO;
	}
	
	private Boolean objectToBoolean(Object obj) {
		if(obj instanceof BigDecimal) {
			return ((BigDecimal)obj).compareTo(BigDecimal.ZERO) != 0;
		} else if (obj instanceof Boolean) {
			return (Boolean)obj;
		} else {
			return false;
		}
	}

	private Boolean evaluateBooleanStatement(BooleanStatement object) {
		Object start = evaluateBooleanStatementLvl2(object.getStatement());
		for(int i = 0; i < object.getOp().size(); ++i) {
			switch(object.getOp().get(i)) {
				case "||":
					start = objectToBoolean(start) || objectToBoolean(evaluateBooleanStatementLvl2(object.getRest().get(i)));
					break;
			}
		}
		return objectToBoolean(start);
	}

	private Object evaluateBooleanStatementLvl2(BooleanStatementLvl2 object) {
		Object start = evaluateBooleanStatementLvl3(object.getStatement());
		for(int i = 0; i < object.getOp().size(); ++i) {
			switch(object.getOp().get(i)) {
				case "&&":
					start = objectToBoolean(start) && objectToBoolean(evaluateBooleanStatementLvl3(object.getRest().get(i)));
					break;
			}
		}
		return start;
	}

	private Object evaluateBooleanStatementLvl3(BooleanStatementLvl3 object) {
		Object start = evaluateBooleanStatementLvl4(object.getStatement());
		for(int i = 0; i < object.getOp().size(); ++i) {
			Object op = evaluateBooleanStatementLvl4(object.getRest().get(i));
			switch(object.getOp().get(i)) {
				case ">":
					start = ((Comparable)start).compareTo(op) > 0;
					break;
				case ">=":
					start = ((Comparable)start).compareTo(op) >= 0;
					break;
				case "<":
					start = ((Comparable)start).compareTo(op) < 0;
					break;
				case "<=":
					start = ((Comparable)start).compareTo(op) <= 0;
					break;
				case "==":
					if(start instanceof BigDecimal && op instanceof BigDecimal) {
						start = ((BigDecimal)start).doubleValue() == ((BigDecimal)op).doubleValue();
					} else {
						start = start.equals(op);
					}
					break;
			}
		}
		return start;
	}

	private Object evaluateBooleanStatementLvl4(BooleanStatementLvl4 object) {
		if(object instanceof WithNot) {
			return !objectToBoolean(evaluateBooleanStatementLvl4(((WithNot)object).getStatement()));
		} else if (object instanceof WithoutNot) {
			return evaluateBooleanStatementLvl5(((WithoutNot)object).getStatement());
		}
		return null;
	}

	private Object evaluateBooleanStatementLvl5(BooleanStatementLvl5 object) {
		Object start = evaluateBooleanStatementLvl6(object.getStatement());
		for(int i = 0; i < object.getOp().size(); ++i) {
			Object op = evaluateBooleanStatementLvl6(object.getRest().get(i));
			switch(object.getOp().get(i)) {
				case "+":
					if(start instanceof BigDecimal && op instanceof BigDecimal) {
						start = ((BigDecimal)start).add(((BigDecimal)op));
					}
					break;
				case "-":
					if(start instanceof BigDecimal && op instanceof BigDecimal) {
						start = ((BigDecimal)start).subtract(((BigDecimal)op));
					}
					break;
			}
		}
		return start;
	}
	
	private Object evaluateBooleanStatementLvl6(BooleanStatementLvl6 object) {
		Object start = evaluateBooleanStatementLvl7(object.getStatement());
		for(int i = 0; i < object.getOp().size(); ++i) {
			Object op = evaluateBooleanStatementLvl7(object.getRest().get(i));
			switch(object.getOp().get(i)) {
				case "*":
					if(start instanceof BigDecimal && op instanceof BigDecimal) {
						start = ((BigDecimal)start).multiply(((BigDecimal)op));
					}
					break;
				case "/":
					if(start instanceof BigDecimal && op instanceof BigDecimal) {
						start = ((BigDecimal)start).divide(((BigDecimal)op));
					}
					break;
				case "%":
					if(start instanceof BigDecimal && op instanceof BigDecimal) {
						start = ((BigDecimal)start).remainder(((BigDecimal)op));
					}
					break;
			}
		}
		return start;
	}
	
	private Object evaluateBooleanStatementLvl7(BooleanStatementLvl7 object) {
		Object start = evaluateBooleanStatementLvl8(object.getStatement());
		for(int i = 0; i < object.getOp().size(); ++i) {
			switch(object.getOp().get(i)) {
				case "**":
					Object power = evaluateBooleanStatementLvl8(object.getRest().get(i));
					if(start instanceof BigDecimal && power instanceof BigDecimal) {
						start = ((BigDecimal)start).pow(((BigDecimal)power).intValue());
					}
					break;
			}
		}
		return start;
	}
	
	private Object evaluateBooleanStatementLvl8(BooleanStatementLvl8 object) {
		if(object instanceof BoolBoolean) {
			return evaluateBoolean(((BoolBoolean)object).getValue());
		} else if(object instanceof BoolInteger) {
			 return evaluateNumber(((BoolInteger)object).getValue());
		} else if (object instanceof BoolT) {
			return timePeriod;
		} else if (object instanceof BoolI) {
			return level; 
		} else if (object instanceof BoolSin) {
			 BigDecimal paramValue = evaluateStatement(((BoolSin)object).getStatement());
			 return BigDecimal.valueOf(Math.sin(paramValue.doubleValue()));
		} else if (object instanceof BoolCos) {
			BigDecimal paramValue = evaluateStatement(((BoolCos)object).getStatement());
			return BigDecimal.valueOf(Math.cos(paramValue.doubleValue()));
		} else if (object instanceof BoolTan) {
			BigDecimal paramValue = evaluateStatement(((BoolTan)object).getStatement());
			return BigDecimal.valueOf(Math.tan(paramValue.doubleValue()));
		} else if (object instanceof BoolParen) {
			 return evaluateBooleanStatement(((BoolParen)object).getStatement());
		} else if (object instanceof BoolRandom) {
			 return BigDecimal.valueOf(Math.random());
		} else if (object instanceof BoolScalingFactor) {
			return scalingFactor;
		}
		return false;
	}

	private Boolean evaluateIsAdaptiveStatement(IsAdaptiveStatement object) {
		return evaluateBoolean(object.getValue());
	}
	
	private Boolean evaluateBoolean(edu.clemson.simdase.simdase.Boolean object) {
		return object.getValue().equals("true");
	}

	private BigDecimal evaluateTMax(TMax object) {
		return evaluateNumber(object.getValue());
	}
	
	private BigDecimal evaluateTMin(TMin object) {
		return evaluateNumber(object.getValue());
	}
	
	private BigDecimal evaluateNumber(edu.clemson.simdase.simdase.Number object) {
		BigDecimal value = new BigDecimal(object.getFloatValue());
		if(object.getNegative() != null) {
			return value.multiply(BigDecimal.valueOf(-1.0));
		} else {
			return value;
		}
	}
}
