/**
 */
package edu.clemson.simdase.simdase;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Cost Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.clemson.simdase.simdase.CostValue#getFirst <em>First</em>}</li>
 *   <li>{@link edu.clemson.simdase.simdase.CostValue#getRest <em>Rest</em>}</li>
 * </ul>
 *
 * @see edu.clemson.simdase.simdase.SimdasePackage#getCostValue()
 * @model
 * @generated
 */
public interface CostValue extends EObject
{
  /**
   * Returns the value of the '<em><b>First</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>First</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>First</em>' containment reference.
   * @see #setFirst(EmployeeStatement)
   * @see edu.clemson.simdase.simdase.SimdasePackage#getCostValue_First()
   * @model containment="true"
   * @generated
   */
  EmployeeStatement getFirst();

  /**
   * Sets the value of the '{@link edu.clemson.simdase.simdase.CostValue#getFirst <em>First</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>First</em>' containment reference.
   * @see #getFirst()
   * @generated
   */
  void setFirst(EmployeeStatement value);

  /**
   * Returns the value of the '<em><b>Rest</b></em>' containment reference list.
   * The list contents are of type {@link edu.clemson.simdase.simdase.EmployeeStatement}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Rest</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Rest</em>' containment reference list.
   * @see edu.clemson.simdase.simdase.SimdasePackage#getCostValue_Rest()
   * @model containment="true"
   * @generated
   */
  EList<EmployeeStatement> getRest();

} // CostValue
