/**
 */
package edu.clemson.simdase.simdase;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Variant Names</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.clemson.simdase.simdase.VariantNames#getFirst <em>First</em>}</li>
 *   <li>{@link edu.clemson.simdase.simdase.VariantNames#getRest <em>Rest</em>}</li>
 * </ul>
 *
 * @see edu.clemson.simdase.simdase.SimdasePackage#getVariantNames()
 * @model
 * @generated
 */
public interface VariantNames extends EObject
{
  /**
   * Returns the value of the '<em><b>First</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>First</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>First</em>' attribute.
   * @see #setFirst(String)
   * @see edu.clemson.simdase.simdase.SimdasePackage#getVariantNames_First()
   * @model
   * @generated
   */
  String getFirst();

  /**
   * Sets the value of the '{@link edu.clemson.simdase.simdase.VariantNames#getFirst <em>First</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>First</em>' attribute.
   * @see #getFirst()
   * @generated
   */
  void setFirst(String value);

  /**
   * Returns the value of the '<em><b>Rest</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Rest</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Rest</em>' attribute list.
   * @see edu.clemson.simdase.simdase.SimdasePackage#getVariantNames_Rest()
   * @model unique="false"
   * @generated
   */
  EList<String> getRest();

} // VariantNames
