/**
 */
package edu.clemson.simdase.simdase.impl;

import edu.clemson.simdase.simdase.CostValue;
import edu.clemson.simdase.simdase.EmployeeStatement;
import edu.clemson.simdase.simdase.SimdasePackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Cost Value</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.clemson.simdase.simdase.impl.CostValueImpl#getFirst <em>First</em>}</li>
 *   <li>{@link edu.clemson.simdase.simdase.impl.CostValueImpl#getRest <em>Rest</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CostValueImpl extends MinimalEObjectImpl.Container implements CostValue
{
  /**
   * The cached value of the '{@link #getFirst() <em>First</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFirst()
   * @generated
   * @ordered
   */
  protected EmployeeStatement first;

  /**
   * The cached value of the '{@link #getRest() <em>Rest</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRest()
   * @generated
   * @ordered
   */
  protected EList<EmployeeStatement> rest;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected CostValueImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return SimdasePackage.Literals.COST_VALUE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EmployeeStatement getFirst()
  {
    return first;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetFirst(EmployeeStatement newFirst, NotificationChain msgs)
  {
    EmployeeStatement oldFirst = first;
    first = newFirst;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SimdasePackage.COST_VALUE__FIRST, oldFirst, newFirst);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setFirst(EmployeeStatement newFirst)
  {
    if (newFirst != first)
    {
      NotificationChain msgs = null;
      if (first != null)
        msgs = ((InternalEObject)first).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SimdasePackage.COST_VALUE__FIRST, null, msgs);
      if (newFirst != null)
        msgs = ((InternalEObject)newFirst).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SimdasePackage.COST_VALUE__FIRST, null, msgs);
      msgs = basicSetFirst(newFirst, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SimdasePackage.COST_VALUE__FIRST, newFirst, newFirst));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<EmployeeStatement> getRest()
  {
    if (rest == null)
    {
      rest = new EObjectContainmentEList<EmployeeStatement>(EmployeeStatement.class, this, SimdasePackage.COST_VALUE__REST);
    }
    return rest;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case SimdasePackage.COST_VALUE__FIRST:
        return basicSetFirst(null, msgs);
      case SimdasePackage.COST_VALUE__REST:
        return ((InternalEList<?>)getRest()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case SimdasePackage.COST_VALUE__FIRST:
        return getFirst();
      case SimdasePackage.COST_VALUE__REST:
        return getRest();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case SimdasePackage.COST_VALUE__FIRST:
        setFirst((EmployeeStatement)newValue);
        return;
      case SimdasePackage.COST_VALUE__REST:
        getRest().clear();
        getRest().addAll((Collection<? extends EmployeeStatement>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case SimdasePackage.COST_VALUE__FIRST:
        setFirst((EmployeeStatement)null);
        return;
      case SimdasePackage.COST_VALUE__REST:
        getRest().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case SimdasePackage.COST_VALUE__FIRST:
        return first != null;
      case SimdasePackage.COST_VALUE__REST:
        return rest != null && !rest.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //CostValueImpl
