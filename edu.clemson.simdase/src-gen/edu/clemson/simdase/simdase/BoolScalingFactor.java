/**
 */
package edu.clemson.simdase.simdase;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Bool Scaling Factor</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.clemson.simdase.simdase.SimdasePackage#getBoolScalingFactor()
 * @model
 * @generated
 */
public interface BoolScalingFactor extends BooleanStatementLvl8
{
} // BoolScalingFactor
