/**
 */
package edu.clemson.simdase.simdase;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Fixed Cost</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.clemson.simdase.simdase.FixedCost#getCost <em>Cost</em>}</li>
 *   <li>{@link edu.clemson.simdase.simdase.FixedCost#getCostReason <em>Cost Reason</em>}</li>
 *   <li>{@link edu.clemson.simdase.simdase.FixedCost#getPeriod <em>Period</em>}</li>
 * </ul>
 *
 * @see edu.clemson.simdase.simdase.SimdasePackage#getFixedCost()
 * @model
 * @generated
 */
public interface FixedCost extends EmployeeStatement
{
  /**
   * Returns the value of the '<em><b>Cost</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Cost</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Cost</em>' containment reference.
   * @see #setCost(edu.clemson.simdase.simdase.Number)
   * @see edu.clemson.simdase.simdase.SimdasePackage#getFixedCost_Cost()
   * @model containment="true"
   * @generated
   */
  edu.clemson.simdase.simdase.Number getCost();

  /**
   * Sets the value of the '{@link edu.clemson.simdase.simdase.FixedCost#getCost <em>Cost</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Cost</em>' containment reference.
   * @see #getCost()
   * @generated
   */
  void setCost(edu.clemson.simdase.simdase.Number value);

  /**
   * Returns the value of the '<em><b>Cost Reason</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Cost Reason</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Cost Reason</em>' attribute.
   * @see #setCostReason(String)
   * @see edu.clemson.simdase.simdase.SimdasePackage#getFixedCost_CostReason()
   * @model
   * @generated
   */
  String getCostReason();

  /**
   * Sets the value of the '{@link edu.clemson.simdase.simdase.FixedCost#getCostReason <em>Cost Reason</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Cost Reason</em>' attribute.
   * @see #getCostReason()
   * @generated
   */
  void setCostReason(String value);

  /**
   * Returns the value of the '<em><b>Period</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Period</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Period</em>' containment reference.
   * @see #setPeriod(edu.clemson.simdase.simdase.Number)
   * @see edu.clemson.simdase.simdase.SimdasePackage#getFixedCost_Period()
   * @model containment="true"
   * @generated
   */
  edu.clemson.simdase.simdase.Number getPeriod();

  /**
   * Sets the value of the '{@link edu.clemson.simdase.simdase.FixedCost#getPeriod <em>Period</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Period</em>' containment reference.
   * @see #getPeriod()
   * @generated
   */
  void setPeriod(edu.clemson.simdase.simdase.Number value);

} // FixedCost
