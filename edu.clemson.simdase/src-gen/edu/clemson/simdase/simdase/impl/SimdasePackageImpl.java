/**
 */
package edu.clemson.simdase.simdase.impl;

import edu.clemson.simdase.simdase.ActiveVariant;
import edu.clemson.simdase.simdase.ActiveVariants;
import edu.clemson.simdase.simdase.BoolBoolean;
import edu.clemson.simdase.simdase.BoolCos;
import edu.clemson.simdase.simdase.BoolI;
import edu.clemson.simdase.simdase.BoolInteger;
import edu.clemson.simdase.simdase.BoolParen;
import edu.clemson.simdase.simdase.BoolRandom;
import edu.clemson.simdase.simdase.BoolScalingFactor;
import edu.clemson.simdase.simdase.BoolSin;
import edu.clemson.simdase.simdase.BoolT;
import edu.clemson.simdase.simdase.BoolTan;
import edu.clemson.simdase.simdase.BooleanStatement;
import edu.clemson.simdase.simdase.BooleanStatementLvl2;
import edu.clemson.simdase.simdase.BooleanStatementLvl3;
import edu.clemson.simdase.simdase.BooleanStatementLvl4;
import edu.clemson.simdase.simdase.BooleanStatementLvl5;
import edu.clemson.simdase.simdase.BooleanStatementLvl6;
import edu.clemson.simdase.simdase.BooleanStatementLvl7;
import edu.clemson.simdase.simdase.BooleanStatementLvl8;
import edu.clemson.simdase.simdase.Contract;
import edu.clemson.simdase.simdase.Cos;
import edu.clemson.simdase.simdase.CostProperties;
import edu.clemson.simdase.simdase.CostStatement;
import edu.clemson.simdase.simdase.CostValue;
import edu.clemson.simdase.simdase.EmployeeStatement;
import edu.clemson.simdase.simdase.FixedCost;
import edu.clemson.simdase.simdase.HourlyCost;
import edu.clemson.simdase.simdase.If;
import edu.clemson.simdase.simdase.IsAdaptiveStatement;
import edu.clemson.simdase.simdase.Paren;
import edu.clemson.simdase.simdase.Random;
import edu.clemson.simdase.simdase.ReEvaluateAveraging;
import edu.clemson.simdase.simdase.SIMDASEStatement;
import edu.clemson.simdase.simdase.ScalingFactor;
import edu.clemson.simdase.simdase.SimdaseContract;
import edu.clemson.simdase.simdase.SimdaseContractLibrary;
import edu.clemson.simdase.simdase.SimdaseContractSubclause;
import edu.clemson.simdase.simdase.SimdaseFactory;
import edu.clemson.simdase.simdase.SimdaseLibrary;
import edu.clemson.simdase.simdase.SimdasePackage;
import edu.clemson.simdase.simdase.SimdaseSubclause;
import edu.clemson.simdase.simdase.Sin;
import edu.clemson.simdase.simdase.Statement;
import edu.clemson.simdase.simdase.StatementLvl2;
import edu.clemson.simdase.simdase.StatementLvl3;
import edu.clemson.simdase.simdase.StatementLvl4;
import edu.clemson.simdase.simdase.StatementScalingFactor;
import edu.clemson.simdase.simdase.TMax;
import edu.clemson.simdase.simdase.TMin;
import edu.clemson.simdase.simdase.Tan;
import edu.clemson.simdase.simdase.Variant;
import edu.clemson.simdase.simdase.VariantNames;
import edu.clemson.simdase.simdase.Variants;
import edu.clemson.simdase.simdase.WithNot;
import edu.clemson.simdase.simdase.WithoutNot;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.osate.aadl2.Aadl2Package;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class SimdasePackageImpl extends EPackageImpl implements SimdasePackage
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass simdaseLibraryEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass simdaseSubclauseEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass contractEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass simdaseStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass variantsEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass variantEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass activeVariantsEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass activeVariantEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass variantNamesEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass costPropertiesEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass costStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass costValueEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass employeeStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass booleanEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass numberEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass statementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass statementLvl2EClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass statementLvl3EClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass statementLvl4EClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass booleanStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass booleanStatementLvl2EClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass booleanStatementLvl3EClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass booleanStatementLvl4EClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass booleanStatementLvl5EClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass booleanStatementLvl6EClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass booleanStatementLvl7EClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass booleanStatementLvl8EClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass simdaseContractLibraryEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass simdaseContractSubclauseEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass simdaseContractEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass isAdaptiveStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tMaxEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tMinEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass scalingFactorEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass reEvaluateAveragingEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass hourlyCostEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass fixedCostEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass integerEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass iEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass sinEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass cosEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tanEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass parenEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ifEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass randomEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass statementScalingFactorEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass withNotEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass withoutNotEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass boolBooleanEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass boolTEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass boolIEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass boolIntegerEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass boolSinEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass boolCosEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass boolTanEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass boolParenEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass boolRandomEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass boolScalingFactorEClass = null;

  /**
   * Creates an instance of the model <b>Package</b>, registered with
   * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
   * package URI value.
   * <p>Note: the correct way to create the package is via the static
   * factory method {@link #init init()}, which also performs
   * initialization of the package, or returns the registered package,
   * if one already exists.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.eclipse.emf.ecore.EPackage.Registry
   * @see edu.clemson.simdase.simdase.SimdasePackage#eNS_URI
   * @see #init()
   * @generated
   */
  private SimdasePackageImpl()
  {
    super(eNS_URI, SimdaseFactory.eINSTANCE);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private static boolean isInited = false;

  /**
   * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
   * 
   * <p>This method is used to initialize {@link SimdasePackage#eINSTANCE} when that field is accessed.
   * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #eNS_URI
   * @see #createPackageContents()
   * @see #initializePackageContents()
   * @generated
   */
  public static SimdasePackage init()
  {
    if (isInited) return (SimdasePackage)EPackage.Registry.INSTANCE.getEPackage(SimdasePackage.eNS_URI);

    // Obtain or create and register package
    SimdasePackageImpl theSimdasePackage = (SimdasePackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof SimdasePackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new SimdasePackageImpl());

    isInited = true;

    // Initialize simple dependencies
    Aadl2Package.eINSTANCE.eClass();
    EcorePackage.eINSTANCE.eClass();

    // Create package meta-data objects
    theSimdasePackage.createPackageContents();

    // Initialize created meta-data
    theSimdasePackage.initializePackageContents();

    // Mark meta-data to indicate it can't be changed
    theSimdasePackage.freeze();

  
    // Update the registry and return the package
    EPackage.Registry.INSTANCE.put(SimdasePackage.eNS_URI, theSimdasePackage);
    return theSimdasePackage;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSimdaseLibrary()
  {
    return simdaseLibraryEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSimdaseSubclause()
  {
    return simdaseSubclauseEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getContract()
  {
    return contractEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSIMDASEStatement()
  {
    return simdaseStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getVariants()
  {
    return variantsEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getVariants_Variants()
  {
    return (EReference)variantsEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getVariants_First()
  {
    return (EReference)variantsEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getVariants_Rest()
  {
    return (EReference)variantsEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getVariant()
  {
    return variantEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getVariant_Name()
  {
    return (EAttribute)variantEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getVariant_Variants()
  {
    return (EReference)variantEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getActiveVariants()
  {
    return activeVariantsEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getActiveVariants_Variants()
  {
    return (EReference)activeVariantsEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getActiveVariants_First()
  {
    return (EReference)activeVariantsEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getActiveVariants_Rest()
  {
    return (EReference)activeVariantsEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getActiveVariant()
  {
    return activeVariantEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getActiveVariant_Name()
  {
    return (EAttribute)activeVariantEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getActiveVariant_Variants()
  {
    return (EReference)activeVariantEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getVariantNames()
  {
    return variantNamesEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getVariantNames_First()
  {
    return (EAttribute)variantNamesEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getVariantNames_Rest()
  {
    return (EAttribute)variantNamesEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getCostProperties()
  {
    return costPropertiesEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCostProperties_CostStatements()
  {
    return (EReference)costPropertiesEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getCostStatement()
  {
    return costStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getCostStatement_Type()
  {
    return (EAttribute)costStatementEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCostStatement_Value()
  {
    return (EReference)costStatementEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getCostValue()
  {
    return costValueEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCostValue_First()
  {
    return (EReference)costValueEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCostValue_Rest()
  {
    return (EReference)costValueEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getEmployeeStatement()
  {
    return employeeStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getBoolean()
  {
    return booleanEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getBoolean_Value()
  {
    return (EAttribute)booleanEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getNumber()
  {
    return numberEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getNumber_Negative()
  {
    return (EAttribute)numberEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getNumber_FloatValue()
  {
    return (EAttribute)numberEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getStatement()
  {
    return statementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getStatement_Statement()
  {
    return (EReference)statementEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getStatement_Op()
  {
    return (EAttribute)statementEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getStatement_Rest()
  {
    return (EReference)statementEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getStatementLvl2()
  {
    return statementLvl2EClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getStatementLvl2_Statement()
  {
    return (EReference)statementLvl2EClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getStatementLvl2_Op()
  {
    return (EAttribute)statementLvl2EClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getStatementLvl2_Rest()
  {
    return (EReference)statementLvl2EClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getStatementLvl3()
  {
    return statementLvl3EClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getStatementLvl3_Statement()
  {
    return (EReference)statementLvl3EClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getStatementLvl3_Op()
  {
    return (EAttribute)statementLvl3EClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getStatementLvl3_Rest()
  {
    return (EReference)statementLvl3EClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getStatementLvl4()
  {
    return statementLvl4EClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getBooleanStatement()
  {
    return booleanStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getBooleanStatement_Statement()
  {
    return (EReference)booleanStatementEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getBooleanStatement_Op()
  {
    return (EAttribute)booleanStatementEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getBooleanStatement_Rest()
  {
    return (EReference)booleanStatementEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getBooleanStatementLvl2()
  {
    return booleanStatementLvl2EClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getBooleanStatementLvl2_Statement()
  {
    return (EReference)booleanStatementLvl2EClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getBooleanStatementLvl2_Op()
  {
    return (EAttribute)booleanStatementLvl2EClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getBooleanStatementLvl2_Rest()
  {
    return (EReference)booleanStatementLvl2EClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getBooleanStatementLvl3()
  {
    return booleanStatementLvl3EClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getBooleanStatementLvl3_Statement()
  {
    return (EReference)booleanStatementLvl3EClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getBooleanStatementLvl3_Op()
  {
    return (EAttribute)booleanStatementLvl3EClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getBooleanStatementLvl3_Rest()
  {
    return (EReference)booleanStatementLvl3EClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getBooleanStatementLvl4()
  {
    return booleanStatementLvl4EClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getBooleanStatementLvl5()
  {
    return booleanStatementLvl5EClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getBooleanStatementLvl5_Statement()
  {
    return (EReference)booleanStatementLvl5EClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getBooleanStatementLvl5_Op()
  {
    return (EAttribute)booleanStatementLvl5EClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getBooleanStatementLvl5_Rest()
  {
    return (EReference)booleanStatementLvl5EClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getBooleanStatementLvl6()
  {
    return booleanStatementLvl6EClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getBooleanStatementLvl6_Statement()
  {
    return (EReference)booleanStatementLvl6EClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getBooleanStatementLvl6_Op()
  {
    return (EAttribute)booleanStatementLvl6EClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getBooleanStatementLvl6_Rest()
  {
    return (EReference)booleanStatementLvl6EClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getBooleanStatementLvl7()
  {
    return booleanStatementLvl7EClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getBooleanStatementLvl7_Statement()
  {
    return (EReference)booleanStatementLvl7EClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getBooleanStatementLvl7_Op()
  {
    return (EAttribute)booleanStatementLvl7EClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getBooleanStatementLvl7_Rest()
  {
    return (EReference)booleanStatementLvl7EClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getBooleanStatementLvl8()
  {
    return booleanStatementLvl8EClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSimdaseContractLibrary()
  {
    return simdaseContractLibraryEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSimdaseContractLibrary_Contract()
  {
    return (EReference)simdaseContractLibraryEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSimdaseContractSubclause()
  {
    return simdaseContractSubclauseEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSimdaseContractSubclause_Contract()
  {
    return (EReference)simdaseContractSubclauseEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSimdaseContract()
  {
    return simdaseContractEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSimdaseContract_Statement()
  {
    return (EReference)simdaseContractEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getIsAdaptiveStatement()
  {
    return isAdaptiveStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getIsAdaptiveStatement_Value()
  {
    return (EReference)isAdaptiveStatementEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTMax()
  {
    return tMaxEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTMax_Value()
  {
    return (EReference)tMaxEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTMin()
  {
    return tMinEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTMin_Value()
  {
    return (EReference)tMinEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getScalingFactor()
  {
    return scalingFactorEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getScalingFactor_Statement()
  {
    return (EReference)scalingFactorEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getReEvaluateAveraging()
  {
    return reEvaluateAveragingEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getReEvaluateAveraging_Count()
  {
    return (EReference)reEvaluateAveragingEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getReEvaluateAveraging_Type()
  {
    return (EAttribute)reEvaluateAveragingEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getHourlyCost()
  {
    return hourlyCostEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getHourlyCost_Count()
  {
    return (EReference)hourlyCostEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getHourlyCost_EmployeeType()
  {
    return (EAttribute)hourlyCostEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getHourlyCost_Cost()
  {
    return (EReference)hourlyCostEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFixedCost()
  {
    return fixedCostEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFixedCost_Cost()
  {
    return (EReference)fixedCostEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getFixedCost_CostReason()
  {
    return (EAttribute)fixedCostEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFixedCost_Period()
  {
    return (EReference)fixedCostEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getInteger()
  {
    return integerEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getInteger_Value()
  {
    return (EReference)integerEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getT()
  {
    return tEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getI()
  {
    return iEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSin()
  {
    return sinEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSin_Statement()
  {
    return (EReference)sinEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getCos()
  {
    return cosEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCos_Statement()
  {
    return (EReference)cosEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTan()
  {
    return tanEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTan_Statement()
  {
    return (EReference)tanEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getParen()
  {
    return parenEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getParen_Statement()
  {
    return (EReference)parenEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getIf()
  {
    return ifEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getIf_BooleanStatement()
  {
    return (EReference)ifEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getIf_TrueStatement()
  {
    return (EReference)ifEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getIf_FalseStatement()
  {
    return (EReference)ifEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getRandom()
  {
    return randomEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getStatementScalingFactor()
  {
    return statementScalingFactorEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getWithNot()
  {
    return withNotEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getWithNot_Statement()
  {
    return (EReference)withNotEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getWithoutNot()
  {
    return withoutNotEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getWithoutNot_Statement()
  {
    return (EReference)withoutNotEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getBoolBoolean()
  {
    return boolBooleanEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getBoolBoolean_Value()
  {
    return (EReference)boolBooleanEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getBoolT()
  {
    return boolTEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getBoolI()
  {
    return boolIEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getBoolInteger()
  {
    return boolIntegerEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getBoolInteger_Value()
  {
    return (EReference)boolIntegerEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getBoolSin()
  {
    return boolSinEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getBoolSin_Statement()
  {
    return (EReference)boolSinEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getBoolCos()
  {
    return boolCosEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getBoolCos_Statement()
  {
    return (EReference)boolCosEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getBoolTan()
  {
    return boolTanEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getBoolTan_Statement()
  {
    return (EReference)boolTanEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getBoolParen()
  {
    return boolParenEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getBoolParen_Statement()
  {
    return (EReference)boolParenEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getBoolRandom()
  {
    return boolRandomEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getBoolScalingFactor()
  {
    return boolScalingFactorEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SimdaseFactory getSimdaseFactory()
  {
    return (SimdaseFactory)getEFactoryInstance();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private boolean isCreated = false;

  /**
   * Creates the meta-model objects for the package.  This method is
   * guarded to have no affect on any invocation but its first.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void createPackageContents()
  {
    if (isCreated) return;
    isCreated = true;

    // Create classes and their features
    simdaseLibraryEClass = createEClass(SIMDASE_LIBRARY);

    simdaseSubclauseEClass = createEClass(SIMDASE_SUBCLAUSE);

    contractEClass = createEClass(CONTRACT);

    simdaseStatementEClass = createEClass(SIMDASE_STATEMENT);

    variantsEClass = createEClass(VARIANTS);
    createEReference(variantsEClass, VARIANTS__VARIANTS);
    createEReference(variantsEClass, VARIANTS__FIRST);
    createEReference(variantsEClass, VARIANTS__REST);

    variantEClass = createEClass(VARIANT);
    createEAttribute(variantEClass, VARIANT__NAME);
    createEReference(variantEClass, VARIANT__VARIANTS);

    activeVariantsEClass = createEClass(ACTIVE_VARIANTS);
    createEReference(activeVariantsEClass, ACTIVE_VARIANTS__VARIANTS);
    createEReference(activeVariantsEClass, ACTIVE_VARIANTS__FIRST);
    createEReference(activeVariantsEClass, ACTIVE_VARIANTS__REST);

    activeVariantEClass = createEClass(ACTIVE_VARIANT);
    createEAttribute(activeVariantEClass, ACTIVE_VARIANT__NAME);
    createEReference(activeVariantEClass, ACTIVE_VARIANT__VARIANTS);

    variantNamesEClass = createEClass(VARIANT_NAMES);
    createEAttribute(variantNamesEClass, VARIANT_NAMES__FIRST);
    createEAttribute(variantNamesEClass, VARIANT_NAMES__REST);

    costPropertiesEClass = createEClass(COST_PROPERTIES);
    createEReference(costPropertiesEClass, COST_PROPERTIES__COST_STATEMENTS);

    costStatementEClass = createEClass(COST_STATEMENT);
    createEAttribute(costStatementEClass, COST_STATEMENT__TYPE);
    createEReference(costStatementEClass, COST_STATEMENT__VALUE);

    costValueEClass = createEClass(COST_VALUE);
    createEReference(costValueEClass, COST_VALUE__FIRST);
    createEReference(costValueEClass, COST_VALUE__REST);

    employeeStatementEClass = createEClass(EMPLOYEE_STATEMENT);

    booleanEClass = createEClass(BOOLEAN);
    createEAttribute(booleanEClass, BOOLEAN__VALUE);

    numberEClass = createEClass(NUMBER);
    createEAttribute(numberEClass, NUMBER__NEGATIVE);
    createEAttribute(numberEClass, NUMBER__FLOAT_VALUE);

    statementEClass = createEClass(STATEMENT);
    createEReference(statementEClass, STATEMENT__STATEMENT);
    createEAttribute(statementEClass, STATEMENT__OP);
    createEReference(statementEClass, STATEMENT__REST);

    statementLvl2EClass = createEClass(STATEMENT_LVL2);
    createEReference(statementLvl2EClass, STATEMENT_LVL2__STATEMENT);
    createEAttribute(statementLvl2EClass, STATEMENT_LVL2__OP);
    createEReference(statementLvl2EClass, STATEMENT_LVL2__REST);

    statementLvl3EClass = createEClass(STATEMENT_LVL3);
    createEReference(statementLvl3EClass, STATEMENT_LVL3__STATEMENT);
    createEAttribute(statementLvl3EClass, STATEMENT_LVL3__OP);
    createEReference(statementLvl3EClass, STATEMENT_LVL3__REST);

    statementLvl4EClass = createEClass(STATEMENT_LVL4);

    booleanStatementEClass = createEClass(BOOLEAN_STATEMENT);
    createEReference(booleanStatementEClass, BOOLEAN_STATEMENT__STATEMENT);
    createEAttribute(booleanStatementEClass, BOOLEAN_STATEMENT__OP);
    createEReference(booleanStatementEClass, BOOLEAN_STATEMENT__REST);

    booleanStatementLvl2EClass = createEClass(BOOLEAN_STATEMENT_LVL2);
    createEReference(booleanStatementLvl2EClass, BOOLEAN_STATEMENT_LVL2__STATEMENT);
    createEAttribute(booleanStatementLvl2EClass, BOOLEAN_STATEMENT_LVL2__OP);
    createEReference(booleanStatementLvl2EClass, BOOLEAN_STATEMENT_LVL2__REST);

    booleanStatementLvl3EClass = createEClass(BOOLEAN_STATEMENT_LVL3);
    createEReference(booleanStatementLvl3EClass, BOOLEAN_STATEMENT_LVL3__STATEMENT);
    createEAttribute(booleanStatementLvl3EClass, BOOLEAN_STATEMENT_LVL3__OP);
    createEReference(booleanStatementLvl3EClass, BOOLEAN_STATEMENT_LVL3__REST);

    booleanStatementLvl4EClass = createEClass(BOOLEAN_STATEMENT_LVL4);

    booleanStatementLvl5EClass = createEClass(BOOLEAN_STATEMENT_LVL5);
    createEReference(booleanStatementLvl5EClass, BOOLEAN_STATEMENT_LVL5__STATEMENT);
    createEAttribute(booleanStatementLvl5EClass, BOOLEAN_STATEMENT_LVL5__OP);
    createEReference(booleanStatementLvl5EClass, BOOLEAN_STATEMENT_LVL5__REST);

    booleanStatementLvl6EClass = createEClass(BOOLEAN_STATEMENT_LVL6);
    createEReference(booleanStatementLvl6EClass, BOOLEAN_STATEMENT_LVL6__STATEMENT);
    createEAttribute(booleanStatementLvl6EClass, BOOLEAN_STATEMENT_LVL6__OP);
    createEReference(booleanStatementLvl6EClass, BOOLEAN_STATEMENT_LVL6__REST);

    booleanStatementLvl7EClass = createEClass(BOOLEAN_STATEMENT_LVL7);
    createEReference(booleanStatementLvl7EClass, BOOLEAN_STATEMENT_LVL7__STATEMENT);
    createEAttribute(booleanStatementLvl7EClass, BOOLEAN_STATEMENT_LVL7__OP);
    createEReference(booleanStatementLvl7EClass, BOOLEAN_STATEMENT_LVL7__REST);

    booleanStatementLvl8EClass = createEClass(BOOLEAN_STATEMENT_LVL8);

    simdaseContractLibraryEClass = createEClass(SIMDASE_CONTRACT_LIBRARY);
    createEReference(simdaseContractLibraryEClass, SIMDASE_CONTRACT_LIBRARY__CONTRACT);

    simdaseContractSubclauseEClass = createEClass(SIMDASE_CONTRACT_SUBCLAUSE);
    createEReference(simdaseContractSubclauseEClass, SIMDASE_CONTRACT_SUBCLAUSE__CONTRACT);

    simdaseContractEClass = createEClass(SIMDASE_CONTRACT);
    createEReference(simdaseContractEClass, SIMDASE_CONTRACT__STATEMENT);

    isAdaptiveStatementEClass = createEClass(IS_ADAPTIVE_STATEMENT);
    createEReference(isAdaptiveStatementEClass, IS_ADAPTIVE_STATEMENT__VALUE);

    tMaxEClass = createEClass(TMAX);
    createEReference(tMaxEClass, TMAX__VALUE);

    tMinEClass = createEClass(TMIN);
    createEReference(tMinEClass, TMIN__VALUE);

    scalingFactorEClass = createEClass(SCALING_FACTOR);
    createEReference(scalingFactorEClass, SCALING_FACTOR__STATEMENT);

    reEvaluateAveragingEClass = createEClass(RE_EVALUATE_AVERAGING);
    createEReference(reEvaluateAveragingEClass, RE_EVALUATE_AVERAGING__COUNT);
    createEAttribute(reEvaluateAveragingEClass, RE_EVALUATE_AVERAGING__TYPE);

    hourlyCostEClass = createEClass(HOURLY_COST);
    createEReference(hourlyCostEClass, HOURLY_COST__COUNT);
    createEAttribute(hourlyCostEClass, HOURLY_COST__EMPLOYEE_TYPE);
    createEReference(hourlyCostEClass, HOURLY_COST__COST);

    fixedCostEClass = createEClass(FIXED_COST);
    createEReference(fixedCostEClass, FIXED_COST__COST);
    createEAttribute(fixedCostEClass, FIXED_COST__COST_REASON);
    createEReference(fixedCostEClass, FIXED_COST__PERIOD);

    integerEClass = createEClass(INTEGER);
    createEReference(integerEClass, INTEGER__VALUE);

    tEClass = createEClass(T);

    iEClass = createEClass(I);

    sinEClass = createEClass(SIN);
    createEReference(sinEClass, SIN__STATEMENT);

    cosEClass = createEClass(COS);
    createEReference(cosEClass, COS__STATEMENT);

    tanEClass = createEClass(TAN);
    createEReference(tanEClass, TAN__STATEMENT);

    parenEClass = createEClass(PAREN);
    createEReference(parenEClass, PAREN__STATEMENT);

    ifEClass = createEClass(IF);
    createEReference(ifEClass, IF__BOOLEAN_STATEMENT);
    createEReference(ifEClass, IF__TRUE_STATEMENT);
    createEReference(ifEClass, IF__FALSE_STATEMENT);

    randomEClass = createEClass(RANDOM);

    statementScalingFactorEClass = createEClass(STATEMENT_SCALING_FACTOR);

    withNotEClass = createEClass(WITH_NOT);
    createEReference(withNotEClass, WITH_NOT__STATEMENT);

    withoutNotEClass = createEClass(WITHOUT_NOT);
    createEReference(withoutNotEClass, WITHOUT_NOT__STATEMENT);

    boolBooleanEClass = createEClass(BOOL_BOOLEAN);
    createEReference(boolBooleanEClass, BOOL_BOOLEAN__VALUE);

    boolTEClass = createEClass(BOOL_T);

    boolIEClass = createEClass(BOOL_I);

    boolIntegerEClass = createEClass(BOOL_INTEGER);
    createEReference(boolIntegerEClass, BOOL_INTEGER__VALUE);

    boolSinEClass = createEClass(BOOL_SIN);
    createEReference(boolSinEClass, BOOL_SIN__STATEMENT);

    boolCosEClass = createEClass(BOOL_COS);
    createEReference(boolCosEClass, BOOL_COS__STATEMENT);

    boolTanEClass = createEClass(BOOL_TAN);
    createEReference(boolTanEClass, BOOL_TAN__STATEMENT);

    boolParenEClass = createEClass(BOOL_PAREN);
    createEReference(boolParenEClass, BOOL_PAREN__STATEMENT);

    boolRandomEClass = createEClass(BOOL_RANDOM);

    boolScalingFactorEClass = createEClass(BOOL_SCALING_FACTOR);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private boolean isInitialized = false;

  /**
   * Complete the initialization of the package and its meta-model.  This
   * method is guarded to have no affect on any invocation but its first.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void initializePackageContents()
  {
    if (isInitialized) return;
    isInitialized = true;

    // Initialize package
    setName(eNAME);
    setNsPrefix(eNS_PREFIX);
    setNsURI(eNS_URI);

    // Obtain other dependent packages
    Aadl2Package theAadl2Package = (Aadl2Package)EPackage.Registry.INSTANCE.getEPackage(Aadl2Package.eNS_URI);
    EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);

    // Create type parameters

    // Set bounds for type parameters

    // Add supertypes to classes
    simdaseLibraryEClass.getESuperTypes().add(theAadl2Package.getAnnexLibrary());
    simdaseSubclauseEClass.getESuperTypes().add(theAadl2Package.getAnnexSubclause());
    variantsEClass.getESuperTypes().add(this.getSIMDASEStatement());
    activeVariantsEClass.getESuperTypes().add(this.getSIMDASEStatement());
    costPropertiesEClass.getESuperTypes().add(this.getSIMDASEStatement());
    simdaseContractLibraryEClass.getESuperTypes().add(this.getSimdaseLibrary());
    simdaseContractSubclauseEClass.getESuperTypes().add(this.getSimdaseSubclause());
    simdaseContractEClass.getESuperTypes().add(this.getContract());
    isAdaptiveStatementEClass.getESuperTypes().add(this.getSIMDASEStatement());
    tMaxEClass.getESuperTypes().add(this.getSIMDASEStatement());
    tMinEClass.getESuperTypes().add(this.getSIMDASEStatement());
    scalingFactorEClass.getESuperTypes().add(this.getSIMDASEStatement());
    reEvaluateAveragingEClass.getESuperTypes().add(this.getSIMDASEStatement());
    hourlyCostEClass.getESuperTypes().add(this.getEmployeeStatement());
    fixedCostEClass.getESuperTypes().add(this.getEmployeeStatement());
    integerEClass.getESuperTypes().add(this.getStatementLvl4());
    tEClass.getESuperTypes().add(this.getStatementLvl4());
    iEClass.getESuperTypes().add(this.getStatementLvl4());
    sinEClass.getESuperTypes().add(this.getStatementLvl4());
    cosEClass.getESuperTypes().add(this.getStatementLvl4());
    tanEClass.getESuperTypes().add(this.getStatementLvl4());
    parenEClass.getESuperTypes().add(this.getStatementLvl4());
    ifEClass.getESuperTypes().add(this.getStatementLvl4());
    randomEClass.getESuperTypes().add(this.getStatementLvl4());
    statementScalingFactorEClass.getESuperTypes().add(this.getStatementLvl4());
    withNotEClass.getESuperTypes().add(this.getBooleanStatementLvl4());
    withoutNotEClass.getESuperTypes().add(this.getBooleanStatementLvl4());
    boolBooleanEClass.getESuperTypes().add(this.getBooleanStatementLvl8());
    boolTEClass.getESuperTypes().add(this.getBooleanStatementLvl8());
    boolIEClass.getESuperTypes().add(this.getBooleanStatementLvl8());
    boolIntegerEClass.getESuperTypes().add(this.getBooleanStatementLvl8());
    boolSinEClass.getESuperTypes().add(this.getBooleanStatementLvl8());
    boolCosEClass.getESuperTypes().add(this.getBooleanStatementLvl8());
    boolTanEClass.getESuperTypes().add(this.getBooleanStatementLvl8());
    boolParenEClass.getESuperTypes().add(this.getBooleanStatementLvl8());
    boolRandomEClass.getESuperTypes().add(this.getBooleanStatementLvl8());
    boolScalingFactorEClass.getESuperTypes().add(this.getBooleanStatementLvl8());

    // Initialize classes and features; add operations and parameters
    initEClass(simdaseLibraryEClass, SimdaseLibrary.class, "SimdaseLibrary", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(simdaseSubclauseEClass, SimdaseSubclause.class, "SimdaseSubclause", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(contractEClass, Contract.class, "Contract", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(simdaseStatementEClass, SIMDASEStatement.class, "SIMDASEStatement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(variantsEClass, Variants.class, "Variants", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getVariants_Variants(), this.getVariants(), null, "variants", null, 0, 1, Variants.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getVariants_First(), this.getVariant(), null, "first", null, 0, 1, Variants.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getVariants_Rest(), this.getVariant(), null, "rest", null, 0, -1, Variants.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(variantEClass, Variant.class, "Variant", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getVariant_Name(), theEcorePackage.getEString(), "name", null, 0, 1, Variant.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getVariant_Variants(), this.getVariantNames(), null, "variants", null, 0, 1, Variant.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(activeVariantsEClass, ActiveVariants.class, "ActiveVariants", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getActiveVariants_Variants(), this.getActiveVariants(), null, "variants", null, 0, 1, ActiveVariants.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getActiveVariants_First(), this.getActiveVariant(), null, "first", null, 0, 1, ActiveVariants.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getActiveVariants_Rest(), this.getActiveVariant(), null, "rest", null, 0, -1, ActiveVariants.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(activeVariantEClass, ActiveVariant.class, "ActiveVariant", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getActiveVariant_Name(), theEcorePackage.getEString(), "name", null, 0, 1, ActiveVariant.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getActiveVariant_Variants(), this.getVariantNames(), null, "variants", null, 0, 1, ActiveVariant.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(variantNamesEClass, VariantNames.class, "VariantNames", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getVariantNames_First(), theEcorePackage.getEString(), "first", null, 0, 1, VariantNames.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getVariantNames_Rest(), theEcorePackage.getEString(), "rest", null, 0, -1, VariantNames.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(costPropertiesEClass, CostProperties.class, "CostProperties", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getCostProperties_CostStatements(), this.getCostStatement(), null, "costStatements", null, 0, -1, CostProperties.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(costStatementEClass, CostStatement.class, "CostStatement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getCostStatement_Type(), theEcorePackage.getEString(), "type", null, 0, 1, CostStatement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getCostStatement_Value(), this.getCostValue(), null, "value", null, 0, 1, CostStatement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(costValueEClass, CostValue.class, "CostValue", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getCostValue_First(), this.getEmployeeStatement(), null, "first", null, 0, 1, CostValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getCostValue_Rest(), this.getEmployeeStatement(), null, "rest", null, 0, -1, CostValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(employeeStatementEClass, EmployeeStatement.class, "EmployeeStatement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(booleanEClass, edu.clemson.simdase.simdase.Boolean.class, "Boolean", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getBoolean_Value(), theEcorePackage.getEString(), "value", null, 0, 1, edu.clemson.simdase.simdase.Boolean.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(numberEClass, edu.clemson.simdase.simdase.Number.class, "Number", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getNumber_Negative(), theEcorePackage.getEString(), "negative", null, 0, 1, edu.clemson.simdase.simdase.Number.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getNumber_FloatValue(), theEcorePackage.getEString(), "floatValue", null, 0, 1, edu.clemson.simdase.simdase.Number.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(statementEClass, Statement.class, "Statement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getStatement_Statement(), this.getStatementLvl2(), null, "statement", null, 0, 1, Statement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getStatement_Op(), theEcorePackage.getEString(), "op", null, 0, -1, Statement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getStatement_Rest(), this.getStatementLvl2(), null, "rest", null, 0, -1, Statement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(statementLvl2EClass, StatementLvl2.class, "StatementLvl2", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getStatementLvl2_Statement(), this.getStatementLvl3(), null, "statement", null, 0, 1, StatementLvl2.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getStatementLvl2_Op(), theEcorePackage.getEString(), "op", null, 0, -1, StatementLvl2.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getStatementLvl2_Rest(), this.getStatementLvl3(), null, "rest", null, 0, -1, StatementLvl2.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(statementLvl3EClass, StatementLvl3.class, "StatementLvl3", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getStatementLvl3_Statement(), this.getStatementLvl4(), null, "statement", null, 0, 1, StatementLvl3.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getStatementLvl3_Op(), theEcorePackage.getEString(), "op", null, 0, -1, StatementLvl3.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getStatementLvl3_Rest(), this.getStatementLvl4(), null, "rest", null, 0, -1, StatementLvl3.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(statementLvl4EClass, StatementLvl4.class, "StatementLvl4", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(booleanStatementEClass, BooleanStatement.class, "BooleanStatement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getBooleanStatement_Statement(), this.getBooleanStatementLvl2(), null, "statement", null, 0, 1, BooleanStatement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getBooleanStatement_Op(), theEcorePackage.getEString(), "op", null, 0, -1, BooleanStatement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getBooleanStatement_Rest(), this.getBooleanStatementLvl2(), null, "rest", null, 0, -1, BooleanStatement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(booleanStatementLvl2EClass, BooleanStatementLvl2.class, "BooleanStatementLvl2", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getBooleanStatementLvl2_Statement(), this.getBooleanStatementLvl3(), null, "statement", null, 0, 1, BooleanStatementLvl2.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getBooleanStatementLvl2_Op(), theEcorePackage.getEString(), "op", null, 0, -1, BooleanStatementLvl2.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getBooleanStatementLvl2_Rest(), this.getBooleanStatementLvl3(), null, "rest", null, 0, -1, BooleanStatementLvl2.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(booleanStatementLvl3EClass, BooleanStatementLvl3.class, "BooleanStatementLvl3", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getBooleanStatementLvl3_Statement(), this.getBooleanStatementLvl4(), null, "statement", null, 0, 1, BooleanStatementLvl3.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getBooleanStatementLvl3_Op(), theEcorePackage.getEString(), "op", null, 0, -1, BooleanStatementLvl3.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getBooleanStatementLvl3_Rest(), this.getBooleanStatementLvl4(), null, "rest", null, 0, -1, BooleanStatementLvl3.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(booleanStatementLvl4EClass, BooleanStatementLvl4.class, "BooleanStatementLvl4", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(booleanStatementLvl5EClass, BooleanStatementLvl5.class, "BooleanStatementLvl5", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getBooleanStatementLvl5_Statement(), this.getBooleanStatementLvl6(), null, "statement", null, 0, 1, BooleanStatementLvl5.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getBooleanStatementLvl5_Op(), theEcorePackage.getEString(), "op", null, 0, -1, BooleanStatementLvl5.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getBooleanStatementLvl5_Rest(), this.getBooleanStatementLvl6(), null, "rest", null, 0, -1, BooleanStatementLvl5.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(booleanStatementLvl6EClass, BooleanStatementLvl6.class, "BooleanStatementLvl6", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getBooleanStatementLvl6_Statement(), this.getBooleanStatementLvl7(), null, "statement", null, 0, 1, BooleanStatementLvl6.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getBooleanStatementLvl6_Op(), theEcorePackage.getEString(), "op", null, 0, -1, BooleanStatementLvl6.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getBooleanStatementLvl6_Rest(), this.getBooleanStatementLvl7(), null, "rest", null, 0, -1, BooleanStatementLvl6.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(booleanStatementLvl7EClass, BooleanStatementLvl7.class, "BooleanStatementLvl7", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getBooleanStatementLvl7_Statement(), this.getBooleanStatementLvl8(), null, "statement", null, 0, 1, BooleanStatementLvl7.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getBooleanStatementLvl7_Op(), theEcorePackage.getEString(), "op", null, 0, -1, BooleanStatementLvl7.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getBooleanStatementLvl7_Rest(), this.getBooleanStatementLvl8(), null, "rest", null, 0, -1, BooleanStatementLvl7.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(booleanStatementLvl8EClass, BooleanStatementLvl8.class, "BooleanStatementLvl8", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(simdaseContractLibraryEClass, SimdaseContractLibrary.class, "SimdaseContractLibrary", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getSimdaseContractLibrary_Contract(), this.getContract(), null, "contract", null, 0, 1, SimdaseContractLibrary.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(simdaseContractSubclauseEClass, SimdaseContractSubclause.class, "SimdaseContractSubclause", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getSimdaseContractSubclause_Contract(), this.getContract(), null, "contract", null, 0, 1, SimdaseContractSubclause.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(simdaseContractEClass, SimdaseContract.class, "SimdaseContract", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getSimdaseContract_Statement(), this.getSIMDASEStatement(), null, "statement", null, 0, -1, SimdaseContract.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(isAdaptiveStatementEClass, IsAdaptiveStatement.class, "IsAdaptiveStatement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getIsAdaptiveStatement_Value(), this.getBoolean(), null, "value", null, 0, 1, IsAdaptiveStatement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(tMaxEClass, TMax.class, "TMax", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getTMax_Value(), this.getNumber(), null, "value", null, 0, 1, TMax.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(tMinEClass, TMin.class, "TMin", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getTMin_Value(), this.getNumber(), null, "value", null, 0, 1, TMin.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(scalingFactorEClass, ScalingFactor.class, "ScalingFactor", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getScalingFactor_Statement(), this.getStatement(), null, "statement", null, 0, 1, ScalingFactor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(reEvaluateAveragingEClass, ReEvaluateAveraging.class, "ReEvaluateAveraging", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getReEvaluateAveraging_Count(), this.getNumber(), null, "count", null, 0, 1, ReEvaluateAveraging.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getReEvaluateAveraging_Type(), theEcorePackage.getEString(), "type", null, 0, 1, ReEvaluateAveraging.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(hourlyCostEClass, HourlyCost.class, "HourlyCost", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getHourlyCost_Count(), this.getNumber(), null, "count", null, 0, 1, HourlyCost.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getHourlyCost_EmployeeType(), theEcorePackage.getEString(), "employeeType", null, 0, 1, HourlyCost.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getHourlyCost_Cost(), this.getStatement(), null, "cost", null, 0, 1, HourlyCost.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(fixedCostEClass, FixedCost.class, "FixedCost", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getFixedCost_Cost(), this.getNumber(), null, "cost", null, 0, 1, FixedCost.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getFixedCost_CostReason(), theEcorePackage.getEString(), "costReason", null, 0, 1, FixedCost.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getFixedCost_Period(), this.getNumber(), null, "period", null, 0, 1, FixedCost.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(integerEClass, edu.clemson.simdase.simdase.Integer.class, "Integer", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getInteger_Value(), this.getNumber(), null, "value", null, 0, 1, edu.clemson.simdase.simdase.Integer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(tEClass, edu.clemson.simdase.simdase.T.class, "T", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(iEClass, edu.clemson.simdase.simdase.I.class, "I", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(sinEClass, Sin.class, "Sin", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getSin_Statement(), this.getStatement(), null, "statement", null, 0, 1, Sin.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(cosEClass, Cos.class, "Cos", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getCos_Statement(), this.getStatement(), null, "statement", null, 0, 1, Cos.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(tanEClass, Tan.class, "Tan", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getTan_Statement(), this.getStatement(), null, "statement", null, 0, 1, Tan.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(parenEClass, Paren.class, "Paren", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getParen_Statement(), this.getStatement(), null, "statement", null, 0, 1, Paren.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(ifEClass, If.class, "If", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getIf_BooleanStatement(), this.getBooleanStatement(), null, "booleanStatement", null, 0, 1, If.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getIf_TrueStatement(), this.getStatement(), null, "trueStatement", null, 0, 1, If.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getIf_FalseStatement(), this.getStatement(), null, "falseStatement", null, 0, 1, If.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(randomEClass, Random.class, "Random", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(statementScalingFactorEClass, StatementScalingFactor.class, "StatementScalingFactor", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(withNotEClass, WithNot.class, "WithNot", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getWithNot_Statement(), this.getBooleanStatementLvl4(), null, "statement", null, 0, 1, WithNot.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(withoutNotEClass, WithoutNot.class, "WithoutNot", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getWithoutNot_Statement(), this.getBooleanStatementLvl5(), null, "statement", null, 0, 1, WithoutNot.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(boolBooleanEClass, BoolBoolean.class, "BoolBoolean", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getBoolBoolean_Value(), this.getBoolean(), null, "value", null, 0, 1, BoolBoolean.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(boolTEClass, BoolT.class, "BoolT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(boolIEClass, BoolI.class, "BoolI", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(boolIntegerEClass, BoolInteger.class, "BoolInteger", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getBoolInteger_Value(), this.getNumber(), null, "value", null, 0, 1, BoolInteger.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(boolSinEClass, BoolSin.class, "BoolSin", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getBoolSin_Statement(), this.getStatement(), null, "statement", null, 0, 1, BoolSin.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(boolCosEClass, BoolCos.class, "BoolCos", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getBoolCos_Statement(), this.getStatement(), null, "statement", null, 0, 1, BoolCos.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(boolTanEClass, BoolTan.class, "BoolTan", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getBoolTan_Statement(), this.getStatement(), null, "statement", null, 0, 1, BoolTan.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(boolParenEClass, BoolParen.class, "BoolParen", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getBoolParen_Statement(), this.getBooleanStatement(), null, "statement", null, 0, 1, BoolParen.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(boolRandomEClass, BoolRandom.class, "BoolRandom", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(boolScalingFactorEClass, BoolScalingFactor.class, "BoolScalingFactor", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    // Create resource
    createResource(eNS_URI);
  }

} //SimdasePackageImpl
