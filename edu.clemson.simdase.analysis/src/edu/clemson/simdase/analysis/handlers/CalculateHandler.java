package edu.clemson.simdase.analysis.handlers;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.text.TextSelection;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.xtext.EcoreUtil2;
import org.eclipse.xtext.resource.EObjectAtOffsetHelper;
import org.eclipse.xtext.ui.editor.XtextEditor;
import org.eclipse.xtext.ui.editor.outline.impl.EObjectNode;
import org.eclipse.xtext.ui.editor.utils.EditorUtils;
import edu.clemson.simdase.analysis.Activator;
import org.eclipse.core.resources.WorkspaceJob;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.util.concurrent.IUnitOfWork;
import org.osate.aadl2.AadlPackage;
import org.osate.aadl2.AnnexSubclause;
import org.osate.aadl2.Classifier;
import org.osate.aadl2.ComponentImplementation;
import org.osate.aadl2.ComponentType;
import org.osate.aadl2.instance.SystemInstance;
import org.osate.aadl2.instantiation.InstantiateModel;
import org.osate.aadl2.modelsupport.WriteToFile;
import org.osate.aadl2.modelsupport.util.AadlUtil;
import org.eclipse.core.resources.ResourcesPlugin;
import org.osate.aadl2.Element;
import org.osate.aadl2.Subcomponent;
import org.osate.aadl2.impl.ComponentImplementationImpl;

public class CalculateHandler extends AbstractHandler {
	private IWorkbenchWindow window;

	@Override
	public Object execute(ExecutionEvent event) {
		URI uri = getSelectionURI(HandlerUtil.getCurrentSelection(event));
		if (uri == null) {
			return null;
		}
		
		window = HandlerUtil.getActiveWorkbenchWindow(event);
		if (window == null) {
			return null;
		}

		return executeURI(uri);
	}
	
	private List<CostRow> costRows = new ArrayList<CostRow>();
	SystemInstance systemInstance;
	
	private void writeReportToCSV(List<CostRow> reportRows, SystemInstance si) {
		String[] columns = {"Component Name","Level","Time Period","Architecture Definition Cost","Architecture Evaluation Cost","Component Development Cost","Mining Existing Assets Cost","Requirements Engineering Cost","Software System Integration Cost","Testing Cost","Understanding Relevant Domains Cost","Using Externally Available Software Cost","Configuration Management Cost","Commission Analysis Cost","Measurement Tracking Cost","Process Discipline Cost","Scoping Cost","Technical Planning Cost","Technical Risk Management Cost","Tool Support Cost","Building Business Case Cost","Customer Interface Management Cost","Developing Acquisition Strategy Cost","Funding Cost","Launching Institutionalizing Cost","Market Analysis Cost","Operations Cost","Organizational Planning Cost","Organizational Risk Management Cost","Structuring Organization Cost","Technology Forecasting Cost","Training Cost"};
		List<String> additionalColumns = new ArrayList<String>();
		
		if(reportRows != null) {
			for(CostRow row : reportRows) {
				if(row.getOtherCosts().size() > 0) {
					for(String key : row.getOtherCosts().keySet()) {
						if(!additionalColumns.contains(key)) {
							additionalColumns.add(key);
						}
					}
				}
			}
		}
		
		WriteToFile writeToFile = new WriteToFile("SIMDASE", si);
		writeToFile.setFileExtension("csv");
		
		String s = "";
		// first row
		boolean first = true;
		for(String column : columns) {
			s += (first ? "" : ",") + String.format("\"%s\"", column);
			first = false;
		}
		for(String column : additionalColumns) {
			s += "," + String.format("\"%s\"", column);
		}
		s += "\n";
		writeToFile.addOutput(s);
		int i = 1;
		for(CostRow reportRow : reportRows) {
			System.out.println("writing line " + i + " out of " + reportRows.size());
			++i;
			s = String.format("\"%s\"", reportRow.getComponentName()) + ",";
			s += String.format("\"%s\"", reportRow.getI().toString()) + ",";
			s += String.format("\"%s\"", reportRow.getT().toString()) + ",";
			s += String.format("\"%s\"", reportRow.getArchitectureDefinitionCostAsString()) + ",";
			s += String.format("\"%s\"", reportRow.getArchitectureEvaluationCostAsString()) + ",";
			s += String.format("\"%s\"", reportRow.getComponentDevelopmentCostAsString()) + ",";
			s += String.format("\"%s\"", reportRow.getMiningExistingAssetsCostAsString()) + ",";
			s += String.format("\"%s\"", reportRow.getRequirementsEngineeringCostAsString()) + ",";
			s += String.format("\"%s\"", reportRow.getSoftwareSystemIntegrationCostAsString()) + ",";
			s += String.format("\"%s\"", reportRow.getTestingCostAsString()) + ",";
			s += String.format("\"%s\"", reportRow.getUnderstandingRelevantDomainsCostAsString()) + ",";
			s += String.format("\"%s\"", reportRow.getUsingExternallyAvailableSoftwareCostAsString()) + ",";
			s += String.format("\"%s\"", reportRow.getConfigurationManagementCostAsString()) + ",";
			s += String.format("\"%s\"", reportRow.getCommissionAnalysisCostAsString()) + ",";
			s += String.format("\"%s\"", reportRow.getMeasurementTrackingCostAsString()) + ",";
			s += String.format("\"%s\"", reportRow.getProcessDisciplineCostAsString()) + ",";
			s += String.format("\"%s\"", reportRow.getScopingCostAsString()) + ",";
			s += String.format("\"%s\"", reportRow.getTechnicalPlanningCostAsString()) + ",";
			s += String.format("\"%s\"", reportRow.getTechnicalRiskManagementCostAsString()) + ",";
			s += String.format("\"%s\"", reportRow.getToolSupportCostAsString()) + ",";
			s += String.format("\"%s\"", reportRow.getBuildingBusinessCaseCostAsString()) + ",";
			s += String.format("\"%s\"", reportRow.getCustomerInterfaceManagementCostAsString()) + ",";
			s += String.format("\"%s\"", reportRow.getDevelopingAcquisitionStrategyCostAsString()) + ",";
			s += String.format("\"%s\"", reportRow.getFundingCostAsString()) + ",";
			s += String.format("\"%s\"", reportRow.getLaunchingInstitutionalizingCostAsString()) + ",";
			s += String.format("\"%s\"", reportRow.getMarketAnalysisCostAsString()) + ",";
			s += String.format("\"%s\"", reportRow.getOperationsCostAsString()) + ",";
			s += String.format("\"%s\"", reportRow.getOrganizationalPlanningCostAsString()) + ",";
			s += String.format("\"%s\"", reportRow.getOrganizationalRiskManagementCostAsString()) + ",";
			s += String.format("\"%s\"", reportRow.getStructuringOrganizationCostAsString()) + ",";
			s += String.format("\"%s\"", reportRow.getTechnologyForecastingCostAsString()) + ",";
			s += String.format("\"%s\"", reportRow.getTrainingCostAsString());
			for(String column : additionalColumns) {
				s += "," + String.format("\"%s\"", reportRow.getOtherCostAsString(column));
			}
			s += "\n";
			writeToFile.addOutput(s);
		}
		
		writeToFile.saveToFile();
	}
	
	private SystemInstance instantiateComponent(ComponentImplementation ci) {
    	try {
    		return InstantiateModel.buildInstanceModelFile(ci);
		} catch (Exception e) {
			throw new RuntimeException("Error Instantiating model");
		}
    }
    
    public static boolean deleteDirectory(File dir) {
        if (dir.isDirectory()) {
            File[] children = dir.listFiles();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDirectory(children[i]);
                if (!success) {
                    return false;
                }
            }
        }

        // either file or an empty directory
        System.out.println("removing file or directory : " + dir.getName());
        return dir.delete();
    }

	protected IWorkbenchWindow getWindow() {
		return window;
	}
	
	public Object executeURI(final URI uri) {
		final XtextEditor xtextEditor = EditorUtils.getActiveXtextEditor();
		if (xtextEditor == null) {
			return null;
		}

		WorkspaceJob job = getWorkspaceJob(xtextEditor, uri);
		job.setRule(ResourcesPlugin.getWorkspace().getRoot());
		job.schedule();
		return null;
	}
	
	protected WorkspaceJob getWorkspaceJob(XtextEditor xtextEditor, URI uri) {
		return new WorkspaceJob("SIMDASE_CALCUATE") {
			@Override
			public IStatus runInWorkspace(IProgressMonitor monitor) {
				return xtextEditor.getDocument().readOnly(getUnitOfWork(uri, monitor));
			}
		};
	}

	protected IUnitOfWork<IStatus, XtextResource> getUnitOfWork(URI uri, IProgressMonitor monitor) {
		return new IUnitOfWork<IStatus, XtextResource>() {
			@Override
			public IStatus exec(XtextResource resource) throws Exception {
				deleteDirectory(new File("instances"));
				costRows.clear();
				
				EObject eobj = resource.getResourceSet().getEObject(uri, true);
				SystemInstance si = CalculateHandler.getSysInstance((Element) eobj);
				systemInstance = si;
				
				Boolean analysisCompleted = false;
				ComponentImplementationImpl componentImpl = (ComponentImplementationImpl)si.getComponentImplementation();
				if(componentImpl != null) { 
					if(containsSimdaseAnnex(componentImpl)) {
						SimdaseEvaluator evaluator = new SimdaseEvaluator();
						SimdaseAnnex annex = evaluator.evaluateSimdaseSubclause(getSimdaseAnnex(componentImpl), true);
						if(annex.getTimePeriodStart() != null && annex.getTimePeriodEnd() != null) {
							for(int i = annex.getTimePeriodStart().intValue(); i <= annex.getTimePeriodEnd().intValue(); ++i) {
								Node root = fromComponentImplementation(componentImpl, 0, i);
								costRows.addAll(generateCostRowsFromNode(root));
							}
							analysisCompleted = true;
						}
					}
					if(!analysisCompleted) {
						Node root = fromComponentImplementation(componentImpl, 0, 0);
						costRows.addAll(generateCostRowsFromNode(root));
					}
					if(costRows != null && costRows.size() > 0) {
						writeReportToCSV(costRows, si);
					}
					try {
						return new Status(IStatus.OK, Activator.PLUGIN_ID, null);
			        } catch (Throwable e) {
			            return new Status(IStatus.ERROR, Activator.PLUGIN_ID, null);
			        }
				}
				return new Status(IStatus.ERROR, Activator.PLUGIN_ID, null);
			}
		};
	}
	
	private CostRow populateCostRow(CostRow row, Node node, String key) {
		switch(key) {
		case "architecture_definition":
			row.setArchitectureDefinitionCost(node.getSimdaseAnnex().getEmployeeCosts().get(key));
			break;
		case "architecture_evaluation":
			row.setArchitectureEvaluationCost(node.getSimdaseAnnex().getEmployeeCosts().get(key));
			break;
		case "component_development":
			row.setComponentDevelopmentCost(node.getSimdaseAnnex().getEmployeeCosts().get(key));
			break;
		case "mining_existing_assets":
			row.setMiningExistingAssetsCost(node.getSimdaseAnnex().getEmployeeCosts().get(key));
			break;
		case "requirements_engineering":
			row.setRequirementsEngineeringCost(node.getSimdaseAnnex().getEmployeeCosts().get(key));
			break;
		case "software_system_integration":
			row.setSoftwareSystemIntegrationCost(node.getSimdaseAnnex().getEmployeeCosts().get(key));
			break;
		case "testing":
			row.setTestingCost(node.getSimdaseAnnex().getEmployeeCosts().get(key));
			break;
		case "understanding_relevant_domains":
			row.setUnderstandingRelevantDomainsCost(node.getSimdaseAnnex().getEmployeeCosts().get(key));
			break;
		case "using_externally_available_software":
			row.setUsingExternallyAvailableSoftwareCost(node.getSimdaseAnnex().getEmployeeCosts().get(key));
			break;
		case "configuration_management":
			row.setConfigurationManagementCost(node.getSimdaseAnnex().getEmployeeCosts().get(key));
			break;
		case "commission_analysis":
			row.setCommissionAnalysisCost(node.getSimdaseAnnex().getEmployeeCosts().get(key));
			break;
		case "measurement_tracking":
			row.setMeasurementTrackingCost(node.getSimdaseAnnex().getEmployeeCosts().get(key));
			break;
		case "process_discipline":
			row.setProcessDisciplineCost(node.getSimdaseAnnex().getEmployeeCosts().get(key));
			break;
		case "scoping":
			row.setScopingCost(node.getSimdaseAnnex().getEmployeeCosts().get(key));
			break;
		case "technical_planning":
			row.setTechnicalPlanningCost(node.getSimdaseAnnex().getEmployeeCosts().get(key));
			break;
		case "technical_risk_management":
			row.setTechnicalRiskManagementCost(node.getSimdaseAnnex().getEmployeeCosts().get(key));
			break;
		case "tool_support":
			row.setToolSupportCost(node.getSimdaseAnnex().getEmployeeCosts().get(key));
			break;
		case "building_business_case":
			row.setBuildingBusinessCaseCost(node.getSimdaseAnnex().getEmployeeCosts().get(key));
			break;
		case "customer_interface_management":
			row.setCustomerInterfaceManagementCost(node.getSimdaseAnnex().getEmployeeCosts().get(key));
			break;
		case "developing_acquisition_strategy":
			row.setDevelopingAcquisitionStrategyCost(node.getSimdaseAnnex().getEmployeeCosts().get(key));
			break;
		case "funding":
			row.setFundingCost(node.getSimdaseAnnex().getEmployeeCosts().get(key));
			break;
		case "launching_institutionalizing":
			row.setLaunchingInstitutionalizingCost(node.getSimdaseAnnex().getEmployeeCosts().get(key));
			break;
		case "market_analysis":
			row.setMarketAnalysisCost(node.getSimdaseAnnex().getEmployeeCosts().get(key));
			break;
		case "operations":
			row.setOperationsCost(node.getSimdaseAnnex().getEmployeeCosts().get(key));
			break;
		case "organizational_planning":
			row.setOrganizationalPlanningCost(node.getSimdaseAnnex().getEmployeeCosts().get(key));
			break;
		case "organizational_risk_management":
			row.setOrganizationalRiskManagementCost(node.getSimdaseAnnex().getEmployeeCosts().get(key));
			break;
		case "structuring_organization":
			row.setStructuringOrganizationCost(node.getSimdaseAnnex().getEmployeeCosts().get(key));
			break;
		case "technology_forecasting":
			row.setTechnologyForecastingCost(node.getSimdaseAnnex().getEmployeeCosts().get(key));
			break;
		case "training":
			row.setTrainingCost(node.getSimdaseAnnex().getEmployeeCosts().get(key));
			break;
		default:
			row.getOtherCosts().put(key, node.getSimdaseAnnex().getEmployeeCosts().get(key));
			break;
		}
		return row;
	}
		
	private List<CostRow> generateCostRowsFromNode(Node node) {
		List<CostRow> myCostRows = new ArrayList<CostRow>();
		if(node.getSimdaseAnnex() != null) {
			CostRow costRow = new CostRow();
			costRow.setI(node.getComponentLevel());
			costRow.setT(node.getTimePeriod());
			costRow.setComponentName(node.getComponent().getFullName());
			
			for(String key : node.getSimdaseAnnex().getEmployeeCosts().keySet()) {
				costRow = populateCostRow(costRow, node, key);
			}
			
			myCostRows.add(costRow);
		}
		if(node.getVariants().size() > 0) {
			for(Node childNode : node.getChildren()) {
				for(List<Node> activeVariantNodes : node.getVariants()) {
					if(activeVariantNodes.contains(childNode)) {
						myCostRows.addAll(generateCostRowsFromNode(childNode));
					}
				}
			}
		} else {
			for(Node childNode : node.getChildren()) {
				myCostRows.addAll(generateCostRowsFromNode(childNode));
			}
		}
		return myCostRows;
	}
	
	private Stack<Map<String, List<String>>> activeVariantsStack = new Stack<Map<String, List<String>>>();
	
	private Node fromComponentImplementation(ComponentImplementation componentImpl, Integer level, Integer timePeriod) {
		Node node = new Node();
		node.setComponent(componentImpl);
		node.setComponentLevel(level);
		node.setTimePeriod(timePeriod);
		if(componentImpl != null) {
			SimdaseEvaluator evaluator = new SimdaseEvaluator();
			if(containsSimdaseAnnex(componentImpl)) {
				SimdaseAnnex annex = evaluator.evaluateSimdaseSubclause(getSimdaseAnnex(componentImpl), true);
				for(BigDecimal i = BigDecimal.ZERO; i.compareTo(annex.getReevaluateNTimes()) < 0; i = i.add(BigDecimal.ONE)) {
					if(annex.getTimePeriodStart() != null && annex.getTimePeriodEnd() != null) {
						if(annex.getTimePeriodStart().intValue() <= timePeriod && timePeriod <= annex.getTimePeriodEnd().intValue()) {
							evaluator = new SimdaseEvaluator(BigDecimal.valueOf(level), BigDecimal.valueOf(timePeriod), annex.getScalingFactor());
							if(annex.getActiveVariants().size() != 0) {
								activeVariantsStack.push(annex.getActiveVariants());
							}
							node.setSimdaseAnnex(evaluator.evaluateSimdaseSubclause(getSimdaseAnnex(componentImpl)), annex.getUseWhichResult());
						}
					} else {
						evaluator = new SimdaseEvaluator(BigDecimal.valueOf(level), BigDecimal.valueOf(timePeriod), annex.getScalingFactor());
						if(annex.getActiveVariants().size() != 0) {
							activeVariantsStack.push(annex.getActiveVariants());
						}
						node.setSimdaseAnnex(evaluator.evaluateSimdaseSubclause(getSimdaseAnnex(componentImpl)), annex.getUseWhichResult());
					}
				}
				if(annex.getUseWhichResult().equals("average")) {
					if(node.getSimdaseAnnex() != null && node.getSimdaseAnnex().getEmployeeCosts() != null) {
						for(String cost : node.getSimdaseAnnex().getEmployeeCosts().keySet()) {
							List<CostRow.EmployeeCost> currentCosts = node.getSimdaseAnnex().getEmployeeCosts().get(cost);
							for(int i = 0; i < currentCosts.size(); ++i) {
								currentCosts.get(i).setCostPerEmployee(currentCosts.get(i).getCostPerEmployee().divide(annex.getReevaluateNTimes()));
							}
							node.getSimdaseAnnex().getEmployeeCosts().put(cost, currentCosts);
						}
					}
				}
			}
			for(Element component : componentImpl.getChildren()) {
				if(component instanceof Subcomponent) {
					ComponentImplementation compImpl = ((Subcomponent)component).getComponentImplementation();
					Node childNode = fromComponentImplementation(compImpl, level+1, timePeriod);
					node.getChildren().add(childNode);
				}
			}
			if(node.getSimdaseAnnex() != null && node.getSimdaseAnnex().getVariants() != null && node.getSimdaseAnnex().getVariants().size() > 0) {
				List<String> activeVariants = getActiveVariants(componentImpl.getFullName());
				for(String variant : node.getSimdaseAnnex().getVariants().keySet()) {
					if(activeVariants.contains(variant)) {
						List<Node> variantNodes = new ArrayList<Node>();
						for(Node childNode : node.getChildren()) {
							for(String variantComponent : node.getSimdaseAnnex().getVariants().get(variant)) {
								if(childNode.getComponent() != null && childNode.getComponent().getFullName().equals(variantComponent)) {
									variantNodes.add(childNode);
								}
							}
						}
						node.getVariants().add(variantNodes);
					}
				}
			}
			if(node.getVariants().size() > 0) {
				for(Node childNode : node.getChildren()) {
					for(List<Node> activeVariantNodes : node.getVariants()) {
						if(activeVariantNodes.contains(childNode)) {
							node.mergeCosts(childNode);
							break;
						}
					}
				}
			} else {
				for(Node childNode : node.getChildren()) {
					node.mergeCosts(childNode);
				}
			}
			if(node.getSimdaseAnnex() != null && node.getSimdaseAnnex().getActiveVariants().size() != 0) {
				activeVariantsStack.pop();
			}
		}
		return node;
	}
	
	private List<String> getActiveVariants(String name) {
		for(int i = activeVariantsStack.size() - 1; i >= 0; --i) {
			if(activeVariantsStack.get(i).containsKey(name)) {
				return activeVariantsStack.get(i).get(name);
			}
		}
		return new ArrayList<String>();
	}
	
	private boolean containsSimdaseAnnex(ComponentImplementation componentImpl) {
		return getSimdaseAnnex(componentImpl) != null;
	}
	
	private Set<ComponentImplementation> instantiated = new HashSet<ComponentImplementation>();
	
	private AnnexSubclause getSimdaseAnnex(ComponentImplementation componentImpl) {
		for(AnnexSubclause subclause : componentImpl.getOwnedAnnexSubclauses()) {
			if(subclause.getName().equals("simdase")) {
				if(!instantiated.contains(componentImpl)) {
					instantiateComponent(componentImpl);
					instantiated.add(componentImpl);
				}
				return subclause;
			}
		}
		//also check parents
		if(componentImpl.getExtended() != null) {
			return getSimdaseAnnex(componentImpl.getExtended());
		} else {
			return null;
		}
	}
	
	private URI getSelectionURI(ISelection currentSelection) {
		if (currentSelection instanceof IStructuredSelection) {
			IStructuredSelection iss = (IStructuredSelection) currentSelection;
			if (iss.size() == 1) {
				EObjectNode node = (EObjectNode) iss.getFirstElement();
				return node.getEObjectURI();
			}
		} else if (currentSelection instanceof TextSelection) {
			XtextEditor xtextEditor = EditorUtils.getActiveXtextEditor();
			TextSelection ts = (TextSelection) xtextEditor.getSelectionProvider().getSelection();
			return xtextEditor.getDocument().readOnly(resource -> {
				EObject e = new EObjectAtOffsetHelper().resolveContainedElementAt(resource, ts.getOffset());
				return EcoreUtil2.getURI(e);
			});
		}
		return null;
	}
	
	protected static SystemInstance getSysInstance(Element root) {
		ComponentImplementation ci = getComponentImplementation(root);
		try {
			return InstantiateModel.buildInstanceModelFile(ci);
		} catch (Exception e) {
			return null;
		}
	}
	
	private static ComponentImplementation getComponentImplementation(Element root) {
		Classifier classifier = getOutermostClassifier(root);
		if (classifier instanceof ComponentImplementation) {
			return (ComponentImplementation) classifier;
		}
		if (!(classifier instanceof ComponentType)) {
			throw new RuntimeException("Must select an AADL Component Type or Implementation");
		}
		ComponentType ct = (ComponentType) classifier;
		List<ComponentImplementation> cis = getComponentImplementations(ct);
		if (cis.size() == 0) {
			throw new RuntimeException("AADL Component Type has no implementation to calculate");
		} else if (cis.size() == 1) {
			ComponentImplementation ci = cis.get(0);
			return ci;
		} else {
			throw new RuntimeException("AADL Component Type has multiple implementations to calculate: please select just one");
		}
	}
	
	private static List<ComponentImplementation> getComponentImplementations(ComponentType ct) {
		List<ComponentImplementation> result = new ArrayList<>();
		AadlPackage pkg = AadlUtil.getContainingPackage(ct);
		for (ComponentImplementation ci : EcoreUtil2.getAllContentsOfType(pkg, ComponentImplementation.class)) {
			if (ci.getType().equals(ct)) {
				result.add(ci);
			}
		}
		return result;
	}
	
	protected static Classifier getOutermostClassifier(Element element) {
		List<EObject> containers = new ArrayList<>();
		EObject curr = element;
		while (curr != null) {
			containers.add(curr);
			curr = curr.eContainer();
		}
		Collections.reverse(containers);
		for (EObject container : containers) {
			if (container instanceof Classifier) {
				System.out.println(container);
				return (Classifier) container;
			}
		}
		return null;
	}
}
